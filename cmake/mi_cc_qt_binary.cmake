# Copyright 2019 The MI Authors
#
# Licensed under the Apache License v2.0 with LLVM Exceptions.
# See https://llvm.org/LICENSE.txt for license information.
# SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception

include(CMakeParseArguments)

function(mi_qt_binary)
	cmake_parse_arguments(
		_RULE
		"EXCLUDE_FROM_ALL;HOSTONLY;TESTONLY"
		"NAME"
		"SRCS;COPTS;DEFINES;LINKOPTS;DATA;DEPS"
		${ARGN}
	)

	if(_RULE_TESTONLY AND NOT MI_BUILD_TESTS)
		return()
	endif()

	# Prefix the library with the package name, so we get: mi_package_name
	mi_package_name(_PACKAGE_NAME)
	mi_package_ns(_PACKAGE_NS)
	if("${_PACKAGE_NAME}" STREQUAL "")
		set(_NAME "${_RULE_NAME}")
	else()
		set(_NAME "${_PACKAGE_NAME}_${_RULE_NAME}")
	endif()

	find_package(Qt6 REQUIRED COMPONENTS Core)
	qt_standard_project_setup()

	qt_add_executable(${_NAME} "")

	set_target_properties(${_NAME}
		PROPERTIES
		DEBUG_POSTFIX ${CMAKE_DEBUG_POSTFIX}
		OUTPUT_NAME "${_RULE_NAME}"
		RUNTIME_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/bin"
		LIBRARY_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/lib"
		ARCHIVE_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/lib"
		PDB_OUTPUT_DIRECTORY         "${CMAKE_BINARY_DIR}/bin"
		COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
	)

	if(NOT "${_PACKAGE_NS}" STREQUAL "")
		# Alias the mi_package_name binary to mi::package::name.
		# This lets us more clearly map to Bazel and makes it possible to
		# disambiguate the underscores in paths vs. the separators.
		add_executable(${_PACKAGE_NS}::${_RULE_NAME} ALIAS ${_NAME})

		# If the binary name matches the package then treat it as a default. For
		# example, foo/bar/ library 'bar' would end up as 'foo::bar'. This isn't
		# likely to be common for binaries, but is consistent with the behavior for
		# libraries and in Bazel.
		mi_package_dir(_PACKAGE_DIR)
		if("${_RULE_NAME}" STREQUAL "${_PACKAGE_DIR}")
			add_executable(${_PACKAGE_NS} ALIAS ${_NAME})
		endif()
	endif()

	set_target_properties(${_NAME} PROPERTIES OUTPUT_NAME "${_RULE_NAME}")
	if(_RULE_SRCS)
		target_sources(${_NAME} PRIVATE ${_RULE_SRCS})
	else()
		set(_DUMMY_SRC "${CMAKE_CURRENT_BINARY_DIR}/${_NAME}_dummy.cc")
		file(WRITE ${_DUMMY_SRC} "")
		target_sources(${_NAME} PRIVATE ${_DUMMY_SRC})
	endif()
	target_include_directories(${_NAME} SYSTEM
		PUBLIC
		"$<BUILD_INTERFACE:${MI_SOURCE_DIR}>/src"
		"$<BUILD_INTERFACE:${MI_BINARY_DIR}>/src"
	)

	target_compile_definitions(${_NAME} PUBLIC ${_RULE_DEFINES})
	target_compile_options(${_NAME} PRIVATE ${MI_DEFAULT_COPTS} ${_RULE_COPTS})
	target_link_options(${_NAME} PRIVATE ${MI_DEFAULT_LINKOPTS} ${_RULE_LINKOPTS})

	# Replace dependencies passed by ::name with mi::package::name
	list(TRANSFORM _RULE_DEPS REPLACE "^::" "${_PACKAGE_NS}::")

	# Implicit deps.
	if(MI_IMPLICIT_DEFS_CC_DEPS)
		list(APPEND _RULE_DEPS ${MI_IMPLICIT_DEFS_CC_DEPS})
	endif()

	target_link_libraries(${_NAME} PUBLIC ${_RULE_DEPS})

	mi_add_data_dependencies(NAME ${_NAME} DATA ${_RULE_DATA})

	# Add all MI targets to a folder in the IDE for organization.
	mi_package_dir(_PACKAGE_DIR)
	set_property(TARGET ${_NAME} PROPERTY FOLDER ${MI_IDE_FOLDER}/binaries)

	set_property(TARGET ${_NAME} PROPERTY CXX_STANDARD ${MI_CXX_STANDARD})
	set_property(TARGET ${_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)


	if(_RULE_EXCLUDE_FROM_ALL)
		set_property(TARGET ${_NAME} PROPERTY EXCLUDE_FROM_ALL ON)

		# Копирует зависимости (Coin, Quarter)
		install(
			TARGETS ${_NAME}
			RENAME ${_RULE_NAME}
			COMPONENT ${_RULE_NAME}
			ARCHIVE LIBRARY RUNTIME FRAMEWORK BUNDLE PUBLIC_HEADER RESOURCE DESTINATION bin
			EXCLUDE_FROM_ALL)
	else()
		install(
			TARGETS ${_NAME}
			RENAME ${_RULE_NAME}
			COMPONENT ${_RULE_NAME}
			ARCHIVE LIBRARY RUNTIME FRAMEWORK BUNDLE PUBLIC_HEADER RESOURCE DESTINATION bin)
	endif()

	# Без этого не работает.
	install(FILES $<TARGET_RUNTIME_DLLS:${_NAME}> TYPE BIN)

	# Создает скрипт для копирования всех зависимостей при запуске install.
	qt_generate_deploy_app_script(
		TARGET ${_NAME}
		FILENAME_VARIABLE deploy_script
		NO_UNSUPPORTED_PLATFORM_ERROR
	)
	install(SCRIPT ${deploy_script})

	# Копирует Qt6*.dll в папку _build
	windeployqt(${_NAME})
endfunction()
