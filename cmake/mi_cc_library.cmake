
# CMake function to imitate Bazel's cc_library rule.
#
# Parameters:
# PACKAGE: Name of the package (overrides actual path)
# NAME: name of target (see Note)
# HDRS: List of public header files for the library
# TEXTUAL_HDRS: List of public header files that cannot be compiled on their own
# SRCS: List of source files for the library
# DATA: List of other targets and files required for this binary
# DEPS: List of other libraries to be linked in to the binary targets
# COPTS: List of private compile options
# DEFINES: List of public defines
# INCLUDES: Include directories to add to dependencies
# LINKOPTS: List of link options
# PUBLIC: Add this so that this library will be exported under iree::
# Also in IDE, target will appear in IREE folder while non PUBLIC will be in IREE/internal.
# TESTONLY: When added, this target will only be built if user passes -DIREE_BUILD_TESTS=ON to CMake.
# SHARED: If set, will compile to a shared object.
# WINDOWS_DEF_FILE: If set, will add a windows .def file to a shared library link
# Note:
# By default, iree_cc_library will always create a library named iree_${NAME},
# and alias target iree::${NAME}. The iree:: form should always be used.
# This is to reduce namespace pollution.
#
# iree_cc_library(
#   NAME
#     awesome
#   HDRS
#     "a.h"
#   SRCS
#     "a.cc"
# )
# iree_cc_library(
#   NAME
#     fantastic_lib
#   SRCS
#     "b.cc"
#   DEPS
#     iree::package::awesome # not "awesome" !
#   PUBLIC
# )
#
# iree_cc_library(
#   NAME
#     main_lib
#   ...
#   DEPS
#     iree::package::fantastic_lib
# )
function(mi_cc_library)
  cmake_parse_arguments(
    _RULE
    "PUBLIC;TESTONLY;SHARED"
    "NAME;WINDOWS_DEF_FILE"
    "HDRS;TEXTUAL_HDRS;SRCS;COPTS;DEFINES;LINKOPTS;DATA;DEPS;INCLUDES"
    ${ARGN}
  )

  if(_RULE_TESTONLY AND NOT MI_ENABLE_TASTING)
    return()
  endif()

  # Replace dependencies passed by ::name with mi::package::name
  mi_package_ns(_PACKAGE_NS)
  list(TRANSFORM _RULE_DEPS REPLACE "^::" "${_PACKAGE_NS}::")

  # Prefix the library with the package name, so we get: mi_package_name.
  mi_package_name(_PACKAGE_NAME)
  set(_NAME "${_PACKAGE_NAME}_${_RULE_NAME}")
  set(_OBJECTS_NAME ${_NAME}.objects)

  # Check if this is a header-only library.
  # Note that as of February 2019, many popular OS's (for example, Ubuntu
  # 16.04 LTS) only come with cmake 3.5 by default.  For this reason, we can't
  # use list(FILTER...)
  set(_CC_SRCS "${_RULE_SRCS}")
  foreach(_SRC_FILE IN LISTS _CC_SRCS)
    if(${_SRC_FILE} MATCHES ".*\\.(h|inc|hpp)")
      list(REMOVE_ITEM _CC_SRCS "${_SRC_FILE}")
    endif()
  endforeach()
  if("${_CC_SRCS}" STREQUAL "")
    set(_RULE_IS_INTERFACE 1)
  else()
    set(_RULE_IS_INTERFACE 0)
  endif()

  # Wrap user specified INCLUDES in the $<BUILD_INTERFACE:>
  # generator.
  list(TRANSFORM _RULE_INCLUDES PREPEND "$<BUILD_INTERFACE:")
  list(TRANSFORM _RULE_INCLUDES APPEND ">")

  # Implicit deps.
  if(MI_IMPLICIT_DEFS_CC_DEPS)
    list(APPEND _RULE_DEPS ${MI_IMPLICIT_DEFS_CC_DEPS})
  endif()

	mi_package_dir(_PACKAGE_DIR)

  if(NOT _RULE_IS_INTERFACE)
    add_library(${_OBJECTS_NAME} OBJECT)
    if(_RULE_SHARED)
      add_library(${_NAME} SHARED "$<TARGET_OBJECTS:${_OBJECTS_NAME}>")
      if(_RULE_WINDOWS_DEF_FILE AND WIN32)
        target_sources(${_NAME} PRIVATE "${_RULE_WINDOWS_DEF_FILE}")
      endif()
    else()
      add_library(${_NAME} STATIC "$<TARGET_OBJECTS:${_OBJECTS_NAME}>")
      if(_RULE_WINDOWS_DEF_FILE AND WIN32)
        message(SEND_ERROR "If specifying a .def file library must be shared")
      endif()
    endif()

    # Sources get added to the object library.
    target_sources(${_OBJECTS_NAME}
      PRIVATE
        ${_RULE_SRCS}
        ${_RULE_TEXTUAL_HDRS}
        ${_RULE_HDRS}
    )

    # Keep track of objects transitively in our special property.
    set_property(TARGET ${_NAME} PROPERTY
      INTERFACE_MI_TRANSITIVE_OBJECTS "$<TARGET_OBJECTS:${_OBJECTS_NAME}>")
    _mi_cc_library_add_object_deps(${_NAME} ${_RULE_DEPS})

    # We define everything else on the regular rule. However, the object
    # library needs compiler definition related properties, so we forward them.
    # We also forward link libraries -- not because the OBJECT libraries do
    # linking but because they get transitive compile definitions from them.
    # Yes. This is state of the art.
    # Note that SYSTEM scope matches here, in the property name and in the
    # include directories below on the main rule. If ever removing this,
    # remove it from all places.
    target_include_directories(${_OBJECTS_NAME} SYSTEM PUBLIC $<TARGET_PROPERTY:${_NAME},INTERFACE_SYSTEM_INCLUDE_DIRECTORIES>)
    target_include_directories(${_OBJECTS_NAME}        PUBLIC $<TARGET_PROPERTY:${_NAME},INTERFACE_INCLUDE_DIRECTORIES>)

    target_compile_options(${_OBJECTS_NAME} PRIVATE $<TARGET_PROPERTY:${_NAME},COMPILE_OPTIONS>)

	target_compile_definitions(${_OBJECTS_NAME} PUBLIC "$<$<NOT:$<BOOL:${_RULE_SHARED}>>:MI_STATIC_DEFINE>")
	target_compile_definitions(${_OBJECTS_NAME} PRIVATE MI_EXPORTS)
    target_compile_definitions(${_OBJECTS_NAME} PUBLIC $<TARGET_PROPERTY:${_NAME},INTERFACE_COMPILE_DEFINITIONS>)
    target_link_libraries(${_OBJECTS_NAME} PUBLIC $<TARGET_PROPERTY:${_NAME},INTERFACE_LINK_LIBRARIES>)

    target_include_directories(${_NAME} SYSTEM PUBLIC
        "$<BUILD_INTERFACE:${MI_SOURCE_DIR}>/src"

		# Из-за этого при написании "#include <MI/Common/...>" в подсказках появляются "MI_Common_Check.objects.dir" и т.д.
        #"$<BUILD_INTERFACE:${MI_BINARY_DIR}>/src"
    )
    target_include_directories(${_NAME}
      PUBLIC
        ${_RULE_INCLUDES}
    )
    target_compile_options(${_NAME}
      PRIVATE
        ${MI_DEFAULT_COPTS}
        ${_RULE_COPTS}
    )
    target_link_options(${_NAME}
      PRIVATE
        ${MI_DEFAULT_LINKOPTS}
        ${_RULE_LINKOPTS}
    )
    target_link_libraries(${_NAME}
      PUBLIC
        ${_RULE_DEPS}
    )

	set_target_properties(${_OBJECTS_NAME}
		PROPERTIES
		DEBUG_POSTFIX 			     "${CMAKE_DEBUG_POSTFIX}"
		RUNTIME_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/bin"
		LIBRARY_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/lib"
		ARCHIVE_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/lib"
		PDB_OUTPUT_DIRECTORY         "${CMAKE_BINARY_DIR}/bin"
		COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
	)

	set_target_properties(${_NAME}
		PROPERTIES
		DEBUG_POSTFIX 			     "${CMAKE_DEBUG_POSTFIX}"
		RUNTIME_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/bin"
		LIBRARY_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/lib"
		ARCHIVE_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}/lib"
		PDB_OUTPUT_DIRECTORY         "${CMAKE_BINARY_DIR}/bin"
		COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
	)

    mi_add_data_dependencies(NAME ${_NAME} DATA ${_RULE_DATA})
    target_compile_definitions(${_NAME}
      PUBLIC
        ${_RULE_DEFINES}
    )

    # Add all MI targets to a folder in the IDE for organization.
    if(_RULE_PUBLIC)
      set_property(TARGET ${_NAME}         PROPERTY FOLDER ${MI_IDE_FOLDER}/public)
      set_property(TARGET ${_OBJECTS_NAME} PROPERTY FOLDER ${MI_IDE_FOLDER}/public)
    elseif(_RULE_TESTONLY)
      set_property(TARGET ${_NAME}         PROPERTY FOLDER ${MI_IDE_FOLDER}/test)
      set_property(TARGET ${_OBJECTS_NAME} PROPERTY FOLDER ${MI_IDE_FOLDER}/test)
    else()
      set_property(TARGET ${_NAME}         PROPERTY FOLDER ${MI_IDE_FOLDER}/internal)
      set_property(TARGET ${_OBJECTS_NAME} PROPERTY FOLDER ${MI_IDE_FOLDER}/internal)
    endif()

    # INTERFACE libraries can't have the CXX_STANDARD property set so only
    # set here.
    set_property(TARGET ${_OBJECTS_NAME} PROPERTY CXX_STANDARD ${MI_CXX_STANDARD})
    set_property(TARGET ${_OBJECTS_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)
  else()
	message("WARNING: header-only library ${_NAME}. ")
    # Generating header-only library.
    add_library(${_NAME} INTERFACE)
    target_include_directories(${_NAME} SYSTEM
      INTERFACE
      	#TODO
        "$<BUILD_INTERFACE:${MI_SOURCE_DIR}>"
        "$<BUILD_INTERFACE:${MI_BINARY_DIR}>"
        ${_RULE_INCLUDES}
    )
    target_link_options(${_NAME}
      INTERFACE
        ${MI_DEFAULT_LINKOPTS}
        ${_RULE_LINKOPTS}
    )
    target_link_libraries(${_NAME}
      INTERFACE
        ${_RULE_DEPS}
    )
    _mi_cc_library_add_object_deps(${_NAME} ${_RULE_DEPS})
    mi_add_data_dependencies(NAME ${_NAME} DATA ${_RULE_DATA})
    target_compile_definitions(${_NAME}
      INTERFACE
        ${_RULE_DEFINES}
    )
  endif()

	install(
		TARGETS ${_NAME}
		EXPORT MITargets
		RUNTIME COMPONENT MI_Runtime
		LIBRARY COMPONENT MI_Runtime
		NAMELINK_COMPONENT MI_Development
		ARCHIVE COMPONENT MI_Development
		INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
	)

	mi_package_path(_PACKAGE_PATH)

	MACRO(INSTALL_HEADERS_WITH_DIRECTORY HEADER_LIST)
		FOREACH(HEADER ${${HEADER_LIST}})
			get_filename_component(DIR ${HEADER} DIRECTORY)
			INSTALL(FILES ${HEADER} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${_PACKAGE_PATH})
		ENDFOREACH(HEADER)
	ENDMACRO(INSTALL_HEADERS_WITH_DIRECTORY)

	# Headers:
	#   - foo/*.h -> <prefix>/include/foo/*.h
	INSTALL_HEADERS_WITH_DIRECTORY(_RULE_HDRS)

  # Alias the mi_package_name library to mi::package::name.
  # This lets us more clearly map to Bazel and makes it possible to
  # disambiguate the underscores in paths vs. the separators.
  add_library(${_PACKAGE_NS}::${_RULE_NAME} ALIAS ${_NAME})

  if(NOT "${_PACKAGE_NS}" STREQUAL "")
    # If the library name matches the final component of the package then treat
    # it as a default. For example, foo/bar/ library 'bar' would end up as
    # 'foo::bar'.
    mi_package_dir(_PACKAGE_DIR)
    if(${_RULE_NAME} STREQUAL ${_PACKAGE_DIR})
      add_library(${_PACKAGE_NS} ALIAS ${_NAME})
    endif()
  endif()
endfunction()

# _mi_cc_library_add_object_deps()
#
# Helper to add deps to an mi_cc_library. This only operates on the unaliased
# raw name (i.e. 'mi_vm_vm'), not aliased names (i.e. 'mi::vm').
#
# This appends to two properties:
#   INTERFACE_MI_TRANSITIVE_OBJECTS: Transitive list of all objects from
#     this library and all "mi::" prefixed dependent libraries. This will
#     allow you to create mondo objects for any transtive libraries that are
#     part of MI, but it will not contain outside.
#   INTERFACE_MI_TRANSITIVE_OBJECT_LIBS: Transitive list of any dependency
#     targets that are not under the "mi::" namespace but are encountered
#     in the dependency dag.
function(_mi_cc_library_add_object_deps name)
  foreach(_DEP_TARGET ${ARGN})
    if(_DEP_TARGET MATCHES "^MI::")
      set_property(TARGET ${name} APPEND PROPERTY
        INTERFACE_MI_TRANSITIVE_OBJECTS
        "$<GENEX_EVAL:$<TARGET_PROPERTY:${_DEP_TARGET},INTERFACE_MI_TRANSITIVE_OBJECTS>>"
      )
      set_property(TARGET ${name} APPEND PROPERTY
      INTERFACE_MI_TRANSITIVE_OBJECT_LIBS
        "$<GENEX_EVAL:$<TARGET_PROPERTY:${_DEP_TARGET},INTERFACE_MI_TRANSITIVE_OBJECT_LIBS>>"
      )
    else()
      set_property(TARGET ${name} APPEND PROPERTY
        INTERFACE_MI_TRANSITIVE_OBJECT_LIBS
        ${_DEP_TARGET}
      )
    endif()
  endforeach()
endfunction()

# external_cc_library()
#
# CMake function to imitate Bazel's cc_library rule.
# This is used for external libraries (from third_party, etc) that don't live
# in the IREE namespace.
#
# Parameters:
# PACKAGE: Name of the package (overrides actual path)
# NAME: Name of target (see Note)
# ROOT: Path to the source root where files are found
# HDRS: List of public header files for the library
# SRCS: List of source files for the library
# DATA: List of other targets and files required for this binary
# DEPS: List of other libraries to be linked in to the binary targets
# COPTS: List of private compile options
# DEFINES: List of public defines
# INCLUDES: Include directories to add to dependencies
# LINKOPTS: List of link options
# PUBLIC: Add this so that this library will be exported under ${PACKAGE}::
# Also in IDE, target will appear in ${PACKAGE} folder while non PUBLIC will be
# in ${PACKAGE}/internal.
# TESTONLY: When added, this target will only be built if user passes
#    -DIREE_BUILD_TESTS=ON to CMake.
#
# Note:
# By default, external_cc_library will always create a library named
# ${PACKAGE}_${NAME}, and alias target ${PACKAGE}::${NAME}. The ${PACKAGE}::
# form should always be used. This is to reduce namespace pollution.
#
# external_cc_library(
#   PACKAGE
#     some_external_thing
#   NAME
#     awesome
#   ROOT
#     "third_party/foo"
#   HDRS
#     "a.h"
#   SRCS
#     "a.cc"
# )
# external_cc_library(
#   PACKAGE
#     some_external_thing
#   NAME
#     fantastic_lib
#   ROOT
#     "third_party/foo"
#   SRCS
#     "b.cc"
#   DEPS
#     some_external_thing::awesome # not "awesome" !
#   PUBLIC
# )
#
# iree_cc_library(
#   NAME
#     main_lib
#   ...
#   DEPS
#     some_external_thing::fantastic_lib
# )
function(external_cc_library)
  cmake_parse_arguments(_RULE
    "PUBLIC;TESTONLY"
    "PACKAGE;NAME;ROOT"
    "HDRS;SRCS;COPTS;DEFINES;LINKOPTS;DATA;DEPS;INCLUDES"
    ${ARGN}
  )

  if(_RULE_TESTONLY AND NOT MI_BUILD_TESTS)
    return()
  endif()

  # Prefix the library with the package name.
  string(REPLACE "::" "_" _PACKAGE_NAME ${_RULE_PACKAGE})
  set(_NAME "${_PACKAGE_NAME}_${_RULE_NAME}")

  # Prefix paths with the root.
  list(TRANSFORM _RULE_HDRS PREPEND ${_RULE_ROOT})
  list(TRANSFORM _RULE_SRCS PREPEND ${_RULE_ROOT})

  # Check if this is a header-only library.
  # Note that as of February 2019, many popular OS's (for example, Ubuntu
  # 16.04 LTS) only come with cmake 3.5 by default.  For this reason, we can't
  # use list(FILTER...)
  set(_CC_SRCS "${_RULE_SRCS}")
  foreach(_SRC_FILE IN LISTS _CC_SRCS)
    if(${_SRC_FILE} MATCHES ".*\\.(h|inc)$")
      list(REMOVE_ITEM _CC_SRCS "${_SRC_FILE}")
    endif()
  endforeach()
  if("${_CC_SRCS}" STREQUAL "")
    set(_RULE_IS_INTERFACE 1)
  else()
    set(_RULE_IS_INTERFACE 0)
  endif()

  if(NOT _RULE_IS_INTERFACE)
    add_library(${_NAME} STATIC "")
    target_sources(${_NAME}
      PRIVATE
        ${_RULE_SRCS}
        ${_RULE_HDRS}
    )
    target_include_directories(${_NAME} SYSTEM
      PUBLIC
        "$<BUILD_INTERFACE:${MI_SOURCE_DIR}>"
        "$<BUILD_INTERFACE:${MI_BINARY_DIR}>"
        "$<BUILD_INTERFACE:${_RULE_INCLUDES}>"
    )
    target_compile_options(${_NAME}
      PRIVATE
        ${_RULE_COPTS}
        ${MI_DEFAULT_COPTS}
    )
    target_link_options(${_NAME}
      PRIVATE
        ${MI_DEFAULT_LINKOPTS}
        ${_RULE_LINKOPTS}
    )
    target_link_libraries(${_NAME}
      PUBLIC
        ${_RULE_DEPS}
    )
    target_compile_definitions(${_NAME}
      PUBLIC
        ${_RULE_DEFINES}
    )
    mi_add_data_dependencies(NAME ${_NAME} DATA ${_RULE_DATA})

    # Add all external targets to a a folder in the IDE for organization.
    if(_RULE_PUBLIC)
      set_property(TARGET ${_NAME} PROPERTY FOLDER third_party)
    elseif(_RULE_TESTONLY)
      set_property(TARGET ${_NAME} PROPERTY FOLDER third_party/test)
    else()
      set_property(TARGET ${_NAME} PROPERTY FOLDER third_party/internal)
    endif()

    # INTERFACE libraries can't have the CXX_STANDARD property set
    set_property(TARGET ${_NAME} PROPERTY CXX_STANDARD ${MI_CXX_STANDARD})
    set_property(TARGET ${_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)
  else()
    # Generating header-only library
    add_library(${_NAME} INTERFACE)
    target_include_directories(${_NAME} SYSTEM
      INTERFACE
        "$<BUILD_INTERFACE:${MI_SOURCE_DIR}>"
        "$<BUILD_INTERFACE:${MI_BINARY_DIR}>"
        "$<BUILD_INTERFACE:${_RULE_INCLUDES}>"
    )
    target_compile_options(${_NAME}
      INTERFACE
        ${MI_DEFAULT_COPTS}
        ${_RULE_COPTS}
    )
    target_link_options(${_NAME}
      INTERFACE
        ${MI_DEFAULT_LINKOPTS}
        ${_RULE_LINKOPTS}
    )
    target_link_libraries(${_NAME}
      INTERFACE
        ${_RULE_DEPS}
    )
    mi_add_data_dependencies(NAME ${_NAME} DATA ${_RULE_DATA})
    target_compile_definitions(${_NAME}
      INTERFACE
        ${_RULE_DEFINES}
    )
  endif()

  add_library(${_RULE_PACKAGE}::${_RULE_NAME} ALIAS ${_NAME})
  # If the library name matches the final component of the package then treat it
  # as a default. For example, 'foo::bar' library 'bar' would end up as
  # 'foo::bar'.
  string(REGEX REPLACE "^.*::" "" _PACKAGE_DIR ${_RULE_PACKAGE})
  if(${_PACKAGE_DIR} STREQUAL ${_RULE_NAME})

    add_library(${_RULE_PACKAGE} ALIAS ${_NAME})
  endif()
endfunction()
