enable_testing()
# A property is apparently the only way to get an uncached global variable.
set_property(GLOBAL PROPERTY MI_TEST_TMPDIRS "")
set(MI_TEST_TMPDIR_ROOT "${MI_BINARY_DIR}/test_tmpdir")

# mi_configure_test
#
# Registers test for temporary directory creation and adds properties common to
# all MI tests. This should be invoked with each test added with `add_test`.
#
# Parameters:
#   TEST_NAME: the test name, e.g. mi/base/math_test
function(mi_configure_test TEST_NAME)
  set(_TEST_TMPDIR "${MI_TEST_TMPDIR_ROOT}/${TEST_NAME}_test_tmpdir")
  set_property(GLOBAL APPEND PROPERTY MI_TEST_TMPDIRS ${_TEST_TMPDIR})
  set_property(TEST ${TEST_NAME} APPEND PROPERTY ENVIRONMENT "TEST_TMPDIR=${_TEST_TMPDIR}")
  set_property(TEST ${TEST_NAME} APPEND PROPERTY ENVIRONMENT "MI_BINARY_DIR=${MI_BINARY_DIR}")

  # MI_*_DISABLE environment variables may used to skip test cases which
  # require both a compiler target backend and compatible runtime HAL driver.
  #
  # These variables may be set by the test environment, typically as a property
  # of some continuous execution test runner or by an individual developer, or
  # here by the build system.
  #
  # Tests which only depend on a compiler target backend or a runtime HAL
  # driver, but not both, should generally use a different method of filtering.
  #if(NOT MI_TARGET_BACKEND_VULKAN_SPIRV OR NOT MI_HAL_DRIVER_VULKAN)
  #  set_property(TEST ${TEST_NAME} APPEND PROPERTY ENVIRONMENT "MI_VULKAN_DISABLE=1")
  #endif()
endfunction()

# mi_create_ctest_customization
#
# Constructs a CTestCustom.cmake file with custom commands run before ctest
# runs all tests. These commands create new temporary directories for each test
# that was properly configured with `mi_configure_test`.
#
# Note that this must be called after all tests are registered as it depends on
# a global variable (gross, I know).
#
# Takes no arguments
function(mi_create_ctest_customization)
  get_property(MI_TEST_TMPDIRS GLOBAL PROPERTY MI_TEST_TMPDIRS)
  set(MI_CREATE_TEST_TMPDIRS_COMMANDS "")
  set(_CMD_PREFIX "\"cmake -E make_directory")
  set(_CUR_CMD "${_CMD_PREFIX}")
  set(_CMD_LEN_LIMIT 8191)
  foreach(_DIR IN LISTS MI_TEST_TMPDIRS)
    string(LENGTH "${_CUR_CMD}" _CUR_CMD_LEN)
    if(_CUR_CMD_LEN GREATER _CMD_LEN_LIMIT)
      message(SEND_ERROR
          "Make directory command for single test directory is longer than"
          " maximum command length ${_CMD_LEN_LIMIT}: '${_CUR_CMD}'")
    endif()
    string(LENGTH "${_DIR}" _DIR_LEN)
    math(EXPR _NEW_CMD_LEN "${_CUR_CMD_LEN} + ${_DIR_LEN} + 1")
    if(_NEW_CMD_LEN GREATER _CMD_LEN_LIMIT)
      string(APPEND _CUR_CMD "\"\n")
      string(APPEND MI_CREATE_TEST_TMPDIRS_COMMANDS "${_CUR_CMD}")
      set(_CUR_CMD "${_CMD_PREFIX} ${_DIR}")
    else()
      string(APPEND _CUR_CMD " ${_DIR}")
    endif()
  endforeach()
  if(NOT _CUR_CMD STREQUAL _CMD_PREFIX)
    string(APPEND _CUR_CMD "\"\n")
    string(APPEND MI_CREATE_TEST_TMPDIRS_COMMANDS "${_CUR_CMD}")
  endif()

  configure_file("build_tools/cmake/CTestCustom.cmake.in" "${MI_BINARY_DIR}/CTestCustom.cmake" @ONLY)
endfunction()


include(CMakeParseArguments)

# mi_cc_test()
#
# CMake function to imitate Bazel's cc_test rule.
#
# Parameters:
# NAME: name of target. This name is used for the generated executable and
#     CTest target.
# ARGS: List of command line arguments to pass to the test binary.
#     Note: flag passing is only enforced through CTest, so manually running
#     the test binaries (such as under a debugger) will _not_ pass any
#     arguments without extra setup.
# SRCS: List of source files for the binary
# DATA: List of other targets and files required for this binary
# DEPS: List of other libraries to be linked in to the binary targets
# COPTS: List of private compile options
# DEFINES: List of public defines
# LINKOPTS: List of link options
# LABELS: Additional labels to apply to the test. The package path is added
#     automatically.
#
# Note:
# mi_cc_test will create a binary called ${PACKAGE_NAME}_${NAME}, e.g.
# mi_base_foo_test.
#
#
# Usage:
# mi_cc_library(
#   NAME
#     awesome
#   HDRS
#     "a.h"
#   SRCS
#     "a.cc"
#   PUBLIC
# )
#
# mi_cc_test(
#   NAME
#     awesome_test
#   SRCS
#     "awesome_test.cc"
#   DEPS
#     gtest_main
#     mi::awesome
# )
function(mi_cc_test)
	if(NOT MI_BUILD_TESTS)
		return()
	endif()

	cmake_parse_arguments(
		_RULE
		""
		"NAME"
		"ARGS;SRCS;COPTS;DEFINES;LINKOPTS;DATA;DEPS;LABELS;TIMEOUT"
		${ARGN}
	)

	# Prefix the library with the package name, so we get: mi_package_name
	mi_package_name(_PACKAGE_NAME)
	mi_package_ns(_PACKAGE_NS)
	set(_NAME "${_PACKAGE_NAME}_${_RULE_NAME}")

	add_executable(${_NAME} "")
	# Alias the mi_package_name test binary to mi::package::name.
	# This lets us more clearly map to Bazel and makes it possible to
	# disambiguate the underscores in paths vs. the separators.
	add_executable(${_PACKAGE_NS}::${_RULE_NAME} ALIAS ${_NAME})

	# If the test binary name matches the package then treat it as a default.
	# For example, foo/bar/ library 'bar' would end up as 'foo::bar'. This isn't
	# likely to be common for tests, but is consistent with the behavior for
	# libraries.
	mi_package_dir(_PACKAGE_DIR)
	if(${_RULE_NAME} STREQUAL ${_PACKAGE_DIR})
		add_executable(${_PACKAGE_NS} ALIAS ${_NAME})
	endif()

	set_target_properties(${_NAME}
		PROPERTIES
		DEBUG_POSTFIX ${CMAKE_DEBUG_POSTFIX}
		OUTPUT_NAME "${_RULE_NAME}"
		RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
		LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
		ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
		PDB_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
		#COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
	)

	target_sources(${_NAME}
		PRIVATE
		${_RULE_SRCS}
	)
	target_include_directories(${_NAME} SYSTEM
		PUBLIC
		"$<BUILD_INTERFACE:${MI_SOURCE_DIR}>/src"
		"$<BUILD_INTERFACE:${MI_BINARY_DIR}>/src"
	)
	target_compile_definitions(${_NAME}
		PUBLIC
		${_RULE_DEFINES}
	)
	target_compile_options(${_NAME}
		PRIVATE
		${MI_DEFAULT_COPTS}
		${_RULE_COPTS}
	)
	target_link_options(${_NAME}
		PRIVATE
		${MI_DEFAULT_LINKOPTS}
		${_RULE_LINKOPTS}
	)

	# Replace dependencies passed by ::name with mi::package::name

	list(TRANSFORM _RULE_DEPS REPLACE "^::" "${_PACKAGE_NS}::")

	target_link_libraries(${_NAME}
		PUBLIC
		${_RULE_DEPS}
	)
	mi_add_data_dependencies(NAME ${_NAME} DATA ${_RULE_DATA})

	# Add all MI targets to a folder in the IDE for organization.
	mi_package_dir(_PACKAGE_DIR)
	set_property(TARGET ${_NAME} PROPERTY FOLDER ${MI_IDE_FOLDER}/test)

	set_property(TARGET ${_NAME} PROPERTY CXX_STANDARD ${MI_CXX_STANDARD})
	set_property(TARGET ${_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)

	list(APPEND _RULE_DEPS "gmock")

	# Implicit deps.
	if(MI_IMPLICIT_DEFS_CC_DEPS)
		list(APPEND _RULE_DEPS ${MI_IMPLICIT_DEFS_CC_DEPS})
	endif()

	string(REPLACE "::" "/" _PACKAGE_PATH ${_PACKAGE_NS})
	set(_NAME_PATH "${_PACKAGE_PATH}/${_RULE_NAME}")

	# Case for cross-compiling towards Android.
	if(ANDROID)

	elseif((CMAKE_SYSTEM_PROCESSOR STREQUAL "riscv64" OR
			CMAKE_SYSTEM_PROCESSOR STREQUAL "riscv32") AND
			CMAKE_SYSTEM_NAME STREQUAL "Linux")
		# The test target needs to run within the QEMU emulator for RV64 Linux
		# crosscompile build or on-device.
		add_test(
		NAME
			${_NAME_PATH}
		COMMAND
		"${MI_ROOT_DIR}/build_tools/cmake/run_riscv_test.sh"
			"$<TARGET_FILE:${_NAME}>"
			${_RULE_ARGS}
		)
		mi_configure_test(${_NAME_PATH})
	else(ANDROID)
		add_test(
		NAME
			${_NAME_PATH}
		COMMAND
			"$<TARGET_FILE:${_NAME}>"
			${_RULE_ARGS}
		)

		mi_configure_test(${_NAME_PATH})
	endif(ANDROID)

	if (NOT DEFINED _RULE_TIMEOUT)
		set(_RULE_TIMEOUT 60)
	endif()

	list(APPEND _RULE_LABELS "${_PACKAGE_PATH}")
	set_property(TEST ${_NAME_PATH} PROPERTY LABELS "${_RULE_LABELS}")
	set_property(TEST ${_NAME_PATH} PROPERTY TIMEOUT ${_RULE_TIMEOUT})
endfunction()
