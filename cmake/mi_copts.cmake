#-------------------------------------------------------------------------------
# C/C++ options as used within MI
#-------------------------------------------------------------------------------

set(MI_CXX_STANDARD ${CMAKE_CXX_STANDARD})

# TODO(benvanik): fix these names (or remove entirely).
set(MI_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(MI_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(MI_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})

function(mi_select_compiler_opts OPTS)
  cmake_parse_arguments(
    PARSE_ARGV 1
    _IREE_SELECTS
    ""
    ""
    "ALL;CLANG;CLANG_GTE_10;CLANG_CL;MSVC;GCC;CLANG_OR_GCC;MSVC_OR_CLANG_CL"
  )

  set(_OPTS "${${OPTS}}")
  list(APPEND _OPTS "${_IREE_SELECTS_ALL}")
  if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    list(APPEND _OPTS "${_IREE_SELECTS_GCC}")
    list(APPEND _OPTS "${_IREE_SELECTS_CLANG_OR_GCC}")
  elseif("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    if(MSVC)
      list(APPEND _OPTS ${_IREE_SELECTS_CLANG_CL})
      list(APPEND _OPTS ${_IREE_SELECTS_MSVC_OR_CLANG_CL})
    else()
      list(APPEND _OPTS ${_IREE_SELECTS_CLANG})
      if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL 10)
        list(APPEND _OPTS ${_IREE_SELECTS_CLANG_GTE_10})
      endif()
      list(APPEND _OPTS ${_IREE_SELECTS_CLANG_OR_GCC})
    endif()
  elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    list(APPEND _OPTS ${_IREE_SELECTS_MSVC})
    list(APPEND _OPTS ${_IREE_SELECTS_MSVC_OR_CLANG_CL})
  else()
    message(ERROR "Unknown compiler: ${CMAKE_CXX_COMPILER}")
    list(APPEND _OPTS "")
  endif()
  set(${OPTS} ${_OPTS} PARENT_SCOPE)
endfunction()

# Key compilation options
mi_select_compiler_opts(MI_DEFAULT_COPTS
  CLANG_OR_GCC
    "-fvisibility=hidden"

    # NOTE: The RTTI setting must match what LLVM was compiled with (defaults
    # to RTTI disabled).
    "$<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>"
    "$<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>"

  MSVC_OR_CLANG_CL
    # Exclude a bunch of rarely-used APIs, such as crypto/DDE/shell.
    # https://docs.microsoft.com/en-us/windows/win32/winprog/using-the-windows-headers
    # NOTE: this is not really required anymore for build performance but does
    # work around some issues that crop up with header version compatibility
    # (abseil has issues with winsock versions).
    "/DWIN32_LEAN_AND_MEAN"

    # Don't allow windows.h to define MIN and MAX and conflict with the STL.
    # There's no legit use for these macros as any code we are writing ourselves
    # that we want a MIN/MAX in should be using an MI-prefixed version
    # instead: mi_min mi_max
    # https://stackoverflow.com/a/4914108
    "/DNOMINMAX"

    # Adds M_PI and other constants to <math.h>/<cmath> (to match non-windows).
    # https://docs.microsoft.com/en-us/cpp/c-runtime-library/math-constants
    "/D_USE_MATH_DEFINES"

    # Disable the "deprecation" warnings about CRT functions like strcpy.
    # Though the secure versions *are* better, they aren't portable and as such
    # just make cross-platform code annoying. One solution is to reimplement
    # them in a portable fashion and use those - and that's what we try to do
    # in certain places where we can get away with it. Other uses, like getenv,
    # are fine as these are not intended for use in core runtime code that needs
    # to be secure (friends don't let friends ship entire compiler stacks
    # embedded inside security sensitive applications anyway :).
    # https://docs.microsoft.com/en-us/cpp/c-runtime-library/security-features-in-the-crt
    "/D_CRT_SECURE_NO_WARNINGS"

    # With the above said about the "deprecated" functions; this useful flag
    # will at least try to use them when possible without any change to user
    # code. Note however because the new versions use templates they won't be
    # activated in C code; that's fine.
    # https://docs.microsoft.com/en-us/cpp/c-runtime-library/secure-template-overloads
    "/D_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES"

    # Configure RTTI generation.
    # - /GR - Enable generation of RTTI (default)
    # - /GR- - Disables generation of RTTI
    # https://docs.microsoft.com/en-us/cpp/build/reference/gr-enable-run-time-type-information?view=msvc-160
    "/GR"

    # Default max section count is 64k, which is woefully inadequate for some of
    # the insanely bloated tablegen outputs LLVM/MLIR produces. This cranks it
    # up to 2^32. It's not great that we have to generate/link files like that
    # but it's better to not get spurious failures during LTCG.
    # https://docs.microsoft.com/en-us/cpp/build/reference/bigobj-increase-number-of-sections-in-dot-obj-file
    "/bigobj"

    # Use the modern C preprocessor to more closely match standards/clang/gcc behavior.
    "/Zc:preprocessor"

	#
	"/Zc:__cplusplus"

	#"/Zi"

    # Enable C11 standards conforming mode.
    "$<$<COMPILE_LANGUAGE:C>:/std:c11>"
)

# Compiler diagnostics.
# Please keep these in sync with build_tools/bazel/mi.bazelrc
mi_select_compiler_opts(MI_DEFAULT_COPTS
  # Clang diagnostics. These largely match the set of warnings used within
  # Google. They have not been audited super carefully by the MI team but are
  # generally thought to be a good set and consistency with those used
  # internally is very useful when importing. If you feel that some of these
  # should be different (especially more strict), please raise an issue!
  CLANG
    "-Werror"
    "-Wall"

    # Disable warnings we don't care about or that generally have a low
    # signal/noise ratio.
    "-Wno-ambiguous-member-template"
    "-Wno-char-subscripts"
    "-Wno-extern-c-compat" # Matches upstream. Cannot impact due to extern C inclusion method.
    "-Wno-gnu-alignof-expression"
    "-Wno-gnu-variable-sized-type-not-at-end"
    "-Wno-ignored-optimization-argument"
    "-Wno-invalid-offsetof" # Technically UB but needed for intrusive ptrs
    "-Wno-invalid-source-encoding"
    "-Wno-mismatched-tags"
    "-Wno-pointer-sign"
    "-Wno-reserved-user-defined-literal"
    "-Wno-return-type-c-linkage"
    "-Wno-self-assign-overloaded"
    "-Wno-sign-compare"
    "-Wno-signed-unsigned-wchar"
    "-Wno-strict-overflow"
    "-Wno-trigraphs"
    "-Wno-unknown-pragmas"
    "-Wno-unknown-warning-option"
    "-Wno-unused-command-line-argument"
    "-Wno-unused-const-variable"
    "-Wno-unused-function"
    "-Wno-unused-local-typedef"
    "-Wno-unused-private-field"
    "-Wno-user-defined-warnings"

    # Explicitly enable some additional warnings.
    # Some of these aren't on by default, or under -Wall, or are subsets of
    # warnings turned off above.
    "-Wctad-maybe-unsupported"
    "-Wfloat-overflow-conversion"
    "-Wfloat-zero-conversion"
    "-Wfor-loop-analysis"
    "-Wformat-security"
    "-Wgnu-redeclared-enum"
    "-Wimplicit-fallthrough"
    "-Winfinite-recursion"
    "-Wliteral-conversion"
    "-Wnon-virtual-dtor"
    "-Woverloaded-virtual"
    "-Wpointer-arith"
    "-Wself-assign"
    "-Wstring-conversion"
    "-Wtautological-overlap-compare"
    "-Wthread-safety"
    "-Wthread-safety-beta"
    "-Wunused-comparison"
    "-Wvla"

    # Clang is lax by default on SIMD vector typing. GCC is strict by default.
    "-fno-lax-vector-conversions"

  # TODO(#6959): Enable -Werror once we have a presubmit CI.
  GCC
    "-Wall"
    "-Wno-address-of-packed-member"
    "-Wno-comment"
    "-Wno-format-zero-length"
    # Technically UB but needed for intrusive ptrs
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-invalid-offsetof>
    $<$<COMPILE_LANGUAGE:C>:-Wno-pointer-sign>
    "-Wno-sign-compare"
    "-Wno-unused-function"

  MSVC_OR_CLANG_CL
    # Default warning level (severe + significant + production quality).
    # This does not include level 4, "informational", warnings or those that
    # are off by default.
    # https://docs.microsoft.com/en-us/cpp/build/reference/compiler-option-warning-level
    # Note that we set CMake policy CMP0092 (if found), making this explicit:
    # https://cmake.org/cmake/help/v3.15/policy/CMP0092.html
    "/W3"

    # "nonstandard extension used : zero-sized array in struct/union"
    # This happens with unsized or zero-length arrays at the end of structs,
    # which is completely valid in C where we do it and get this warning. Shut
    # it up and rely on the better warnings from clang to catch if we try to
    # use it where it really matters (on a class that has copy/move ctors, etc).
    # https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-levels-2-and-4-c4200
    "/wd4200"

    # "signed/unsigned mismatch in comparison"
    # This is along the lines of a generic implicit conversion warning but tends
    # to crop up in code that implicitly treats unsigned size_t values as if
    # they were signed values instead of properly using ssize_t. In certain
    # cases where the comparison being performed may be guarding access to
    # memory this can cause unexpected behavior ("-1ull < 512ull, great let's
    # dereference buffer[-1ull]!").
    # https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-3-c4018
    #
    # TODO(#3844): remove this (or make it per-file to mi/compiler, as LLVM
    # tends to not care about these kind of things and it crops up there a lot).
    "/wd4018"

    # Also common in LLVM is mismatching signed/unsigned math. That's even more
    # dangerous than C4018: almost always these crop up in doing something with
    # a size_t and a non-size_t value (usually int or something like it) and do
    # you want out-of-bounds access exploits? Because that's how you get
    # out-of-bounds access exploits. Before fuzzers took over finding code and
    # trying to compile it with this warning forced to be an error was a way to
    # narrow down the places to look for attack vectors. I lived through the
    # Microsoft SAL/safe-int code red, and once you get used to using the safe
    # buffer offset/size manipulation functions it eliminates all kinds of
    # annoying bugs - as well as potential security issues.
    #
    # TODO(#3844): work to remove this class of errors from our code. It's
    # almost entirely in LLVM related stuff so per-file mi/compiler/... would
    # be fine.
    "/wd4146"  # operator applied to unsigned type, result still unsigned
    "/wd4244"  # possible loss of data
    "/wd4267"  # initializing: possible loss of data

    # Misc tweaks to better match reasonable clang/gcc behavior:
    "/wd4005"  # allow: macro redefinition
    "/wd4065"  # allow: switch statement contains 'default' but no 'case' labels
    "/wd4141"  # allow: inline used more than once
    "/wd4624"  # allow: destructor was implicitly defined as deleted

    # TODO(benvanik): confirm these are all still required and document:
    "/wd4146"  # operator applied to unsigned type, result still unsigned
    "/wd4244"  # possible loss of data
    "/wd4267"  # initializing: possible loss of data
    "/wd5105"  # allow: macro expansion producing 'defined' has undefined behavior

	#'type' : class 'type1' needs to have dll-interface to be used by clients of class 'type2'
	"/wd4251"

	#'function': not all control paths return a value
	"/wd4715"
)

# Set some things back to warnings that are really annoying as build errors
# during active development (but we still want as errors on CI).
if(MI_DEV_MODE)
  mi_select_compiler_opts(MI_DEFAULT_COPTS
    CLANG_OR_GCC
      "-Wno-error=unused-parameter"
      "-Wno-error=unused-variable"
  )
endif()

# Debug information and __FILE__ macros get expanded with full paths by default.
# This results in binaries that differ based on what directories they are built
# from and that's annoying.
#
# For now in all configurations we make __FILE__ macros relative. We could also
# make debug information relative using -fdebug-prefix-map but deterministic
# builds are most interesting in release modes that have debug info stripped.
get_filename_component(_MI_ROOT_NAME ${MI_ROOT_DIR} NAME)
mi_select_compiler_opts(MI_DEFAULT_COPTS
  # TODO(benvanik): make this CLANG_OR_GCC once clang-9 is no longer supported.
  CLANG_GTE_10
    "-fmacro-prefix-map=${MI_ROOT_DIR}=${_MI_ROOT_NAME}"
  GCC
    "-fmacro-prefix-map=${MI_ROOT_DIR}=${_MI_ROOT_NAME}"
)

# On MSVC, CMake sets /GR by default (enabling RTTI), but we set /GR-
# (disabling it) above. To avoid Command line warning D9025 which warns about
# overriding the flag value, we remove /GR from global CMake flags.
#
# Note: this may have ripple effects on downstream projects using MI. If this
# is a problem for your project, please reach out to us and we'll figure out a
# compatible solution.
#
# See also:
#   https://github.com/mi-org/mi/issues/4665.
#   https://discourse.cmake.org/t/how-to-fix-build-warning-d9025-overriding-gr-with-gr/878
#   https://gitlab.kitware.com/cmake/cmake/-/issues/20610
# if(CMAKE_CXX_FLAGS AND "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
#   string(REPLACE "/GR" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
# endif()

if(MI_ENABLE_THREADING)
  mi_select_compiler_opts(_MI_PTHREADS_LINKOPTS
    CLANG_OR_GCC
      "-lpthread"
  )
endif()

mi_select_compiler_opts(MI_DEFAULT_LINKOPTS
  CLANG_OR_GCC
    # Required by all modern software, effectively:
    "-lm"
    ${_MI_PTHREADS_LINKOPTS}
    ${_MI_LOGGING_LINKOPTS}
  MSVC
    "-natvis:${MI_ROOT_DIR}/src/NatvisFile.natvis"
)

#-------------------------------------------------------------------------------
# Size-optimized build flags
#-------------------------------------------------------------------------------

# TODO(#898): add a dedicated size-constrained configuration.
if(MI_SIZE_OPTIMIZED)
  mi_select_compiler_opts(MI_SIZE_OPTIMIZED_DEFAULT_COPTS
    MSVC_OR_CLANG_CL
      "/GS-"
      "/GL"
      "/Gw"
      "/Gy"
      "/DNDEBUG"
      "/Os"
      "/Oy"
      "/Zi"
      "/c"
  )
  mi_select_compiler_opts(MI_SIZE_OPTIMIZED_DEFAULT_LINKOPTS
    MSVC_OR_CLANG_CL
      "DEBUG:FULL"
      "-LTCG"
      "-opt:ref,icf"
  )
  # TODO(#898): make this only impact the runtime (MI_RUNTIME_DEFAULT_...).
  # These flags come from mi/base/config.h:
  set(MI_DEFAULT_COPTS
      "${MI_DEFAULT_COPTS}"
      "${MI_SIZE_OPTIMIZED_DEFAULT_COPTS}"
  )
  set(MI_DEFAULT_LINKOPTS
      "${MI_DEFAULT_LINKOPTS}"
      "${MI_SIZE_OPTIMIZED_DEFAULT_LINKOPTS}"
  )
endif()
