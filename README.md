# MI.Project

**Персональный проект для демонстрации навыков программирования.**

## Структура проекта

В данный момент дерево проекта выглядит следующим образом:

```
.
└── MI.Project/
    ├── cmake
    ├── src/
    │   ├── base
    │   ├── common
    │   ├── container
    │   └── ...
    ├── scripts
    └── third-party
```

где:

- **cmake** - директория со скриптами _CMake_ для сборки проекта.
- **src** - директория со всем исходным кодом.
- **scripts** - директория со скриптами.
- **third-party** - сторонний код.


**ПP0EKT ТAК}|{E НY}|{EH ДЛ9 6ECЛПAТН0Й ЛИЦEH3ИИ HA Resharper C++. :D**
**ПОЭТОМУ ПРИВЕДУ КРАТКОЕ ОПИСАНИЕ РАНД0МН0Г0 АЛГОРИТМА**

Whisker Weaving is an advancing front algorithm for all-hexahedral mesh generation. It uses global
information derived from grouping the mesh dual into surfaces, the STC, to construct the connectivity of the mesh,
then positions the nodes afterwards. Currently we are able to reliably generate hexahedral meshes for complicated
geometries and surface meshes. However, the surface mesh must be modified locally. Also, in large, highly-
unstructured meshes, there are usually isolated regions where hex quality is poor. Reliability has been achieved by
using new, provable curve-contraction algorithms to sequence the advancing front process. We have also
demonstrated that sheet moving can remove certain types of invalid connectivity.