#!/usr/bin/env python

import os
import sys


def try_utf8(data):
    "Returns a Unicode object on success, or None on failure"
    try:
       return data.decode('utf-8')
    except UnicodeDecodeError:
       return None


def main() -> None:
    has_errors = False

    for file_path in sys.argv[1:]:
        with open(file_path, 'rb') as f:
            data = f.read()
            data = try_utf8(data)
            if data is None:
                has_errors = True
                print(f"File not a UTF-8: {file_path}")

    if has_errors:
        exit(1)

if __name__ == "__main__":
    main()
