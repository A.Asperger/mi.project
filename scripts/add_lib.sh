#!/bin/bash

if [ $# == 0 ]; then
    echo "Example:"
    echo "scripts/add_lib.sh MI/HexGenerator JoinChord"
    exit 1
fi

printf "#pragma once

namespace mi {}  // namespace mi
" > src/$1/MI.$2.hpp

printf "#include <$1/MI.$2.hpp>

namespace mi {
enum class _ { need_more_then_nothing };
}  // namespace mi
" > src/$1/MI.$2.cpp

printf "#include <$1/MI.$2.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace mi::test {}  // namespace mi::test
" > src/$1/MI.$2_test.cpp

echo "

mi_cc_library(
	SHARED
	NAME
		$2
	SRCS
		MI.$2.hpp
		MI.$2.cpp
	DEPS
)

mi_cc_test(
	NAME
		$2_test
	SRCS
		MI.$2_test.cpp
	DEPS
		::$2
		gtest_main
		gmock_main
    benchmark
)" >> src/$1/CMakeLists.txt
