#!/bin/bash

echo "Lines of code in src\\"

( find "src" \( -name '*.h' -o -name '*.cpp' -o -name '*.hpp' \) -print0 | xargs -0 cat ) | wc -l
