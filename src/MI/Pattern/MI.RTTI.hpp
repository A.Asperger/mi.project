﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once
#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Base/MI.Property.hpp"

namespace mi {

// Здесь представлено одно из моих любимых решений, связанных с RTTI.
// МОЕ ЛИЧНОЕ МНЕНИЕ: стандартный RTTI - кусок того самого.
// Поэтому я везде использую LLVM RTTI.
// Ниже находится минимальный пример реализации класса.
// Некоторые функции модифицированы (get_kind вместо GetKind),
// потому что я использую модифицированную библиотеку LLVM RTTI, чтобы везде сохранялся naming_style.

struct MI_BASE_EXPORT Animal {
  public:
    enum class AnimalKind {
      Dog,
      Cat,
    };

  public:
    virtual ~Animal() = default;

  public:
    Animal(const Animal&)     = default;
    Animal(Animal&&) noexcept = default;

  public:
    Animal& operator=(const Animal&)     = default;
    Animal& operator=(Animal&&) noexcept = default;

  public:
    explicit Animal(const AnimalKind kind) : _kind(kind) {}

  public:
    MI_NODISCARD AnimalKind get_kind() const;

  private:
    AnimalKind _kind;
};

struct MI_BASE_EXPORT Dog final : Animal {
  public:
    static bool class_of(const Animal* base);

  public:
    ~Dog() override = default;

  public:
    Dog(const Dog&)     = default;
    Dog(Dog&&) noexcept = default;

  public:
    Dog& operator=(const Dog&)     = default;
    Dog& operator=(Dog&&) noexcept = default;
};

struct MI_BASE_EXPORT Cat final : Animal {
  public:
    static bool class_of(const Animal* base);

  public:
    ~Cat() override = default;

  public:
    Cat(const Cat&)     = default;
    Cat(Cat&&) noexcept = default;

  public:
    Cat& operator=(const Cat&)     = default;
    Cat& operator=(Cat&&) noexcept = default;
};

}  // namespace mi
