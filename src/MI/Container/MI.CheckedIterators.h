// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <iterator>
#include <type_traits>

#include "MI/Common/MI.Check.hpp"
#include "MI/Container/MI.TemplateUtil.hpp"

namespace mi {

template <typename T>
class checked_contiguous_iterator {
  public:
    using difference_type   = STD ptrdiff_t;
    using value_type        = STD remove_cv_t<T>;
    using pointer           = T*;
    using reference         = T&;
    using iterator_category = STD random_access_iterator_tag;

    // Required for converting constructor below.
    template <typename U>
    friend class checked_contiguous_iterator;

    // -----------------------------------------------------------------------------------------------------------------
    // default constructors

    ~checked_contiguous_iterator() = default;

    constexpr checked_contiguous_iterator() = default;

    constexpr checked_contiguous_iterator(const checked_contiguous_iterator& other)     = default;
    constexpr checked_contiguous_iterator(checked_contiguous_iterator&& other) noexcept = default;

    constexpr checked_contiguous_iterator& operator=(const checked_contiguous_iterator& other)     = default;
    constexpr checked_contiguous_iterator& operator=(checked_contiguous_iterator&& other) noexcept = default;

    // -----------------------------------------------------------------------------------------------------------------
    // constructors

    constexpr checked_contiguous_iterator(T* start, const T* end) : checked_contiguous_iterator(start, start, end) {}

    constexpr checked_contiguous_iterator(const T* start, T* current, const T* end)
        : _start(start), _current(current), _end(end) {
      MI_CHECK(start <= current);
      MI_CHECK(current <= end);
    }

    // Converting constructor allowing conversions like CCI<T> to CCI<const T>,
    // but disallowing CCI<const T> to CCI<T> or CCI<Derived> to CCI<Base>, which
    // are unsafe. Furthermore, this is the same condition as used by the
    // converting constructors of STD span<T> and STD unique_ptr<T[]>.
    // See https://wg21.link/n4042 for details.
    template <typename U, STD enable_if_t<STD is_convertible_v<U (*)[], T (*)[]>>* = nullptr>
    // NOLINTNEXTLINE(google-explicit-constructor)
    constexpr /*explicit*/ checked_contiguous_iterator(const checked_contiguous_iterator<U>& other)
        : _start(other._start), _current(other._current), _end(other._end) {
      // We explicitly don't delegate to the 3-argument constructor here. Its
      // CHECKs would be redundant, since we expect |other| to maintain its own
      // invariant. However, DCHECKs never hurt anybody. Presumably.
      MI_DCHECK(other._start <= other._current);
      MI_DCHECK(other._current <= other._end);
    }

    friend constexpr bool operator==(const checked_contiguous_iterator& lhs, const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current == rhs._current;
    }

    friend constexpr bool operator!=(const checked_contiguous_iterator& lhs, const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current != rhs._current;
    }

    friend constexpr bool operator<(const checked_contiguous_iterator& lhs, const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current < rhs._current;
    }

    friend constexpr bool operator<=(const checked_contiguous_iterator& lhs, const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current <= rhs._current;
    }

    friend constexpr bool operator>(const checked_contiguous_iterator& lhs, const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current > rhs._current;
    }

    friend constexpr bool operator>=(const checked_contiguous_iterator& lhs, const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current >= rhs._current;
    }

    constexpr checked_contiguous_iterator& operator++() {
      MI_CHECK(_current != _end);
      _current = STD next(_current);

      return *this;
    }

    constexpr checked_contiguous_iterator operator++(int) {
      checked_contiguous_iterator old = *this;
      ++*this;
      return old;
    }

    constexpr checked_contiguous_iterator& operator--() {
      MI_CHECK(_current != _start);
      _current = STD prev(_current);

      return *this;
    }

    constexpr checked_contiguous_iterator operator--(int) {
      checked_contiguous_iterator old = *this;
      --*this;
      return old;
    }

    constexpr checked_contiguous_iterator& operator+=(difference_type rhs) {
      if (rhs > 0) {
        MI_CHECK(rhs <= _end - _current);
      } else {
        MI_CHECK(-rhs <= _current - _start);
      }
      _current = STD next(_current, rhs);
      return *this;
    }

    constexpr checked_contiguous_iterator operator+(difference_type rhs) const {
      checked_contiguous_iterator it  = *this;
      it                             += rhs;
      return it;
    }

    constexpr checked_contiguous_iterator& operator-=(difference_type rhs) {
      if (rhs < 0) {
        MI_CHECK(-rhs <= _end - _current);
      } else {
        MI_CHECK(rhs <= _current - _start);
      }
      _current = STD prev(_current, rhs);
      return *this;
    }

    constexpr checked_contiguous_iterator operator-(difference_type rhs) const {
      checked_contiguous_iterator it  = *this;
      it                             -= rhs;
      return it;
    }

    constexpr friend difference_type operator-(const checked_contiguous_iterator& lhs,
                                               const checked_contiguous_iterator& rhs) {
      lhs._check_comparable(rhs);
      return lhs._current - rhs._current;
    }

    constexpr reference operator*() const {
      MI_CHECK(_current != _end);
      return *_current;
    }

    constexpr pointer operator->() const {
      MI_CHECK_NE(_current, _end);
      return _current;
    }

    constexpr reference operator[](difference_type rhs) const {
      MI_CHECK(rhs >= 0);
      MI_CHECK(rhs < _end - _current);
      return *STD next(_current, rhs);
    }

    MI_NODISCARD static bool is_range_move_safe(const checked_contiguous_iterator& from_begin,
                                                const checked_contiguous_iterator& from_end,
                                                const checked_contiguous_iterator& to) {
      if (from_end < from_begin) {
        return false;
      }
      const auto from_begin_uintptr = get_uintptr(from_begin._current);
      const auto from_end_uintptr   = get_uintptr(from_end._current);
      const auto to_begin_uintptr   = get_uintptr(to._current);
      const auto to_end_uintptr     = get_uintptr((to + STD distance(from_begin, from_end))._current);

      return to_begin_uintptr >= from_end_uintptr || to_end_uintptr <= from_begin_uintptr;
    }

    MI_NODISCARD MI_CONSTEXPR_20 pointer unwrapped() const noexcept { return MI unfancy(_current); }

    MI_CONSTEXPR_20 void seek_to(const value_type* _It) noexcept {
      _current = refancy<value_type*>(const_cast<value_type*>(_It));
    }

  private:
    constexpr void _check_comparable(const checked_contiguous_iterator& other) const {
      MI_CHECK(_start == other._start);
      MI_CHECK(_end == other._end);
    }

  public:
    constexpr void verify_range(const checked_contiguous_iterator& first,
                                const checked_contiguous_iterator& last) noexcept {
      MI_DCHECK(first <= last) << "transposed pointer range";
    }

    const T* _start = nullptr;
    T* _current     = nullptr;
    const T* _end   = nullptr;
};

template <typename T>
using checked_contiguous_const_iterator = checked_contiguous_iterator<const T>;

}  // namespace mi
