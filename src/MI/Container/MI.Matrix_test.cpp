﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Container/MI.Matrix.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace mi::test {

#define MI_EXPECT_DCHECK_DEATH(statement)           GTEST_UNSUPPORTED_DEATH_TEST(statement, "", )
#define MI_EXPECT_DCHECK_DEATH_WITH(statement, msg) GTEST_UNSUPPORTED_DEATH_TEST(statement, msg, )
#define MI_ASSERT_DCHECK_DEATH(statement)           GTEST_UNSUPPORTED_DEATH_TEST(statement, "", return)
#define MI_ASSERT_DCHECK_DEATH_WITH(statement, msg) GTEST_UNSUPPORTED_DEATH_TEST(statement, msg, return)

#define MI_EXPECT_CHECK_DEATH(statement) EXPECT_DEATH(statement, "")
#define MI_ASSERT_CHECK_DEATH(statement) ASSERT_DEATH(statement, "")

TEST(Matrix, DefaultConstructor) {
  const MI matrix<int, 3, 3> m;

  ASSERT_THAT(m, testing::ElementsAre(0, 0, 0, 0, 0, 0, 0, 0, 0));
}

TEST(Matrix, Mul) {
  const MI matrix<int, 3, 1> m3x1 = {11, 12, 13};
  const MI matrix<int, 3, 3> m3x3 = {
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9}
  };

  const auto m_res = m3x3 * m3x1;

  ASSERT_THAT(m_res, testing::ElementsAre(74, 182, 290));
}

TEST(Matrix, NRows) {
  const MI matrix<int, 3, 1> m3x1;
  const MI matrix<int, 3, 3> m3x3;

  GTEST_ASSERT_EQ(m3x1.n_rows(), 3);
  GTEST_ASSERT_EQ(m3x3.n_rows(), 3);
}

TEST(Matrix, NCols) {
  const MI matrix<int, 3, 1> m3x1;
  const MI matrix<int, 3, 3> m3x3;

  GTEST_ASSERT_EQ(m3x1.n_cols(), 1);
  GTEST_ASSERT_EQ(m3x3.n_cols(), 3);
}

TEST(Matrix, Get) {
  const MI matrix<int, 3, 1> m3x1 = {0, 1, 2};
  const MI matrix<int, 3, 3> m3x3 = {
      {0, 1, 2},
      {3, 4, 5},
      {6, 7, 8}
  };

  // m3x1
  GTEST_ASSERT_EQ(m3x1.get(0), 0);
  GTEST_ASSERT_EQ(m3x1.get(1), 1);
  GTEST_ASSERT_EQ(m3x1.get(2), 2);

  // m3x3
  GTEST_ASSERT_EQ(m3x3.get(0, 0), 0);
  GTEST_ASSERT_EQ(m3x3.get(0, 1), 1);
  GTEST_ASSERT_EQ(m3x3.get(0, 2), 2);

  GTEST_ASSERT_EQ(m3x3.get(1, 0), 3);
  GTEST_ASSERT_EQ(m3x3.get(1, 1), 4);
  GTEST_ASSERT_EQ(m3x3.get(1, 2), 5);

  GTEST_ASSERT_EQ(m3x3.get(2, 0), 6);
  GTEST_ASSERT_EQ(m3x3.get(2, 1), 7);
  GTEST_ASSERT_EQ(m3x3.get(2, 2), 8);

  // m3x1
  MI_EXPECT_DCHECK_DEATH((void)m3x1.get(0, 3));

  // m3x1
  MI_EXPECT_DCHECK_DEATH((void)m3x3.get(0, 3));
  MI_EXPECT_DCHECK_DEATH((void)m3x3.get(1, 3));
  MI_EXPECT_DCHECK_DEATH((void)m3x3.get(2, 3));
  MI_EXPECT_DCHECK_DEATH((void)m3x3.get(3, 0));
}

TEST(Matrix, Cross) {
  const MI matrix<int, 3, 1> v1 = {1, 2, 3};
  const MI matrix<int, 3, 1> v2 = {30, 20, 5};

  EXPECT_THAT(v1.cross(v2), testing::ElementsAre(-50, 85, -40));
}

TEST(Matrix, Dot) {
  const MI matrix<int, 3, 1> v1 = {1, 2, 3};
  const MI matrix<int, 3, 1> v2 = {30, 20, 5};

  ASSERT_EQ(v1.dot(v2), 85);
}

TEST(Matrix, Transpose) {
  const MI matrix<int, 3, 1> v1 = {1, 2, 3};
  const MI matrix<int, 3, 4> v2 = {
      {1,  2,  3,  4},
      {5,  6,  7,  8},
      {9, 10, 11, 12}
  };

  ASSERT_THAT(v1.transpose(), testing::ElementsAre(1, 2, 3));
  ASSERT_THAT(v2.transpose(), testing::ElementsAre(1, 5, 9, 2, 6, 10, 3, 7, 11, 4, 8, 12));
}

TEST(Matrix, Determinant) {
  const MI matrix<int, 3, 3> v0 = {
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9}
  };
  const MI matrix<int, 3, 3> v1 = {
      { 1, 12,  32},
      {44, 54, -12},
      { 0, 66,  98}
  };

  const MI matrix<int, 2, 2> v2 = {
      {12, -23},
      {15,   2}
  };
  const MI matrix<int, 2, 2> v3 = {
      {0, 72},
      {1,  6}
  };

  ASSERT_EQ(v0.determinant(), 0);
  ASSERT_EQ(v1.determinant(), 47268);
  ASSERT_EQ(v2.determinant(), 369);
  ASSERT_EQ(v3.determinant(), -72);
}

TEST(Matrix, Inverse) {
  const MI matrix<int, 3, 3> v0 = {
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9}
  };
  const MI matrix<double, 3, 3> v1 = {
      { 1., 12.,  32.},
      {44., 54., -12.},
      { 0., 66.,  98.}
  };
  const MI matrix<double, 2, 2> v2 = {
      { 3.,  -7.},
      {13., 103.}
  };

  MI_ASSERT_CHECK_DEATH((void)v0.inverse());

  const auto det1 = v1.determinant();
  const auto det2 = v2.determinant();

  EXPECT_THAT(v1.inverse(), testing::ElementsAre(6084. / det1, 936. / det1, -1872. / det1, -4312. / det1, 98. / det1,
                                                 1420. / det1, 2904. / det1, -66. / det1, -474. / det1));

  EXPECT_THAT(v2.inverse(), testing::ElementsAre(103. / det2, 7. / det2, -13. / det2, 3. / det2));
}

TEST(Matrix, SquaredEuclideanNorm) {
  const MI matrix<int, 3, 1> v1    = {-1, -2, -3};
  const MI matrix<int, 1, 3> v2    = {1, 2, 3};
  const MI matrix<double, 3, 3> v3 = {
      {  1., 2., 3.},
      {-10., 1., 2.},
      {  7., 0., 0.}
  };

  ASSERT_EQ(v1.squared_euclidean_norm(), 14.);
  ASSERT_EQ(v2.squared_euclidean_norm(), 14.);
  ASSERT_EQ(v3.squared_euclidean_norm(), 168.);
}

TEST(Matrix, EuclideanNorm) {
  const MI matrix<int, 3, 1> v1    = {-1, -2, -3};
  const MI matrix<int, 1, 3> v2    = {1, 2, 3};
  const MI matrix<double, 3, 3> v3 = {
      {  1., 2., 3.},
      {-10., 1., 2.},
      {  7., 0., 0.}
  };

  EXPECT_DOUBLE_EQ(v1.euclidean_norm(), STD sqrt(14.));
  EXPECT_DOUBLE_EQ(v2.euclidean_norm(), STD sqrt(14.));
  EXPECT_DOUBLE_EQ(v3.euclidean_norm(), STD sqrt(168.));
}

TEST(Matrix, SquaredNorm) {
  const MI matrix<int, 3, 1> v1    = {-1, -2, -3};
  const MI matrix<int, 1, 3> v2    = {1, 2, 3};
  const MI matrix<double, 3, 3> v3 = {
      {  1., 2., 3.},
      {-10., 1., 2.},
      {  7., 0., 0.}
  };

  ASSERT_EQ(v1.squared_norm(), 9.);
  ASSERT_EQ(v2.squared_norm(), 14.);
  ASSERT_EQ(v3.squared_norm(), 105.);
}

TEST(Matrix, Norm) {
  const MI matrix<int, 3, 1> v1    = {-1, -2, -3};
  const MI matrix<int, 1, 3> v2    = {1, 2, 3};
  const MI matrix<double, 3, 3> v3 = {
      {  1., 2., 3.},
      {-10., 1., 2.},
      {  7., 0., 0.}
  };

  EXPECT_DOUBLE_EQ(v1.norm(), STD sqrt(9.));
  EXPECT_DOUBLE_EQ(v2.norm(), STD sqrt(14.));
  EXPECT_DOUBLE_EQ(v3.norm(), STD sqrt(105.));
}

TEST(Matrix, X) {
  const MI matrix<int, 3, 1> v1 = {-1, -2, -3};
  const MI matrix<int, 1, 3> v2 = {1, 2, 3};
  const MI matrix<int, 3, 3> v3 = {
      {  1, 2, 3},
      {-10, 1, 2},
      {  7, 0, 0}
  };

  ASSERT_EQ(v1.x(), -1);
  ASSERT_EQ(v1.y(), -2);
  ASSERT_EQ(v1.z(), -3);

  ASSERT_EQ(v2.x(), 1);
  ASSERT_EQ(v2.y(), 2);
  ASSERT_EQ(v2.z(), 3);

  MI_ASSERT_DCHECK_DEATH((void)v3.x());
  MI_ASSERT_DCHECK_DEATH((void)v3.y());
  MI_ASSERT_DCHECK_DEATH((void)v3.z());
}

TEST(Matrix, MatrixPlusMatrix) {
  const MI matrix<int, 3, 1> v1 = {-1, -2, -3};
  const MI matrix<int, 3, 1> v2 = {2, 1, 11};

  EXPECT_THAT(v1 + v2, testing::ElementsAre(1, -1, 8));
}

TEST(Matrix, MatrixMinusMatrix) {
  const MI matrix<int, 3, 1> v1 = {-1, -2, -3};
  const MI matrix<int, 3, 1> v2 = {2, 1, 11};

  EXPECT_THAT(v1 - v2, testing::ElementsAre(-3, -3, -14));
}

TEST(Matrix, MatrixDivScalar) {
  const MI matrix<int, 3, 1> v1 = {-1, -2, -3};

  EXPECT_THAT(v1 / 2, testing::ElementsAre(-0.5, -1, -1.5));
}

TEST(Matrix, SumAssignOp) {
  MI matrix<int, 3, 1> v1       = {-1, -2, -3};
  const MI matrix<int, 3, 1> v2 = {3, 5, 9};

  EXPECT_THAT(v1 += v2, testing::ElementsAre(2, 3, 6));
}

TEST(Matrix, Normalized) {
  MI matrix<double, 1, 3> v1 = {1., 1., 1.};
  MI matrix<double, 1, 3> v2 = {10., 100., 1000.};
  MI matrix<double, 1, 3> v3 = {-1., -2., 3.};
  MI matrix<double, 1, 3> v4 = {1.000'000'001, 0., 0.};

  v1 = v1.normalized();
  v2 = v2.normalized();
  v3 = v3.normalized();
  v4 = v4.normalized();

  EXPECT_DOUBLE_EQ(v1.euclidean_norm(), 1.);
  EXPECT_DOUBLE_EQ(v2.euclidean_norm(), 1.);
  EXPECT_DOUBLE_EQ(v3.euclidean_norm(), 1.);
  EXPECT_DOUBLE_EQ(v4.euclidean_norm(), 1.);
}

}  // namespace mi::test
