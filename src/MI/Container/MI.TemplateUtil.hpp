// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <memory>
#include <stdint.h>
#include <type_traits>

#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.If.h"

namespace mi {

template <class Ptr>
MI_NODISCARD constexpr auto unfancy(Ptr _Ptr) noexcept {  // converts from a fancy pointer to a plain pointer
  return STD addressof(*_Ptr);
}

template <class Ty>
MI_NODISCARD constexpr Ty* unfancy(Ty* _Ptr) noexcept {  // do nothing for plain pointers
  return _Ptr;
}

template <class Ptr, MI if_t<!STD is_pointer_v<Ptr>> = 0>
MI_CONSTEXPR_20 Ptr refancy(typename std::pointer_traits<Ptr>::element_type* _Ptr) noexcept {
  return STD pointer_traits<Ptr>::pointer_to(*_Ptr);
}

template <class Ptr, MI if_t<STD is_pointer_v<Ptr>> = 0>
MI_CONSTEXPR_20 Ptr refancy(Ptr _Ptr) noexcept {
  return _Ptr;
}

// TODO(crbug.com/817982): What we really need is for checked_math.h to be
// able to do checked arithmetic on pointers.
template <typename T>
uintptr_t get_uintptr(const T* t) {
  return reinterpret_cast<uintptr_t>(t);
}

}  // namespace mi
