#pragma once

#include <algorithm>
#include <functional>

#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.Check.hpp"
#include "MI/Common/MI.Hash.h"
#include "MI/Common/MI.If.h"
#include "MI/Common/MI.InBrackets.h"
#include "MI/Common/MI.TemplateUtil.hpp"
#include "MI/Common/MI.VerifyRange.h"
#include "MI/Container/MI.StaticVector.hpp"

namespace mi {
// Без сортировки
template <class Ty, size_t Size, class Predicate = STD less<Ty>>
class sortable {
  public:
    using container_type = MI static_vector<Ty, Size>;

  public:
    using value_type      = Ty;
    using size_type       = typename container_type::size_type;
    using difference_type = typename container_type::difference_type;
    using pointer         = typename container_type::pointer;
    using const_pointer   = typename container_type::const_pointer;
    using reference       = typename container_type::reference;
    using const_reference = typename container_type::const_reference;

  public:
    using iterator               = typename container_type::iterator;
    using const_iterator         = typename container_type::const_iterator;
    using reverse_iterator       = STD reverse_iterator<iterator>;
    using const_reverse_iterator = STD reverse_iterator<const_iterator>;

  private:
    MI_CONSTEXPR_17 void _sort() noexcept {
      Ty* first      = unchecked_begin();
      const Ty* last = unchecked_end();

      MI verify_range(first, last);

      for (; first != last; first = STD next(first)) {
        // Итератор на минимальный элемент
        auto min_it = first;

        for (auto mid = STD next(first); mid != last; ++mid) {
          if (Predicate()(*mid, *min_it)) {
            min_it = mid;
          }
        }

        // swap
        auto tmp = STD move(*first);
        *first   = STD move(*min_it);
        *min_it  = STD move(tmp);
      }
    }

    MI_CONSTEXPR_17 void _copy_construct(const container_type& lv_new_data) noexcept {
      _data = lv_new_data;

      _sort();
    }

    MI_CONSTEXPR_17 void _move_construct(container_type&& rv_new_data) noexcept {
      _data = STD move(rv_new_data);

      _sort();
    }

    template <class It, MI if_t<STD is_pointer_v<It> || MI IsIterator<It>> = 0>
    MI_CONSTEXPR_17 void _range_construct(It first, It last) noexcept {
      MI adl_verify_range(first, last);

      MI_DCHECK(STD distance(first, last) == Size);

      auto u_first      = MI get_unwrapped(first);
      const auto u_last = MI get_unwrapped(last);

      for (; u_first != u_last; ++u_first) {
        _data.emplace_back(*u_first);
      }

      _sort();
    }

    template <size_t... Idx>
    // NOLINTNEXTLINE
    MI_CONSTEXPR_17 void _sequence_construct(const value_type (&arr)[Size], STD index_sequence<Idx...>) noexcept {
      _data = {{arr[Idx]...}};

      _sort();
    }

    template <size_t... Idx>
    // NOLINTNEXTLINE
    MI_CONSTEXPR_17 void _sequence_construct(value_type (&&arr)[Size], STD index_sequence<Idx...>) noexcept {
      _data = {{STD move(arr[Idx])...}};

      _sort();
    }

  private:
    MI_CONSTEXPR_17 void _tidy() noexcept { _data.clear(); }

  public:
    ~sortable() = default;

  public:
    MI_CONSTEXPR_17 sortable() noexcept = default;

  public:
    MI_CONSTEXPR_17 sortable(const sortable& other)     = default;
    MI_CONSTEXPR_17 sortable(sortable&& other) noexcept = default;

  public:
    MI_CONSTEXPR_17 sortable& operator=(const sortable& other)     = default;
    MI_CONSTEXPR_17 sortable& operator=(sortable&& other) noexcept = default;

  public:
    template <class Iterator, MI if_t<MI IsIterator<Iterator>> = 0>
    MI_CONSTEXPR_17 sortable(Iterator first, Iterator last) : _data() {
      _range_construct(first, last);
    }

  public:
    MI_CONSTEXPR_17 sortable(STD initializer_list<value_type> init_list) noexcept : _data() {
      auto* first = init_list.begin();
      auto* last  = init_list.end();

      _range_construct(first, last);
    }

    // Оператор для initializer_list
    MI_CONSTEXPR_17 sortable& operator=(STD initializer_list<value_type> init_list) noexcept {
      auto* first = init_list.begin();
      auto* last  = init_list.end();

      _range_construct(first, last);

      return *this;
    }

  public:
    template <class Container, if_t<::mi::IsContainer<Container> && !IsInitList<Container>> = 0>
    MI_CONSTEXPR_17 sortable(const Container& cont) : _data() {
      _range_construct(cont.begin(), cont.end());
    }

  public:
    // Конструктор копирования для последовательности
    // NOLINTNEXTLINE
    MI_CONSTEXPR_17 explicit sortable(const Ty (&arr)[Size]) noexcept : _data(container_type{}) {
      _sequence_construct(arr, STD make_index_sequence<Size>{});
    }

    // Конструктор перемещения для последовательности
    // NOLINTNEXTLINE
    MI_CONSTEXPR_17 explicit sortable(Ty (&&arr)[Size]) noexcept : _data(container_type{}) {
      _sequence_construct(STD move(arr), STD make_index_sequence<Size>{});
    }

  public:
    MI_CONSTEXPR_17 void swap(sortable& other) noexcept { _data.swap(other._data); }

    MI_NODISCARD MI_CONSTEXPR_17 iterator begin() noexcept { return _data.begin(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_iterator begin() const noexcept { return _data.begin(); }

    MI_NODISCARD MI_CONSTEXPR_17 iterator end() noexcept { return _data.end(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_iterator end() const noexcept { return _data.end(); }

    MI_NODISCARD MI_CONSTEXPR_17 reverse_iterator rbegin() noexcept { return _data.rbegin(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator rbegin() const noexcept { return _data.rbegin(); }

    MI_NODISCARD MI_CONSTEXPR_17 reverse_iterator rend() noexcept { return _data.rend(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator rend() const noexcept { return _data.rend(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_iterator cbegin() const noexcept { return _data.cbegin(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_iterator cend() const noexcept { return _data.cend(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator crbegin() const noexcept { return _data.crbegin(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator crend() const noexcept { return _data.crend(); }

    MI_NODISCARD MI_CONSTEXPR_17 Ty* unchecked_begin() noexcept { return STD addressof(_data[0]); }

    MI_NODISCARD MI_CONSTEXPR_17 const Ty* unchecked_begin() const noexcept { return STD addressof(_data[0]); }

    MI_NODISCARD MI_CONSTEXPR_17 Ty* unchecked_end() noexcept { return STD next(unchecked_begin(), Size); }

    MI_NODISCARD MI_CONSTEXPR_17 const Ty* unchecked_end() const noexcept { return STD next(unchecked_begin(), Size); }

    MI_NODISCARD MI_CONSTEXPR_17 size_type size() const noexcept { return _data.size(); }

    MI_NODISCARD MI_CONSTEXPR_17 size_type max_size() const noexcept { return Size; }

    MI_NODISCARD MI_CONSTEXPR_17 bool empty() const noexcept { return _data.empty(); }

    MI_NODISCARD MI_CONSTEXPR_17 reference at(size_type position) { return _data[position]; }

    MI_NODISCARD MI_CONSTEXPR_17 const_reference at(size_type position) const { return _data[position]; }

    MI_NODISCARD MI_CONSTEXPR_17 reference operator[](size_type position) noexcept { return _data[position]; }

    MI_NODISCARD MI_CONSTEXPR_17 const_reference operator[](size_type position) const noexcept {
      return _data[position];
    }

    MI_NODISCARD MI_CONSTEXPR_17 reference front() noexcept { return _data[0]; }

    MI_NODISCARD MI_CONSTEXPR_17 const_reference front() const noexcept { return _data[0]; }

    MI_NODISCARD MI_CONSTEXPR_17 reference back() noexcept { return _data[Size - 1]; }

    MI_NODISCARD MI_CONSTEXPR_17 const_reference back() const noexcept { return _data[Size - 1]; }

    MI_NODISCARD MI_CONSTEXPR_17 pointer data() noexcept {  //-V524
      return STD addressof(_data[0]);
    }

    MI_NODISCARD MI_CONSTEXPR_17 const_pointer data() const noexcept {  //-V524
      return STD addressof(_data[0]);
    }

  public:
    template <class OStream, MI if_t<internal::SupportsOstreamOperator<OStream>> = 0>
    MI_NODISCARD MI_CONSTEXPR_17 friend OStream& operator<<(OStream& out, const sortable& right) {
      // "(1, 2, 5)"
      out << MI in_brackets::round(right._data.begin(), right._data.end());

      return out;
    }

    MI_NODISCARD MI_CONSTEXPR_17 friend bool operator==(const sortable& lhs, const sortable& rhs) {
      return lhs._data == rhs._data;
    }

    MI_NODISCARD MI_CONSTEXPR_17 friend bool operator!=(const sortable& lhs, const sortable& rhs) {
      return !(lhs == rhs);
    }

    MI_NODISCARD MI_CONSTEXPR_17 friend bool operator<(const sortable& lhs, const sortable& rhs) {
      return lhs._data < rhs._data;
    }

    MI_NODISCARD MI_CONSTEXPR_17 friend bool operator>(const sortable& lhs, const sortable& rhs) { return rhs < lhs; }

    MI_NODISCARD MI_CONSTEXPR_17 friend bool operator<=(const sortable& lhs, const sortable& rhs) {
      return !(lhs > rhs);
    }

    MI_NODISCARD MI_CONSTEXPR_17 friend bool operator>=(const sortable& lhs, const sortable& rhs) {
      return !(lhs < rhs);
    }

  private:
    container_type _data;
};

#if MI_CPP_VERSION >= 17
// ReSharper disable once CppEnforceFunctionDeclarationStyle
template <class First, class... Rest>
sortable(First, Rest...) -> sortable<typename internal::EnforceSameT<First, Rest...>, 1 + sizeof...(Rest)>;
#endif  // HAS_CXX17

template <class Ty, size_t Size>
MI_CONSTEXPR_17 void swap(sortable<Ty, Size>& left, sortable<Ty, Size>& right) noexcept {
  return left.swap(right);
}
}  // namespace mi

template <class Ty, size_t Size>
struct mi::hash<MI sortable<Ty, Size>> {
    STD size_t operator()(const MI sortable<Ty, Size>& s) const {
      STD size_t seed(0);

      for (const auto& val : s) {
        const STD size_t hash_val = MI hash<Ty>{}(val);
        seed                      = MI hash_combine(seed, hash_val);
      }

      return seed;
    }
};
