#pragma once

#include <deque>
#include <forward_list>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "MI/Common/MI.Hash.h"

namespace mi {
// ---------------------------------------------------------------------------------------------------------------------
// map

template <class KeyTy, class Ty, class PredicateTy = STD less<KeyTy>>
using map = STD pmr::map<KeyTy, Ty>;

template <class KeyTy, class Ty, class PredicateTy = STD less<KeyTy>>
using multimap = STD pmr::multimap<KeyTy, Ty, PredicateTy>;

template <class KeyTy, class Ty, class Hasher = MI hash<KeyTy>, class KeyEq = STD equal_to<KeyTy>>
using unordered_map = STD pmr::unordered_map<KeyTy, Ty, Hasher, KeyEq>;

template <class KeyTy, class Ty, class Hasher = MI hash<KeyTy>, class KeyEq = STD equal_to<KeyTy>>
using unordered_multimap = STD pmr::unordered_multimap<KeyTy, Ty, Hasher, KeyEq>;

// ---------------------------------------------------------------------------------------------------------------------
// set

template <class KeyTy, class PredicateTy = STD less<KeyTy>>
using set = STD pmr::set<KeyTy, KeyTy>;

template <class KeyTy, class PredicateTy = STD less<KeyTy>>
using multiset = STD pmr::multiset<KeyTy, PredicateTy>;

template <class KeyTy, class Hasher = MI hash<KeyTy>, class KeyEq = STD equal_to<KeyTy>>
using unordered_set = STD pmr::unordered_set<KeyTy, Hasher, KeyEq>;

template <class KeyTy, class Hasher = MI hash<KeyTy>, class KeyEq = STD equal_to<KeyTy>>
using unordered_multiset = STD pmr::unordered_multiset<KeyTy, Hasher, KeyEq>;

// ---------------------------------------------------------------------------------------------------------------------
// vector

template <class Ty>
using vector = STD vector<Ty>;

// ---------------------------------------------------------------------------------------------------------------------
// array

template <class Ty, size_t N>
using array = STD array<Ty, N>;

// ---------------------------------------------------------------------------------------------------------------------
// queue

template <class Ty>
using queue = STD queue<Ty>;

template <class Ty>
using priority_queue = STD priority_queue<Ty>;

template <class Ty>
using deque = STD pmr::deque<Ty>;

// ---------------------------------------------------------------------------------------------------------------------
// list

template <class Ty>
using list = STD pmr::list<Ty>;

template <class Ty>
using forward_list = STD pmr::forward_list<Ty>;

// ---------------------------------------------------------------------------------------------------------------------
// string

using string    = STD pmr::string;
using wstring   = STD pmr::wstring;
using u16string = STD pmr::u16string;
using u32string = STD pmr::u32string;

template <class Elem, class Traits = STD char_traits<Elem>>
using basic_string = STD pmr::basic_string<Elem, Traits>;
}  // namespace mi
