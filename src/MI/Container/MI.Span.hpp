// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <array>
#include <cstddef>
#include <iterator>
#include <limits>
#include <type_traits>
#include <utility>

#include "MI/Base/MI.CXX20.ToAddress.h"
#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.Check.hpp"
#include "MI/Common/MI.NarrowingCast.h"
#include "MI/Container/MI.CheckedIterators.h"
#include "MI/Container/MI.ContiguousIterator.h"

namespace mi {

// [views.constants]
constexpr size_t dynamic_extent = STD numeric_limits<size_t>::max();

template <class T, size_t Extent = dynamic_extent>
class span;

namespace internal {

template <size_t I>
using size_constant = STD integral_constant<size_t, I>;

template <class T>
struct extent_impl : size_constant<dynamic_extent> {};

template <class T, size_t N>
struct extent_impl<T[N]> : size_constant<N> {};

template <class T, size_t N>
struct extent_impl<STD array<T, N>> : size_constant<N> {};

template <class T, size_t N>
struct extent_impl<MI span<T, N>> : size_constant<N> {};

template <class T>
using extent = extent_impl<MI remove_cvref_t<T>>;

template <class T>
struct is_span_impl : STD false_type {};

template <class T, size_t Extent>
struct is_span_impl<span<T, Extent>> : STD true_type {};

template <class T>
using is_not_span = STD negation<is_span_impl<STD decay_t<T>>>;

template <class T>
struct is_std_array_impl : STD false_type {};

template <class T, size_t N>
struct is_std_array_impl<STD array<T, N>> : STD true_type {};

template <class T>
using is_not_std_array = STD negation<is_std_array_impl<STD decay_t<T>>>;

template <class T>
using is_not_c_array = STD negation<STD is_array<STD remove_reference_t<T>>>;

template <class From, class To>
using is_legal_data_conversion = STD is_convertible<From (*)[], To (*)[]>;

template <class Iter, class T>
using iterator_has_convertible_reference_type =
    is_legal_data_conversion<STD remove_reference_t<MI iter_reference_t<Iter>>, T>;

template <class Iter, class T>
using enable_if_compatible_contiguous_iterator =
    STD enable_if_t<STD conjunction_v<is_contiguous_iterator<Iter>, iterator_has_convertible_reference_type<Iter, T>>>;

template <class Container, class T>
using container_has_convertible_data =
    is_legal_data_conversion<STD remove_pointer_t<decltype(STD data(STD declval<Container>()))>, T>;

template <class Container>
using container_has_integral_size = STD is_integral<decltype(STD size(STD declval<Container>()))>;

template <class From, size_t FromExtent, class To, size_t ToExtent>
using enable_if_legal_span_conversion = STD
    enable_if_t<(ToExtent == dynamic_extent || ToExtent == FromExtent) && is_legal_data_conversion<From, To>::value>;

// SFINAE check if Array can be converted to a span<T>.
template <class Array, class T, size_t Extent>
using enable_if_span_compatible_array = STD enable_if_t<(Extent == dynamic_extent || Extent == extent<Array>::value) &&
                                                        container_has_convertible_data<Array, T>::value>;

// SFINAE check if Container can be converted to a span<T>.
template <class Container, class T>
using is_span_compatible_container =
    STD conjunction<is_not_span<Container>, is_not_std_array<Container>, is_not_c_array<Container>,
                    container_has_convertible_data<Container, T>, container_has_integral_size<Container>>;

template <class Container, class T>
using enable_if_span_compatible_container = STD enable_if_t<is_span_compatible_container<Container, T>::value>;

template <class Container, class T, size_t Extent>
using enable_if_span_compatible_container_and_span_is_dynamic =
    STD enable_if_t<is_span_compatible_container<Container, T>::value && Extent == dynamic_extent>;

// A helper template for storing the size of a span. Spans with static extents
// don't require additional storage, since the extent itself is specified in the
// template parameter.
template <size_t Extent>
struct extent_storage {
  public:
    constexpr explicit extent_storage(const size_t /* size */) noexcept {}

    MI_NODISCARD constexpr size_t size() const noexcept { return Extent; }
};

// Specialization of extent_storage for dynamic extents, which do require
// explicit storage for the size.
template <>
struct extent_storage<dynamic_extent> {
    constexpr explicit extent_storage(const size_t size) noexcept : _size(size) {}

    MI_NODISCARD constexpr size_t size() const noexcept { return _size; }

  private:
    size_t _size;
};

// must_not_be_dynamic_extent prevents |dynamic_extent| from being returned in a
// constexpr context.
template <size_t Extent>
constexpr size_t must_not_be_dynamic_extent() {
  static_assert(Extent != dynamic_extent, "MI_EXTENT should only be used for containers with a static extent.");
  return Extent;
}

}  // namespace internal

// A span is a value type that represents an array of elements of type T. Since
// it only consists of a pointer to memory with an associated size, it is very
// light-weight. It is cheap to construct, copy, move and use spans, so that
// users are encouraged to use it as a pass-by-value parameter. A span does not
// own the underlying memory, so care must be taken to ensure that a span does
// not outlive the backing store.
//
// span is somewhat analogous to StringPiece, but with arbitrary element types,
// allowing mutation if T is non-const.
//
// span is implicitly convertible from C++ arrays, as well as most [1]
// container-like types that provide a data() and size() method (such as
// MI vector<T>). A mutable span<T> can also be implicitly converted to an
// immutable span<const T>.
//
// Consider using a span for functions that take a data pointer and size
// parameter: it allows the function to still act on an array-like type, while
// allowing the caller code to be a bit more concise.
//
// For read-only data access pass a span<const T>: the caller can supply either
// a span<const T> or a span<T>, while the callee will have a read-only view.
// For read-write access a mutable span<T> is required.
//
// Without span:
//   Read-Only:
//     // STD string HexEncode(const uint8_t* data, size_t size);
//     MI vector<uint8_t> data_buffer = GenerateData();
//     STD string r = HexEncode(data_buffer.data(), data_buffer.size());
//
//  Mutable:
//     // ssize_t SafeSNPrintf(char* buf, size_t N, const char* fmt, Args...);
//     char str_buffer[100];
//     SafeSNPrintf(str_buffer, sizeof(str_buffer), "Pi ~= %lf", 3.14);
//
// With span:
//   Read-Only:
//     // STD string HexEncode(MI span<const uint8_t> data);
//     MI vector<uint8_t> data_buffer = GenerateData();
//     STD string r = HexEncode(data_buffer);
//
//  Mutable:
//     // ssize_t SafeSNPrintf(MI span<char>, const char* fmt, Args...);
//     char str_buffer[100];
//     SafeSNPrintf(str_buffer, "Pi ~= %lf", 3.14);
//
// Spans with "const" and pointers
// -------------------------------
//
// Const and pointers can get confusing. Here are vectors of pointers and their
// corresponding spans:
//
//   const MI vector<int*>        =>  MI span<int* const>
//   MI vector<const int*>        =>  MI span<const int*>
//   const MI vector<const int*>  =>  MI span<const int* const>
//
// Differences from the C++20 draft
// --------------------------------
//
// http://eel.is/c++draft/views contains the latest C++20 draft of STD span.
// Chromium tries to follow the draft as close as possible. Differences between
// the draft and the implementation are documented in subsections below.
//
// Differences from [span.objectrep]:
// - as_bytes() and as_writable_bytes() return spans of uint8_t instead of
//   STD byte (STD byte is a C++17 feature)
//
// Differences from [span.cons]:
// - Constructing a static span (i.e. Extent != dynamic_extent) from a dynamic
//   sized container (e.g. MI vector) requires an explicit conversion (in the
//   C++20 draft this is simply UB)
//
// Furthermore, all constructors and methods are marked noexcept due to the lack
// of exceptions in Chromium.
//
// Due to the lack of class template argument deduction guides in C++14
// appropriate make_span() utility functions are provided.

// [span], class template span
template <class T, size_t Extent>
class span : public internal::extent_storage<Extent> {
  private:
    using extent_storage_t = internal::extent_storage<Extent>;

  public:
    using element_type    = T;
    using value_type      = STD remove_cv_t<T>;
    using size_type       = size_t;
    using difference_type = ptrdiff_t;

    using pointer       = T*;
    using const_pointer = const T*;

    using reference       = T&;
    using const_reference = const T&;

    using iterator = checked_contiguous_iterator<T>;
    // using const_iterator         = iterator;
    using reverse_iterator = STD reverse_iterator<iterator>;
    // using const_reverse_iterator = STD reverse_iterator<const_iterator>;

    static constexpr size_t extent = Extent;

    // -----------------------------------------------------------------------------------------------------------------
    // [span.cons], span constructors, copy, assignment, and destructor

    ~span() noexcept = default;

    constexpr span(const span& other) noexcept = default;
    constexpr span(span&& other) noexcept      = default;

    constexpr span& operator=(const span& other) noexcept = default;
    constexpr span& operator=(span&& other) noexcept      = default;

    // -----------------------------------------------------------------------------------------------------------------
    // span constructors

    constexpr span() noexcept : extent_storage_t(0), _data(nullptr) {
      static_assert(Extent == dynamic_extent || Extent == 0, "Invalid Extent");
    }

    template <class It, class = internal::enable_if_compatible_contiguous_iterator<It, T>>
    constexpr span(It first, size_t count) noexcept
        : extent_storage_t(count),
          // The use of to_address() here is to handle the case where the iterator
          // `first` is pointing to the container's `end()`. In that case we can
          // not use the address returned from the iterator, or dereference it
          // through the iterator's `operator*`, but we can store it. We must assume
          // in this case that `count` is 0, since the iterator does not point to
          // valid data. Future hardening of iterators may disallow pulling the
          // address from `end()`, as demonstrated by asserts() in libstdc++:
          // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=93960.
          //
          // The span API dictates that the `data()` is accessible when size is 0,
          // since the pointer may be valid, so we cannot prevent storing and
          // giving out an invalid pointer here without breaking API compatibility
          // and our unit tests. Thus protecting against this can likely only be
          // successful from inside iterators themselves, where the context about
          // the pointer is known.
          //
          // We can not protect here generally against an invalid iterator/count
          // being passed in, since we have no context to determine if the
          // iterator or count are valid.
          _data(MI to_address(first)) {
      MI_CHECK(Extent == dynamic_extent || Extent == count);
    }

    template <class It, class End, class = internal::enable_if_compatible_contiguous_iterator<It, T>,
              class = STD enable_if_t<!STD is_convertible_v<End, size_t>>>
    constexpr span(It begin, End end) noexcept : span(begin, STD distance(begin, end)) {
      // Note: CHECK_LE is not constexpr, hence regular CHECK must be used.
      MI_CHECK(begin <= end);
    }

    template <size_t N, class = internal::enable_if_span_compatible_array<T (&)[N], T, Extent>>
    constexpr /*explicit*/ span(T (&array)[N]) noexcept  // NOLINT(google-explicit-constructor)
        : span(STD data(array), N) {}

    template <class U, size_t N, class = internal::enable_if_span_compatible_array<STD array<U, N>&, T, Extent>>
    constexpr /*explicit*/ span(STD array<U, N>& array) noexcept  // NOLINT(google-explicit-constructor)
        : span(STD data(array), N) {}

    template <class U, size_t N, class = internal::enable_if_span_compatible_array<const STD array<U, N>&, T, Extent>>
    constexpr /*explicit*/ span(const STD array<U, N>& array) noexcept  // NOLINT(google-explicit-constructor)
        : span(STD data(array), N) {}

    // Conversion from a container that has compatible STD data() and integral
    // STD size().
    template <class Container,
              class = internal::enable_if_span_compatible_container_and_span_is_dynamic<Container&, T, Extent>>
    constexpr /*explicit*/ span(Container& container) noexcept  // NOLINT(google-explicit-constructor)
        : span(STD data(container), STD size(container)) {}

    template <class Container,
              class = internal::enable_if_span_compatible_container_and_span_is_dynamic<const Container&, T, Extent>>
    constexpr /*explicit*/ span(const Container& container) noexcept  // NOLINT(google-explicit-constructor)
        : span(STD data(container), STD size(container)) {}

    // Conversions from spans of compatible types and extents: this allows a
    // span<T> to be seamlessly used as a span<const T>, but not the other way
    // around. If extent is not dynamic, OtherExtent has to be equal to Extent.
    template <class U, size_t OtherExtent, class = internal::enable_if_legal_span_conversion<U, OtherExtent, T, Extent>>
    constexpr /*explicit*/ span(const span<U, OtherExtent>& other)  // NOLINT(google-explicit-constructor)
        : span(other.data(), other.size()) {}

    // -----------------------------------------------------------------------------------------------------------------
    // [span.sub], span subviews

    template <size_t Count>
    MI_NODISCARD constexpr span<T, Count> first() const noexcept {
      static_assert(Count <= Extent, "Count must not exceed Extent");
      MI_CHECK(Extent != dynamic_extent || Count <= size());
      return {data(), Count};
    }

    template <size_t Count>
    MI_NODISCARD constexpr span<T, Count> last() const noexcept {
      static_assert(Count <= Extent, "Count must not exceed Extent");
      MI_CHECK(Extent != dynamic_extent || Count <= size());
      return {STD next(data(), MI narrow_cast<MI iter_difference_t<iterator>>(size() - Count)), Count};
    }

    template <size_t Offset, size_t Count = dynamic_extent>
    MI_NODISCARD constexpr span<
        T, (Count != dynamic_extent ? Count : (Extent != dynamic_extent ? Extent - Offset : dynamic_extent))>
    subspan() const noexcept {
      static_assert(Offset <= Extent, "Offset must not exceed Extent");
      static_assert(Count == dynamic_extent || Count <= Extent - Offset, "Count must not exceed Extent - Offset");
      MI_CHECK(Extent != dynamic_extent || Offset <= size());
      MI_CHECK(Extent != dynamic_extent || Count == dynamic_extent || Count <= size() - Offset);
      return {STD next(data(), Offset), Count != dynamic_extent ? Count : size() - Offset};
    }

    MI_NODISCARD constexpr span<T, dynamic_extent> first(const size_t count) const noexcept {
      // Note: CHECK_LE is not constexpr, hence regular CHECK must be used.
      MI_CHECK(count <= size());
      return {data(), count};
    }

    MI_NODISCARD constexpr span<T, dynamic_extent> last(const size_t count) const noexcept {
      // Note: CHECK_LE is not constexpr, hence regular CHECK must be used.
      MI_CHECK(count <= size());
      return {STD next(data(), MI narrow_cast<MI iter_difference_t<iterator>>(size() - count)), count};
    }

    MI_NODISCARD constexpr span<T, dynamic_extent> subspan(const size_t offset,
                                                           const size_t count = dynamic_extent) const noexcept {
      // Note: CHECK_LE is not constexpr, hence regular CHECK must be used.
      MI_CHECK(offset <= size());
      MI_CHECK(count == dynamic_extent || count <= size() - offset);
      return {STD next(data(), MI narrow_cast<MI iter_difference_t<iterator>>(offset)),
              count != dynamic_extent ? count : size() - offset};
    }

    // [span.obs], span observers
    MI_NODISCARD constexpr size_t size() const noexcept { return extent_storage_t::size(); }

    MI_NODISCARD constexpr size_t size_bytes() const noexcept { return size() * sizeof(T); }

    MI_NODISCARD constexpr bool empty() const noexcept { return size() == 0; }

    // [span.elem], span element access
    MI_NODISCARD constexpr T& operator[](const size_t idx) const noexcept {
      // Note: CHECK_LT is not constexpr, hence regular CHECK must be used.
      MI_CHECK(idx < size());
      return *(STD next(data(), MI narrow_cast<MI iter_difference_t<iterator>>(idx)));
    }

    MI_NODISCARD constexpr T& front() const noexcept {
      static_assert(Extent == dynamic_extent || Extent > 0, "Extent must not be 0");
      MI_CHECK(Extent != dynamic_extent || !empty());
      return *data();
    }

    MI_NODISCARD constexpr T& back() const noexcept {
      static_assert(Extent == dynamic_extent || Extent > 0, "Extent must not be 0");
      MI_CHECK(Extent != dynamic_extent || !empty());
      return *(STD next(data(), MI narrow_cast<MI iter_difference_t<iterator>>(size() - 1)));
    }

    MI_NODISCARD constexpr T* data() const noexcept { return _data; }

    // [span.iter], span iterator support
    MI_NODISCARD constexpr iterator begin() const {
      return iterator(_data, STD next(_data, MI narrow_cast<MI iter_difference_t<iterator>>(size())));
    }

    MI_NODISCARD constexpr iterator end() const {
      return iterator(_data, STD next(_data, MI narrow_cast<MI iter_difference_t<iterator>>(size())),
                      STD next(_data, MI narrow_cast<MI iter_difference_t<iterator>>(size())));
    }

    MI_NODISCARD constexpr reverse_iterator rbegin() const noexcept { return reverse_iterator(end()); }

    MI_NODISCARD constexpr reverse_iterator rend() const noexcept { return reverse_iterator(begin()); }

  private:
    T* _data;
};

// span<T, Extent>::extent can not be declared inline prior to C++17, hence this
// definition is required.
template <class T, size_t Extent>
constexpr size_t span<T, Extent>::extent;  // NOLINT(readability-redundant-declaration)

// [span.objectrep], views of object representation
template <class T, size_t X>
span<const STD byte, (X == dynamic_extent ? dynamic_extent : sizeof(T) * X)> as_bytes(span<T, X> s) noexcept {
  return {static_cast<const STD byte*>(static_cast<const void*>(s.data())), s.size_bytes()};
}

template <class T, size_t X, class = STD enable_if_t<!STD is_const_v<T>>>
span<STD byte, (X == dynamic_extent ? dynamic_extent : sizeof(T) * X)> as_writable_bytes(span<T, X> s) noexcept {
  return {static_cast<STD byte*>(static_cast<void*>(s.data())), s.size_bytes()};
}

// Type-deducing helpers for constructing a span.
template <int&... ExplicitArgumentBarrier, class It>
constexpr auto make_span(It it, size_t size) noexcept {
  using type = STD remove_reference_t<MI iter_reference_t<It>>;
  return span<type>(it, size);
}

template <int&... ExplicitArgumentBarrier, class It, class End,
          class = STD enable_if_t<!STD is_convertible_v<End, size_t>>>
constexpr auto make_span(It it, End end) noexcept {
  using type = STD remove_reference_t<MI iter_reference_t<It>>;
  return span<type>(it, end);
}

// make_span utility function that deduces both the span's value_type and extent
// from the passed in argument.
//
// Usage: auto span = MI make_span(...);
template <int&... ExplicitArgumentBarrier, class Container>
constexpr auto make_span(Container&& container) noexcept {
  using type   = STD remove_pointer_t<decltype(STD data(STD declval<Container>()))>;
  using extent = internal::extent<Container>;
  return span<type, extent::value>(STD forward<Container>(container));
}

// make_span utility functions that allow callers to explicit specify the span's
// extent, the value_type is deduced automatically. This is useful when passing
// a dynamically sized container to a method expecting static spans, when the
// container is known to have the correct size.
//
// Note: This will CHECK that N indeed matches size(container).
//
// Usage: auto static_span = MI make_span<N>(...);
template <size_t N, int&... ExplicitArgumentBarrier, class It>
constexpr auto make_span(It it, size_t size) noexcept {
  using type = STD remove_reference_t<MI iter_reference_t<It>>;
  return span<type, N>(it, size);
}

template <size_t N, int&... ExplicitArgumentBarrier, class It, class End,
          class = STD enable_if_t<!STD is_convertible_v<End, size_t>>>
constexpr auto make_span(It it, End end) noexcept {
  using type = STD remove_reference_t<MI iter_reference_t<It>>;
  return span<type, N>(it, end);
}

template <size_t N, int&... ExplicitArgumentBarrier, class Container>
constexpr auto make_span(Container&& container) noexcept {
  using type = STD remove_pointer_t<decltype(STD data(STD declval<Container>()))>;
  return span<type, N>(STD data(container), STD size(container));
}

}  // namespace mi

// MI_EXTENT returns the size of any type that can be converted to a |MI span|
// with definite extent, i.e. everything that is a contiguous storage of some
// sort with static size. Specifically, this works for STD array in a constexpr
// context. Note:
//   * |STD size| should be preferred for plain arrays.
//   * In run-time contexts, functions such as |STD array::size| should be
//     preferred.
#define MI_EXTENT(x) MI internal::must_not_be_dynamic_extent<decltype(MI make_span(x))::extent>()
