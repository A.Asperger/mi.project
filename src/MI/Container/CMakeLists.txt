mi_cc_library(
	SHARED
	NAME
		StaticVector
	SRCS
		MI.StaticVector.hpp
	DEPS
    MI::Common::Check
)

mi_cc_test(
	NAME
		StaticVector_test
	SRCS
		MI.StaticVector_test.cpp
	DEPS
		::StaticVector
		gtest_main
		gmock_main
    benchmark
)


mi_cc_library(
	SHARED
	NAME
		TemplateUtil
	SRCS
		MI.TemplateUtil.hpp
	DEPS
)



mi_cc_library(
	SHARED
	NAME
		StaticQueue
	SRCS
		MI.StaticQueue.hpp
	DEPS
    ::StaticVector
)

mi_cc_test(
	NAME
		StaticQueue_test
	SRCS
		MI.StaticQueue_test.cpp
	DEPS
		::StaticQueue
		gtest_main
		gmock_main
    benchmark
)


mi_cc_library(
	SHARED
	NAME
		Matrix
	SRCS
		MI.Matrix.h
	DEPS
		MI::Base::DoubleEq
		MI::Common::Check
		MI::Common::MinMax
		MI::Common::TemplateUtil
		MI::Container::StaticVector
		MI::Base::Property
)

mi_cc_test(
	NAME Matrix_test
	SRCS
		MI.Matrix_test.cpp
	DEPS
		::Matrix
		gtest_main
		gmock_main
)

mi_cc_library(
	SHARED
	NAME
		Sortable
	SRCS
		MI.Sortable.hpp
		MI.SortableBase.hpp
	DEPS
    MI::Common::Check
		MI::Common::Hash
		MI::Common::If
		MI::Common::InBrackets
		MI::Common::TemplateUtil
		MI::Common::VerifyRange
		MI::Container::StaticVector
		MI::Base::Property
)

mi_cc_test(
	NAME
		Sortable_test
	SRCS
		MI.Sortable_test.cpp
	DEPS
		::Sortable
		gtest_main
		gmock_main
    benchmark
)


mi_cc_library(
	SHARED
	NAME
		Span
	SRCS
		MI.Span.hpp
	DEPS
  	MI::Base::CXX20.ToAddress
		MI::Common::Check
		MI::Common::NarrowingCast
		MI::Container::CheckedIterators
		MI::Container::ContiguousIterator
)

mi_cc_test(
	NAME
		Span_test
	SRCS
		MI.Span_test.cpp
	DEPS
		::Span
		gtest_main
		gmock_main
    benchmark
)

mi_cc_library(
	SHARED
	NAME
		ContiguousIterator
	SRCS
		MI.ContiguousIterator.h
	DEPS
		MI::Container::CheckedIterators
		MI::Container::STDContainers
)

mi_cc_test(
	NAME ContiguousIterator_test
	SRCS
		MI.ContiguousIterator_unittest.cpp
	DEPS
		::ContiguousIterator
		gtest_main
		gmock_main
)


mi_cc_library(
	SHARED
	NAME
		CheckedIterators
	SRCS
		MI.CheckedIterators.h
	DEPS
		MI::Common::Check
		MI::Container::Util
)

mi_cc_test(
	NAME
		CheckedIterators_test
	SRCS
		MI.CheckedIterators_unittest.cpp
	DEPS
		::CheckedIterators
		gtest_main
		gmock_main
)


mi_cc_library(
	SHARED
	NAME
		STDContainers
	SRCS
		MI.STDContainers.h
	DEPS
		MI::Common::Hash
)


mi_cc_library(
	SHARED
	NAME
		Util
	SRCS
		MI.Util.h
)