﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Container/MI.StaticQueue.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace mi::test {

TEST(Q, Emplace) {
  static_queue<int, 4> q;

  q.emplace(1);
  q.emplace(2);
  q.emplace(3);
  q.emplace(4);

  EXPECT_THAT(q._get_container(), testing::ElementsAre(1, 2, 3, 4));

  q.pop();
  q.emplace(5);

  q.pop();
  q.emplace(6);

  q.pop();
  q.emplace(7);

  q.pop();
  q.emplace(8);

  EXPECT_THAT(q._get_container(), testing::ElementsAre(5, 6, 7, 8));

  q.pop();
  q.pop();
  q.pop();
  q.pop();

  q.emplace(9);
  q.emplace(10);
  q.emplace(11);
  q.emplace(12);

  EXPECT_THAT(q._get_container(), testing::ElementsAre(9, 10, 11, 12));

  EXPECT_DEATH(q.emplace(42), "");
}

}  // namespace mi::test
