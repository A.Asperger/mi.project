﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once
#include "MI/Base/MI.Property.hpp"
#include "MI/Container/MI.StaticVector.hpp"

// Тут для примера создан базовый класс `BaseMatrix`, в котором определены базовые функции,
// присущие всем видам матриц (функции инициализации, функция получения данных `get` и т.п.).
//
// С помощью CRTP в производные классы были добавлены функции `det` (определитель матрицы).

namespace mi {
template <class Ty, size_t Rows, size_t Cols>
class matrix;

// Структура, которая определяет, какого типа будет real_scalar через тип scalar.
// Если scalar == double              => double
// Если scalar == float               => float
// Если scalar == long_double         => long_double
// Если scalar == "Любой другой тип"  => double
template <class ScalarType>
struct real_scalar_impl {
    using type = double;
};

template <>
struct real_scalar_impl<float> {
    using type = float;
};

template <>
struct real_scalar_impl<double> {
    using type = double;
};

template <>
struct real_scalar_impl<long double> {
    using type = long double;
};

// Структура, которая выводит типы для матрицы.
template <class Derived>
struct traits;

template <class Ty, size_t Rows, size_t Cols>
struct traits<MI matrix<Ty, Rows, Cols>> {
  public:
    using scalar      = Ty;
    using real_scalar = typename real_scalar_impl<scalar>::type;

  public:
    using size_type = size_t;

  public:
    using rows = STD integral_constant<size_t, Rows>;
    using cols = STD integral_constant<size_t, Cols>;
};
}  // namespace mi

namespace mi::internal {
// is_vector
template <class Derived>
struct is_vector : STD bool_constant<false> {};

template <class Ty, size_t Cols>
struct is_vector<MI matrix<Ty, 1, Cols>> : STD bool_constant<true> {};

template <class Ty, size_t Rows>
struct is_vector<MI matrix<Ty, Rows, 1>> : STD bool_constant<true> {};

// is_vector2
template <class Derived>
struct is_vector2 : STD bool_constant<false> {};

template <class Ty>
struct is_vector2<MI matrix<Ty, 1, 2>> : STD bool_constant<true> {};

template <class Ty>
struct is_vector2<MI matrix<Ty, 2, 1>> : STD bool_constant<true> {};

// is_vector3
template <class Derived>
struct is_vector3 : STD bool_constant<false> {};

template <class Ty>
struct is_vector3<MI matrix<Ty, 1, 3>> : STD bool_constant<true> {};

template <class Ty>
struct is_vector3<MI matrix<Ty, 3, 1>> : STD bool_constant<true> {};

// is_quad_matrix
template <class Derived>
struct is_quad_matrix : STD bool_constant<false> {};

template <class Ty, size_t ColsAndRows>
struct is_quad_matrix<MI matrix<Ty, ColsAndRows, ColsAndRows>> : STD bool_constant<true> {};

//
template <class Derived>
struct is_real_scalar : STD is_floating_point<typename traits<Derived>::scalar> {};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
struct just_scalar_return_type_impl {
    using type = typename traits<Derived>::scalar;
};

template <class Derived>
struct just_real_scalar_return_type_impl {
    using type = typename traits<Derived>::real_scalar;
};

template <class Derived>
struct just_derived_return_type_impl {
    using type = Derived;
};

template <class Derived>
struct get_return_type {
  private:
    using scalar = typename traits<Derived>::scalar;

  public:
    using type = scalar&;
};

template <class Derived>
struct get_const_return_type {
  private:
    using scalar = typename traits<Derived>::scalar;

  public:
    using type = const scalar&;
};

template <class Derived>
struct x_const_return_type : get_const_return_type<Derived> {};

template <class Derived>
struct x_return_type : get_return_type<Derived> {};

template <class Derived>
struct y_const_return_type : get_const_return_type<Derived> {};

template <class Derived>
struct y_return_type : get_return_type<Derived> {};

template <class Derived>
struct z_const_return_type : get_const_return_type<Derived> {};

template <class Derived>
struct z_return_type : get_return_type<Derived> {};

// op

template <class DerivedLhs,                                        //
          class DerivedRhs,                                        //
          class Scalar1    = typename traits<DerivedLhs>::scalar,  //
          class Scalar2    = typename traits<DerivedRhs>::scalar,  //
          class ResultType = STD common_type_t<Scalar1, Scalar2>>
struct matrix_product_return_type {
  private:
    static_assert(traits<DerivedLhs>::cols::value == traits<DerivedRhs>::rows::value);

    using cols = typename traits<DerivedRhs>::cols;
    using rows = typename traits<DerivedLhs>::rows;

  public:
    using type = matrix<ResultType, rows::value, cols::value>;
};

template <class Derived,                                        //
          class OtherScalar,                                    //
          class Scalar1    = typename traits<Derived>::scalar,  //
          class scalar2    = OtherScalar,                       //
          class ResultType = STD common_type_t<Scalar1, scalar2>>
struct matrix_product_scalar_return_type {
  private:
    using cols = typename traits<Derived>::cols;
    using rows = typename traits<Derived>::rows;

  public:
    using type = matrix<ResultType, rows::value, cols::value>;
};

template <class DerivedLhs, class DerivedRhs>
struct matrix_plus_matrix_return_type : just_derived_return_type_impl<DerivedLhs> {};

template <class DerivedLhs, class DerivedRhs>
struct matrix_sub_matrix_return_type : just_derived_return_type_impl<DerivedLhs> {};

template <class Derived, class OtherScalar>
struct matrix_div_scalar_return_type {
  private:
    using rows        = typename traits<Derived>::rows;
    using cols        = typename traits<Derived>::cols;
    using real_scalar = typename traits<Derived>::real_scalar;

  public:
    using type = matrix<real_scalar, rows::value, cols::value>;
};

// assign op

// +=
template <class DerivedLhs, class DerivedRhs>
struct sum_assign_return_type : just_derived_return_type_impl<DerivedLhs> {};

// cross
template <class OtherDerived>
struct cross_product_return_type {
  private:
    using scalar = typename traits<OtherDerived>::scalar;
    using rows   = typename traits<OtherDerived>::rows;
    using cols   = typename traits<OtherDerived>::cols;

  public:
    using type = matrix<scalar, rows::value, cols::value>;
};

// dot
template <class Derived, class OtherDerived>
struct dot_product_return_type {
  private:
    using Scalar1 = typename traits<Derived>::scalar;
    using scalar2 = typename traits<OtherDerived>::scalar;

    using result_scalar = STD common_type_t<Scalar1, scalar2>;

  public:
    using type = result_scalar;
};

// transpose
template <class Derived>
struct transpose_return_type {
  private:
    using scalar = typename traits<Derived>::scalar;
    using rows   = typename traits<Derived>::cols;  // rows == cols
    using cols   = typename traits<Derived>::rows;  // cols == rows

  public:
    using type = matrix<scalar, rows::value, cols::value>;
};

// trace
template <class Derived>
struct trace_return_type : just_scalar_return_type_impl<Derived> {};

// result
template <class Derived>
struct determinant_return_type : just_scalar_return_type_impl<Derived> {};

// inverse
template <class Derived>
struct inverse_return_type : just_derived_return_type_impl<Derived> {};

// squared_norm
template <class Derived>
struct squared_norm_return_type : just_real_scalar_return_type_impl<Derived> {};

// norm
template <class Derived>
struct norm_return_type {
  private:
    using real_scalar = typename traits<Derived>::real_scalar;

  public:
    using type = real_scalar;
};

// frobenius_norm
template <class Derived>
struct squared_euclidean_norm_return_type : just_real_scalar_return_type_impl<Derived> {};

// norm
template <class Derived>
struct euclidean_norm_return_type {
  private:
    using real_scalar = typename traits<Derived>::real_scalar;

  public:
    using type = real_scalar;
};

// normalized
template <class Derived>
struct normalized_return_type : just_derived_return_type_impl<Derived> {};

template <class Derived>
struct is_unit_matrix_return_type {
    using type = bool;
};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
class determinant_impl;

template <class Ty>
class determinant_impl<MI matrix<Ty, 3, 3>> {
  private:
    using derived_type = matrix<Ty, 3, 3>;
    using base         = typename derived_type::base;
    using return_type  = typename determinant_return_type<derived_type>::type;

  public:
    MI_NODISCARD static return_type result(const base& base) {
      return_type det;

      det = base.get(0, 0) * base.get(1, 1) * base.get(2, 2) +  //
            base.get(0, 1) * base.get(1, 2) * base.get(2, 0) +  //
            base.get(0, 2) * base.get(1, 0) * base.get(2, 1) -  //
            base.get(0, 2) * base.get(1, 1) * base.get(2, 0) -  //
            base.get(0, 0) * base.get(1, 2) * base.get(2, 1) -  //
            base.get(0, 1) * base.get(1, 0) * base.get(2, 2);

      return det;
    }
};

template <class Ty>
class determinant_impl<MI matrix<Ty, 2, 2>> {
  private:
    using derived_type = matrix<Ty, 2, 2>;
    using base         = typename derived_type::base;
    using return_type  = typename determinant_return_type<derived_type>::type;

  public:
    MI_NODISCARD static return_type result(const base& base) {
      return_type det;

      det = base.get(0, 0) * base.get(1, 1) -  //
            base.get(0, 1) * base.get(1, 0);

      return det;
    }
};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
class inverse_impl;

template <class Ty>
class inverse_impl<MI matrix<Ty, 3, 3>> {
  private:
    using derived_type = matrix<Ty, 3, 3>;
    using base         = typename derived_type::base;
    using return_type  = typename inverse_return_type<derived_type>::type;

  public:
    MI_NODISCARD static return_type result(const base& base) {
      const auto det = base.determinant();

      MI_CHECK(det != 0.);

      const auto a00 = base.get(1, 1) * base.get(2, 2) - base.get(2, 1) * base.get(1, 2);
      const auto a01 = -(base.get(0, 1) * base.get(2, 2) - base.get(2, 1) * base.get(0, 2));
      const auto a02 = base.get(0, 1) * base.get(1, 2) - base.get(1, 1) * base.get(0, 2);

      const auto a10 = -(base.get(1, 0) * base.get(2, 2) - base.get(2, 0) * base.get(1, 2));
      const auto a11 = base.get(0, 0) * base.get(2, 2) - base.get(2, 0) * base.get(0, 2);
      const auto a12 = -(base.get(0, 0) * base.get(1, 2) - base.get(1, 0) * base.get(0, 2));

      const auto a20 = base.get(1, 0) * base.get(2, 1) - base.get(2, 0) * base.get(1, 1);
      const auto a21 = -(base.get(0, 0) * base.get(2, 1) - base.get(2, 0) * base.get(0, 1));
      const auto a22 = base.get(0, 0) * base.get(1, 1) - base.get(1, 0) * base.get(0, 1);

      return_type res = {a00 / det, a01 / det, a02 / det, a10 / det, a11 / det,
                         a12 / det, a20 / det, a21 / det, a22 / det};

      return res;
    }
};

template <class Ty>
class inverse_impl<MI matrix<Ty, 2, 2>> {
  private:
    using derived_type = matrix<Ty, 2, 2>;
    using base         = typename derived_type::base;
    using return_type  = typename inverse_return_type<derived_type>::type;

  public:
    MI_NODISCARD static return_type result(const base& base) {
      const auto det = base.determinant();

      const auto a00 = base.get(1, 1);
      const auto a01 = -(base.get(0, 1));

      const auto a10 = -(base.get(1, 0));
      const auto a11 = base.get(0, 0);

      return_type res = {a00 / det, a01 / det, a10 / det, a11 / det};

      return res;
    }
};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
class is_unit_matrix_impl {
  private:
    using base = typename Derived::base;
    using rows = typename traits<Derived>::rows;
    using cols = typename traits<Derived>::cols;
    using real = typename traits<Derived>::real_scalar;

  public:
    MI_NODISCARD static typename is_unit_matrix_return_type<Derived>::type result(const base& base) {
      for (size_t row = 0; row < rows::value; ++row) {
        for (size_t col = 0; col < cols::value; ++col) {
          if (row == col) {
            if (base.get(row, col) != real{1}) {
              return false;
            }
          } else {
            if (base.get(row, col) != real{0}) {
              return false;
            }
          }
        }
      }

      return true;
    }
};

}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
class squared_norm_impl {
  private:
    using derived_type = Derived;
    using base         = typename derived_type::base;
    using return_type  = typename squared_norm_return_type<derived_type>::type;

  public:
    MI_NODISCARD static return_type result(const base& base) {
      return_type max_sum_of_row = (STD numeric_limits<return_type>::min)();

      for (size_t n_row = 0; n_row < base.n_rows(); ++n_row) {
        return_type current_row_sum = {0};

        for (size_t n_col = 0; n_col < base.n_cols(); ++n_col) {
          current_row_sum += STD pow(base.get(n_row, n_col), 2);
        }

        max_sum_of_row = STD max(max_sum_of_row, current_row_sum);
      }

      return max_sum_of_row;
    }
};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
class squared_euclidean_norm_impl {
  private:
    using derived_type = Derived;
    using base         = typename derived_type::base;
    using return_type  = typename squared_norm_return_type<derived_type>::type;

  public:
    MI_NODISCARD static return_type result(const base& base) {
      return_type result = {0};

      for (size_t n = 0; n < base.n_rows() * base.n_cols(); ++n) {
        result += STD pow(base.get(n), 2);
      }

      return result;
    }
};
}  // namespace mi::internal

// M * scalar
namespace mi::internal {
template <class Derived, class OtherScalar>
class matrix_product_scalar_impl {
  private:
    using base_lhs = typename Derived::base;

  private:
    using return_type = typename matrix_product_scalar_return_type<Derived, OtherScalar>::type;

  public:
    MI_NODISCARD static return_type result(const base_lhs& base, const OtherScalar scalar) {
      return_type res;

      for (size_t n = 0; n < base.n_rows() * base.n_cols(); ++n) {
        res.get(n) = base.get(n) * scalar;
      }

      return res;
    }
};
}  // namespace mi::internal

// M + M
namespace mi::internal {
template <class DerivedLhs, class DerivedRhs>
class matrix_plus_matrix_impl {
  private:
    using base_lhs = typename DerivedLhs::base;
    using base_rhs = typename DerivedRhs::base;

  private:
    using return_type = typename matrix_plus_matrix_return_type<DerivedLhs, DerivedRhs>::type;

  public:
    MI_NODISCARD static return_type result(const base_lhs& base_lhs, const base_rhs& base_rhs) {
      return_type res;

      for (size_t n = 0; n < base_lhs.n_rows() * base_rhs.n_cols(); ++n) {
        res.get(n) = base_lhs.get(n) + base_rhs.get(n);
      }

      return res;
    }
};
}  // namespace mi::internal

// M - M
namespace mi::internal {
template <class DerivedLhs, class DerivedRhs>
class matrix_sub_matrix_impl {
  private:
    using base_lhs = typename DerivedLhs::base;
    using base_rhs = typename DerivedRhs::base;

  private:
    using return_type = typename matrix_sub_matrix_return_type<DerivedLhs, DerivedRhs>::type;

  public:
    MI_NODISCARD static return_type result(const base_lhs& base_lhs, const base_rhs& base_rhs) {
      return_type res;

      for (size_t n = 0; n < base_lhs.n_rows() * base_rhs.n_cols(); ++n) {
        res.get(n) = base_lhs.get(n) - base_rhs.get(n);
      }

      return res;
    }
};
}  // namespace mi::internal

// M += M
namespace mi::internal {
template <class DerivedLhs, class DerivedRhs>
class sum_assign_op_impl {
  private:
    using base_lhs = typename DerivedLhs::base;
    using base_rhs = typename DerivedRhs::base;

  public:
    static void result(base_lhs& base_lhs, const base_rhs& base_rhs) {
      for (size_t n = 0; n < base_lhs.n_rows() * base_lhs.n_cols(); ++n) {
        base_lhs.get(n) += base_rhs.get(n);
      }
    }
};
}  // namespace mi::internal

// M /= scalar
namespace mi::internal {
template <class DerivedLhs, class Scalar>
class div_assign_scalar_op_impl {
  private:
    using base_lhs = typename DerivedLhs::base;

  public:
    static void result(base_lhs& base, const Scalar scalar) {
      for (size_t n = 0; n < base.n_rows() * base.n_cols(); ++n) {
        base.get(n) /= scalar;
      }
    }
};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived, class OtherScalar>
class matrix_div_scalar_impl {
  private:
    using base              = typename Derived::base;
    using real_scalar       = typename traits<Derived>::real_scalar;
    using other_scalar_type = OtherScalar;

  private:
    using return_type = typename matrix_div_scalar_return_type<Derived, OtherScalar>::type;

  public:
    MI_NODISCARD static return_type result(const base& base, other_scalar_type scalar) {
      return_type res;

      for (size_t n = 0; n < base.n_rows() * base.n_cols(); ++n) {
        res.get(n) = static_cast<real_scalar>(base.get(n)) / scalar;
      }

      return res;
    }
};
}  // namespace mi::internal

namespace mi::internal {
template <class Derived>
class matrix_base {
  public:
    using scalar      = typename traits<Derived>::scalar;
    using real_scalar = typename traits<Derived>::real_scalar;
    using size_type   = typename traits<Derived>::size_type;
    using rows        = typename traits<Derived>::rows;
    using cols        = typename traits<Derived>::cols;

    using data_type = MI static_vector<scalar, rows::value * cols::value>;

    using iterator               = typename data_type::iterator;
    using const_iterator         = typename data_type::const_iterator;
    using reverse_iterator       = typename data_type::reverse_iterator;
    using const_reverse_iterator = typename data_type::const_reverse_iterator;

  public:
    ~matrix_base() = default;

  public:
    MI_CONSTEXPR_17 matrix_base() { fill({0}); }

    MI_CONSTEXPR_17 matrix_base(const matrix_base&)     = default;
    MI_CONSTEXPR_17 matrix_base(matrix_base&&) noexcept = default;

  public:
    MI_CONSTEXPR_17 matrix_base& operator=(const matrix_base&)     = default;
    MI_CONSTEXPR_17 matrix_base& operator=(matrix_base&&) noexcept = default;

  public:
    MI_CONSTEXPR_17 matrix_base(STD initializer_list<scalar> init_list) {
      MI_DCHECK(init_list.size() == n_rows() * n_cols());

      for (const auto& value : init_list) {
        _m_storage.emplace_back(value);
      }
    }

    MI_CONSTEXPR_17 matrix_base(STD initializer_list<STD initializer_list<scalar>> init_lists) {
      MI_DCHECK(init_lists.size() == n_rows());

      for (const auto init_list : init_lists) {
        MI_DCHECK(init_list.size() == n_cols());

        for (const auto value : init_list) {
          _m_storage.emplace_back(value);
        }
      }
    }

    template <class... OtherScalars, MI if_t<MI internal::IsAllOfV<STD is_convertible<OtherScalars, scalar>...>> = 0>
    MI_CONSTEXPR_17 explicit matrix_base(OtherScalars&&... scalars)
        : _m_storage({STD forward<OtherScalars>(scalars)...}) {}

  public:
    MI_NODISCARD const Derived& derived() const { return *static_cast<const Derived*>(this); }

    MI_NODISCARD Derived& derived() { return *static_cast<Derived*>(this); }

  public:
    MI_NODISCARD typename get_const_return_type<Derived>::type get(const size_type row, const size_type col) const {
      MI_DCHECK(row < n_rows());
      MI_DCHECK(col < n_cols());
      MI_DCHECK(row * n_cols() + col < n_rows() * n_cols());

      return _m_storage[row * n_cols() + col];
    }

    MI_NODISCARD typename get_return_type<Derived>::type get(const size_type row, const size_type col) {
      MI_DCHECK(row < n_rows());
      MI_DCHECK(col < n_cols());
      MI_DCHECK(row * n_cols() + col < n_rows() * n_cols());

      return _m_storage[row * n_cols() + col];
    }

    MI_NODISCARD typename get_const_return_type<Derived>::type get(const size_type n) const {
      MI_DCHECK(n < n_rows() * n_cols());

      return _m_storage[n];
    }

    MI_NODISCARD typename get_return_type<Derived>::type get(const size_type n) {
      MI_DCHECK(n < n_rows() * n_cols());

      return _m_storage[n];
    }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_iterator begin() const { return _m_storage.begin(); }

    MI_NODISCARD MI_CONSTEXPR_17 iterator begin() { return _m_storage.begin(); }

    MI_NODISCARD MI_CONSTEXPR_17 const_iterator end() const { return _m_storage.end(); }

    MI_NODISCARD MI_CONSTEXPR_17 iterator end() { return _m_storage.end(); }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 size_t n_rows() const { return rows::value; }

    MI_NODISCARD MI_CONSTEXPR_17 size_t n_cols() const { return cols::value; }

  public:
    MI_NODISCARD bool operator==(const matrix_base& rhs) const { return _m_storage == rhs._m_storage; }

  public:
    void fill(const scalar& value) {
      for (size_t n = 0; n < n_rows() * n_cols(); ++n) {
        _m_storage.emplace_back(value);
      }
    }

  public:
    template <class OtherDerived>
    MI_NODISCARD typename matrix_plus_matrix_return_type<Derived, OtherDerived>::type operator+(
        const matrix_base<OtherDerived>& other) {
      return matrix_plus_matrix_impl<Derived, OtherDerived>::result(derived(), other.derived());
    }

    template <class OtherDerived>
    MI_NODISCARD typename matrix_plus_matrix_return_type<Derived, OtherDerived>::type operator+(
        const matrix_base<OtherDerived>& other) const {
      return matrix_plus_matrix_impl<Derived, OtherDerived>::result(derived(), other.derived());
    }

  public:
    template <class OtherDerived>
    MI_NODISCARD typename matrix_sub_matrix_return_type<Derived, OtherDerived>::type operator-(
        const matrix_base<OtherDerived>& other) {
      return matrix_sub_matrix_impl<Derived, OtherDerived>::result(derived(), other.derived());
    }

    template <class OtherDerived>
    MI_NODISCARD typename matrix_sub_matrix_return_type<Derived, OtherDerived>::type operator-(
        const matrix_base<OtherDerived>& other) const {
      return matrix_sub_matrix_impl<Derived, OtherDerived>::result(derived(), other.derived());
    }

  public:
    template <class OtherScalar>
    MI_NODISCARD typename matrix_div_scalar_return_type<Derived, OtherScalar>::type operator/(
        const OtherScalar& scalar) {
      return matrix_div_scalar_impl<Derived, OtherScalar>::result(derived(), scalar);
    }

    template <class OtherScalar>
    MI_NODISCARD typename matrix_div_scalar_return_type<Derived, OtherScalar>::type operator/(
        const OtherScalar& scalar) const {
      return matrix_div_scalar_impl<Derived, OtherScalar>::result(derived(), scalar);
    }

  public:
    template <class OtherDerived>
    MI_NODISCARD typename matrix_product_return_type<Derived, OtherDerived>::type operator*(
        const matrix_base<OtherDerived>& other) const {
      typename matrix_product_return_type<Derived, OtherDerived>::type res;

      for (size_t i = 0; i < traits<Derived>::rows::value; ++i) {
        for (size_t j = 0; j < traits<OtherDerived>::cols::value; ++j) {
          for (size_t k = 0; k < traits<Derived>::cols::value; ++k) {
            res.get(i, j) += get(i, k) * other.get(k, j);
          }
        }
      }

      return res;
    }

  public:
    template <class OtherScalar>
    MI_NODISCARD typename matrix_product_scalar_return_type<Derived, OtherScalar>::type operator*(
        const OtherScalar& scalar) const {
      return matrix_product_scalar_impl<Derived, OtherScalar>::result(derived(), scalar);
    }

    // op=

  public:
    template <class OtherDerived>
    Derived& operator+=(const matrix_base<OtherDerived>& other) {
      sum_assign_op_impl<Derived, OtherDerived>::result(derived(), other.derived());

      return derived();
    }

    template <class OtherScalar>
    Derived& operator/=(const OtherScalar scalar) {
      div_assign_scalar_op_impl<Derived, OtherScalar>::result(derived(), scalar);

      return derived();
    }

  public:
    template <class OtherDerived>
    MI_NODISCARD typename cross_product_return_type<OtherDerived>::type cross(
        const matrix_base<OtherDerived>& other) const {
      static_assert(internal::is_vector3<Derived>::value);
      static_assert(internal::is_vector3<OtherDerived>::value);

      const auto& a = *this;
      const auto& b = other;

      typename cross_product_return_type<OtherDerived>::type res;

      res.get(0) = a.y() * b.z() - a.z() * b.y();
      res.get(1) = a.z() * b.x() - a.x() * b.z();
      res.get(2) = a.x() * b.y() - a.y() * b.x();

      return res;
    }

    template <class OtherDerived>
    MI_NODISCARD typename dot_product_return_type<Derived, OtherDerived>::type dot(
        const matrix_base<OtherDerived>& other) const {
      MI_DCHECK(is_vector<Derived>::value);
      MI_DCHECK(is_vector<OtherDerived>::value);

      typename dot_product_return_type<Derived, OtherDerived>::type res = {0};

      for (size_t i = 0; i < n_rows(); ++i) {
        res += get(i) * other.get(i);
      }

      return res;
    }

    MI_NODISCARD typename transpose_return_type<Derived>::type transpose() const {
      typename transpose_return_type<Derived>::type res;

      for (size_t n_row = 0; n_row < n_rows(); ++n_row) {
        for (size_t n_col = 0; n_col < n_cols(); ++n_col) {
          res.get(n_col, n_row) = get(n_row, n_col);
        }
      }

      return res;
    }

    MI_NODISCARD typename trace_return_type<Derived>::type trace() const {
      MI_CHECK(is_quad_matrix<Derived>::value);

      typename trace_return_type<Derived>::type res = {0};

      for (size_t i = 0; i < n_rows(); ++i) {
        res += get(i, i);
      }

      return res;
    }

    MI_NODISCARD typename determinant_return_type<Derived>::type determinant() const {
      MI_DCHECK(is_quad_matrix<Derived>::value);

      return determinant_impl<Derived>::result(derived());
    }

    // Обратная матрица.
    // Для единичной матрицы сложность почти нулевая.
    MI_NODISCARD typename inverse_return_type<Derived>::type inverse() const {
      MI_DCHECK(is_real_scalar<Derived>::value);

      // E ^ (-1) = E
      if (is_unit_matrix()) {
        return derived();
      }

      return inverse_impl<Derived>::result(derived());
    }

  public:
    MI_NODISCARD typename is_unit_matrix_return_type<Derived>::type is_unit_matrix() const {
      return is_unit_matrix_impl<Derived>::result(derived());
    }

  public:
    // Максимальная сумма квадратов элементов В СТРОКЕ.
    MI_NODISCARD typename squared_norm_return_type<Derived>::type squared_norm() const {
      return squared_norm_impl<Derived>::result(derived());
    }

    // Корень максимальной суммы квадратов элементов В СТРОКЕ.
    MI_NODISCARD typename norm_return_type<Derived>::type norm() const { return STD sqrt(squared_norm()); }

  public:
    // Сумма квадратов всех элементов.
    MI_NODISCARD typename squared_euclidean_norm_return_type<Derived>::type squared_euclidean_norm() const {
      return squared_euclidean_norm_impl<Derived>::result(derived());
    }

    // Корень суммы квадратов всех элементов.
    MI_NODISCARD typename euclidean_norm_return_type<Derived>::type euclidean_norm() const {
      return STD sqrt(squared_euclidean_norm());
    }

  public:
    // Длина вектора.
    MI_NODISCARD typename euclidean_norm_return_type<Derived>::type len() const {
      static_assert(is_vector2<Derived>::value || is_vector3<Derived>::value,  //
                    "Function len() is only defined for vector2 and vector3.");

      return euclidean_norm();
    }

  public:
    MI_NODISCARD typename normalized_return_type<Derived>::type normalized() const {
      typename normalized_return_type<Derived>::type res(derived());

      const auto n = squared_euclidean_norm();

      if (n > static_cast<real_scalar>(0)) {
        res /= STD sqrt(n);
      }

      return res;
    }

    MI_NODISCARD bool is_normalized() const {
      // return MI internal::double_eq(euclidean_norm(), 1.);
      return euclidean_norm() == 1.;
    }

  public:
    MI_NODISCARD typename x_const_return_type<Derived>::type x() const {
      MI_DCHECK(is_vector3<Derived>::value);

      return get(0);
    }

    MI_NODISCARD typename x_return_type<Derived>::type x() {
      MI_DCHECK(is_vector3<Derived>::value);

      return get(0);
    }

    MI_NODISCARD typename y_const_return_type<Derived>::type y() const {
      MI_DCHECK(is_vector3<Derived>::value);

      return get(1);
    }

    MI_NODISCARD typename y_return_type<Derived>::type y() {
      MI_DCHECK(is_vector3<Derived>::value);

      return get(1);
    }

    MI_NODISCARD typename z_const_return_type<Derived>::type z() const {
      MI_DCHECK(is_vector3<Derived>::value);

      return get(2);
    }

    MI_NODISCARD typename z_return_type<Derived>::type z() {
      MI_DCHECK(is_vector3<Derived>::value);

      return get(2);
    }

  private:
    data_type _m_storage;
};
}  // namespace mi::internal

namespace mi {
template <class Ty, size_t Rows, size_t Cols>
class matrix : public internal::matrix_base<matrix<Ty, Rows, Cols>> {
  public:
    using base = MI internal::matrix_base<matrix>;

  public:
    using value_type = Ty;

  public:
    ~matrix() = default;

  public:
    MI_CONSTEXPR_17 matrix() = default;

    MI_CONSTEXPR_17 matrix(const matrix&)     = default;
    MI_CONSTEXPR_17 matrix(matrix&&) noexcept = default;

  public:
    MI_CONSTEXPR_17 matrix& operator=(const matrix&)     = default;
    MI_CONSTEXPR_17 matrix& operator=(matrix&&) noexcept = default;

  public:
    MI_CONSTEXPR_17 matrix(STD initializer_list<value_type> init_list) : base(init_list) {}

    MI_CONSTEXPR_17 matrix(STD initializer_list<STD initializer_list<value_type>> init_list) : base(init_list) {}

    template <class... OtherScalars>
    MI_CONSTEXPR_17 explicit matrix(OtherScalars&&... scalars) : base(STD forward<OtherScalars>(scalars)...) {}
};

template <class Ty>
class matrix<Ty, 3, 3> : public internal::matrix_base<matrix<Ty, 3, 3>> {
  public:
    using base = internal::matrix_base<matrix>;

  public:
    using value_type = Ty;

  public:
    ~matrix() = default;

  public:
    MI_CONSTEXPR_17 matrix() = default;

    MI_CONSTEXPR_17 matrix(const matrix&)     = default;
    MI_CONSTEXPR_17 matrix(matrix&&) noexcept = default;

  public:
    MI_CONSTEXPR_17 matrix& operator=(const matrix&)     = default;
    MI_CONSTEXPR_17 matrix& operator=(matrix&&) noexcept = default;

  public:
    MI_CONSTEXPR_17 matrix(STD initializer_list<value_type> init_list) : base(init_list) {}

    MI_CONSTEXPR_17 matrix(STD initializer_list<STD initializer_list<value_type>> init_list) : base(init_list) {}

    template <class... OtherScalars>
    MI_CONSTEXPR_17 explicit matrix(OtherScalars&&... scalars) : base(STD forward<OtherScalars>(scalars)...) {}

  public:
    template <class OtherDerived>
    MI_NODISCARD typename internal::cross_product_return_type<OtherDerived>::type cross(
        const internal::matrix_base<OtherDerived>& other) const {
      static_assert(internal::is_vector3<matrix>::value);
      static_assert(internal::is_vector3<OtherDerived>::value);

      const auto& a = *this;
      const auto& b = other;

      typename internal::cross_product_return_type<OtherDerived>::type res;

      res.get(0) = a.y() * b.z() - a.z() * b.y();
      res.get(1) = a.z() * b.x() - a.x() * b.z();
      res.get(2) = a.x() * b.y() - a.y() * b.x();

      return res;
    }
};

using matrix3d = matrix<double, 3, 3>;
using matrix2d = matrix<double, 2, 2>;
using vector3d = matrix<double, 3, 1>;
}  // namespace mi
