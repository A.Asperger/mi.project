// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <array>
#include <deque>
#include <forward_list>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <iterator>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <valarray>
#include <vector>

#include "MI/Container/MI.ContiguousIterator.h"
#include "MI/Container/MI.Span.hpp"

namespace mi::test {

TEST(ContiguousIteratorTest, ForwardIterator) {
  using ForwardIterator = STD forward_list<int>::iterator;
  static_assert(
      STD is_same<STD forward_iterator_tag, typename STD iterator_traits<ForwardIterator>::iterator_category>::value,
      "Error: The iterator_category of ForwardIterator is not "
      "STD forward_iterator_tag.");
  static_assert(!is_contiguous_iterator<ForwardIterator>::value,
                "Error: ForwardIterator should not be considered a contiguous iterator.");
  static_assert(!is_contiguous_iterator<const ForwardIterator>::value,
                "Error: const ForwardIterator should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<ForwardIterator&>::value,
                "Error: ForwardIterator& should not be considered a contiguous "
                "iterator.");
  static_assert(!is_contiguous_iterator<const ForwardIterator&>::value,
                "Error: const ForwardIterator& should not be considered a "
                "contiguous iterator.");
}

TEST(ContiguousIteratorTest, BidirectionalIterator) {
  using BidirectionalIterator = STD set<int>::iterator;
  static_assert(STD is_same<STD bidirectional_iterator_tag,
                            typename STD iterator_traits<BidirectionalIterator>::iterator_category>::value,
                "Error: The iterator_category of BidirectionalIterator is not "
                "STD bidirectional_iterator_tag.");
  static_assert(!is_contiguous_iterator<BidirectionalIterator>::value,
                "Error: BidirectionalIterator should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<const BidirectionalIterator>::value,
                "Error: const BidirectionalIterator should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<BidirectionalIterator&>::value,
                "Error: BidirectionalIterator& should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<const BidirectionalIterator&>::value,
                "Error: const BidirectionalIterator& should not be considered "
                "a contiguous iterator.");
  static_assert(!is_contiguous_iterator<STD reverse_iterator<BidirectionalIterator>>::value,
                "Error: A reverse BidirectionalIterator should not be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIteratorTest, RandomAccessIterator) {
  using RandomAccessIterator = STD deque<int>::iterator;
  static_assert(STD is_same<STD random_access_iterator_tag,
                            typename STD iterator_traits<RandomAccessIterator>::iterator_category>::value,
                "Error: The iterator_category of RandomAccessIterator is not "
                "STD random_access_iterator_tag.");
  static_assert(!is_contiguous_iterator<RandomAccessIterator>::value,
                "Error: RandomAccessIterator should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<const RandomAccessIterator>::value,
                "Error: const RandomAccessIterator should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<RandomAccessIterator&>::value,
                "Error: RandomAccessIterator& should not be considered a "
                "contiguous iterator.");
  static_assert(!is_contiguous_iterator<const RandomAccessIterator&>::value,
                "Error: const RandomAccessIterator& should not be considered "
                "a contiguous iterator.");
  static_assert(!is_contiguous_iterator<STD reverse_iterator<RandomAccessIterator>>::value,
                "Error: A reverse RandomAccessIterator should not be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIterator, Pointer) {
  static_assert(is_contiguous_iterator<int*>::value, "Error: int* should be considered a contiguous iterator.");

  static_assert(is_contiguous_iterator<int* const>::value,
                "Error: int* const should be considered a contiguous iterator.");

  static_assert(!is_contiguous_iterator<void (*)()>::value,
                "Error: A function pointer should not be considered a "
                "contiguous iterator.");
}

TEST(ContiguousIterator, VectorInt) {
  static_assert(is_contiguous_iterator<MI vector<int>::iterator>::value,
                "Error: MI vector<int>::iterator should be considered a "
                "contiguous iterator.");

  static_assert(is_contiguous_iterator<MI vector<int>::const_iterator>::value,
                "Error: MI vector<int>::const_iterator should be considered "
                "a contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<int>::reverse_iterator>::value,
                "Error: MI vector<int>::reverse_iterator should not be considered a "
                "contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<int>::const_reverse_iterator>::value,
                "Error: MI vector<int>::const_reverse_iterator should not be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIterator, VectorString) {
  static_assert(is_contiguous_iterator<MI vector<STD string>::iterator>::value,
                "Error: MI vector<STD string>::iterator should be "
                "considered a contiguous iterator.");

  static_assert(is_contiguous_iterator<MI vector<STD string>::const_iterator>::value,
                "Error: MI vector<STD string>::const_iterator should be considered a "
                "contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<STD string>::reverse_iterator>::value,
                "Error: MI vector<STD string>::reverse_iterator should not be "
                "considered a contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<STD string>::const_reverse_iterator>::value,
                "Error: MI vector<STD string>::const_reverse_iterator "
                "should not be considered a contiguous iterator.");
}

TEST(ContiguousIterator, VectorBool) {
  static_assert(!is_contiguous_iterator<MI vector<bool>::iterator>::value,
                "Error: MI vector<bool>::iterator should not be considered "
                "a contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<bool>::const_iterator>::value,
                "Error: MI vector<bool>::const_iterator should not be "
                "considered a contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<bool>::reverse_iterator>::value,
                "Error: MI vector<bool>::reverse_iterator should not be considered a "
                "contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI vector<bool>::const_reverse_iterator>::value,
                "Error: MI vector<bool>::const_reverse_iterator should not be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIterator, ArrayInt) {
  static_assert(is_contiguous_iterator<STD array<int, 1>::iterator>::value,
                "Error: STD array<int, 1>::iterator should be considered a "
                "contiguous iterator.");

  static_assert(is_contiguous_iterator<STD array<int, 1>::const_iterator>::value,
                "Error: STD array<int, 1>::const_iterator should be "
                "considered a contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD array<int, 1>::reverse_iterator>::value,
                "Error: STD array<int, 1>::reverse_iterator should not be considered "
                "a contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD array<int, 1>::const_reverse_iterator>::value,
                "Error: STD array<int, 1>::const_reverse_iterator should not be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIterator, ArrayString) {
  static_assert(is_contiguous_iterator<STD array<STD string, 1>::iterator>::value,
                "Error: STD array<STD string, 1>::iterator should be considered a "
                "contiguous iterator.");

  static_assert(is_contiguous_iterator<STD array<STD string, 1>::const_iterator>::value,
                "Error: STD array<STD string, 1>::const_iterator should be considered "
                "a contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD array<STD string, 1>::reverse_iterator>::value,
                "Error: STD array<STD string, 1>::reverse_iterator should "
                "not be considered a contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD array<STD string, 1>::const_reverse_iterator>::value,
                "Error: STD array<STD string, 1>::const_reverse_iterator "
                "should not be considered a contiguous iterator.");
}

TEST(ContiguousIterator, String) {
  static_assert(is_contiguous_iterator<STD string::iterator>::value,
                "Error: STD string:iterator should be considered a contiguous"
                "iterator.");

  static_assert(is_contiguous_iterator<STD string::const_iterator>::value,
                "Error: STD string::const_iterator should be considered a "
                "contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD string::reverse_iterator>::value,
                "Error: STD string::reverse_iterator should not be considered "
                "a contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD string::const_reverse_iterator>::value,
                "Error: STD string::const_reverse_iterator should not be considered "
                "a contiguous iterator.");
}

TEST(ContiguousIterator, String16) {
  static_assert(is_contiguous_iterator<STD u16string::iterator>::value,
                "Error: STD u16string:iterator should be considered a "
                "contiguous iterator.");

  static_assert(is_contiguous_iterator<STD u16string::const_iterator>::value,
                "Error: STD u16string::const_iterator should be considered a "
                "contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD u16string::reverse_iterator>::value,
                "Error: STD u16string::reverse_iterator should not be "
                "considered a contiguous iterator.");

  static_assert(!is_contiguous_iterator<STD u16string::const_reverse_iterator>::value,
                "Error: STD u16string::const_reverse_iterator should not be considered a"
                "contiguous iterator.");
}

TEST(ContiguousIterator, ValarrayInt) {
  static_assert(is_contiguous_iterator<decltype(STD begin(STD declval<STD valarray<int>&>()))>::value,
                "Error: STD valarray<int>::iterator should be considered a "
                "contiguous iterator.");

  static_assert(is_contiguous_iterator<decltype(STD begin(STD declval<const STD valarray<int>&>()))>::value,
                "Error: STD valarray<int>::const_iterator should be considered a "
                "contiguous iterator.");
}

TEST(ContiguousIterator, ValarrayString) {
  static_assert(is_contiguous_iterator<decltype(STD begin(STD declval<STD valarray<STD string>&>()))>::value,
                "Error: STD valarray<STD string>::iterator should be "
                "considered a contiguous iterator.");

  static_assert(is_contiguous_iterator<decltype(STD begin(STD declval<const STD valarray<STD string>&>()))>::value,
                "Error: STD valarray<STD string>::const_iterator should be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIterator, StringPiece) {
  // static_assert(is_contiguous_iterator<STD string_view::const_iterator>::value,
  //               "Error: MI StringPiece::const_iterator should be considered a "
  //               "contiguous iterator.");
  //
  // static_assert(!is_contiguous_iterator<STD string_view::const_reverse_iterator>::value,
  //               "Error: MI StringPiece::const_reverse_iterator should not be "
  //               "considered a contiguous iterator.");
}

TEST(ContiguousIterator, SpanInt) {
  static_assert(is_contiguous_iterator<MI span<int>::iterator>::value,
                "Error: MI span<int>::iterator should be considered a "
                "contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI span<int>::reverse_iterator>::value,
                "Error: MI span<int>::reverse_iterator should not be "
                "considered a contiguous iterator.");
}

TEST(ContiguousIterator, SpanString) {
  static_assert(is_contiguous_iterator<MI span<STD string>::iterator>::value,
                "Error: MI span<STD string>::iterator should be considered "
                "a contiguous iterator.");

  static_assert(!is_contiguous_iterator<MI span<STD string>::reverse_iterator>::value,
                "Error: MI span<STD string>::reverse_iterator should not be "
                "considered a contiguous iterator.");
}

}  // namespace mi::test
