﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once
#include "MI.StaticVector.hpp"

namespace mi {
template <class T, size_t Size, class Container = static_vector<T, Size>>
struct static_queue {
  public:
    using container_type  = Container;
    using value_type      = typename Container::value_type;
    using size_type       = typename Container::size_type;
    using reference       = typename Container::reference;
    using const_reference = typename Container::const_reference;

    // -- [Правило 6] --------------------------------------------------------------------------------------------------

  public:
    ~static_queue() = default;

  public:
    static_queue() = default;

  public:
    static_queue(const static_queue&)     = default;
    static_queue(static_queue&&) noexcept = default;

  public:
    static_queue& operator=(const static_queue&)     = default;
    static_queue& operator=(static_queue&&) noexcept = default;

  public:
    // -- [Element access] ---------------------------------------------------------------------------------------------

    const value_type& front() const& { return _container[it_first]; }
    value_type& front() & { return _container[it_first]; }

    const value_type& back() const& { return _container[_container.size() - 1]; }
    value_type& back() & { return _container[_container.size() - 1]; }

  public:
    // -- [Capacity] ---------------------------------------------------------------------------------------------------

    MI_NODISCARD size_t size() const { return _container.size() - it_first; }

    MI_NODISCARD bool empty() const { return size() == 0; }

  public:
    // -- [Modifiers] --------------------------------------------------------------------------------------------------

    template <class... Types>
    decltype(auto) emplace(Types&&... args) {
      if (_container.size() == Size) {
        _shift_to_left();
      }
      return _container.emplace_back(STD forward<Types>(args)...);
    }

    template <class... Types>
    decltype(auto) push(Types&&... args) {
      return emplace(STD forward<Types>(args)...);
    }

    void pop() {
      MI_DCHECK(it_first < _container.size());
      it_first++;
    }

  private:
    void _shift_to_left() {
      container_type res;
      for (size_t it = it_first; it < _container.size(); ++it) {
        res.emplace_back(_container[it]);
      }

      _container = res;

      it_first = 0;
    }

  public:
    const container_type& _get_container() const { return _container; }

  private:
    container_type _container;
    size_t it_first = 0;
};

}  // namespace mi
