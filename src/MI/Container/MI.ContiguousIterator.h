// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <array>
#include <string>
#include <type_traits>

#include "MI/Common/MI.TemplateUtil.hpp"
#include "MI/Container/MI.CheckedIterators.h"
#include "MI/Container/MI.STDContainers.h"

namespace mi {

namespace internal {

template <typename T>
struct points_to_object : STD true_type {};

// STD iter_value_t is not defined for `T*` where T is not an object type.
template <typename T>
struct points_to_object<T*> : STD is_object<T> {};

// A pointer is a contiguous iterator.
// Reference: https://wg21.link/iterator.traits#5
template <typename T>
struct is_pointer : STD is_pointer<T> {};

template <typename T, typename StringT = STD basic_string<MI iter_value_t<T>>>
struct is_string_iter_impl
    : STD disjunction<STD is_same<T, typename StringT::const_iterator>, STD is_same<T, typename StringT::iterator>> {};

// An iterator to STD basic_string is contiguous.
// Reference: https://wg21.link/basic.string.general#2
//
// Note: Requires indirection via `is_string_iter_impl` to avoid triggering a
// `static_assert(is_trivial_v<value_type>)` inside libc++'s STD basic_string.
template <typename T>
struct is_string_iter : STD conjunction<STD is_trivial<MI iter_value_t<T>>, is_string_iter_impl<T>> {};

// An iterator to STD array is contiguous.
// Reference: https://wg21.link/array.overview#1
template <typename T, typename ArrayT = STD array<MI iter_value_t<T>, 1>>
struct is_array_iter
    : STD disjunction<STD is_same<T, typename ArrayT::const_iterator>, STD is_same<T, typename ArrayT::iterator>> {};

// An iterator to a non-bool MI vector is contiguous.
// Reference: https://wg21.link/vector.overview#2
template <typename T, typename VectorT = MI vector<MI iter_value_t<T>>>
struct is_vector_iter : STD conjunction<STD negation<STD is_same<MI iter_value_t<T>, bool>>,
                                        STD disjunction<STD is_same<T, typename VectorT::const_iterator>,
                                                        STD is_same<T, typename VectorT::iterator>>> {};

// The result of passing a STD valarray to STD begin is a contiguous iterator.
// Note: Since all common standard library implementations (i.e. libc++,
// stdlibc++ and MSVC's STL) just use a pointer here, we perform a similar
// optimization. The corresponding unittest still ensures that this is working
// as intended.
// Reference: https://wg21.link/valarray.range#1
template <typename T>
struct is_value_array_iter : STD is_pointer<T> {};

// base's CheckedContiguousIterator is a contiguous iterator.
template <typename T, typename ValueT = MI iter_value_t<T>>
struct is_checked_contiguous_iter : STD disjunction<STD is_same<T, MI checked_contiguous_const_iterator<ValueT>>,
                                                    STD is_same<T, MI checked_contiguous_iterator<ValueT>>> {};

// Check that the iterator points to an actual object, and is one of the
// iterator types mentioned above.
template <typename T, bool B = points_to_object<T>::value>
struct is_contiguous_iterator_impl : STD false_type {};

template <typename T>
struct is_contiguous_iterator_impl<T, true>
    : STD disjunction<is_pointer<T>, is_string_iter<T>, is_array_iter<T>, is_vector_iter<T>, is_value_array_iter<T>,
                      is_checked_contiguous_iter<T>> {};

}  // namespace internal

// is_contiguous_iterator is a type trait that determines whether a given type is
// a contiguous iterator. It is similar to C++20's contiguous_iterator concept,
// but due to a lack of the corresponding contiguous_iterator_tag relies on
// explicitly instantiating the type with iterators that are supposed to be
// contiguous iterators.
// References:
// - https://wg21.link/iterator.concept.contiguous
// - https://wg21.link/std.iterator.tags#lib:contiguous_iterator_tag
// - https://wg21.link/n4284
template <typename T>
struct is_contiguous_iterator : internal::is_contiguous_iterator_impl<MI remove_cvref_t<T>> {};

}  // namespace mi
