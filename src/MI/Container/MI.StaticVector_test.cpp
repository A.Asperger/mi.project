﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI.StaticVector.hpp"

#ifndef MI
  #define MI ::mi::
#endif

namespace mi::test {

TEST(StaticVector, DefaultConstructor) {
  MI static_vector<int, 1> a;
  MI static_vector<int, 3> b;

  STD ignore = a;
  STD ignore = b;
}

TEST(StaticVector, CopyConstructor) {
  MI static_vector<int, 3> a = {1, 2, 3};
  MI static_vector<int, 3> b(a);

  EXPECT_EQ(a.size(), 3);
  EXPECT_THAT(a, testing::ElementsAre(1, 2, 3));

  EXPECT_EQ(b.size(), 3);
  EXPECT_THAT(b, testing::ElementsAre(1, 2, 3));
}

TEST(StaticVector, MoveConstructor) {
  MI static_vector<int, 3> a = {1, 2, 3};
  MI static_vector<int, 3> b(STD move(a));

  EXPECT_EQ(a.size(), 0);
  EXPECT_EQ(b.size(), 3);

  EXPECT_THAT(b, testing::ElementsAre(1, 2, 3));
}

TEST(StaticVector, CopyOperator) {
  MI static_vector<int, 3> a = {1, 2, 3};
  MI static_vector<int, 3> b;

  b = a;

  EXPECT_EQ(a.size(), 3);
  EXPECT_THAT(a, testing::ElementsAre(1, 2, 3));

  EXPECT_EQ(b.size(), 3);
  EXPECT_THAT(b, testing::ElementsAre(1, 2, 3));
}

TEST(StaticVector, MoveOperator) {
  MI static_vector<int, 3> a       = {1, 2, 3};
  const MI static_vector<int, 3> b = STD move(a);

  EXPECT_EQ(a.size(), 0);

  EXPECT_EQ(b.size(), 3);
  EXPECT_THAT(b, testing::ElementsAre(1, 2, 3));
}

TEST(StaticVector, InitListConstructor) {
  MI static_vector<int, 3> a({1, 2, 42});

  EXPECT_EQ(a.size(), 3);
  EXPECT_THAT(a, testing::ElementsAre(1, 2, 42));
}

TEST(StaticVector, InitListOperator) {
  MI static_vector<int, 2> a = {1, 2};

  EXPECT_EQ(a.size(), 2);
  EXPECT_THAT(a, testing::ElementsAre(1, 2));
}

TEST(StaticVector, IteratorConstructor) {
  // STD vector init_list = {1, 2, 42};
  // MI static_vector<int, 3> a(init_list.begin(), init_list.end());
  //
  // EXPECT_EQ(a.size(), 3);
  // EXPECT_THAT(a, testing::ElementsAre(1, 2, 42));
}

TEST(StaticVector, Size) {
  MI static_vector<int, 3> m_0;
  MI static_vector<int, 3> m_2 = {0, 1};

  GTEST_ASSERT_EQ(m_0.size(), 0);
  GTEST_ASSERT_EQ(m_2.size(), 2);
}

TEST(StaticVector, MaxSize) {
  MI static_vector<int, 3> m;

  GTEST_ASSERT_EQ(m.max_size(), 3);
  GTEST_ASSERT_EQ((MI static_vector<int, 3>::max_size()), 3);
}

TEST(StaticVector, Empty) {
  MI static_vector<int, 3> m_a;
  MI static_vector<int, 3> m_b = {1, 2, 3};

  GTEST_ASSERT_TRUE(m_a.empty());
  GTEST_ASSERT_TRUE(!m_b.empty());
}

TEST(StaticVector, Full) {
  MI static_vector<int, 3> m_a;

  MI static_vector<int, 3> m_b = {1, 2, 3};

  GTEST_ASSERT_TRUE(!m_a.full());
  GTEST_ASSERT_TRUE(m_b.full());
}

TEST(StaticVector, Fill) {
  MI static_vector<int, 3> m;
  m.fill(666);

  EXPECT_THAT(m, testing::ElementsAre(666, 666, 666));
}

TEST(StaticVector, Swap) {
  const MI static_vector<int, 3> m_a_const = {1, 2, 3};
  const MI static_vector<int, 3> m_b_const = {4, 5, 6};
  MI static_vector<int, 3> m_a             = m_a_const;
  MI static_vector<int, 3> m_b             = m_b_const;

  m_a.swap(m_b);

  ASSERT_EQ(m_a, m_b_const);
  ASSERT_EQ(m_b, m_a_const);
}

TEST(StaticVector, Iterators) {
  static MI static_vector<int, 3> m({1, 2, 3});
  MI static_vector<int, 3> m_empty;

  ASSERT_EQ(m_empty.begin(), m_empty.end());
  ASSERT_NE(m.begin(), m.end());
}

TEST(StaticVector, At) {
  static MI static_vector<int, 3> m = {1, 2, 3};

  ASSERT_EQ(m.at(0), 1);
  ASSERT_EQ(m.at(1), 2);
  ASSERT_EQ(m.at(2), 3);
  // MI_EXPECT_CHECK_DEATH((MI_DISABLE_4834)m.at(3));
}

TEST(StaticVector, Erase) {
  {
    static MI static_vector m = {1, 2, 3, 42, -2};
    m.erase(0);

    EXPECT_TRUE(m.size() == 4);
    EXPECT_THAT(m, testing::ElementsAre(2, 3, 42, -2));
  }

  {
    static MI static_vector m = {1, 2, 3, 42, -2};
    m.erase(1);

    EXPECT_TRUE(m.size() == 4);
    EXPECT_THAT(m, testing::ElementsAre(1, 3, 42, -2));
  }

  {
    static MI static_vector m = {1, 2, 3, 42, -2};
    m.erase(4);

    EXPECT_TRUE(m.size() == 4);
    EXPECT_THAT(m, testing::ElementsAre(1, 2, 3, 42));
  }

  {
    static MI static_vector m = {1, 2, 3, 42, -2};

    EXPECT_DEATH(m.erase(5), "");
  }
}

}  // namespace mi::test
