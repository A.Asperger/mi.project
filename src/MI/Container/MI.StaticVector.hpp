﻿#pragma once

#include <algorithm>
#include <array>

#include "MI/Base/MI.Property.hpp"
#include "MI/Base/Ranges/MI.Algorithm.h"
#include "MI/Common/MI.Check.hpp"
#include "MI/Common/MI.GetUnwrapped.h"
#include "MI/Common/MI.Hash.h"
#include "MI/Common/MI.If.h"
#include "MI/Common/MI.InBrackets.h"
#include "MI/Common/MI.VerifyRange.h"
#include "MI/Container/MI.Span.hpp"

namespace mi {
template <class, size_t>
class static_vector;

template <class Ty, size_t Size>
class static_vector {
  public:
    using static_capacity = STD integral_constant<size_t, Size>;

    using value_type      = STD remove_cv_t<Ty>;
    using size_type       = size_t;
    using difference_type = ptrdiff_t;

    using pointer       = Ty*;
    using const_pointer = const Ty*;

    using reference       = Ty&;
    using const_reference = const Ty&;

    using iterator       = MI checked_contiguous_iterator<value_type>;
    using const_iterator = MI checked_contiguous_const_iterator<value_type>;

    using reverse_iterator       = STD reverse_iterator<iterator>;
    using const_reverse_iterator = STD reverse_iterator<const_iterator>;

  public:
    // Только default, иначе не constexpr.
    ~static_vector() = default;

  public:
    MI_CONSTEXPR_17 static_vector() : _arr(), _size(size_type{0}) {}

  public:
    MI_CONSTEXPR_17 static_vector(const static_vector& lv_other) : _arr() {
      ranges::copy_n(lv_other._arr, lv_other._size, _arr);
      _size = lv_other._size;
    }

    MI_CONSTEXPR_17 static_vector& operator=(const static_vector& lv_other) {
      if (this != STD addressof(lv_other)) {
        ranges::copy_n(lv_other._arr, lv_other._size, _arr);
        _size = lv_other._size;
      }

      return *this;
    }

  public:
    MI_CONSTEXPR_17 static_vector(static_vector&& rv_other) noexcept {
      ranges::move(rv_other._arr, rv_other._arr + rv_other._size, _arr);
      _size = rv_other._size;

      rv_other.clear();
    }

    MI_CONSTEXPR_17 static_vector& operator=(static_vector&& rv_other) noexcept {
      ranges::move(rv_other._arr, rv_other._arr + rv_other._size, _arr);
      _size = rv_other._size;

      rv_other.clear();

      return *this;
    }

  public:
    MI_CONSTEXPR_17 explicit static_vector(const size_type count) : _arr() {
      MI_DCHECK(count <= static_capacity::value);

      ranges::fill_n(_arr, count, value_type{});
      _size = count;
    }

    MI_CONSTEXPR_17 static_vector(const size_type count, const value_type& value) {
      MI_DCHECK(count <= static_capacity::value);

      ranges::fill_n(_arr, count, value);
      _size = count;
    }

  public:
    MI_CONSTEXPR_17 static_vector(const STD initializer_list<value_type>& list) : _arr(), _size(size_type{0}) {
      MI_DCHECK(list.size() <= static_capacity::value);

      ranges::copy_n(list.begin(), list.size(), _arr);
      _size = list.size();
    }

    MI_CONSTEXPR_17 static_vector& operator=(STD initializer_list<value_type> list) {
      MI_DCHECK(list.size() <= static_capacity::value);

      ranges::copy_n(list.begin(), list.size(), _arr);
      _size = list.size();

      return *this;
    }

  public:
    template <class T2>
    MI_CONSTEXPR_17 explicit static_vector(const MI span<T2>& s) : _arr() {
      ranges::copy_n(s.begin(), s.size(), _arr);
      _size = s.size();
    }

  public:
    template <class ItTy, if_t<STD is_pointer_v<ItTy> || MI IsIterator<ItTy>> = 0>
    MI_CONSTEXPR_17 static_vector(ItTy first, ItTy last) {
      ranges::copy(first, last, unchecked_begin());
      _size = STD distance(first, last);
    }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 size_type size() const { return _size; }

    MI_NODISCARD MI_CONSTEXPR_17 static size_type max_size() { return static_capacity::value; }

    MI_NODISCARD MI_CONSTEXPR_17 bool empty() const { return _size == 0; }

    MI_NODISCARD MI_CONSTEXPR_17 bool full() const { return _size == max_size(); }

  public:
    MI_CONSTEXPR_17 void fill(const value_type& value) {
      MI ranges::fill(unchecked_begin(), _arr + max_size(), value);
      _size = max_size();
    }

    MI_CONSTEXPR_17 void swap(static_vector& other) {
      MI ranges::swap_ranges(unchecked_begin(), unchecked_end(), other.unchecked_begin(), other.unchecked_end());
      STD swap(_size, other._size);
    }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_iterator begin() const { return {_arr, _arr, _arr + Size}; }
    MI_NODISCARD MI_CONSTEXPR_17 iterator begin() { return {_arr, _arr + Size}; }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_iterator end() const { return STD next(begin(), _size); }
    MI_NODISCARD MI_CONSTEXPR_17 iterator end() { return STD next(begin(), _size); }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator rbegin() const { return (const_reverse_iterator{end()}); }
    MI_NODISCARD MI_CONSTEXPR_17 reverse_iterator rbegin() { return (reverse_iterator{end()}); }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator rend() const { return (const_reverse_iterator{begin()}); }
    MI_NODISCARD MI_CONSTEXPR_17 reverse_iterator rend() { return (reverse_iterator{begin()}); }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator crbegin() const { return (const_reverse_iterator{begin()}); }
    MI_NODISCARD MI_CONSTEXPR_17 const_iterator cbegin() { return (const_iterator{begin()}); }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 const_iterator cend() const { return (const_iterator{end()}); }
    MI_NODISCARD MI_CONSTEXPR_17 const_reverse_iterator crend() const { return (const_reverse_iterator{end()}); }

  public:
    MI_CONSTEXPR_17 pointer unchecked_begin() { return _arr; }
    MI_CONSTEXPR_17 const_pointer unchecked_begin() const { return _arr; }

  public:
    MI_CONSTEXPR_17 pointer unchecked_end() { return _arr + _size; }
    MI_CONSTEXPR_17 const_pointer unchecked_end() const { return _arr + _size; }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 reference at(size_type pos) {
      MI_CHECK(pos < size());

      return _arr[pos];
    }

    MI_NODISCARD MI_CONSTEXPR_17 const_reference at(size_type pos) const {
      MI_CHECK(pos < size());

      return _arr[pos];
    }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 reference operator[](size_type pos) {
      MI_DCHECK(pos < _size);

      return _arr[pos];
    }

    MI_NODISCARD MI_CONSTEXPR_17 const_reference operator[](size_type pos) const {
      MI_DCHECK(pos < _size);

      return _arr[pos];
    }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 reference front() { return _arr[0]; }
    MI_NODISCARD MI_CONSTEXPR_17 const_reference front() const { return _arr[0]; }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 reference back() { return _arr[_size - 1]; }
    MI_NODISCARD MI_CONSTEXPR_17 const_reference back() const { return _arr[_size - 1]; }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 pointer data() { return &_arr[0]; }
    MI_NODISCARD MI_CONSTEXPR_17 const_pointer data() const { return &_arr[0]; }

  public:
    MI_CONSTEXPR_17 MI_NODISCARD friend bool operator==(const static_vector& lhs, const static_vector& rhs) {
      return MI ranges::equal(lhs._arr, lhs._arr + Size, rhs._arr, rhs._arr + Size);
    }

    MI_CONSTEXPR_17 MI_NODISCARD friend bool operator!=(const static_vector& lhs, const static_vector& rhs) {
      return !(lhs == rhs);
    }

    MI_CONSTEXPR_17 MI_NODISCARD friend bool operator<(const static_vector& lhs, const static_vector& rhs) {
      return MI ranges::lexicographical_compare(lhs._arr, lhs._arr + Size, rhs._arr, rhs._arr + Size);
    }

    MI_CONSTEXPR_17 MI_NODISCARD friend bool operator>(const static_vector& lhs, const static_vector& rhs) {
      return rhs < lhs;
    }

    MI_CONSTEXPR_17 MI_NODISCARD friend bool operator<=(const static_vector& lhs, const static_vector& rhs) {
      return !(lhs > rhs);
    }

    MI_CONSTEXPR_17 MI_NODISCARD friend bool operator>=(const static_vector& lhs, const static_vector& rhs) {
      return !(lhs < rhs);
    }

  public:
    template <class... TyVal>
    MI_CONSTEXPR_17 void emplace_back(TyVal&&... values) {
      MI_CHECK(!full());

      _arr[_size] = {STD forward<TyVal>(values)...};
      ++_size;
    }

    template <class... TyVal>
    MI_CONSTEXPR_17 void push_back(TyVal&&... values) {
      emplace_back(STD forward<TyVal>(values)...);
    }

  public:
    MI_NODISCARD MI_CONSTEXPR_17 bool contains(const Ty& value) const {
      return ranges::find(unchecked_begin(), unchecked_end(), value) != unchecked_end();
    }

  public:
    MI_CONSTEXPR_17 void resize(const size_type new_size) {
      if (new_size > _size) {
        ranges::fill_n(unchecked_begin() + _size, new_size - _size, value_type{});
      } else {
        ranges::fill_n(unchecked_begin() + new_size, _size - new_size, value_type{});
      }

      _size = new_size;
    }

  public:
    MI_CONSTEXPR_17 void clear() { _size = size_type{0}; }

    MI_CONSTEXPR_17 iterator erase(size_t n) {
      ranges::rotate(_arr + n, _arr + n + 1, _arr + _size);
      --_size;

      return iterator(_arr, _arr + n, _arr + _size);
    }

    MI_NODISCARD MI_CONSTEXPR_17 STD string to_string() const { return in_brackets::curly(*this); }

  private:
    value_type _arr[Size];
    size_type _size;  // Текущий размер массива
};

template <class First, class... Rest>
static_vector(First, Rest...) -> static_vector<internal::EnforceSameT<First, Rest...>, 1 + sizeof...(Rest)>;
}  // namespace mi

template <class Ty, size_t Size>
struct MI hash<MI static_vector<Ty, Size>> {
    STD size_t operator()(const static_vector<Ty, Size>& s) const {
      STD size_t seed(0);

      for (const auto& val : s) {
        const STD size_t hash_val = MI hash<Ty>{}(val);
        seed                      = MI hash_combine(seed, hash_val);
      }

      return seed;
    }
};
