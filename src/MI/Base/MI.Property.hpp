#pragma once

#if defined(__linux__)
  #define MI_OS_LINUX 1
#endif

#if defined(_WIN32)
  #define MI_OS_WIN 1
#endif

// MI_CLANG_COMPILER
#if defined(__clang__)
  #define MI_CLANG_COMPILER
#endif

// MI_MSVC_COMPILER
#if defined(_MSC_VER)
  #define MI_MSVC_COMPILER
#endif

// MI_GNU_COMPILER
#if defined(__GNUC__) || defined(__GNUG__)
  #define MI_GNU_COMPILER
#endif

#ifdef NDEBUG
  #define MI_RELEASE
  #define MI_CONFIG_STR "RELEASE"
#else
  #define MI_DEBUG
  #define MI_CONFIG_STR "DEBUG"
#endif

#ifndef MI_CPP_VERSION
  #if __cplusplus == 199711L
    #error cpp version < 11.
  #elif __cplusplus == 201103L
    #define MI_CPP_VERSION 11
  #elif __cplusplus == 201402L
    #define MI_CPP_VERSION 14
  #elif __cplusplus == 201703L
    #define MI_CPP_VERSION 17
  #elif __cplusplus == 202002L
    #define MI_CPP_VERSION 20
  #else
    #error unknown cpp version.
  #endif
#endif  // MI_CPP_VERSION

#if MI_CPP_VERSION >= 20
  #define MI_HAS_CHAR8_T
#endif

#if MI_CPP_VERSION >= 11
  #define MI_HAS_UNICODE_CHAR_T
#endif

// MI/Common/Int128.h
// #if true
//   #define MI_HAS_INT128
// #endif

#if (MI_CPP_VERSION == 11 || MI_CPP_VERSION == 14)
  #define MI_CONSTEXPR_17 inline
  #define MI_CONSTEXPR_20 inline
  #define MI_CONSTEXPR_VAR_17
  #define MI_CONSTEXPR_VAR_20
  #define MI_CONSTEXPR_DESTRUCTOR
#elif (MI_CPP_VERSION == 17)
  #define MI_CONSTEXPR_17     constexpr
  #define MI_CONSTEXPR_20     inline
  #define MI_CONSTEXPR_VAR_17 constexpr
  #define MI_CONSTEXPR_VAR_20 inline
  #define MI_CONSTEXPR_DESTRUCTOR
#elif (MI_CPP_VERSION >= 20)
  #define MI_CONSTEXPR_17         constexpr
  #define MI_CONSTEXPR_20         constexpr
  #define MI_CONSTEXPR_VAR_17     constexpr
  #define MI_CONSTEXPR_VAR_20     constexpr
  #define MI_CONSTEXPR_DESTRUCTOR constexpr
#else
  #error "I don't know your c++ version"
#endif

#ifndef MI_NODISCARD
  #define MI_NODISCARD [[nodiscard]]
#endif

#if (MI_CPP_VERSION >= 17)
  #define MI_INLINE_VAR inline
#else   // MI_HAS_CXX17
  #define MI_INLINE_VAR
#endif  // MI_HAS_CXX17

#define MI_NORETURN [[noreturn]]

#ifndef STD
  #define STD ::std::
#endif

#ifndef MI
  #define MI ::mi::
#endif
