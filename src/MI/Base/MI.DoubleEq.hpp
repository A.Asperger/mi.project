// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Base/MI.Property.hpp"

namespace mi::internal {
MI_NODISCARD MI_BASE_EXPORT bool double_eq_tolerance(double val1, double val2, double tolerance = 1e-6);
MI_NODISCARD MI_BASE_EXPORT bool float_eq_tolerance(float val1, float val2, float tolerance = 1e-6F);

// Сравнивает два числа типа double.
//
// Сравнение происходит с помощью ULP's. По умолчанию max_ulps = 4, что означает, что разница между val1 и val2 в
// битовом представлении - 4.
// Это достаточное число для повседневного использования.
MI_NODISCARD MI_BASE_EXPORT bool double_eq(double val1, double val2);

// Сравнивает два числа типа float.
//
// Сравнение происходит с помощью ULP's. По умолчанию max_ulps = 4, что означает, что разница между val1 и val2 в
// битовом представлении - 4.
// Это достаточное число для повседневного использования.
MI_NODISCARD MI_BASE_EXPORT bool float_eq(float val1, float val2);
}  // namespace mi::internal
