// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include <MI/Base/MI.BaseExport.hpp>
#include <MI/Base/MI.Numbers.h>

namespace mi {
class Degrees;

class Radians {
    double value;

  public:
    // Constructors:
    explicit constexpr Radians(double radiansValue);

    Radians(const Radians& radiansValue) = default;
    Radians(Radians&& radiansValue)      = default;

    // Assignment operator:
    Radians& operator=(const Radians& rhs) &     = default;
    Radians& operator=(Radians&& rhs) & noexcept = default;

    Radians(Degrees degreesValue);

    // Accessor:
    MI_NODISCARD constexpr double getValue() const;  // NOLINT(modernize-use-nodiscard)

    // Arithmetic operators:
    constexpr Radians operator-() const;
    constexpr Radians operator+(Radians rhs) const;
    constexpr Radians operator-(Radians rhs) const;
    constexpr Radians operator*(double rhs) const;
    constexpr Radians operator/(double rhs) const;
    constexpr double operator/(Radians rhs) const;
    constexpr friend Radians operator*(double f, Radians d) { return Radians(d.value * f); }

    // Comparison and relational operators:
    constexpr bool operator==(Radians rhs) const;
    constexpr bool operator!=(Radians rhs) const;
    constexpr bool operator>(Radians rhs) const;
    constexpr bool operator<(Radians rhs) const;
    constexpr bool operator>=(Radians rhs) const;
    constexpr bool operator<=(Radians rhs) const;

    // Compound assignment operators:
    Radians& operator+=(Radians rhs);
    Radians& operator-=(Radians rhs);
    Radians& operator*=(double rhs);
    Radians& operator/=(double rhs);
};

class Degrees {
    double value;

  public:
    // Constructors:
    explicit constexpr Degrees(double degreesValue);

    Degrees(const Degrees& degreesValue) = default;
    Degrees(Degrees&& degreesValue)      = default;

    // Assignment operator:
    Degrees& operator=(const Degrees& rhs) &     = default;
    Degrees& operator=(Degrees&& rhs) & noexcept = default;

    Degrees(Radians radiansValue);

    // Accessor:
    MI_NODISCARD constexpr double getValue() const;

    // Arithmetic operators:
    constexpr Degrees operator-() const;
    constexpr Degrees operator+(Degrees rhs) const;
    constexpr Degrees operator-(Degrees rhs) const;
    constexpr Degrees operator*(double rhs) const;
    constexpr Degrees operator/(double rhs) const;
    constexpr double operator/(Degrees rhs) const;
    constexpr friend Degrees operator*(double f, Degrees d) { return Degrees(d.value * f); }

    // Comparison and relational operators:
    constexpr bool operator==(Degrees rhs) const;
    constexpr bool operator!=(Degrees rhs) const;
    constexpr bool operator>(Degrees rhs) const;
    constexpr bool operator<(Degrees rhs) const;
    constexpr bool operator>=(Degrees rhs) const;
    constexpr bool operator<=(Degrees rhs) const;

    // Compound assignment operators:
    Degrees& operator+=(Degrees rhs);
    Degrees& operator-=(Degrees rhs);
    Degrees& operator*=(double rhs);
    Degrees& operator/=(double rhs);
};

// User-defined literal suffix operators.
MI_BASE_EXPORT Radians operator"" _rad(long double r);
MI_BASE_EXPORT Radians operator"" _rad(unsigned long long r);
MI_BASE_EXPORT Degrees operator"" _deg(long double d);
MI_BASE_EXPORT Degrees operator"" _deg(unsigned long long d);

MI_BASE_EXPORT double radian_to_degree(double radians);
MI_BASE_EXPORT double degree_to_radian(double degrees);
}  // namespace mi
