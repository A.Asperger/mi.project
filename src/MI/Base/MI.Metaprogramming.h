#pragma once

namespace mi {
// C++17-compatible emulation of a C++20 `requires` expression, which queries
// whether a given expression is well-formed. The syntax is best explained
// in terms of an example:
//
// template <typename T>
// static constexpr bool IsStreamableToRawOstream =
//     Requires<const T, llvm::raw_ostream>(
//         [](auto&& t, auto&& out) -> decltype(out << t) {});
//
// template <typename T>
// static constexpr bool IsContainer = Requires<T>([](auto&& t) -> typename T::value_type {}) &&  //
//                                     Requires<T>([](auto&& t) -> typename T::iterator {}) &&    //
//                                     Requires<T>([](auto&& t) -> decltype(t.begin()) {}) &&  //
//                                     Requires<T>([](auto&& t) -> decltype(t.end()) {}));
//
//
//
// The expression enclosed in `decltype` is the expression whose validity the
// trait queries. The lambda parameters declare the names that are used in
// that expression, and the types of those names are specified by the
// corresponding template arguments of the `Requires` call. The lambda
// parameters should always have type `auto&&`, and the template arguments
// should not have `&` or `&&` qualifiers.
template <typename... T, typename F>
constexpr auto Requires(F /* f */) -> bool {
  return std::is_invocable_v<F, T...>;
}

template <class T, class = void>
static constexpr bool HasValueType = false;

template <class T>
static constexpr bool HasValueType<T, STD void_t<typename T::value_type>> = true;

template <class T, class = void>
static constexpr bool HasSizeType = false;

template <class T>
static constexpr bool HasSizeType<T, STD void_t<typename T::size_type>> = true;

template <class T, class = void>
static constexpr bool HasIterator = false;

template <class T>
static constexpr bool HasIterator<T, STD void_t<typename T::iterator>> = true;

template <class T, class = void>
static constexpr bool HasConstIterator = false;

template <class T>
static constexpr bool HasConstIterator<T, STD void_t<typename T::const_iterator>> = true;

template <class T, class = void>
static constexpr bool HasDifferenceType = false;

template <class T>
static constexpr bool HasDifferenceType<T, STD void_t<typename T::difference_type>> = true;

template <class T, class = void>
static constexpr bool HasReference = false;

template <class T>
static constexpr bool HasReference<T, STD void_t<typename T::reference>> = true;

template <class T, class = void>
static constexpr bool HasConstReference = false;

template <class T>
static constexpr bool HasConstReference<T, STD void_t<typename T::const_reference>> = true;

template <class T, class = void>
static constexpr bool HasPointer = false;

template <class T>
static constexpr bool HasPointer<T, STD void_t<typename T::pointer>> = true;

template <class T, class = void>
static constexpr bool HasConstPointer = false;

template <class T>
static constexpr bool HasConstPointer<T, STD void_t<typename T::const_pointer>> = true;

template <class T, class = void>
static constexpr bool HasIteratorCategory = false;

template <class T>
static constexpr bool HasIteratorCategory<T, STD void_t<typename T::iterator_category>> = true;

template <class T, class = void>
static constexpr bool HasKeyType = false;

template <class T>
static constexpr bool HasKeyType<T, STD void_t<typename T::key_type>> = true;

template <class It>
using iterator_category_t = typename STD iterator_traits<It>::iterator_category;

template <class It>
static constexpr bool IsInputIterator = STD is_same_v<iterator_category_t<It>, STD input_iterator_tag>;

template <class It>
static constexpr bool IsOutputIterator = STD is_same_v<iterator_category_t<It>, STD output_iterator_tag>;

template <class It>
static constexpr bool IsForwardIterator = STD is_same_v<iterator_category_t<It>, STD forward_iterator_tag>;

template <class It>
static constexpr bool IsBidirectionalIterator = STD is_same_v<iterator_category_t<It>, STD bidirectional_iterator_tag>;

template <class It>
static constexpr bool IsRandomIterator = STD is_same_v<iterator_category_t<It>, STD random_access_iterator_tag>;

}  // namespace mi
