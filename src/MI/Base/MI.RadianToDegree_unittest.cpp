// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI.RadianToDegree.h"

namespace mi::test {
// Я взял значения из Windows калькулятор.
TEST(RadianToDegree, RadianToDegree) {
  ASSERT_EQ(mi::radian_to_degree(0.), 0.);
  EXPECT_DOUBLE_EQ(mi::radian_to_degree(1.), 57.295779513082320876798154814105);
  EXPECT_DOUBLE_EQ(mi::radian_to_degree(123.), 7047.3808801091254678461730421349);
  EXPECT_DOUBLE_EQ(mi::radian_to_degree(-12.), -687.54935415698785052157785776926);
}

TEST(RadianToDegree, DegreeToRadian) {
  EXPECT_DOUBLE_EQ(mi::degree_to_radian(0.), 0.);
  EXPECT_DOUBLE_EQ(mi::degree_to_radian(1.), 0.01745329251994329576923690768489);
  EXPECT_DOUBLE_EQ(mi::degree_to_radian(123.), 2.146754979953025379616139645241);
  EXPECT_DOUBLE_EQ(mi::degree_to_radian(18.75), 0.32724923474893679567319201909161);
  EXPECT_DOUBLE_EQ(mi::degree_to_radian(-12.), -0.20943951023931954923084289221863);
}
}  // namespace mi::test
