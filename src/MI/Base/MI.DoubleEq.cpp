// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Base/MI.DoubleEq.hpp>
#include <MI/Base/MI.FloatingPoint.hpp>
#include <MI/Base/MI.Property.hpp>

#include <cmath>

namespace mi::internal {
bool double_eq_tolerance(const double val1, const double val2, const double tolerance) {
  return STD abs(val1 - val2) < tolerance;
}

bool float_eq_tolerance(const float val1, const float val2, const float tolerance) {
  return STD abs(val1 - val2) < tolerance;
}

bool double_eq(const double val1, const double val2) {
  const floating_point<double> lhs(val1);
  const floating_point<double> rhs(val2);

  return lhs.almost_equals(rhs);
}

bool float_eq(const float val1, const float val2) {
  const floating_point<float> lhs(val1);
  const floating_point<float> rhs(val2);

  return lhs.almost_equals(rhs);
}
}  // namespace mi::internal
