#pragma once

#include <cstdint>
#include <limits>
#include <numeric>

#include "MI/Base/MI.Property.hpp"

namespace mi::internal {
template <size_t Size>
class type_with_size {
  public:
    // Это предотвращает использование пользователем типа с размером <N> с неправильными значениями N.
    using uint_t = void;
};

// Специализация для размера 4.
template <>
class type_with_size<4> {
  public:
    using uint_t = std::uint32_t;
};

// Специализация для размера 8.
template <>
class type_with_size<8> {
  public:
    using uint_t = std::uint64_t;
};

template <typename RawType>
class floating_point {
  public:
    // Определяет целочисленный тип без знака, который имеет тот же размер, что и
    // число с плавающей запятой.
    using bits_t = typename type_with_size<sizeof(RawType)>::uint_t;

    // Constants.

    // # битов в числе.
    static MI_CONSTEXPR_17 size_t bit_count = 8 * sizeof(RawType);

    // # дробных битов в числе.
    static MI_CONSTEXPR_17 size_t fraction_bit_count = std::numeric_limits<RawType>::digits - 1;

    // # разрядов экспоненты в числе.
    static MI_CONSTEXPR_17 size_t exponent_bit_count = bit_count - 1 - fraction_bit_count;

    // Маска для бита знака.
    static MI_CONSTEXPR_17 bits_t sign_bit_mask = static_cast<bits_t>(1) << (bit_count - 1);

    // Маска для дробных битов.
    static MI_CONSTEXPR_17 bits_t fraction_bit_mask = ~static_cast<bits_t>(0) >> (exponent_bit_count + 1);

    // Маска для битов экспоненты.
    static MI_CONSTEXPR_17 bits_t exponent_bit_mask = ~(sign_bit_mask | fraction_bit_mask);

    // Сколько ULP's (Units in the Last Place) мы хотим терпеть, когда сравниваете два числа.  Чем больше значение, тем
    // больше ошибок мы допускаем. Значение 0 означает, что два числа должны быть абсолютно одинаковыми, чтобы считаться
    // равными.
    //
    // Максимальная ошибка одной операции с плавающей запятой составляет 0.5 ULP. На процессорах Intel все операции с
    // плавающей запятой вычисления выполняются с точностью до 80 бит, а у double — с 64-битной точностью. Поэтому 4
    // должно хватить для обычного использования.
    //
    // http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
    static MI_CONSTEXPR_17 uint32_t max_ulps = 4;

    // Создает число с плавающей запятой из необработанного числа с плавающей запятой.
    //
    // На процессоре Intel передача ненормализованного NAN (не числа) может изменить его биты, хотя новое значение
    // гарантировано будет также NAN. Поэтому не ожидайте, что этот конструктор сохранит биты в x, когда x является NAN.
    explicit MI_CONSTEXPR_17 floating_point(const RawType& val) { _u.value = val; }

    // Static methods

    // Переинтерпретирует битовый шаблон как число с плавающей запятой.
    //
    // Эта функция нужна для проверки метода almost_equals().
    static MI_CONSTEXPR_17 RawType reinterpret_bits(const bits_t bits) {
      floating_point val(0);
      val._u.bits = bits;

      return val._u.value;
    }

    // Возвращает число с плавающей запятой, представляющее положительную бесконечность.
    static MI_CONSTEXPR_17 RawType infinity() { return reinterpret_bits(exponent_bit_mask); }

    // Non-static methods

    // Возвращает биты, представляющие это число.
    MI_NODISCARD MI_CONSTEXPR_17 bits_t bits() const { return _u.bits; }

    // Возвращает биты экспоненты этого числа.
    MI_NODISCARD MI_CONSTEXPR_17 bits_t exponent_bits() const { return exponent_bit_mask & _u.bits; }

    // Возвращает дробные биты этого числа.
    MI_NODISCARD MI_CONSTEXPR_17 bits_t fraction_bits() const { return fraction_bit_mask & _u.bits; }

    // Возвращает бит знака этого числа.
    MI_NODISCARD MI_CONSTEXPR_17 bits_t sign_bit() const { return sign_bit_mask & _u.bits; }

    // Возвращает true тогда и только тогда, когда это NAN (не число).
    MI_NODISCARD MI_CONSTEXPR_17 bool is_nan() const {
      // Это NAN, если все биты экспоненты равны единицам, а дробь
      // биты не полностью нулевые.
      return (exponent_bits() == exponent_bit_mask) && (fraction_bits() != 0);
    }

    // Возвращает истину тогда и только тогда, когда это число не превышает максимальное значение ULP.
    // от rhs. В частности, эта функция:
    //
    // - возвращает false, если одно из чисел (или оба) NAN.
    // - считает действительно большие числа почти равными бесконечности.
    // - считает +0.0 и -0.0 равными 0 DLP.
    // - 0.0 == A, только тогда, когда A ОЧЕНЬ(!) мало (0. + 1e-323).
    MI_NODISCARD MI_CONSTEXPR_17 bool almost_equals(const floating_point& rhs) const {
      // Стандарт IEEE говорит, что любая операция сравнения, включающая
      // NAN должен возвращать false.
      if (is_nan() || rhs.is_nan()) {
        return false;
      }

      return distance_between_sign_and_magnitude_numbers(_u.bits, rhs._u.bits) <= max_ulps;
    }

  private:
    // Тип данных, используемый для хранения фактического числа с плавающей запятой.
    union floating_point_union {
        RawType value;  // Необработанное число с плавающей запятой.
        bits_t bits;    // Биты, представляющие число.
    };

    // Converts an integer from the sign-and-magnitude representation to
    // the biased representation.  More precisely, let N be 2 to the
    // power of (bit_count - 1), an integer x is represented by the
    // unsigned number x + N.
    //
    // For instance,
    //
    //   -N + 1 (the most negative number representable using
    //          sign-and-magnitude) is represented by 1;
    //   0      is represented by N; and
    //   N - 1  (the biggest number representable using
    //          sign-and-magnitude) is represented by 2N - 1.
    //
    // Read http://en.wikipedia.org/wiki/Signed_number_representations
    // for more details on signed number representations.
    static MI_CONSTEXPR_17 bits_t sign_and_magnitude_to_biased(const bits_t& sam) {
      bits_t result;

      if (sign_bit_mask & sam) {
        // sam представляет отрицательное число.
        result = ~sam + 1;
      } else {
        // sam представляет положительное число.
        result = sign_bit_mask | sam;
      }

      return result;
    }

    // Учитывая два числа в представлении знака и величины,
    // возвращает расстояние между ними как беззнаковое число.
    static MI_CONSTEXPR_17 bits_t distance_between_sign_and_magnitude_numbers(const bits_t& sam1, const bits_t& sam2) {
      const bits_t biased1 = sign_and_magnitude_to_biased(sam1);
      const bits_t biased2 = sign_and_magnitude_to_biased(sam2);
      return (biased1 >= biased2) ? (biased1 - biased2) : (biased2 - biased1);
    }

    floating_point_union _u;
};
}  // namespace mi::internal
