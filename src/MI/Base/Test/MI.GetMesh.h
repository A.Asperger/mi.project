#pragma once

#include "MI/Mesh/MI.Mesh.h"
#include "MI/Mesh/MI.MeshReader.h"
#include "MI/Mesh/MI.MeshWriter.h"

namespace mi::internal {

enum class mesh_files {
  bolt,      // Болт.
  box,       //
  build_0,   // Коробка, на которой цилиндры и коробки.
  deadbolt,  // Засов.
  glass,     // Стакан.
  hook,      // Крюк 12тн.
  pin,       // Штифт.
  sidewall,  // Боковина.
  tor,       //
  tube,      // Изогнутый цилиндр (макаронина).
};

template <mesh_files MeshFile, MI MeshElement::Descriptor Quality>
struct get_mesh;

template <>
struct get_mesh<mesh_files::bolt, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/bolt_5.txt"); }
};

template <>
struct get_mesh<mesh_files::bolt, 15> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/bolt_15.txt"); }
};

template <>
struct get_mesh<mesh_files::box, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/box_5.txt"); }
};

template <>
struct get_mesh<mesh_files::box, 15> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/box_15.txt"); }
};

template <>
struct get_mesh<mesh_files::build_0, 5> {
    static MI Mesh run() {
      return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/build_0_5.txt");
    }
};

template <>
struct get_mesh<mesh_files::build_0, 15> {
    static MI Mesh run() {
      return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/build_0_15.txt");
    }
};

template <>
struct get_mesh<mesh_files::deadbolt, 5> {
    static MI Mesh run() {
      return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/deadbolt_5.txt");
    }
};

template <>
struct get_mesh<mesh_files::deadbolt, 15> {
    static MI Mesh run() {
      return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/deadbolt_15.txt");
    }
};

template <>
struct get_mesh<mesh_files::glass, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/glass_5.txt"); }
};

template <>
struct get_mesh<mesh_files::glass, 15> {
    static MI Mesh run() { return mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/glass_15.txt"); }
};

template <>
struct get_mesh<mesh_files::hook, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/hook_5.txt"); }
};

template <>
struct get_mesh<mesh_files::hook, 15> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/hook_15.txt"); }
};

template <>
struct get_mesh<mesh_files::pin, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/pin_5.txt"); }
};

template <>
struct get_mesh<mesh_files::pin, 15> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/pin_15.txt"); }
};

template <>
struct get_mesh<mesh_files::sidewall, 5> {
    static MI Mesh run() {
      return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/sidewall_5.txt");
    }
};

template <>
struct get_mesh<mesh_files::sidewall, 15> {
    static MI Mesh run() {
      return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/sidewall_15.txt");
    }
};

template <>
struct get_mesh<mesh_files::tor, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/tor_5.txt"); }
};

template <>
struct get_mesh<mesh_files::tor, 15> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/tor_15.txt"); }
};

template <>
struct get_mesh<mesh_files::tube, 5> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/tube_5.txt"); }
};

template <>
struct get_mesh<mesh_files::tube, 15> {
    static MI Mesh run() { return MI mesh_reader::read_fullplc_format("C:/_Work/SuperProject/tmp/meshes/tube_15.txt"); }
};
}  // namespace mi::internal
