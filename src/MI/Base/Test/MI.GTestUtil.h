#pragma once

#include "MI/Base/MI.DoubleEq.hpp"
#include "MI/Common/MI.Check.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#define MI_EXPECT_DCHECK_DEATH(statement)           GTEST_UNSUPPORTED_DEATH_TEST(statement, "", )
#define MI_EXPECT_DCHECK_DEATH_WITH(statement, msg) GTEST_UNSUPPORTED_DEATH_TEST(statement, msg, )
#define MI_ASSERT_DCHECK_DEATH(statement)           GTEST_UNSUPPORTED_DEATH_TEST(statement, "", return)
#define MI_ASSERT_DCHECK_DEATH_WITH(statement, msg) GTEST_UNSUPPORTED_DEATH_TEST(statement, msg, return)

#define MI_EXPECT_CHECK_DEATH(statement) EXPECT_DEATH(statement, "")
#define MI_ASSERT_CHECK_DEATH(statement) ASSERT_DEATH(statement, "")

// Отключает предупреждение c4834 "Отмена возвращаемого значения".
//
// Использовать так:
// EXPECT_DEATH((MI_DISABLE_4834)foo());
#define MI_DISABLE_4834 void
