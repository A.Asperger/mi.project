#pragma once

#include <utility>

namespace mi {

// Implementation of C++20's std::identity.
//
// Reference:
// - https://en.cppreference.com/w/cpp/utility/functional/identity
// - https://wg21.link/func.identity
struct identity {
    template <typename T>
    constexpr T&& operator()(T&& value) const noexcept {
      return std::forward<T>(value);
    }

    using is_transparent = void;
};

}  // namespace mi
