#pragma once

namespace mi {}  // namespace mi
#pragma once

#include "MI/Base/MI.Property.hpp"

#ifdef MI_STATIC_DEFINE
  #define MI_BASE_EXPORT
#else
  #ifndef MI_BASE_EXPORT
    #if defined(MI_OS_WIN)
      #if defined(MI_EXPORTS)
        /* We are building this library */
        #define MI_BASE_EXPORT __declspec(dllexport)
      #else
        /* We are using this library */
        #define MI_BASE_EXPORT __declspec(dllimport)
      #endif
    #else
      #if defined(MI_EXPORTS)
        /* We are building this library */
        #define MI_BASE_EXPORT __attribute__((visibility("default")))
      #else
        /* We are using this library */
        #define MI_BASE_EXPORT
      #endif
    #endif
  #endif
#endif
