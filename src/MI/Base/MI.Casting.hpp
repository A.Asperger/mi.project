#pragma once
#include <memory>
#include <optional>
#include <type_traits>

#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.Check.hpp"

namespace mi {
/// If T is a pointer, just return it. If it is not, return T&.
template <typename T, typename Enable = void>
struct AddLvalueReferenceIfNotPointer {
    using Type = T&;
};

template <typename T>
struct AddLvalueReferenceIfNotPointer<T, STD enable_if_t<STD is_pointer_v<T>>> {
    using Type = T;
};

/// If T is a pointer to X, return a pointer to const X. If it is not,
/// return const T.
template <typename T, typename Enable = void>
struct AddConstPastPointer {
    using Type = const T;
};

template <typename T>
struct AddConstPastPointer<T, STD enable_if_t<STD is_pointer_v<T>>> {
    using Type = const STD remove_pointer_t<T>*;
};

//===----------------------------------------------------------------------===//
// simplify_type
//===----------------------------------------------------------------------===//

/// Define a template that can be specialized by smart pointers to reflect the
/// fact that they are automatically dereferenced, and are not involved with the
/// template selection process...  the default implementation is a noop.
// TODO: rename this and/or replace it with other cast traits.
template <typename From>
struct SimplifyType {
    using SimpleType = From;  // The real type this represents...

    // An accessor to get the real value...
    static SimpleType& getSimplifiedValue(From& val) { return val; }
};

template <typename From>
struct SimplifyType<const From> {
    using NonConstSimpleType = typename SimplifyType<From>::SimpleType;
    using SimpleType         = typename AddConstPastPointer<NonConstSimpleType>::Type;
    using RetType            = typename AddLvalueReferenceIfNotPointer<SimpleType>::Type;

    static RetType getSimplifiedValue(const From& val) {
      return SimplifyType<From>::getSimplifiedValue(const_cast<From&>(val));
    }
};

namespace detail {

//===----------------------------------------------------------------------===//
// isa_impl
//===----------------------------------------------------------------------===//

// The core of the implementation of isa<X> is here; To and From should be
// the names of classes.  This template can be specialized to customize the
// implementation of isa<> without rewriting it from scratch.
template <typename To, typename From, typename Enabler = void>
struct IsaImpl {
    inline static bool doit(const From& val) { return To::class_of(&val); }
};

// Always allow upcasts, and perform no dynamic check for them.
template <typename To, typename From>
struct IsaImpl<To, From, STD enable_if_t<STD is_base_of_v<To, From>>> {
    inline static bool doit(const From& /* val */) { return true; }
};

template <typename To, typename From>
struct IsaImplCl {
    inline static bool doit(const From& val) { return IsaImpl<To, From>::doit(val); }
};

template <typename To, typename From>
struct IsaImplCl<To, const From> {
    inline static bool doit(const From& val) { return IsaImpl<To, From>::doit(val); }
};

template <typename To, typename From>
struct IsaImplCl<To, const STD unique_ptr<From>> {
    inline static bool doit(const STD unique_ptr<From>& val) {
      MI_DCHECK(val) << "isa<> used on a null pointer";
      return IsaImplCl<To, From>::doit(*val);
    }
};

template <typename To, typename From>
struct IsaImplCl<To, From*> {
    inline static bool doit(const From* val) {
      MI_DCHECK(val) << "isa<> used on a null pointer";
      return IsaImpl<To, From>::doit(*val);
    }
};

template <typename To, typename From>
struct IsaImplCl<To, From* const> {
    inline static bool doit(const From* val) {
      MI_DCHECK(val) << "isa<> used on a null pointer";
      return IsaImpl<To, From>::doit(*val);
    }
};

template <typename To, typename From>
struct IsaImplCl<To, const From*> {
    inline static bool doit(const From* val) {
      MI_DCHECK(val) << "isa<> used on a null pointer";
      return IsaImpl<To, From>::doit(*val);
    }
};

template <typename To, typename From>
struct IsaImplCl<To, const From* const> {
    inline static bool doit(const From* val) {
      MI_DCHECK(val) << "isa<> used on a null pointer";
      return IsaImpl<To, From>::doit(*val);
    }
};

template <typename To, typename From, typename SimpleFrom>
struct IsaImplWrap {
    // When From != SimplifiedType, we can simplify the type some more by using
    // the simplify_type template.
    static bool doit(const From& val) {
      return IsaImplWrap<To, SimpleFrom, typename SimplifyType<SimpleFrom>::SimpleType>::doit(
          SimplifyType<const From>::getSimplifiedValue(val));
    }
};

template <typename To, typename FromTy>
struct IsaImplWrap<To, FromTy, FromTy> {
    // When From == SimpleType, we are as simple as we are going to get.
    static bool doit(const FromTy& val) { return IsaImplCl<To, FromTy>::doit(val); }
};

//===----------------------------------------------------------------------===//
// cast_retty + cast_retty_impl
//===----------------------------------------------------------------------===//

template <class To, class From>
struct CastRetty;

// Calculate what type the 'cast' function should return, based on a requested
// type of To and a source type of From.
template <class To, class From>
struct CastRettyImpl {
    using ReturnType = To&;  // Normal case, return Ty&
};

template <class To, class From>
struct CastRettyImpl<To, const From> {
    using ReturnType = const To&;  // Normal case, return Ty&
};

template <class To, class From>
struct CastRettyImpl<To, From*> {
    using ReturnType = To*;  // Pointer arg case, return Ty*
};

template <class To, class From>
struct CastRettyImpl<To, const From*> {
    using ReturnType = const To*;  // Constant pointer arg case, return const Ty*
};

template <class To, class From>
struct CastRettyImpl<To, const From* const> {
    using ReturnType = const To*;  // Constant pointer arg case, return const Ty*
};

template <class To, class From>
struct CastRettyImpl<To, STD unique_ptr<From>> {
  private:
    using PointerType = typename CastRettyImpl<To, From*>::ReturnType;
    using ResultType  = STD remove_pointer_t<PointerType>;

  public:
    using ReturnType = STD unique_ptr<ResultType>;
};

template <class To, class From, class SimpleFrom>
struct CastRettyWrap {
    // When the simplified type and the from type are not the same, use the type
    // simplifier to reduce the type, then reuse cast_retty_impl to get the
    // resultant type.
    using ReturnType = typename CastRetty<To, SimpleFrom>::ReturnType;
};

template <class To, class FromTy>
struct CastRettyWrap<To, FromTy, FromTy> {
    // When the simplified type is equal to the from type, use it directly.
    using ReturnType = typename CastRettyImpl<To, FromTy>::ReturnType;
};

template <class To, class From>
struct CastRetty {
    using ReturnType = typename CastRettyWrap<To, From, typename SimplifyType<From>::SimpleType>::ReturnType;
};

//===----------------------------------------------------------------------===//
// cast_convert_val
//===----------------------------------------------------------------------===//

// Ensure the non-simple values are converted using the simplify_type template
// that may be specialized by smart pointers...
//
template <class To, class From, class SimpleFrom>
struct CastConvertVal {
    // This is not a simple type, use the template to simplify it...
    static typename CastRetty<To, From>::ReturnType doit(const From& val) {
      return CastConvertVal<To, SimpleFrom, typename SimplifyType<SimpleFrom>::SimpleType>::doit(
          SimplifyType<From>::getSimplifiedValue(const_cast<From&>(val)));
    }
};

template <class To, class FromTy>
struct CastConvertVal<To, FromTy, FromTy> {
    // If it's a reference, switch to a pointer to do the cast and then deref it.
    static typename CastRetty<To, FromTy>::ReturnType doit(const FromTy& val) {
      return *static_cast<STD remove_reference_t<typename CastRetty<To, FromTy>::ReturnType>*>(
          &const_cast<FromTy&>(val));
    }
};

template <class To, class FromTy>
struct CastConvertVal<To, FromTy*, FromTy*> {
    // If it's a pointer, we can use c-style casting directly.
    static typename CastRetty<To, FromTy*>::ReturnType doit(const FromTy* val) {
      return static_cast<typename CastRetty<To, FromTy*>::ReturnType>(const_cast<FromTy*>(val));
    }
};

//===----------------------------------------------------------------------===//
// is_simple_type
//===----------------------------------------------------------------------===//

template <class X>
struct IsSimpleType {
    static const bool value = STD is_same_v<X, typename SimplifyType<X>::SimpleType>;
};

}  // namespace detail

//===----------------------------------------------------------------------===//
// CastIsPossible
//===----------------------------------------------------------------------===//

/// This struct provides a way to check if a given cast is possible. It provides
/// a static function called isPossible that is used to check if a cast can be
/// performed. It should be overridden like this:
///
/// template<> struct CastIsPossible<foo, bar> {
///   static inline bool isPossible(const bar &b) {
///     return bar.isFoo();
///   }
/// };
template <typename To, typename From, typename Enable = void>
struct CastIsPossible {
    inline static bool isPossible(const From& f) {
      return detail::IsaImplWrap<To, const From, typename SimplifyType<const From>::SimpleType>::doit(f);
    }
};

// Needed for optional unwrapping. This could be implemented with isa_impl, but
// we want to implement things in the new method and move old implementations
// over. In fact, some of the isa_impl templates should be moved over to
// CastIsPossible.
template <typename To, typename From>
struct CastIsPossible<To, STD optional<From>> {
    inline static bool isPossible(const STD optional<From>& f) {
      MI_DCHECK(f) << "CastIsPossible::isPossible called on a nullopt!";
      return detail::IsaImplWrap<To, const From, typename SimplifyType<const From>::SimpleType>::doit(*f);
    }
};

/// Upcasting (from derived to base) and casting from a type to itself should
/// always be possible.
template <typename To, typename From>
struct CastIsPossible<To, From, STD enable_if_t<STD is_base_of_v<To, From>>> {
    inline static bool isPossible(const From& /* f */) { return true; }
};

//===----------------------------------------------------------------------===//
// Cast traits
//===----------------------------------------------------------------------===//

/// All of these cast traits are meant to be implementations for useful casts
/// that users may want to use that are outside the standard behavior. An
/// example of how to use a special cast called `CastTrait` is:
///
/// template<> struct CastInfo<foo, bar> : public CastTrait<foo, bar> {};
///
/// Essentially, if your use case falls directly into one of the use cases
/// supported by a given cast trait, simply inherit your special CastInfo
/// directly from one of these to avoid having to reimplement the boilerplate
/// `isPossible/castFailed/doCast/doCastIfPossible`. A cast trait can also
/// provide a subset of those functions.

/// This cast trait just provides castFailed for the specified `To` type to make
/// CastInfo specializations more declarative. In order to use this, the target
/// result type must be `To` and `To` must be constructible from `nullptr`.
template <typename To>
struct NullableValueCastFailed {
    static To castFailed() { return To(nullptr); }
};

/// This cast trait just provides the default implementation of doCastIfPossible
/// to make CastInfo specializations more declarative. The `Derived` template
/// parameter *must* be provided for forwarding castFailed and doCast.
template <typename To, typename From, typename Derived>
struct DefaultDoCastIfPossible {
    static To doCastIfPossible(From f) {
      if (!Derived::isPossible(f)) {
        return Derived::castFailed();
      }
      return Derived::doCast(f);
    }
};

namespace detail {
/// A helper to derive the type to use with `Self` for cast traits, when the
/// provided CRTP derived type is allowed to be void.
template <typename OptionalDerived, typename Default>
using SelfType = STD conditional_t<STD is_same_v<OptionalDerived, void>, Default, OptionalDerived>;
}  // namespace detail

/// This cast trait provides casting for the specific case of casting to a
/// value-typed object from a pointer-typed object. Note that `To` must be
/// nullable/constructible from a pointer to `From` to use this cast.
template <typename To, typename From, typename Derived = void>
struct ValueFromPointerCast
    : public CastIsPossible<To, From*>,
      public NullableValueCastFailed<To>,
      public DefaultDoCastIfPossible<To, From*, detail::SelfType<Derived, ValueFromPointerCast<To, From>>> {
    inline static To doCast(From* f) { return To(f); }
};

/// This cast trait provides STD unique_ptr casting. It has the semantics of
/// moving the contents of the input unique_ptr into the output unique_ptr
/// during the cast. It's also a good example of how to implement a move-only
/// cast.
template <typename To, typename From, typename Derived = void>
struct UniquePtrCast : public CastIsPossible<To, From*> {
    using Self           = detail::SelfType<Derived, UniquePtrCast<To, From>>;
    using CastResultType = STD unique_ptr<STD remove_reference_t<typename detail::CastRetty<To, From>::ReturnType>>;

    inline static CastResultType doCast(STD unique_ptr<From>&& f) {
      return CastResultType(static_cast<typename CastResultType::element_type*>(f.release()));
    }

    inline static CastResultType castFailed() { return CastResultType(nullptr); }

    inline static CastResultType doCastIfPossible(STD unique_ptr<From>&& f) {
      if (!Self::isPossible(f)) {
        return castFailed();
      }
      return doCast(f);
    }
};

/// This cast trait provides STD optional<T> casting. This means that if you
/// have a value type, you can cast it to another value type and have dyn_cast
/// return an STD optional<T>.
template <typename To, typename From, typename Derived = void>
struct OptionalValueCast
    : public CastIsPossible<To, From>,
      public DefaultDoCastIfPossible<STD optional<To>, From, detail::SelfType<Derived, OptionalValueCast<To, From>>> {
    inline static STD optional<To> castFailed() { return STD optional<To>{}; }

    inline static STD optional<To> doCast(const From& f) { return To(f); }
};

/// Provides a cast trait that strips `const` from types to make it easier to
/// implement a const-version of a non-const cast. It just removes boilerplate
/// and reduces the amount of code you as the user need to implement. You can
/// use it like this:
///
/// template<> struct CastInfo<foo, bar> {
///   ...verbose implementation...
/// };
///
/// template<> struct CastInfo<foo, const bar> : public
///        ConstStrippingForwardingCast<foo, const bar, CastInfo<foo, bar>> {};
///
template <typename To, typename From, typename ForwardTo>
struct ConstStrippingForwardingCast {
    // Remove the pointer if it exists, then we can get rid of consts/volatiles.
    using DecayedFrom = STD remove_cv_t<STD remove_pointer_t<From>>;
    // Now if it's a pointer, add it back. Otherwise, we want a ref.
    using NonConstFrom = STD conditional_t<STD is_pointer_v<From>, DecayedFrom*, DecayedFrom&>;

    inline static bool isPossible(const From& f) { return ForwardTo::isPossible(const_cast<NonConstFrom>(f)); }

    inline static decltype(auto) castFailed() { return ForwardTo::castFailed(); }

    inline static decltype(auto) doCast(const From& f) { return ForwardTo::doCast(const_cast<NonConstFrom>(f)); }

    inline static decltype(auto) doCastIfPossible(const From& f) {
      return ForwardTo::doCastIfPossible(const_cast<NonConstFrom>(f));
    }
};

/// Provides a cast trait that uses a defined pointer to pointer cast as a base
/// for reference-to-reference casts. Note that it does not provide castFailed
/// and doCastIfPossible because a pointer-to-pointer cast would likely just
/// return `nullptr` which could cause nullptr dereference. You can use it like
/// this:
///
///   template <> struct CastInfo<foo, bar *> { ... verbose implementation... };
///
///   template <>
///   struct CastInfo<foo, bar>
///       : public ForwardToPointerCast<foo, bar, CastInfo<foo, bar *>> {};
///
template <typename To, typename From, typename ForwardTo>
struct ForwardToPointerCast {
    inline static bool isPossible(const From& f) { return ForwardTo::isPossible(&f); }

    inline static decltype(auto) doCast(const From& f) { return *ForwardTo::doCast(&f); }
};

//===----------------------------------------------------------------------===//
// CastInfo
//===----------------------------------------------------------------------===//

/// This struct provides a method for customizing the way a cast is performed.
/// It inherits from CastIsPossible, to support the case of declaring many
/// CastIsPossible specializations without having to specialize the full
/// CastInfo.
///
/// In order to specialize different behaviors, specify different functions in
/// your CastInfo specialization.
/// For isa<> customization, provide:
///
///   `static bool isPossible(const From &f)`
///
/// For cast<> customization, provide:
///
///  `static To doCast(const From &f)`
///
/// For dyn_cast<> and the *_if_present<> variants' customization, provide:
///
///  `static To castFailed()` and `static To doCastIfPossible(const From &f)`
///
/// Your specialization might look something like this:
///
///  template<> struct CastInfo<foo, bar> : public CastIsPossible<foo, bar> {
///    static inline foo doCast(const bar &b) {
///      return foo(const_cast<bar &>(b));
///    }
///    static inline foo castFailed() { return foo(); }
///    static inline foo doCastIfPossible(const bar &b) {
///      if (!CastInfo<foo, bar>::isPossible(b))
///        return castFailed();
///      return doCast(b);
///    }
///  };

// The default implementations of CastInfo don't use cast traits for now because
// we need to specify types all over the place due to the current expected
// casting behavior and the way cast_retty works. New use cases can and should
// take advantage of the cast traits whenever possible!

template <typename To, typename From, typename Enable = void>
struct CastInfo : public CastIsPossible<To, From> {
    using Self = CastInfo<To, From, Enable>;

    using CastReturnType = typename detail::CastRetty<To, From>::ReturnType;

    inline static CastReturnType doCast(const From& f) {
      return detail::CastConvertVal<To, From, typename SimplifyType<From>::SimpleType>::doit(const_cast<From&>(f));
    }

    // This assumes that you can construct the cast return type from `nullptr`.
    // This is largely to support legacy use cases - if you don't want this
    // behavior you should specialize CastInfo for your use case.
    inline static CastReturnType castFailed() { return CastReturnType(nullptr); }

    inline static CastReturnType doCastIfPossible(const From& f) {
      if (!Self::isPossible(f)) {
        return castFailed();
      }
      return doCast(f);
    }
};

/// This struct provides an overload for CastInfo where From has simplify_type
/// defined. This simply forwards to the appropriate CastInfo with the
/// simplified type/value, so you don't have to implement both.
template <typename To, typename From>
struct CastInfo<To, From, STD enable_if_t<!detail::IsSimpleType<From>::value>> {
    using Self           = CastInfo<To, From>;
    using SimpleFrom     = typename SimplifyType<From>::SimpleType;
    using SimplifiedSelf = CastInfo<To, SimpleFrom>;

    inline static bool isPossible(From& f) {
      return SimplifiedSelf::isPossible(SimplifyType<From>::getSimplifiedValue(f));
    }

    inline static decltype(auto) doCast(From& f) {
      return SimplifiedSelf::doCast(SimplifyType<From>::getSimplifiedValue(f));
    }

    inline static decltype(auto) castFailed() { return SimplifiedSelf::castFailed(); }

    inline static decltype(auto) doCastIfPossible(From& f) {
      return SimplifiedSelf::doCastIfPossible(SimplifyType<From>::getSimplifiedValue(f));
    }
};

//===----------------------------------------------------------------------===//
// Pre-specialized CastInfo
//===----------------------------------------------------------------------===//

/// Provide a CastInfo specialized for STD unique_ptr.
template <typename To, typename From>
struct CastInfo<To, STD unique_ptr<From>> : public UniquePtrCast<To, From> {};

/// Provide a CastInfo specialized for STD optional<From>. It's assumed that if
/// the input is STD optional<From> that the output can be STD optional<To>. If
/// that's not the case, specialize CastInfo for your use case.
template <typename To, typename From>
struct CastInfo<To, STD optional<From>> : public OptionalValueCast<To, From> {};

/// isa<X> - Return true if the parameter to the template is an instance of one
/// of the template type arguments.  Used like this:
///
///  if (isa<Type>(myVal)) { ... }
///  if (isa<Type0, Type1, Type2>(myVal)) { ... }
template <typename To, typename From>
MI_NODISCARD inline bool isa(const From& val) {
  return CastInfo<To, const From>::isPossible(val);
}

template <typename First, typename Second, typename... Rest, typename From>
MI_NODISCARD inline bool isa(const From& val) {
  return isa<First>(val) || isa<Second, Rest...>(val);
}

/// cast<X> - Return the argument parameter cast to the specified type.  This
/// casting operator asserts that the type is correct, so it does not return
/// null on failure.  It does not allow a null argument (use cast_if_present for
/// that). It is typically used like this:
///
///  cast<Instruction>(myVal)->getParent()

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) cast(const From& val) {
  MI_DCHECK(isa<To>(val)) << "cast<Ty>() argument of incompatible type!";
  return CastInfo<To, const From>::doCast(val);
}

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) cast(From& val) {
  MI_DCHECK(isa<To>(val)) << "cast<Ty>() argument of incompatible type!";
  return CastInfo<To, From>::doCast(val);
}

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) cast(From* val) {
  MI_DCHECK(isa<To>(val)) << "cast<Ty>() argument of incompatible type!";
  return CastInfo<To, From*>::doCast(val);
}

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) cast(STD unique_ptr<From>&& val) {
  MI_DCHECK(isa<To>(val)) << "cast<Ty>() argument of incompatible type!";
  return CastInfo<To, STD unique_ptr<From>>::doCast(STD move(val));
}

//===----------------------------------------------------------------------===//
// ValueIsPresent
//===----------------------------------------------------------------------===//

template <typename T>
constexpr bool IsNullable = STD is_pointer_v<T> || STD is_constructible_v<T, STD nullptr_t>;

/// ValueIsPresent provides a way to check if a value is, well, present. For
/// pointers, this is the equivalent of checking against nullptr, for Optionals
/// this is the equivalent of checking hasValue(). It also provides a method for
/// unwrapping a value (think calling .value() on an optional).

// Generic values can't *not* be present.
template <typename T, typename Enable = void>
struct ValueIsPresent {
    using UnwrappedType = T;

    inline static bool isPresent(const T& /* t */) { return true; }

    inline static decltype(auto) unwrapValue(T& t) { return t; }
};

// STD optional provides its own way to check if something is present.
template <typename T>
struct ValueIsPresent<STD optional<T>> {
    using UnwrappedType = T;

    inline static bool isPresent(const STD optional<T>& t) { return t.has_value(); }

    inline static decltype(auto) unwrapValue(STD optional<T>& t) { return t.value(); }
};

// If something is "nullable" then we just compare it to nullptr to see if it
// exists.
template <typename T>
struct ValueIsPresent<T, STD enable_if_t<IsNullable<T>>> {
    using UnwrappedType = T;

    inline static bool isPresent(const T& t) { return t != T(nullptr); }

    inline static decltype(auto) unwrapValue(T& t) { return t; }
};

namespace detail {
// Convenience function we can use to check if a value is present. Because of
// simplify_type, we have to call it on the simplified type for now.
template <typename T>
inline bool isPresent(const T& t) {
  return ValueIsPresent<typename SimplifyType<T>::SimpleType>::isPresent(
      SimplifyType<T>::getSimplifiedValue(const_cast<T&>(t)));
}

// Convenience function we can use to unwrap a value.
template <typename T>
inline decltype(auto) unwrapValue(T& t) {
  return ValueIsPresent<T>::unwrapValue(t);
}
}  // namespace detail

/// dyn_cast<X> - Return the argument parameter cast to the specified type. This
/// casting operator returns null if the argument is of the wrong type, so it
/// can be used to test for a type as well as cast if successful. The value
/// passed in must be present, if not, use dyn_cast_if_present. This should be
/// used in the context of an if statement like this:
///
///  if (const Instruction *I = dyn_cast<Instruction>(myVal)) { ... }

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) dyn_cast(const From& val) {
  MI_DCHECK(detail::isPresent(val)) << "dyn_cast on a non-existent value";
  return CastInfo<To, const From>::doCastIfPossible(val);
}

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) dyn_cast(From& val) {
  MI_DCHECK(detail::isPresent(val)) << "dyn_cast on a non-existent value";
  return CastInfo<To, From>::doCastIfPossible(val);
}

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) dyn_cast(From* val) {
  MI_DCHECK(detail::isPresent(val)) << "dyn_cast on a non-existent value";
  return CastInfo<To, From*>::doCastIfPossible(val);
}

template <typename To, typename From>
MI_NODISCARD inline decltype(auto) dyn_cast(STD unique_ptr<From>&& val) {
  MI_DCHECK(detail::isPresent(val)) << "dyn_cast on a non-existent value";
  return CastInfo<To, STD unique_ptr<From>>::doCastIfPossible(STD forward<STD unique_ptr<From>&&>(val));
}

/// isa_and_present<X> - Functionally identical to isa, except that a null value
/// is accepted.
template <typename... X, class Y>
MI_NODISCARD inline bool isa_and_present(const Y& val) {
  if (!detail::isPresent(val)) {
    return false;
  }
  return isa<X...>(val);
}

template <typename... X, class Y>
MI_NODISCARD inline bool isa_and_nonnull(const Y& val) {
  return isa_and_present<X...>(val);
}

/// cast_if_present<X> - Functionally identical to cast, except that a null
/// value is accepted.
template <class X, class Y>
MI_NODISCARD inline auto cast_if_present(const Y& val) {
  if (!detail::isPresent(val)) {
    return CastInfo<X, const Y>::castFailed();
  }
  MI_DCHECK(isa<X>(val)) << "cast_if_present<Ty>() argument of incompatible type!";
  return cast<X>(detail::unwrapValue(val));
}

template <class X, class Y>
MI_NODISCARD inline auto cast_if_present(Y& val) {
  if (!detail::isPresent(val)) {
    return CastInfo<X, Y>::castFailed();
  }
  MI_DCHECK(isa<X>(val)) << "cast_if_present<Ty>() argument of incompatible type!";
  return cast<X>(detail::unwrapValue(val));
}

template <class X, class Y>
MI_NODISCARD inline auto cast_if_present(Y* val) {
  if (!detail::isPresent(val)) {
    return CastInfo<X, Y*>::castFailed();
  }
  MI_DCHECK(isa<X>(val)) << "cast_if_present<Ty>() argument of incompatible type!";
  return cast<X>(detail::unwrapValue(val));
}

template <class X, class Y>
MI_NODISCARD inline auto cast_if_present(STD unique_ptr<Y>&& val) {
  if (!detail::isPresent(val)) {
    return UniquePtrCast<X, Y>::castFailed();
  }
  return UniquePtrCast<X, Y>::doCast(STD move(val));
}

// Provide a forwarding from cast_or_null to cast_if_present for current
// users. This is deprecated and will be removed in a future patch, use
// cast_if_present instead.
template <class X, class Y>
auto cast_or_null(const Y& val) {
  return cast_if_present<X>(val);
}

template <class X, class Y>
auto cast_or_null(Y& val) {
  return cast_if_present<X>(val);
}

template <class X, class Y>
auto cast_or_null(Y* val) {
  return cast_if_present<X>(val);
}

template <class X, class Y>
auto cast_or_null(STD unique_ptr<Y>&& val) {
  return cast_if_present<X>(STD move(val));
}

/// dyn_cast_if_present<X> - Functionally identical to dyn_cast, except that a
/// null (or none in the case of optionals) value is accepted.
template <class X, class Y>
auto dyn_cast_if_present(const Y& val) {
  if (!detail::isPresent(val)) {
    return CastInfo<X, const Y>::castFailed();
  }
  return CastInfo<X, const Y>::doCastIfPossible(detail::unwrapValue(val));
}

template <class X, class Y>
auto dyn_cast_if_present(Y& val) {
  if (!detail::isPresent(val)) {
    return CastInfo<X, Y>::castFailed();
  }
  return CastInfo<X, Y>::doCastIfPossible(detail::unwrapValue(val));
}

template <class X, class Y>
auto dyn_cast_if_present(Y* val) {
  if (!detail::isPresent(val)) {
    return CastInfo<X, Y*>::castFailed();
  }
  return CastInfo<X, Y*>::doCastIfPossible(detail::unwrapValue(val));
}

// Forwards to dyn_cast_if_present to avoid breaking current users. This is
// deprecated and will be removed in a future patch, use
// cast_if_present instead.
template <class X, class Y>
auto dyn_cast_or_null(const Y& val) {
  return dyn_cast_if_present<X>(val);
}

template <class X, class Y>
auto dyn_cast_or_null(Y& val) {
  return dyn_cast_if_present<X>(val);
}

template <class X, class Y>
auto dyn_cast_or_null(Y* val) {
  return dyn_cast_if_present<X>(val);
}

/// unique_dyn_cast<X> - Given a unique_ptr<Y>, try to return a unique_ptr<X>,
/// taking ownership of the input pointer iff isa<X>(val) is true.  If the
/// cast is successful, From refers to nullptr on exit and the casted value
/// is returned.  If the cast is unsuccessful, the function returns nullptr
/// and From is unchanged.
template <class X, class Y>
MI_NODISCARD inline typename CastInfo<X, STD unique_ptr<Y>>::CastResultType unique_dyn_cast(STD unique_ptr<Y>& val) {
  if (!isa<X>(val)) {
    return nullptr;
  }
  return cast<X>(STD move(val));
}

template <class X, class Y>
MI_NODISCARD inline auto unique_dyn_cast(STD unique_ptr<Y>&& val) {
  return unique_dyn_cast<X, Y>(val);
}

// unique_dyn_cast_or_null<X> - Functionally identical to unique_dyn_cast,
// except that a null value is accepted.
template <class X, class Y>
MI_NODISCARD inline typename CastInfo<X, STD unique_ptr<Y>>::CastResultType unique_dyn_cast_or_null(
    STD unique_ptr<Y>& val) {
  if (!val) {
    return nullptr;
  }
  return unique_dyn_cast<X, Y>(val);
}

template <class X, class Y>
MI_NODISCARD inline auto unique_dyn_cast_or_null(STD unique_ptr<Y>&& val) {
  return unique_dyn_cast_or_null<X, Y>(val);
}

}  // namespace mi
