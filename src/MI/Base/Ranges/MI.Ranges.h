// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef BASE_RANGES_RANGES_H
#define BASE_RANGES_RANGES_H

#include <array>
#include <type_traits>
#include <utility>

#include "MI/Common/MI.TemplateUtil.hpp"

namespace mi {

namespace internal {

// Overload for C array.
template <typename T, size_t N>
constexpr T* begin(T (&array)[N], PriorityTag<2> /* tag */) {  // NOLINT
  return array;
}

// Overload for mutable STD array. Required since STD array::begin is not
// constexpr prior to C++17. Needs to dispatch to the const overload since only
// const operator[] is constexpr in C++14.
template <typename T, size_t N>
constexpr T* begin(STD array<T, N>& array, PriorityTag<2> tag) {
  return const_cast<T*>(begin(const_cast<const STD array<T, N>&>(array), tag));
}

// Overload for const STD array. Required since STD array::begin is not
// constexpr prior to C++17.
template <typename T, size_t N>
constexpr const T* begin(const STD array<T, N>& array, PriorityTag<2> /* tag */) {
  return N != 0 ? &array[0] : nullptr;
}

// Generic container overload.
template <typename Range>
constexpr auto begin(Range&& range, PriorityTag<1> /* tag */) -> decltype(STD forward<Range>(range).begin()) {
  return STD forward<Range>(range).begin();
}

// Overload for free begin() function.
template <typename Range>
constexpr auto begin(Range&& range, PriorityTag<0> /* tag */) -> decltype(begin(STD forward<Range>(range))) {
  return begin(STD forward<Range>(range));
}

// Overload for C array.
template <typename T, size_t N>
constexpr T* end(T (&array)[N], PriorityTag<2> /* tag */) {
  return array + N;
}

// Overload for mutable STD array. Required since STD array::end is not
// constexpr prior to C++17. Needs to dispatch to the const overload since only
// const operator[] is constexpr in C++14.
template <typename T, size_t N>
constexpr T* end(STD array<T, N>& array, PriorityTag<2> tag) {
  return const_cast<T*>(end(const_cast<const STD array<T, N>&>(array), tag));
}

// Overload for const STD array. Required since STD array::end is not
// constexpr prior to C++17.
template <typename T, size_t N>
constexpr const T* end(const STD array<T, N>& array, PriorityTag<2> /* tag */) {
  return N != 0 ? (&array[0]) + N : nullptr;
}

// Generic container overload.
template <typename Range>
constexpr auto end(Range&& range, PriorityTag<1> /* tag */) -> decltype(STD forward<Range>(range).end()) {
  return STD forward<Range>(range).end();
}

// Overload for free end() function.
template <typename Range>
constexpr auto end(Range&& range, PriorityTag<0> /* tag */) -> decltype(end(STD forward<Range>(range))) {
  return end(STD forward<Range>(range));
}

}  // namespace internal

namespace ranges {

// Simplified implementation of C++20's STD ranges::begin.
// As opposed to STD ranges::begin, this implementation does does not check
// whether begin() returns an iterator and does not inhibit ADL.
//
// The trailing return type and dispatch to the internal implementation is
// necessary to be SFINAE friendly.
//
// Reference: https://wg21.link/range.access.begin
template <typename Range>
constexpr auto begin(Range&& range) noexcept
    -> decltype(internal::begin(STD forward<Range>(range), internal::PriorityTag<2>())) {
  return internal::begin(STD forward<Range>(range), internal::PriorityTag<2>());
}

// Simplified implementation of C++20's STD ranges::end.
// As opposed to STD ranges::end, this implementation does does not check
// whether end() returns an iterator and does not inhibit ADL.
//
// The trailing return type and dispatch to the internal implementation is
// necessary to be SFINAE friendly.
//
// Reference: - https://wg21.link/range.access.end
template <typename Range>
constexpr auto end(Range&& range) noexcept
    -> decltype(internal::end(STD forward<Range>(range), internal::PriorityTag<2>())) {
  return internal::end(STD forward<Range>(range), internal::PriorityTag<2>());
}

// Implementation of C++20's STD ranges::iterator_t.
//
// Reference: https://wg21.link/ranges.syn#:~:text=iterator_t
template <typename Range>
using iterator_t = decltype(ranges::begin(STD declval<Range&>()));

// Implementation of C++20's STD ranges::range_value_t.
//
// Reference: https://wg21.link/ranges.syn#:~:text=range_value_t
template <typename Range>
using range_value_t = iter_value_t<iterator_t<Range>>;

}  // namespace ranges

}  // namespace mi

#endif  // BASE_RANGES_RANGES_H
