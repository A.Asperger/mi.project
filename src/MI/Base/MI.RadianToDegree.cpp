// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Base/MI.RadianToDegree.h>

namespace mi {
namespace internal {
// Соотношение радиан к градусам.
constexpr double RAD2DEG = 180. / numbers::pi;
constexpr double DEG2RAD = numbers::pi / 180.;
}  // namespace internal

// ---------------------------------------------------------------------------------------------------------------------

constexpr Radians::Radians(const double radiansValue) : value(radiansValue) {}

Radians::Radians(const Degrees degreesValue) : value(degreesValue.getValue() * internal::DEG2RAD) {}

Radians& Radians::operator+=(const Radians rhs) {
  value += rhs.value;
  return *this;
}

Radians& Radians::operator-=(const Radians rhs) {
  value -= rhs.value;
  return *this;
}

Radians& Radians::operator*=(const double rhs) {
  value *= rhs;
  return *this;
}

Radians& Radians::operator/=(const double rhs) {
  value /= rhs;
  return *this;
}

constexpr double Radians::getValue() const { return value; }

constexpr Radians Radians::operator-() const { return Radians(-value); }

constexpr Radians Radians::operator+(const Radians rhs) const { return Radians(value + rhs.value); }

constexpr Radians Radians::operator-(const Radians rhs) const { return Radians(value - rhs.value); }

constexpr Radians Radians::operator*(const double rhs) const { return Radians(value * rhs); }

constexpr Radians Radians::operator/(const double rhs) const { return Radians(value / rhs); }

constexpr double Radians::operator/(const Radians rhs) const { return (value / rhs.value); }

constexpr bool Radians::operator==(const Radians rhs) const { return value == rhs.value; }

constexpr bool Radians::operator!=(const Radians rhs) const { return value != rhs.value; }

constexpr bool Radians::operator>(const Radians rhs) const { return value > rhs.value; }

constexpr bool Radians::operator<(const Radians rhs) const { return value < rhs.value; }

constexpr bool Radians::operator>=(const Radians rhs) const { return value >= rhs.value; }

constexpr bool Radians::operator<=(const Radians rhs) const { return value <= rhs.value; }

// ---------------------------------------------------------------------------------------------------------------------

constexpr Degrees::Degrees(const double degreesValue) : value(degreesValue) {}

Degrees::Degrees(const Radians radiansValue) : value(radiansValue.getValue() * internal::RAD2DEG) {}

Degrees& Degrees::operator+=(const Degrees rhs) {
  value += rhs.value;
  return *this;
}

Degrees& Degrees::operator-=(const Degrees rhs) {
  value -= rhs.value;
  return *this;
}

Degrees& Degrees::operator*=(const double rhs) {
  value *= rhs;
  return *this;
}

Degrees& Degrees::operator/=(const double rhs) {
  value /= rhs;
  return *this;
}

constexpr double Degrees::getValue() const { return value; }

constexpr Degrees Degrees::operator-() const { return Degrees(-value); }

constexpr Degrees Degrees::operator+(const Degrees rhs) const { return Degrees(value + rhs.value); }

constexpr Degrees Degrees::operator-(const Degrees rhs) const { return Degrees(value - rhs.value); }

constexpr Degrees Degrees::operator*(const double rhs) const { return Degrees(value * rhs); }

constexpr Degrees Degrees::operator/(const double rhs) const { return Degrees(value / rhs); }

constexpr double Degrees::operator/(const Degrees rhs) const { return (value / rhs.value); }

constexpr bool Degrees::operator==(const Degrees rhs) const { return value == rhs.value; }

constexpr bool Degrees::operator!=(const Degrees rhs) const { return value != rhs.value; }

constexpr bool Degrees::operator>(const Degrees rhs) const { return value > rhs.value; }

constexpr bool Degrees::operator<(const Degrees rhs) const { return value < rhs.value; }

constexpr bool Degrees::operator>=(const Degrees rhs) const { return value >= rhs.value; }

constexpr bool Degrees::operator<=(const Degrees rhs) const { return value <= rhs.value; }

// ---------------------------------------------------------------------------------------------------------------------

double radian_to_degree(const double radians) { return radians * internal::RAD2DEG; }
double degree_to_radian(const double degrees) { return degrees * internal::DEG2RAD; }

Radians operator""_rad(const long double r) { return Radians(r); }
Radians operator""_rad(const unsigned long long r) { return Radians(r); }
Degrees operator""_deg(const long double d) { return Degrees(d); }
Degrees operator""_deg(const unsigned long long d) { return Degrees(d); }
}  // namespace mi
