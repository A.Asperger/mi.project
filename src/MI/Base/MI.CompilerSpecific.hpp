#ifndef MI_BASE_COMPILER_SPECIFIC
#define MI_BASE_COMPILER_SPECIFIC

#include "MI/Base/MI.Property.hpp"

// Это оболочка вокруг атрибута `__has_cpp_attribute`, которую можно использовать для проверки
// наличие атрибута. Если компилятор не поддерживает это
// макрос, он просто будет оцениваться как 0.
#if defined(__has_cpp_attribute)
  #define MI_HAS_CPP_ATTRIBUTE(x) __has_cpp_attribute(x)
#else
  #define MI_HAS_CPP_ATTRIBUTE(x) 0
#endif

// Обертка вокруг `__has_builtin`, похожий на HAS_CPP_ATTRIBUTE.
#if defined(__has_builtin)
  #define MI_HAS_BUILTIN(x) __has_builtin(x)
#else
  #define MI_HAS_BUILTIN(x) 0
#endif

#if defined(MI_GNU_COMPILER) || defined(MI_CLANG_COMPILER)
  #define MI_NOINLINE __attribute__((noinline))
#elif defined(MI_MSVC_COMPILER)
  #define MI_NOINLINE __declspec(noinline)
#else
  #define MI_NOINLINE
#endif

#if defined(MI_GNU_COMPILER) && defined(MI_RELEASE)
  #define MI_FORCE_INLINE inline __attribute__((__always_inline__))
#elif defined(MI_MSVC_COMPILER) && defined(MI_RELEASE)
  #define MI_FORCE_INLINE __forceinline
#else
  #define MI_FORCE_INLINE inline
#endif

#if defined(__has_feature)
  #define MI_HAS_FEATURE(FEATURE) __has_feature(FEATURE)
#else
  #define MI_HAS_FEATURE(FEATURE) 0
#endif

#if defined(MI_GNU_COMPILER)
  #define MI_PRETTY_FUNCTION __PRETTY_FUNCTION__
#else
  // See https://en.cppreference.com/w/c/language/function_definition#func
  #define MI_PRETTY_FUNCTION __func__
#endif

#endif
