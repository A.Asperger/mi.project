#pragma once

#include <MI/Common/MI.Hash.h>

#include <algorithm>    // for forward
#include <cstddef>      // for ptrdiff_t, nullptr_t, MI MeshElement::Descriptor
#include <iosfwd>       // for ostream
#include <type_traits>  // for enable_if_t, is_convertible, is_assignable

//
// Temporary until MSVC STL supports no-exceptions mode.
// Currently terminate is a no-op in this mode, so we add termination behavior back
//
#if defined(_MSC_VER) && (defined(_KERNEL_MODE) || (defined(_HAS_EXCEPTIONS) && !_HAS_EXCEPTIONS))
  #define GSL_KERNEL_MODE

  #define GSL_MSVC_USE_STL_NOEXCEPTION_WORKAROUND
  #include <intrin.h>
  #define RANGE_CHECKS_FAILURE 0

  #if defined(__clang__)
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Winvalid-noreturn"
  #endif  // defined(__clang__)

#else     // defined(_MSC_VER) && (defined(_KERNEL_MODE) || (defined(_HAS_EXCEPTIONS) &&
          // !_HAS_EXCEPTIONS))

  #include <exception>

#endif  // defined(_MSC_VER) && (defined(_KERNEL_MODE) || (defined(_HAS_EXCEPTIONS) &&
        // !_HAS_EXCEPTIONS))

//
// make suppress attributes parse for some compilers
// Hopefully temporary until suppression standardization occurs
//
#if defined(__clang__)
  #define GSL_SUPPRESS(x) [[gsl::suppress("x")]]
#else
  #if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
    #define GSL_SUPPRESS(x) [[gsl::suppress(x)]]
  #else
    #define GSL_SUPPRESS(x)
  #endif  // _MSC_VER
#endif    // __clang__

#define GSL_STRINGIFY_DETAIL(x) #x
#define GSL_STRINGIFY(x)        GSL_STRINGIFY_DETAIL(x)

#if defined(__clang__) || defined(__GNUC__)
  #define GSL_LIKELY(x)   __builtin_expect(!!(x), 1)
  #define GSL_UNLIKELY(x) __builtin_expect(!!(x), 0)

#else

  #define GSL_LIKELY(x)   (!!(x))
  #define GSL_UNLIKELY(x) (!!(x))
#endif  // defined(__clang__) || defined(__GNUC__)

//
// GSL_ASSUME(cond)
//
// Tell the optimizer that the predicate cond must hold. It is unspecified
// whether or not cond is actually evaluated.
//
#ifdef _MSC_VER
  #define GSL_ASSUME(cond) __assume(cond)
#elif defined(__GNUC__)
  #define GSL_ASSUME(cond) ((cond) ? static_cast<void>(0) : __builtin_unreachable())
#else
  #define GSL_ASSUME(cond) static_cast<void>((cond) ? 0 : 0)
#endif

//
// GSL.assert: assertions
//

namespace mi {
namespace details {
#if defined(GSL_MSVC_USE_STL_NOEXCEPTION_WORKAROUND)

typedef void(__cdecl* terminate_handler)();

// clang-format off
    GSL_SUPPRESS(f.6) // NO-FORMAT: attribute
// clang-format on
[[noreturn]] inline void __cdecl default_terminate_handler() { __fastfail(RANGE_CHECKS_FAILURE); }

inline gsl::details::terminate_handler& get_terminate_handler() noexcept {
  static terminate_handler handler = &default_terminate_handler;
  return handler;
}

#endif  // defined(GSL_MSVC_USE_STL_NOEXCEPTION_WORKAROUND)

[[noreturn]] inline void terminate() noexcept {
#if defined(GSL_MSVC_USE_STL_NOEXCEPTION_WORKAROUND)
  (*gsl::details::get_terminate_handler())();
#else
  std::terminate();
#endif  // defined(GSL_MSVC_USE_STL_NOEXCEPTION_WORKAROUND)
}

}  // namespace details

#define GSL_CONTRACT_CHECK(type, cond) (GSL_LIKELY(cond) ? static_cast<void>(0) : mi::details::terminate())

#define Expects(cond) GSL_CONTRACT_CHECK("Precondition", cond)
#define Ensures(cond) GSL_CONTRACT_CHECK("Postcondition", cond)

#if defined(GSL_MSVC_USE_STL_NOEXCEPTION_WORKAROUND) && defined(__clang__)
  #pragma clang diagnostic pop
#endif

namespace details {
template <typename T, typename = void>
struct IsComparableToNullptr : std::false_type {};

template <typename T>
struct IsComparableToNullptr<T, std::enable_if_t<std::is_convertible_v<decltype(std::declval<T>() != nullptr), bool>>>
    : std::true_type {};
}  // namespace details

//
// owner
//
// owner<T> is designed as a bridge for code that must deal directly with owning pointers for some
// reason
//
// T must be a pointer type
// - disallow construction from any type other than pointer type
//
template <class T, class = std::enable_if_t<std::is_pointer_v<T>>>
using Owner = T;

namespace details {

//
// not_null
//
// Restricts a pointer or smart pointer to only hold non-null values.
//
// Has zero size overhead over T.
//
// If T is a pointer (i.e. T == U*) then
// - allow construction from U*
// - disallow construction from nullptr_t
// - disallow default construction
// - ensure construction from null U* fails
// - allow implicit conversion to U*
//
template <class T>
class NonNull {
  public:
    static_assert(details::IsComparableToNullptr<T>::value, "T cannot be compared to nullptr.");

    template <typename U, typename = std::enable_if_t<std::is_convertible_v<U, T>>>
    constexpr NonNull(U&& u) : ptr_(std::forward<U>(u)) {
      Expects(ptr_ != nullptr);
    }

    template <typename = std::enable_if_t<!std::is_same_v<std::nullptr_t, T>>>
    constexpr NonNull(T u) : ptr_(std::move(u)) {
      Expects(ptr_ != nullptr);
    }

    template <typename U, typename = std::enable_if_t<std::is_convertible_v<U, T>>>
    constexpr NonNull(const NonNull<U>& other) : NonNull(other.get()) {}

    NonNull(const NonNull& other)            = default;
    NonNull& operator=(const NonNull& other) = default;
    constexpr std::conditional_t<std::is_copy_constructible_v<T>, T, const T&> get() const {
      Ensures(ptr_ != nullptr);
      return ptr_;
    }

    constexpr operator T() const { return get(); }
    constexpr decltype(auto) operator->() const { return get(); }
    constexpr decltype(auto) operator*() const { return *get(); }

    // prevents compilation when someone attempts to assign a null pointer constant
    NonNull(std::nullptr_t)            = delete;
    NonNull& operator=(std::nullptr_t) = delete;

    // unwanted operators...pointers only point to single objects!
    NonNull& operator++()                 = delete;
    NonNull& operator--()                 = delete;
    NonNull operator++(int)               = delete;
    NonNull operator--(int)               = delete;
    NonNull& operator+=(std::ptrdiff_t)   = delete;
    NonNull& operator-=(std::ptrdiff_t)   = delete;
    void operator[](std::ptrdiff_t) const = delete;

  private:
    T ptr_;
};

template <class T>
auto make_not_null(T&& t) noexcept {
  return NonNull<std::remove_cv_t<std::remove_reference_t<T>>>{std::forward<T>(t)};
}

template <class T>
std::ostream& operator<<(std::ostream& os, const NonNull<T>& val) {
  os << val.get();
  return os;
}

template <class T, class U>
auto operator==(const NonNull<T>& lhs, const NonNull<U>& rhs) noexcept(noexcept(lhs.get() == rhs.get()))
    -> decltype(lhs.get() == rhs.get()) {
  return lhs.get() == rhs.get();
}

template <class T, class U>
auto operator!=(const NonNull<T>& lhs, const NonNull<U>& rhs) noexcept(noexcept(lhs.get() != rhs.get()))
    -> decltype(lhs.get() != rhs.get()) {
  return lhs.get() != rhs.get();
}

template <class T, class U>
auto operator<(const NonNull<T>& lhs, const NonNull<U>& rhs) noexcept(noexcept(lhs.get() < rhs.get()))
    -> decltype(lhs.get() < rhs.get()) {
  return lhs.get() < rhs.get();
}

template <class T, class U>
auto operator<=(const NonNull<T>& lhs, const NonNull<U>& rhs) noexcept(noexcept(lhs.get() <= rhs.get()))
    -> decltype(lhs.get() <= rhs.get()) {
  return lhs.get() <= rhs.get();
}

template <class T, class U>
auto operator>(const NonNull<T>& lhs, const NonNull<U>& rhs) noexcept(noexcept(lhs.get() > rhs.get()))
    -> decltype(lhs.get() > rhs.get()) {
  return lhs.get() > rhs.get();
}

template <class T, class U>
auto operator>=(const NonNull<T>& lhs, const NonNull<U>& rhs) noexcept(noexcept(lhs.get() >= rhs.get()))
    -> decltype(lhs.get() >= rhs.get()) {
  return lhs.get() >= rhs.get();
}

// more unwanted operators
template <class T, class U>
std::ptrdiff_t operator-(const NonNull<T>&, const NonNull<U>&) = delete;

template <class T>
NonNull<T> operator-(const NonNull<T>&, std::ptrdiff_t) = delete;

template <class T>
NonNull<T> operator+(const NonNull<T>&, std::ptrdiff_t) = delete;

template <class T>
NonNull<T> operator+(std::ptrdiff_t, const NonNull<T>&) = delete;

#if (defined(__cpp_deduction_guides) && (__cpp_deduction_guides >= 201611L))

// deduction guides to prevent the ctad-maybe-unsupported warning
template <class T>
NonNull(T) -> NonNull<T>;

#endif  // (defined(__cpp_deduction_guides) && (__cpp_deduction_guides >= 201611L))

}  // namespace details

template <class T>
struct MI hash<details::NonNull<T>> {
    std::size_t operator()(const details::NonNull<T>& value) const { return MI hash<T>{}(value.get()); }
};

//
// strict_not_null
//
// Restricts a pointer or smart pointer to only hold non-null values,
//
// - provides a strict (i.e. explicit constructor from T) wrapper of not_null
// - to be used for new code that wishes the design to be cleaner and make not_null
//   checks intentional, or in old code that would like to make the transition.
//
//   To make the transition from not_null, incrementally replace not_null
//   by strict_not_null and fix compilation errors
//
//   Expect to
//   - remove all unneeded conversions from raw pointer to not_null and back
//   - make API clear by specifying not_null in parameters where needed
//   - remove unnecessary asserts
//
template <class T>
class NonNull : public details::NonNull<T> {
  public:
    template <typename U, typename = std::enable_if_t<std::is_convertible_v<U, T>>>
    constexpr explicit NonNull(U&& u) : details::NonNull<T>(std::forward<U>(u)) {}

    template <typename = std::enable_if_t<!std::is_same_v<std::nullptr_t, T>>>
    constexpr explicit NonNull(T u) : details::NonNull<T>(u) {}

    template <typename U, typename = std::enable_if_t<std::is_convertible_v<U, T>>>
    constexpr NonNull(const details::NonNull<U>& other) : details::NonNull<T>(other) {}

    template <typename U, typename = std::enable_if_t<std::is_convertible_v<U, T>>>
    constexpr NonNull(const NonNull<U>& other) : details::NonNull<T>(other) {}

    NonNull(NonNull&& other) noexcept        = default;
    NonNull(const NonNull& other)            = default;
    NonNull& operator=(const NonNull& other) = default;
    NonNull& operator=(const details::NonNull<T>& other) {
      NonNull<T>::operator=(other);
      return *this;
    }

    // prevents compilation when someone attempts to assign a null pointer constant
    NonNull(std::nullptr_t)            = delete;
    NonNull& operator=(std::nullptr_t) = delete;

    // unwanted operators...pointers only point to single objects!
    NonNull& operator++()                 = delete;
    NonNull& operator--()                 = delete;
    NonNull operator++(int)               = delete;
    NonNull operator--(int)               = delete;
    NonNull& operator+=(std::ptrdiff_t)   = delete;
    NonNull& operator-=(std::ptrdiff_t)   = delete;
    void operator[](std::ptrdiff_t) const = delete;
};

// more unwanted operators
template <class T, class U>
std::ptrdiff_t operator-(const NonNull<T>&, const NonNull<U>&) = delete;
template <class T>
NonNull<T> operator-(const NonNull<T>&, std::ptrdiff_t) = delete;
template <class T>
NonNull<T> operator+(const NonNull<T>&, std::ptrdiff_t) = delete;
template <class T>
NonNull<T> operator+(std::ptrdiff_t, const NonNull<T>&) = delete;

template <class T>
auto make_strict_not_null(T&& t) noexcept {
  return NonNull<std::remove_cv_t<std::remove_reference_t<T>>>{std::forward<T>(t)};
}

#if (defined(__cpp_deduction_guides) && (__cpp_deduction_guides >= 201611L))

template <class T>
NonNull(T) -> NonNull<T>;

#endif  // ( defined(__cpp_deduction_guides) && (__cpp_deduction_guides >= 201611L) )

template <class T>
struct MI hash<NonNull<T>> {
    std::size_t operator()(const NonNull<T>& value) const { return MI hash<T>{}(value.get()); }
};
}  // namespace mi
