#pragma once

#include <array>

#include "MI/Container/MI.Matrix.hpp"

namespace mi {
using point3d = matrix<double, 3, 1>;
}  // namespace mi
