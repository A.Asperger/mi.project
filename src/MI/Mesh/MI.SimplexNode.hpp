#pragma once
#include <MI/Base/MI.Casting.hpp>

#include "MI.Mesh.hpp"
#include "MI/Container/MI.StaticVector.hpp"
#include "MeshElement/MI.Hexahedron.h"
#include "MeshElement/MI.Quad.h"
#include "MeshElement/MI.Triangle.h"

namespace mi {

struct SimplexNode {
  public:
    using Descriptor = MeshElement::Descriptor;

  public:
    enum class kind {
      simplex_node_2d,
      simplex_node_3d,
    };

  public:
    virtual ~SimplexNode() = default;

  public:
    explicit SimplexNode(const kind kind) : _kind(kind) {}

  public:
    SimplexNode(const SimplexNode&)     = default;
    SimplexNode(SimplexNode&&) noexcept = default;

  public:
    SimplexNode& operator=(const SimplexNode&)     = default;
    SimplexNode& operator=(SimplexNode&&) noexcept = default;

  public:
    MI_NODISCARD kind get_kind() const { return _kind; }

  public:
    MI_NODISCARD virtual Descriptor left() const {
      MI_NOT_IMPLEMENTED();
      return {};
    }

    MI_NODISCARD virtual Descriptor mid() const {
      MI_NOT_IMPLEMENTED();
      return {};
    }

    MI_NODISCARD virtual Descriptor right() const {
      MI_NOT_IMPLEMENTED();
      return {};
    }

    MI_NODISCARD virtual Descriptor up() const {
      MI_NOT_IMPLEMENTED();
      return {};
    }

  private:
    kind _kind;
};

struct SimplexNode2D : public SimplexNode {
  public:
    SimplexNode2D() : SimplexNode(kind::simplex_node_2d) {}

  public:
    SimplexNode2D(const Descriptor left, const Descriptor mid, const Descriptor right)
        : SimplexNode(kind::simplex_node_2d), m_data({left, mid, right}) {}

  public:
    static bool class_of(const SimplexNode* base) { return base->get_kind() == kind::simplex_node_2d; }

  public:
    MI_NODISCARD Descriptor left() const override { return m_data[0]; }

    MI_NODISCARD Descriptor mid() const override { return m_data[1]; }

    MI_NODISCARD Descriptor right() const override { return m_data[2]; }

  public:
    MI static_vector<Descriptor, 3> m_data;
};

struct SimplexNode3D : public SimplexNode {
  public:
    SimplexNode3D() : SimplexNode(kind::simplex_node_3d) {}

  public:
    SimplexNode3D(const Descriptor mid, const Descriptor left, const Descriptor up, const Descriptor right)
        : SimplexNode(kind::simplex_node_3d), m_data({mid, left, up, right}) {}

  public:
    static bool class_of(const SimplexNode* base) { return base->get_kind() == kind::simplex_node_3d; }

  public:
    MI_NODISCARD Descriptor mid() const override { return m_data[0]; }

    MI_NODISCARD Descriptor left() const override { return m_data[1]; }

    MI_NODISCARD Descriptor up() const override { return m_data[2]; }

    MI_NODISCARD Descriptor right() const override { return m_data[3]; }

  public:
    MI static_vector<Descriptor, 4> m_data;
};

MI_NODISCARD inline STD unique_ptr<SimplexNode> simplex_node_quad_impl(const MI MeshElement::Descriptor n) {
  MI_DCHECK(n < 4);

  switch (n) {
    case 0: return STD make_unique<SimplexNode2D>(3, 0, 1);
    case 1: return STD make_unique<SimplexNode2D>(0, 1, 2);
    case 2: return STD make_unique<SimplexNode2D>(1, 2, 3);
    case 3: return STD make_unique<SimplexNode2D>(2, 3, 0);
  }
}

MI_NODISCARD inline STD unique_ptr<SimplexNode> simplex_node_hex_impl(const MI MeshElement::Descriptor n) {
  MI_DCHECK(n < 8);
  // (0, 3, 4, 1), (1, 0, 5, 2), (2, 1, 6, 3), (3, 2, 7, 0),
  // (4, 7, 5, 0), (5, 4, 6, 1), (6, 5, 7, 2), (7, 6, 4, 3)
  switch (n) {
    case 0: return STD make_unique<SimplexNode3D>(0, 3, 4, 1);
    case 1: return STD make_unique<SimplexNode3D>(1, 0, 5, 2);
    case 2: return STD make_unique<SimplexNode3D>(2, 1, 6, 3);
    case 3: return STD make_unique<SimplexNode3D>(3, 2, 7, 0);
    case 4: return STD make_unique<SimplexNode3D>(4, 7, 5, 0);
    case 5: return STD make_unique<SimplexNode3D>(5, 4, 6, 1);
    case 6: return STD make_unique<SimplexNode3D>(6, 5, 7, 2);
    case 7: return STD make_unique<SimplexNode3D>(7, 6, 4, 3);
  }
}

MI_NODISCARD inline STD unique_ptr<SimplexNode> simplex_node(const MI MeshElement* element,
                                                             const MI MeshElement::Descriptor n) {
  if (isa<Triangle>(element)) {
  } else if (isa<Quad>(element)) {
    return simplex_node_quad_impl(n);
  } else if (isa<Hexahedron>(element)) {
    return simplex_node_hex_impl(n);
  }

  MI_NOT_REACHED();
  return {};
}
}  // namespace mi
