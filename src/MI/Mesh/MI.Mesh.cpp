// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Mesh/MeshElement/MI.Hexahedron.h>
#include <MI/Mesh/MeshElement/MI.Quad.h>
#include <MI/Mesh/MeshElement/MI.Triangle.h>

#include <MI/Base/MI.Casting.hpp>
#include <MI/Mesh/MI.Mesh.hpp>

#include "MI.Auxiliary.h"
#include "MI.Point.hpp"

namespace mi {

Mesh::~Mesh() = default;

Mesh::Mesh(const Mesh& other) : _m_vertices(other._m_vertices) {
  for (const MI MeshElement* element : other.get_m_elements()) {
    if (isa<Triangle>(element)) {
      add_element<Triangle>(*cast<Triangle>(element));
    } else if (isa<Quad>(element)) {
      add_element<Quad>(*cast<Quad>(element));
    } else if (isa<Hexahedron>(element)) {
      add_element<Hexahedron>(*cast<Hexahedron>(element));
    } else {
      MI_NOT_IMPLEMENTED();
    }
  }
}

Mesh& Mesh::operator=(const Mesh& other) {
  _m_vertices.clear();
  _m_elements.clear();
  _arena.clear();

  _m_vertices = other._m_vertices;

  for (const MI MeshElement* element : other.get_m_elements()) {
    if (isa<Triangle>(element)) {
      add_element<Triangle>(*cast<Triangle>(element));
    } else if (isa<Quad>(element)) {
      add_element<Quad>(*cast<Quad>(element));
    } else if (isa<Hexahedron>(element)) {
      add_element<Hexahedron>(*cast<Hexahedron>(element));
    } else {
      MI_CHECK(false);
    }
  }

  return *this;
}

Mesh::descriptor Mesh::add_vertex(const double x, const double y, const double z) {
  _m_vertices.emplace_back(MI point3d{x, y, z});
  return static_cast<descriptor>(_m_vertices.size()) - 1;
}

Mesh::descriptor Mesh::add_vertex(const point3d& p) {
  _m_vertices.emplace_back(p);
  return static_cast<descriptor>(_m_vertices.size()) - 1;
}

const mi::vector<Mesh::point_type>& Mesh::get_m_vertices() const { return _m_vertices; }

mi::vector<Mesh::point_type>& Mesh::get_m_vertices() { return _m_vertices; }

Mesh::size_type Mesh::get_n_vertices() const { return static_cast<size_type>(get_m_vertices().size()); }

Mesh::point_type& Mesh::get_vertex(const size_type n) { return get_m_vertices()[n]; }

const Mesh::point_type& Mesh::get_vertex(const size_type n) const { return get_m_vertices()[n]; }

const Mesh::m_elements_type& Mesh::get_m_elements() const { return (_m_elements); }

Mesh::m_elements_type& Mesh::get_m_elements() { return (_m_elements); }

Mesh::size_type Mesh::get_n_elements() const { return static_cast<size_type>(get_m_elements().size()); }

MeshElement* Mesh::get_element(const size_type n) const { return (_m_elements[n]); }

MeshElement* Mesh::get_element(const size_type n) { return (_m_elements[n]); }

void Mesh::remove_element(const size_type n_element) {
  // Удалить сам элемент.
  _m_elements.erase(STD next(_m_elements.begin(), n_element));
}

FacesAroundVertexMap get_faces_around_vertex_map(const Mesh& mesh) {
  FacesAroundVertexMap _faces_around_vertex;

  // Заполняется соответствие граней
  for (MeshElement::Descriptor n_element = 0; n_element < mesh.get_n_elements(); ++n_element) {
    const MeshElement* el = mesh.get_element(n_element);

    for (const MeshElement::Descriptor index : el->m_indexes()) {
      _faces_around_vertex[index].emplace_back(n_element);
    }
  }

  return _faces_around_vertex;
}

FacesAroundEdgeMap get_faces_around_edge_map(const Mesh& mesh) {
  FacesAroundEdgeMap _faces_around_edge;

  // Заполняется соответствие граней
  for (MeshElement::Descriptor n_element = 0; n_element < mesh.get_n_elements(); ++n_element) {
    const MeshElement* el = mesh.get_element(n_element);

    for (size_t n_edge = 0; n_edge < el->n_edges(); ++n_edge) {
      SortedEdge sorted_edge = get_sorted_edge(el, n_edge);
      _faces_around_edge[sorted_edge].emplace_back(n_element);
    }
  }

  return _faces_around_edge;
}

AdjacentFacesMap get_adjacent_faces_map(const Mesh& mesh) {
  AdjacentFacesMap res;
  const FacesAroundEdgeMap map_faces_around_edges = get_faces_around_edge_map(mesh);

  for (const auto& [_, pair_adjacent_index] : map_faces_around_edges) {
    res[pair_adjacent_index[0]].emplace_back(pair_adjacent_index[1]);
    res[pair_adjacent_index[1]].emplace_back(pair_adjacent_index[0]);
  }

  return res;
}

}  // namespace mi
