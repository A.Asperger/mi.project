// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include "MI/Mesh/MI.Quality.hpp"

#include "MI/Base/MI.DoubleEq.hpp"
#include "MI/Mesh/MI.IsDegenerated.h"

namespace mi {

// Вокруг оси X.
MI_NODISCARD MI point3d rotate_x(const MI point3d& vector, const double angle_radian) {
  const double x     = vector.x();
  const double y     = vector.y();
  const double z     = vector.z();
  const double cos_a = cos(angle_radian);
  const double sin_a = sin(angle_radian);

  MI point3d res = {x,                      //
                    y * cos_a - z * sin_a,  //
                    y * sin_a + z * cos_a};

  const double n1 = res.squared_euclidean_norm();
  const double n2 = vector.squared_euclidean_norm();

  if (n1 > 0.) {
    return res = res * (n2 / n1);
  }

  return res;
}

// Вокруг оси Y.
MI_NODISCARD MI point3d rotate_y(const MI point3d& vector, const double angle_radian) {
  const double x     = vector.x();
  const double y     = vector.y();
  const double z     = vector.z();
  const double cos_a = cos(angle_radian);
  const double sin_a = sin(angle_radian);

  MI point3d res = {x * cos_a + z * sin_a,  //
                    y,                      //
                    -x * sin_a + z * cos_a};

  const double n1 = res.squared_euclidean_norm();
  const double n2 = vector.squared_euclidean_norm();

  if (n1 > 0.) {
    return res = res * (n2 / n1);
  }

  return res;
}

// Вокруг оси Z.
MI_NODISCARD MI point3d rotate_z(const MI point3d& vector, const double angle_radian) {
  const double x     = vector.x();
  const double y     = vector.y();
  const double z     = vector.z();
  const double cos_a = cos(angle_radian);
  const double sin_a = sin(angle_radian);

  MI point3d res = {x * cos_a - y * sin_a,  //
                    x * sin_a + y * cos_a,  //
                    z};

  const double n1 = res.squared_euclidean_norm();
  const double n2 = vector.squared_euclidean_norm();

  if (n1 > 0.) {
    const double coeff = n2 / n1;
    return res         = res * coeff;
  }

  return res;
}

// Повернуть вектор вокруг трех осей.
MI_NODISCARD MI point3d rotate_xyz(const MI point3d& vector, const double angle_radian_x, const double angle_radian_y,
                                   const double angle_radian_z) {
  return rotate_z(rotate_y(rotate_x(vector, angle_radian_x), angle_radian_y), angle_radian_z);
}

// Возвращает матрицу перехода для некоторого вектора из Декартовой системы координат в новый базис.
MI_NODISCARD MI matrix3d get_transition_matrix(const MI point3d& new_axis_x, const MI point3d& new_axis_y,
                                               const MI point3d& new_axis_z) {
  return matrix3d{
      {new_axis_x.x(), new_axis_x.y(), new_axis_x.z()},
      {new_axis_y.x(), new_axis_y.y(), new_axis_y.z()},
      {new_axis_z.x(), new_axis_z.y(), new_axis_z.z()}
  };
}

// Переносит вектор из Декартовой системы координат в новый базис с помощью матрицы перехода.
//
// v - vector.
// ax - axis_x.
// ay - axis_y.
// az - axis_z.
//
//  | ax.x, ax.y, ax.z |   | v.x |
//  | ay.x, ay.y, ay.z | * | v.y | = {v.x', v.y', v.z'}
//  | az.x, az.y, az.z |   | v.y |
//
MI_NODISCARD MI point3d transition_another_basis(const MI matrix3d& transition_matrix, const MI point3d& vector) {
  return transition_matrix * vector;
}

MI_NODISCARD MI static_vector<MI point3d, 3> transfer_to_plane_z(const MI point3d& v0, const MI point3d& v1,
                                                                 const MI point3d& v2) {
  const MI point3d vec0 = (v1 - v0).normalized();
  const MI point3d vec1 = (v2 - v0).normalized();

  // Новый базис

  const MI point3d& axis_x = vec0;
  const MI point3d axis_z  = axis_x.cross(vec1).normalized();
  const MI point3d axis_y  = axis_z.cross(axis_x).normalized();

  const MI matrix3d transition_matrix = get_transition_matrix(axis_x, axis_y, axis_z);

  return {transition_another_basis(transition_matrix, v0),  //
          transition_another_basis(transition_matrix, v1),  //
          transition_another_basis(transition_matrix, v2)};
}

MI_NODISCARD static MI matrix2d w_tri() {
  return matrix2d{
      {1.,           1. / 2.},
      {0., STD sqrt(3.) / 2.}
  };
}

MI_NODISCARD static MI matrix2d w_quad() {
  return matrix2d{
      {1., 0.},
      {0., 1.}
  };
}

MI_NODISCARD static MI matrix3d w_hex() {
  return matrix3d{
      {1., 0., 0},
      {0., 1., 0},
      {0., 0., 1},
  };
}

// Проверяет качество и возвращает его обратно, если допустимое или ошибку.
ErrorOr<double> quality_check_impl(double q) {
  // ОЧЕНЬ редко бывает, что качество получается больше 1.0 (напр 1.0000000000213), тогда его нужно округлить до 1.0,
  // т.к. качество может быть только (0., 1.].
  if (MI internal::double_eq(q, 1.)) {
    return 1.;
  }

  // Принадлежит (0., 1.]
  if (q > 0. && q <= 1.) {
    return q;
  } else {
    return ErrorBuilder("ERROR", MI_PRETTY_FUNCTION) << "The quality was bad. (q == " << q << "). ";
  }
}

ErrorOr<double> quality(const Triangle& element, const Mesh& mesh) {
  if (MI is_degenerated(element, mesh)) {
    return Error("Degenerated element");
  }

  const point3d& v0 = mesh.get_vertex(element.index(0));
  const point3d& v1 = mesh.get_vertex(element.index(1));
  const point3d& v2 = mesh.get_vertex(element.index(2));

  constexpr MI MeshElement::Descriptor mid   = 0;
  constexpr MI MeshElement::Descriptor left  = 2;
  constexpr MI MeshElement::Descriptor right = 1;

  const MI static_vector<MI point3d, 3> rotated_vertices = MI transfer_to_plane_z(v0, v1, v2);

  MI point3d vec1 = rotated_vertices[right] - rotated_vertices[mid];
  MI point3d vec2 = rotated_vertices[left] - rotated_vertices[mid];

  // D(Tk) - Это матрица для симплекс-узла.
  const MI matrix2d dtk = {
      {vec1.x(), vec2.x()},
      {vec1.y(), vec2.y()}
  };

  // Следует из определения 2.
  // " ... Элемент называется действительным, если det(D(T_k)) > 0 ... "
  if (dtk.determinant() <= 0.) {
    return MI Error("D(T_{k}) <= 0. ");
  }

  constexpr MI MeshElement::Descriptor m = 2;

  const auto sk          = dtk * w_tri().inverse();
  const auto numerator   = m * STD pow(sk.determinant(), 2. / m);
  const auto denominator = sk.squared_euclidean_norm();
  const auto quality     = numerator / denominator;

  return quality_check_impl(quality);
}

ErrorOr<double> quality(const Quad& element, const Mesh& mesh) {
  if (MI is_degenerated(element, mesh)) {
    return Error("Degenerated element");
  }

  double new_quality = 0.;

  for (MI MeshElement::Descriptor n_node = 0; n_node < element.n_vertices(); ++n_node) {
    const STD unique_ptr<SimplexNode> simplex = MI simplex_node(&element, n_node);
    const MI MeshElement::Descriptor mid      = simplex->mid();
    const MI MeshElement::Descriptor left     = simplex->left();
    const MI MeshElement::Descriptor right    = simplex->right();

    const MI static_vector<MI point3d, 3> rotated_vertices =
        MI transfer_to_plane_z(mesh.get_vertex(element.index(left)), mesh.get_vertex(element.index(mid)),
                               mesh.get_vertex(element.index(right)));

    // Вектор от центра симплекс узла до правого края
    MI point3d v1 = rotated_vertices[2] - rotated_vertices[1];

    // Вектор от центра симплекс узла до левого края
    MI point3d v2 = rotated_vertices[0] - rotated_vertices[1];

    // D(Tk) - Это матрица для симплекс-узла.
    const MI matrix2d dtk = {
        {v1.x(), v2.x()},
        {v1.y(), v2.y()}
    };

    // Следует из определения 2.
    // " ... Элемент называется действительным, если det(D(T_k)) > 0 ... "
    if (dtk.determinant() <= 0.) {
      return MI Error("D(T_{k}) <= 0. ");
    }

    constexpr MI MeshElement::Descriptor m = 2;
    const auto sk                          = dtk * w_quad().inverse();
    const auto numerator                   = m * STD pow(sk.determinant(), 2. / m);
    const auto denominator                 = sk.squared_euclidean_norm();
    const auto current_quality             = numerator / denominator;

    new_quality += current_quality;
  }

  new_quality /= 4.;

  return quality_check_impl(new_quality);
}

ErrorOr<double> quality(const Hexahedron& element, const Mesh& mesh) {
  // if (MI is_degenerated(element, mesh)) {
  //   return Error("Degenerated element");
  // }

  double new_quality = 0.;

  for (MI MeshElement::Descriptor n_node = 0; n_node < element.n_vertices(); ++n_node) {
    const STD unique_ptr<SimplexNode> simplex = MI simplex_node(&element, n_node);
    const MI MeshElement::Descriptor mid      = simplex->mid();
    const MI MeshElement::Descriptor left     = simplex->left();
    const MI MeshElement::Descriptor up       = simplex->up();
    const MI MeshElement::Descriptor right    = simplex->right();

    // Вектор от центра симплекс узла до правого края
    MI point3d v1 = mesh.get_vertex(element.index(left)) - mesh.get_vertex(element.index(mid));
    MI point3d v2 = mesh.get_vertex(element.index(up)) - mesh.get_vertex(element.index(mid));
    MI point3d v3 = mesh.get_vertex(element.index(right)) - mesh.get_vertex(element.index(mid));

    // D(Tk) - Это матрица для симплекс-узла.
    const MI matrix3d dtk = {
        {v1.x(), v2.x(), v3.x()},
        {v1.y(), v2.y(), v3.y()},
        {v1.z(), v2.z(), v3.z()}
    };

    // Следует из определения 2.
    // " ... Элемент называется действительным, если det(D(T_k)) > 0 ... "
    if (dtk.determinant() <= 0.) {
      return MI Error("D(T_{k}) <= 0. ");
    }

    constexpr MI MeshElement::Descriptor m = 3;
    const auto sk                          = dtk * w_hex().inverse();
    const auto numerator                   = m * STD pow(sk.determinant(), 2. / m);
    const auto denominator                 = sk.squared_euclidean_norm();
    const auto current_quality             = numerator / denominator;

    new_quality += current_quality;
  }

  new_quality /= 8.;

  return quality_check_impl(new_quality);
}

ErrorOr<double> quality(const MeshElement* element, const Mesh& mesh) {
  if (isa<Triangle>(element)) {
    return quality(*cast<Triangle>(element), mesh);
  }

  if (isa<Quad>(element)) {
    return quality(*cast<Quad>(element), mesh);
  }

  if (isa<Hexahedron>(element)) {
    return quality(*cast<Hexahedron>(element), mesh);
  }

  return Error("Bad type. ");
}

namespace internal {
ErrorOr<double> quality_quad_impl(const MI static_vector<point3d, 4>& vv) {
  if (MI is_degenerated_quad_impl(vv)) {
    return Error("Degenerated element");
  }

  double new_quality = 0.;

  for (MI MeshElement::Descriptor n_node = 0; n_node < 4; ++n_node) {
    const STD unique_ptr<SimplexNode> simplex = MI simplex_node_quad_impl(n_node);
    const MI MeshElement::Descriptor mid      = simplex->mid();
    const MI MeshElement::Descriptor left     = simplex->left();
    const MI MeshElement::Descriptor right    = simplex->right();

    const MI static_vector<MI point3d, 3> rotated_vertices = MI transfer_to_plane_z(vv[left], vv[mid], vv[right]);

    // Вектор от центра симплекс узла до правого края
    MI point3d v1 = rotated_vertices[2] - rotated_vertices[1];

    // Вектор от центра симплекс узла до левого края
    MI point3d v2 = rotated_vertices[0] - rotated_vertices[1];

    // D(Tk) - Это матрица для симплекс-узла.
    const MI matrix2d dtk = {
        {v1.x(), v2.x()},
        {v1.y(), v2.y()}
    };

    // Следует из определения 2.
    // " ... Элемент называется действительным, если det(D(T_k)) > 0 ... "
    if (dtk.determinant() <= 0.) {
      return MI Error("D(T_{k}) <= 0. ");
    }

    constexpr MI MeshElement::Descriptor m = 2;
    const auto sk                          = dtk * w_quad().inverse();
    const auto numerator                   = m * STD pow(sk.determinant(), 2. / m);
    const auto denominator                 = sk.squared_euclidean_norm();
    const auto current_quality             = numerator / denominator;

    new_quality += current_quality;
  }

  new_quality /= 4.;

  return quality_check_impl(new_quality);
}

ErrorOr<double> quality(const Quad& element, const Mesh& mesh) {
  if (MI is_degenerated(element, mesh)) {
    return Error("Degenerated element");
  }

  double new_quality = 0.;

  for (MI MeshElement::Descriptor n_node = 0; n_node < element.n_vertices(); ++n_node) {
    const STD unique_ptr<SimplexNode> simplex = MI simplex_node(&element, n_node);
    const MI MeshElement::Descriptor mid      = simplex->mid();
    const MI MeshElement::Descriptor left     = simplex->left();
    const MI MeshElement::Descriptor right    = simplex->right();

    const MI static_vector<MI point3d, 3> rotated_vertices =
        MI transfer_to_plane_z(mesh.get_vertex(element.index(left)), mesh.get_vertex(element.index(mid)),
                               mesh.get_vertex(element.index(right)));

    // Вектор от центра симплекс узла до правого края
    MI point3d v1 = rotated_vertices[2] - rotated_vertices[1];

    // Вектор от центра симплекс узла до левого края
    MI point3d v2 = rotated_vertices[0] - rotated_vertices[1];

    // D(Tk) - Это матрица для симплекс-узла.
    const MI matrix2d dtk = {
        {v1.x(), v2.x()},
        {v1.y(), v2.y()}
    };

    // Следует из определения 2.
    // " ... Элемент называется действительным, если det(D(T_k)) > 0 ... "
    if (dtk.determinant() <= 0.) {
      return MI Error("D(T_{k}) <= 0. ");
    }

    constexpr MI MeshElement::Descriptor m = 2;
    const auto sk                          = dtk * w_quad().inverse();
    const auto numerator                   = m * STD pow(sk.determinant(), 2. / m);
    const auto denominator                 = sk.squared_euclidean_norm();
    const auto current_quality             = numerator / denominator;

    new_quality += current_quality;
  }

  new_quality /= 4.;

  return quality_check_impl(new_quality);
}
}  // namespace internal
}  // namespace mi
