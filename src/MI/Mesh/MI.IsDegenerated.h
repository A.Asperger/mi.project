#pragma once

#include "MI/Base/MI.Casting.hpp"
#include "MI/Base/MI.DoubleEq.hpp"
#include "MI/Mesh/MI.Mesh.hpp"
#include "MI/Mesh/MI.VectorsDirectional.h"
#include "MeshElement/MI.Quad.h"
#include "MeshElement/MI.Triangle.h"

namespace mi::internal {
MI_NODISCARD inline double triangle_area(const MI point3d& v0, const MI point3d& v1, const MI point3d& v2) {
  MI point3d a = (v0 - v1).normalized();
  MI point3d b = (v2 - v1).normalized();

  return a.cross(b).squared_euclidean_norm();
}

// Элемент полностью или частично вырожден в прямую линию.
MI_NODISCARD inline bool degenerated_into_straight_line_quad_impl(const static_vector<point3d, 4>& vv) {
  // D     C
  //  +---+
  //  |   |
  //  +---+
  // A     B

  // Если площадь треугольника равна нулю, то три вершины находятся на одной прямой.

  return MI internal::double_eq_tolerance(triangle_area(vv[0], vv[1], vv[2]), 0.) ||  //
         MI internal::double_eq_tolerance(triangle_area(vv[1], vv[2], vv[3]), 0.) ||  //
         MI internal::double_eq_tolerance(triangle_area(vv[2], vv[3], vv[0]), 0.) ||  //
         MI internal::double_eq_tolerance(triangle_area(vv[3], vv[0], vv[1]), 0.);
}

// Элемент полностью или частично вырожден в прямую линию.
MI_NODISCARD inline bool degenerated_into_straight_line(const MI Quad& quad, const MI Mesh& mesh) {
  const MI point3d& a = mesh.get_vertex(quad.index(0));
  const MI point3d& b = mesh.get_vertex(quad.index(1));
  const MI point3d& c = mesh.get_vertex(quad.index(2));
  const MI point3d& d = mesh.get_vertex(quad.index(3));

  // D     C
  //  +---+
  //  |   |
  //  +---+
  // A     B

  // Если площадь треугольника равна нулю, то три вершины находятся на одной прямой.

  return MI internal::double_eq_tolerance(triangle_area(a, b, c), 0.) ||  //
         MI internal::double_eq_tolerance(triangle_area(b, c, d), 0.) ||  //
         MI internal::double_eq_tolerance(triangle_area(c, d, a), 0.) ||  //
         MI internal::double_eq_tolerance(triangle_area(d, a, b), 0.);
}

// Элемент полностью или частично вырожден в прямую линию.
MI_NODISCARD inline bool degenerated_into_straight_line(const MI Triangle& triangle, const MI Mesh& mesh) {
  const MI point3d& a = mesh.get_vertex(triangle.index(0));
  const MI point3d& b = mesh.get_vertex(triangle.index(1));
  const MI point3d& c = mesh.get_vertex(triangle.index(2));

  return MI internal::double_eq_tolerance(triangle_area(a, b, c), 0.);
}

// Выгнутый.
MI_NODISCARD inline bool is_curved_quad_impl(const static_vector<point3d, 4>& vv) {
  // Если квад частично или полностью вырожден в прямую, то эта функция не будет правильно работать.
  // Это происходит из-за того, что при умножении двух параллельных векторов мы получаем нулевой вектор.
  MI_DCHECK(!degenerated_into_straight_line_quad_impl(vv));

  // D     C
  //  +---+
  //  |   |
  //  +---+
  // A     B

  const MI point3d& a = vv[0];
  const MI point3d& b = vv[1];
  const MI point3d& c = vv[2];
  const MI point3d& d = vv[3];

  // Нормали к каждой вершине, перпендикулярные плоскости элемента.

  const auto normal_a = (b - a).cross(d - a);
  const auto normal_b = (c - b).cross(a - b);
  const auto normal_c = (d - c).cross(b - c);
  const auto normal_d = (a - d).cross(c - d);

  const MI vectors_dir a_c_directional = MI vectors_directional(normal_a, normal_c);
  const MI vectors_dir b_d_directional = MI vectors_directional(normal_b, normal_d);

  // Если вектора normal_a и normal_c однонаправленные или ортогональные
  // и вектора normal_b и normal_d однонаправленные или ортогональные,
  // то элемент выгнутый.
  return a_c_directional == MI vectors_dir::co_directional &&  //
         b_d_directional == MI vectors_dir::co_directional;
}

// Выгнутый.
MI_NODISCARD inline bool is_curved(const MI Quad& quad, const MI Mesh& mesh) {
  // Если квад частично или полностью вырожден в прямую, то эта функция не будет правильно работать.
  // Это происходит из-за того, что при умножении двух параллельных векторов мы получаем нулевой вектор.
  MI_DCHECK(!degenerated_into_straight_line(quad, mesh));

  // D     C
  //  +---+
  //  |   |
  //  +---+
  // A     B

  const MI point3d& a = mesh.get_vertex(quad.index(0));
  const MI point3d& b = mesh.get_vertex(quad.index(1));
  const MI point3d& c = mesh.get_vertex(quad.index(2));
  const MI point3d& d = mesh.get_vertex(quad.index(3));

  // Нормали к каждой вершине, перпендикулярные плоскости элемента.

  const auto normal_a = (b - a).cross(d - a);
  const auto normal_b = (c - b).cross(a - b);
  const auto normal_c = (d - c).cross(b - c);
  const auto normal_d = (a - d).cross(c - d);

  const MI vectors_dir a_c_directional = MI vectors_directional(normal_a, normal_c);
  const MI vectors_dir b_d_directional = MI vectors_directional(normal_b, normal_d);

  // Если вектора normal_a и normal_c однонаправленные или ортогональные
  // и вектора normal_b и normal_d однонаправленные или ортогональные,
  // то элемент выгнутый.
  return a_c_directional == MI vectors_dir::co_directional &&  //
         b_d_directional == MI vectors_dir::co_directional;
}

// Выгнутый.
MI_NODISCARD inline bool is_curved(const MI Triangle& /* triangle */, const MI Mesh& /* mesh */) { return true; }

// Вогнутый.
MI_NODISCARD inline bool is_concave(const MI Quad& quad, const MI Mesh& mesh) {
  MI_DCHECK(!degenerated_into_straight_line(quad, mesh));

  // D     C
  //  +---+
  //  |   |
  //  +---+
  // A     B

  const MI point3d& a = mesh.get_vertex(quad.index(0));
  const MI point3d& b = mesh.get_vertex(quad.index(1));
  const MI point3d& c = mesh.get_vertex(quad.index(2));
  const MI point3d& d = mesh.get_vertex(quad.index(3));

  // Нормали к каждой вершине, перпендикулярные плоскости элемента.

  const auto normal_a = (b - a).cross(d - a);
  const auto normal_b = (c - b).cross(a - b);
  const auto normal_c = (d - c).cross(b - c);
  const auto normal_d = (a - d).cross(c - d);

  const MI vectors_dir a_c_directional = MI vectors_directional(normal_a, normal_c);
  const MI vectors_dir b_d_directional = MI vectors_directional(normal_b, normal_d);

  // Если вектора normal_a и normal_c разнонаправленные,
  // и вектора normal_b и normal_d разнонаправленные,
  // то элемент вогнутый.
  return a_c_directional == MI vectors_dir::multi_directional ||  //
         b_d_directional == MI vectors_dir::multi_directional;
}

MI_NODISCARD inline bool is_concave(const MI Triangle& /* triangle */, const MI Mesh& /* mesh */) { return false; }
}  // namespace mi::internal

namespace mi {
// Блок схема для is_degenerated.
// ==============================
//
//           +~~~~~~~~~~~~~~~~~~~~~+
//           | Начало.             |
//           +~~~~~~~~~~+~~~~~~~~~~+
//                      |
//                      v
//      Да.  +----------+----------+
//    +<----(| Удаленный элемент?  |)
//    |      +----------+----------+
//    |                 | Нет.
//    |                 v
//    |      +----------+----------+ Да.
//    |     (| Вырожден в прямую?  |)-------------->+
//    |      +----------+----------+                |
//    |                 | Нет.                      |
//    |                 v                           |
//    |      +----------+----------+ Да.            v
//    |     (| Вогнутый?           |)-------------->+
//    |      +----------+----------+                |
//    |                 | Нет.                      |
//    |                 v                           v
//    v      +~~~~~~~~~~+~~~~~~~~~~+     +~~~~~~~~~~+~~~~~~~~~~+
//    +----->| Не вырожденный.     |     | Вырожденный.        |
//           +~~~~~~~~~~~~~~~~~~~~~+     +~~~~~~~~~~~~~~~~~~~~~+

// Определяет, вырожденный ли элемент.
MI_NODISCARD inline bool is_degenerated_quad_impl(const MI static_vector<point3d, 4>& vv) {
  bool degenerated = false;

  // if (MI internal::is_deleted_element(quad)) {
  //   degenerated = false;
  // } else {
  if (MI internal::degenerated_into_straight_line_quad_impl(vv)) {
    degenerated = true;
  } else {
    degenerated = !static_cast<bool>(MI internal::is_curved_quad_impl(vv));
  }
  //}

  return degenerated;
}

// Определяет, вырожденный ли элемент.
MI_NODISCARD inline bool is_degenerated(const MI Quad& quad, const MI Mesh& mesh) {
  bool degenerated = false;

  // if (MI internal::is_deleted_element(quad)) {
  //   degenerated = false;
  // } else {
  if (MI internal::degenerated_into_straight_line(quad, mesh)) {
    degenerated = true;
  } else {
    degenerated = !static_cast<bool>(MI internal::is_curved(quad, mesh));
  }
  //}

  return degenerated;
}

// Определяет, вырожденный ли элемент.
MI_NODISCARD inline bool is_degenerated(const MI Triangle& triangle, const MI Mesh& mesh) {
  return internal::degenerated_into_straight_line(triangle, mesh) ||  //
         !internal::is_curved(triangle, mesh);
}

// Определяет, вырожденный ли элемент.
MI_NODISCARD inline bool is_degenerated(const MeshElement* element, const MI Mesh& mesh) {
  if (isa<Quad>(element)) {
    return is_degenerated(*dyn_cast<Quad>(element), mesh);
  } else if (isa<Triangle>(element)) {
    return is_degenerated(*dyn_cast<Triangle>(element), mesh);
  }

  MI_NOT_REACHED();
  return {};
}

// Определяет, вырожденный ли элемент.
MI_NODISCARD inline bool is_degenerated(const MeshElement::Descriptor n, const MI Mesh& mesh) {
  return is_degenerated(mesh.get_element(n), mesh);
}
}  // namespace mi
