// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Base/MI.Casting.hpp>
#include <MI/Common/MI.Contains.h>
#include <MI/Container/MI.SortableBase.hpp>
#include <MI/Mesh/MI.Auxiliary.h>

#include <fmt/format.h>

#include "MeshElement/MI.Triangle.h"

namespace mi {
// -- [vertices] -------------------------------------------------------------------------------------------------------

size_t n_common_vertices(const MeshElement* el0, const MeshElement* el1) {
  MI_DCHECK(ranges::is_unique(el0->m_indexes()));
  MI_DCHECK(ranges::is_unique(el1->m_indexes()));

  size_t n_common_vertices = 0;

  for (const MeshElement::Descriptor v0 : el0->m_indexes()) {
    for (const MeshElement::Descriptor v1 : el1->m_indexes()) {
      if (v0 == v1) {
        ++n_common_vertices;
      }
    }
  }

  return n_common_vertices;
}
size_t n_common_vertices(const Quad& quad_a, const Quad& quad_b) {
  MI_DCHECK(ranges::is_unique(quad_a));
  MI_DCHECK(ranges::is_unique(quad_b));

  size_t n_common_vertices = 0;

  for (size_t nva = 0; nva < Quad::kn_vertices; ++nva) {
    for (size_t nvb = 0; nvb < Quad::kn_vertices; ++nvb) {
      if (quad_a[nva] == quad_b[nvb]) {
        ++n_common_vertices;
      }
    }
  }

  return n_common_vertices;
}
size_t n_common_vertices(const Quad::m_indexes_type& quad_a, const Quad::m_indexes_type& quad_b) {
  MI_DCHECK(ranges::is_unique(quad_a));
  MI_DCHECK(ranges::is_unique(quad_b));

  size_t n_common_vertices = 0;

  for (size_t nva = 0; nva < Quad::kn_vertices; ++nva) {
    for (size_t nvb = 0; nvb < Quad::kn_vertices; ++nvb) {
      if (quad_a[nva] == quad_b[nvb]) {
        ++n_common_vertices;
      }
    }
  }

  return n_common_vertices;
}
size_t n_common_vertices(const Hexahedron& hex_a, const Hexahedron& hex_b) {
  MI_DCHECK(ranges::is_unique(hex_a));
  MI_DCHECK(ranges::is_unique(hex_b));

  size_t n_common_vertices = 0;

  for (size_t nva = 0; nva < Hexahedron::kn_vertices; ++nva) {
    for (size_t nvb = 0; nvb < Hexahedron::kn_vertices; ++nvb) {
      if (hex_a[nva] == hex_b[nvb]) {
        ++n_common_vertices;
      }
    }
  }

  return n_common_vertices;
}

bool has_only_one_common_vertex(const MeshElement* el0, const MeshElement* el1) {
  return n_common_vertices(el0, el1) == 1;
}
bool has_only_one_common_vertex(const Quad& quad_a, const Quad& quad_b) {
  return n_common_vertices(quad_a, quad_b) == 1;
}
bool has_only_one_common_vertex(const Quad::m_indexes_type& quad_a, const Quad::m_indexes_type& quad_b) {
  return n_common_vertices(quad_a, quad_b) == 1;
}
bool has_only_one_common_vertex(const Hexahedron& hex_a, const Hexahedron& hex_b) {
  return n_common_vertices(hex_a, hex_b) == 1;
}

STD vector<MeshElement::Descriptor> get_common_vertices(const MeshElement* element_a, const MeshElement* element_b) {
  STD vector<MI MeshElement::Descriptor> res;

  for (MeshElement::Descriptor descriptor_a : *element_a) {
    if (MI contains(*element_b, descriptor_a)) {
      res.emplace_back(descriptor_a);
    }
  }

  return res;
}

MeshElement::Descriptor get_one_common_vertex(const MeshElement* element_a, const MeshElement* element_b) {
  MI_DCHECK(n_common_vertices(element_a, element_b) == 1);

  MI_DCHECK(ranges::is_unique(element_a->m_indexes()));
  MI_DCHECK(ranges::is_unique(element_b->m_indexes()));

  for (const MeshElement::Descriptor v0 : element_a->m_indexes()) {
    for (const MeshElement::Descriptor v1 : element_b->m_indexes()) {
      if (v0 == v1) {
        return v0;
      }
    }
  }
}

MeshElement::Descriptor get_common_vertex(const Quad::m_indexes_type& quad_a,  //
                                          const Quad::m_indexes_type& quad_b,  //
                                          const Quad::m_indexes_type& quad_c) {
  size_t n_common_vertices = 0;
  Quad::Descriptor common_v;

  for (const Quad::Descriptor index_a : quad_a) {
    for (const Quad::Descriptor index_b : quad_b) {
      for (const Quad::Descriptor index_c : quad_c) {
        if (index_a == index_b && index_b == index_c) {
          ++n_common_vertices;
          common_v = index_a;
        }
      }
    }
  }

  MI_CHECK(n_common_vertices == 1) << "Three quads don't have common v or have several vv. ";

  return common_v;
}

// -- [edges] ----------------------------------------------------------------------------------------------------------

size_t n_common_edges(const Hexahedron& hex_a, const Hexahedron& hex_b) {
  size_t n_common_edges = 0;

  for (size_t n_edge_a = 0; n_edge_a < Hexahedron::kn_edges; ++n_edge_a) {
    const Hexahedron::edge_type edge_a = hex_a.indexes_of_edge(n_edge_a);

    for (size_t n_edge_b = 0; n_edge_b < Hexahedron::kn_edges; ++n_edge_b) {
      const Hexahedron::edge_type edge_b = hex_b.indexes_of_edge(n_edge_b);

      if (edge_a[0] == edge_b[1] &&  //
          edge_a[1] == edge_b[0]) {
        ++n_common_edges;
      }
    }
  }

  return n_common_edges;
}
size_t n_common_edges(const Quad& quad_a, const Quad& quad_b) {
  size_t n_common_edges = 0;

  for (size_t n_edge_a = 0; n_edge_a < Quad::kn_edges; ++n_edge_a) {
    const Quad::edge_type edge_a = quad_a.indexes_of_edge(n_edge_a);

    for (size_t n_edge_b = 0; n_edge_b < Quad::kn_edges; ++n_edge_b) {
      const Quad::edge_type edge_b = quad_b.indexes_of_edge(n_edge_b);

      if (edge_a[0] == edge_b[1] &&  //
          edge_a[1] == edge_b[0]) {
        ++n_common_edges;
      }
    }
  }

  return n_common_edges;
}
size_t n_common_edges(const Quad::m_indexes_type& quad_a, const Quad::m_indexes_type& quad_b) {
  size_t n_common_edges = 0;

  for (size_t n_edge_a = 0; n_edge_a < Quad::kn_edges; ++n_edge_a) {
    const Quad::edge_type edge_a = Quad::get_edge(quad_a, n_edge_a);

    for (size_t n_edge_b = 0; n_edge_b < Quad::kn_edges; ++n_edge_b) {
      const Quad::edge_type edge_b = Quad::get_edge(quad_b, n_edge_b);

      if (edge_a[0] == edge_b[1] &&  //
          edge_a[1] == edge_b[0]) {
        ++n_common_edges;
      }
    }
  }

  return n_common_edges;
}
size_t n_common_edges(const MeshElement* el_a, const MeshElement* el_b) {
  if (isa<Quad>(el_a) && isa<Quad>(el_b)) {
    return has_only_one_common_edge(*dyn_cast<Quad>(el_a), *dyn_cast<Quad>(el_b));
  }

  if (isa<Hexahedron>(el_a) && isa<Hexahedron>(el_b)) {
    return has_only_one_common_edge(*dyn_cast<Hexahedron>(el_a), *dyn_cast<Hexahedron>(el_b));
  }

  MI_FATAL();
}

bool has_only_one_common_edge(const Hexahedron& hex_a, const Hexahedron& hex_b) {
  return n_common_edges(hex_a, hex_b) == 1;
}
bool has_only_one_common_edge(const Quad& quad_a, const Quad& quad_b) { return n_common_edges(quad_a, quad_b) == 1; }
bool has_only_one_common_edge(const Quad::m_indexes_type& quad_a, const Quad::m_indexes_type& quad_b) {
  return n_common_edges(quad_a, quad_b) == 1;
}
bool has_only_one_common_edge(const MeshElement* el_a, const MeshElement* el_b) {
  if (isa<Quad>(el_a) && isa<Quad>(el_b)) {
    return has_only_one_common_edge(*dyn_cast<Quad>(el_a), *dyn_cast<Quad>(el_b));
  }

  if (isa<Hexahedron>(el_a) && isa<Hexahedron>(el_b)) {
    return has_only_one_common_edge(*dyn_cast<Hexahedron>(el_a), *dyn_cast<Hexahedron>(el_b));
  }

  MI_FATAL();
}

bool has_common_edge(const MeshElement* element_a, const MeshElement* element_b) {
  return n_common_edges(element_a, element_b) > 0;
}
bool has_one_common_edge(const MeshElement* element_a, const MeshElement* element_b) {
  return n_common_edges(element_a, element_b) == 1;
}

STD vector<Edge> get_common_edges(const MeshElement* element_a, const MeshElement* element_b) {
  STD vector<Edge> res;

  for (MI MeshElement::Descriptor n_edges_a = 0; n_edges_a < element_a->n_edges(); ++n_edges_a) {
    for (MI MeshElement::Descriptor n_edges_b = 0; n_edges_b < element_b->n_edges(); ++n_edges_b) {
      const MI static_vector<MeshElement::Descriptor, 2> edge_a = element_a->indexes_of_edge(n_edges_a);
      const MI static_vector<MeshElement::Descriptor, 2> edge_b = element_b->indexes_of_edge(n_edges_b);

      if ((edge_a[0] == edge_b[0] && edge_a[1] == edge_b[1]) ||  //
          (edge_a[0] == edge_b[1] && edge_a[1] == edge_b[0])) {
        res.emplace_back(edge_a);
      }
    }
  }

  return res;
}

Hexahedron::edge_type get_one_common_edge(const Hexahedron& hex_a, const Hexahedron& hex_b) {
  Hexahedron::edge_type res;

  for (size_t n_edge_a = 0; n_edge_a < Hexahedron::kn_edges; ++n_edge_a) {
    const Hexahedron::edge_type edge_a = hex_a.indexes_of_edge(n_edge_a);

    for (size_t n_edge_b = 0; n_edge_b < Hexahedron::kn_edges; ++n_edge_b) {
      const Hexahedron::edge_type edge_b = hex_b.indexes_of_edge(n_edge_b);

      if (edge_a[0] == edge_b[1] &&  //
          edge_a[1] == edge_b[0]) {
        res = edge_a;
      }
    }
  }

  MI_CHECK(n_common_edges(hex_a, hex_b) == 1)  //
      << fmt::format("The hex_a({}) and the hex_b({}) has {} common edges.",
                     in_brackets::curly(hex_a.m_indexes()),  //
                     in_brackets::curly(hex_b.m_indexes()),  //
                     n_common_edges(hex_a, hex_b));

  return res;
}

Quad::edge_type get_one_common_edge(const Quad::m_indexes_type& quad_a, const Quad::m_indexes_type& quad_b) {
  Quad::edge_type res;

  for (size_t n_edge_a = 0; n_edge_a < Quad::kn_edges; ++n_edge_a) {
    const Quad::edge_type edge_a = Quad::get_edge(quad_a, n_edge_a);

    for (size_t n_edge_b = 0; n_edge_b < Quad::kn_edges; ++n_edge_b) {
      const Quad::edge_type edge_b = Quad::get_edge(quad_b, n_edge_b);

      if (edge_a[0] == edge_b[1] &&  //
          edge_a[1] == edge_b[0]) {
        res = edge_a;
      }
    }
  }

  MI_CHECK(n_common_edges(quad_a, quad_b) == 1)  //
      << fmt::format("The quad_a({}) and the quad_b({}) has {} common edges.",
                     in_brackets::curly(quad_a),  //
                     in_brackets::curly(quad_b),  //
                     n_common_edges(quad_a, quad_b));

  return res;
}

Quad::edge_type get_one_common_edge(const Quad& quad_a, const Quad& quad_b) {
  Hexahedron::edge_type res;

  for (size_t n_edge_a = 0; n_edge_a < Quad::kn_edges; ++n_edge_a) {
    const Quad::edge_type edge_a = quad_a.indexes_of_edge(n_edge_a);

    for (size_t n_edge_b = 0; n_edge_b < Quad::kn_edges; ++n_edge_b) {
      const Quad::edge_type edge_b = quad_b.indexes_of_edge(n_edge_b);

      if (edge_a[0] == edge_b[1] &&  //
          edge_a[1] == edge_b[0]) {
        res = edge_a;
      }
    }
  }

  MI_CHECK(n_common_edges(quad_a, quad_b) == 1)  //
      << fmt::format("The quad_a({}) and the quad_b({}) has {} common edges.",
                     in_brackets::curly(quad_a.m_indexes()),  //
                     in_brackets::curly(quad_b.m_indexes()),  //
                     n_common_edges(quad_a, quad_b));

  return res;
}

Edge get_one_common_edge(const MeshElement* el_a, const MeshElement* el_b) {
  MI_CHECK(has_one_common_edge(el_a, el_b));

  if (isa<Quad>(el_a) && isa<Quad>(el_b)) {
    return get_one_common_edge(*dyn_cast<Quad>(el_a), *dyn_cast<Quad>(el_b));
  }

  if (isa<Hexahedron>(el_a) && isa<Hexahedron>(el_b)) {
    return get_one_common_edge(*dyn_cast<Hexahedron>(el_a), *dyn_cast<Hexahedron>(el_b));
  }

  MI_FATAL();
}

Edge get_one_sorted_common_edge(const MeshElement* element_a, const MeshElement* element_b) {
  Edge edge = get_one_common_edge(element_a, element_b);

  if (edge[1] > edge[0]) {
    STD swap(edge[1], edge[0]);
  }

  return edge;
}

STD vector<static_vector<MeshElement::Descriptor, 2>> get_edges(const MeshElement* element) {
  STD vector<static_vector<MeshElement::Descriptor, 2>> edges;

  for (MI MeshElement::Descriptor n_edge = 0; n_edge < element->n_edges(); ++n_edge) {
    const auto edge = element->indexes_of_edge(n_edge);
    edges.emplace_back(static_vector<MeshElement::Descriptor, 2>{edge[0], edge[1]});
  }

  return edges;
}
STD vector<sortable<MeshElement::Descriptor, 2>> get_sorted_edges(const MeshElement* element) {
  STD vector<MI sortable<MeshElement::Descriptor, 2>> res;

  for (MI MeshElement::Descriptor n_edge = 0; n_edge < element->n_edges(); ++n_edge) {
    const auto edge = element->indexes_of_edge(n_edge);
    res.emplace_back(MI sortable<MeshElement::Descriptor, 2>{edge[0], edge[1]});
  }

  return res;
}

sortable<MeshElement::Descriptor, 2> get_sorted_edge(const MeshElement* element, const MeshElement::Descriptor n) {
  return MI sortable<MeshElement::Descriptor, 2>{element->indexes_of_edge(n)};
}

sortable<MeshElement::Descriptor, 2> get_sorted_edge(const Triangle& element, const MeshElement::Descriptor n) {
  return MI sortable<MeshElement::Descriptor, 2>{element.indexes_of_edge(n)};
}

bool is_equal_faces(const MeshElement* f0, const MeshElement* f1) {
  MI_CHECK(isa<Quad>(f0));
  MI_CHECK(isa<Quad>(f1));

  auto f0_tmp = dyn_cast<Quad>(f0)->indexes_of_face(0);
  auto f1_tmp = dyn_cast<Quad>(f1)->indexes_of_face(0);

  ranges::sort(f0_tmp);
  ranges::sort(f1_tmp);

  return f0_tmp == f1_tmp;
}

// -- [faces] ----------------------------------------------------------------------------------------------------------
size_t n_common_faces(const MeshElement* element_a, const MeshElement* element_b) {
  size_t n_common_faces = 0;

  for (size_t n_face_a = 0; n_face_a < element_a->n_faces(); ++n_face_a) {
    const auto face_a = element_a->get_face(n_face_a);

    for (size_t n_face_b = 0; n_face_b < element_b->n_faces(); ++n_face_b) {
      const auto face_b = element_b->get_face(n_face_b);

      if (is_equal_faces(face_a.get(), face_b.get())) {
        ++n_common_faces;
      }
    }
  }

  return n_common_faces;
}

bool has_common_face(const MeshElement* element_a, const MeshElement* element_b) {
  return n_common_faces(element_a, element_b) > 0;
}

bool has_only_one_common_face(const MeshElement* el_a, const MeshElement* el_b) {
  return n_common_faces(el_a, el_b) == 1;
}

sortable<MeshElement::Descriptor, 4> sorted_indexes_quad_face(const MeshElement* element,
                                                              const MeshElement::Descriptor n_face) {
  // Любые объемные
  // MI_CHECK(isa<Quad>(element) || isa<Triangle>(element) );
  const auto indexes = element->indexes_of_face(n_face);

  return {indexes.begin(), indexes.end()};
}
// -- [faces] ----------------------------------------------------------------------------------------------------------
bool elements_has_common_edges_and_common_vertex(const MeshElement* element_a, const MeshElement* element_b,
                                                 const MeshElement* element_c) {
  using edge_type = sortable<MeshElement::Descriptor, 2>;

  // 12 хватит для шестигранника и для квада, хотя полностью он не будет никогда заполнен.
  MI static_vector<edge_type, 12> m_edges_a;  // Отсортированные ребра для элемента a
  MI static_vector<edge_type, 12> m_edges_b;  // Отсортированные ребра для элемента b
  MI static_vector<edge_type, 12> m_edges_c;  // Отсортированные ребра для элемента c

  MI static_vector<edge_type, 12> m_edges_ab;  // Общие отсортированные ребра у элементов a & b
  MI static_vector<edge_type, 12> m_edges_ac;  // Общие отсортированные ребра у элементов a & c
  MI static_vector<edge_type, 12> m_edges_bc;  // Общие отсортированные ребра у элементов b & c

  // Заполнить массив отсортированными ребрами.
  for (size_t n_edge = 0; n_edge < element_a->n_edges(); ++n_edge) {
    const auto indexes = element_a->indexes_of_edge(n_edge);
    m_edges_a.emplace_back(indexes[0], indexes[1]);
  }

  for (size_t n_edge = 0; n_edge < element_b->n_edges(); ++n_edge) {
    const auto indexes = element_b->indexes_of_edge(n_edge);
    m_edges_b.emplace_back(indexes[0], indexes[1]);
  }

  for (size_t n_edge = 0; n_edge < element_c->n_edges(); ++n_edge) {
    const auto indexes = element_c->indexes_of_edge(n_edge);
    m_edges_c.emplace_back(indexes[0], indexes[1]);
  }

  // Заполнить массив ребрами, которые являются общими для элементов a & b.
  for (const auto& edge_a : m_edges_a) {
    for (const auto& edge_b : m_edges_b) {
      if (edge_a == edge_b) {
        m_edges_ab.emplace_back(edge_a);
      }
    }
  }

  // Заполнить массив ребрами, которые являются общими для элементов a & c.
  for (const auto& edge_a : m_edges_a) {
    for (const auto& edge_c : m_edges_c) {
      if (edge_a == edge_c) {
        m_edges_ac.emplace_back(edge_a);
      }
    }
  }

  // Заполнить массив ребрами, которые являются общими для элементов b & c.
  for (const auto& edge_b : m_edges_b) {
    for (const auto& edge_c : m_edges_c) {
      if (edge_b == edge_c) {
        m_edges_bc.emplace_back(edge_b);
      }
    }
  }

  for (const auto& edge_ab : m_edges_ab) {
    for (const auto& edge_ac : m_edges_ac) {
      for (const auto& edge_bc : m_edges_bc) {
        const auto u = edge_ab[0];
        const auto v = edge_ab[1];

        // Если у трех общих ребер есть одна общая вершина, т.е. все они исходят из одной вершины.
        if (MI contains(edge_ab, u) &&  //
            MI contains(edge_ac, u) &&  //
            MI contains(edge_bc, u)) {
          return true;
        }

        // Если у трех общих ребер есть одна общая вершина, т.е. все они исходят из одной вершины.
        if (MI contains(edge_ab, v) &&  //
            MI contains(edge_ac, v) &&  //
            MI contains(edge_bc, v)) {
          return true;
        }
      }
    }
  }

  return false;
}
bool two_faces_has_3_common_vertex(const Quad& element_a, const Quad& element_b) {
  const auto indexes_a = element_a.m_indexes();
  const auto indexes_b = element_b.m_indexes();

  MI static_vector<MeshElement::Descriptor, 4> res;

  MI ranges::set_intersection(indexes_a, indexes_b, STD back_inserter(res));

  return res.size() == 3;
}
bool two_faces_has_2_common_vertex(const Quad& element_a, const Quad& element_b) {
  const auto indexes_a = element_a.m_indexes();
  const auto indexes_b = element_b.m_indexes();

  MI static_vector<MeshElement::Descriptor, 4> res;

  MI ranges::set_intersection(indexes_a, indexes_b, STD back_inserter(res));

  return res.size() == 2;
}

}  // namespace mi
