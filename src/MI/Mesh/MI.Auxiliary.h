// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include <MI/Base/MI.BaseExport.hpp>
#include <MI/Base/MI.Property.hpp>
#include <MI/Container/MI.Sortable.hpp>
#include <MI/Container/MI.StaticVector.hpp>
#include <MI/Mesh/MeshElement/MI.Edge.h>
#include <MI/Mesh/MeshElement/MI.Hexahedron.h>
#include <MI/Mesh/MeshElement/MI.Quad.h>

#include "MeshElement/MI.Triangle.h"

namespace mi {
// -- [vertices] -------------------------------------------------------------------------------------------------------

MI_NODISCARD MI_BASE_EXPORT size_t n_common_vertices(const MeshElement* el0, const MeshElement* el1);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_vertices(const Hexahedron& hex_a, const Hexahedron& hex_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_vertices(const Quad& quad_a, const Quad& quad_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_vertices(const Quad::m_indexes_type& quad_a,
                                                     const Quad::m_indexes_type& quad_b);

MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_vertex(const MeshElement* el_a, const MeshElement* el_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_vertex(const Hexahedron& hex_a, const Hexahedron& quad_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_vertex(const Quad& quad_a, const Quad& quad_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_vertex(const Quad::m_indexes_type& quad_a,
                                                            const Quad::m_indexes_type& quad_b);

MI_NODISCARD MI_BASE_EXPORT bool has_common_vertex(const MI MeshElement* element_a, const MI MeshElement* element_b);
MI_NODISCARD MI_BASE_EXPORT bool has_one_common_vertices(const MI MeshElement* element_a,
                                                         const MI MeshElement* element_b);

MI_NODISCARD MI_BASE_EXPORT STD vector<MI MeshElement::Descriptor> get_common_vertices(const MI MeshElement* element_a,
                                                                                       const MI MeshElement* element_b);

// (!) Должна быть только 1 общая вершина.
MI_NODISCARD MI_BASE_EXPORT MI MeshElement::Descriptor get_one_common_vertex(const MI MeshElement* element_a,
                                                                             const MI MeshElement* element_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT Quad::edge_type get_one_common_vertex(const Quad& quad_a, const Quad& quad_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT Hexahedron::edge_type get_one_common_vertex(const Hexahedron& hex_a,
                                                                        const Hexahedron& hex_b);

MI_NODISCARD MI_BASE_EXPORT STD vector<static_vector<MeshElement::Descriptor, 2>> get_vertices(
    const MI MeshElement* element);

MI_NODISCARD MI_BASE_EXPORT MeshElement::Descriptor get_common_vertex(const Quad::m_indexes_type& quad_a,
                                                                      const Quad::m_indexes_type& quad_b,
                                                                      const Quad::m_indexes_type& quad_c);

// -- [edges] ----------------------------------------------------------------------------------------------------------

MI_NODISCARD MI_BASE_EXPORT size_t n_common_edges(const MeshElement* el_a, const MeshElement* el_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_edges(const Hexahedron& hex_a, const Hexahedron& hex_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_edges(const Quad& quad_a, const Quad& quad_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_edges(const Quad::m_indexes_type& quad_a,
                                                  const Quad::m_indexes_type& quad_b);

MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_edge(const MeshElement* el_a, const MeshElement* el_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_edge(const Hexahedron& hex_a, const Hexahedron& quad_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_edge(const Quad& quad_a, const Quad& quad_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_edge(const Quad::m_indexes_type& quad_a,
                                                          const Quad::m_indexes_type& quad_b);

MI_NODISCARD MI_BASE_EXPORT bool has_common_edge(const MI MeshElement* element_a, const MI MeshElement* element_b);
MI_NODISCARD MI_BASE_EXPORT bool has_one_common_edge(const MI MeshElement* element_a, const MI MeshElement* element_b);

// Общие ребра.
// Никак не отсортированы. Если попадутся ребра {a, b} & {b, a}, то добавится первое.
MI_NODISCARD MI_BASE_EXPORT STD vector<Edge> get_common_edges(const MI MeshElement* element_a,
                                                              const MI MeshElement* element_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT Quad::edge_type get_one_common_edge(const Quad::m_indexes_type& quad_a,
                                                                const Quad::m_indexes_type& quad_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT Quad::edge_type get_one_common_edge(const Quad& quad_a, const Quad& quad_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT Hexahedron::edge_type get_one_common_edge(const Hexahedron& hex_a, const Hexahedron& hex_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT Edge get_one_common_edge(const MI MeshElement* element_a, const MI MeshElement* element_b);

// Общее ребро с проверкой того, что оно только одно.
MI_NODISCARD MI_BASE_EXPORT Edge get_one_sorted_common_edge(const MI MeshElement* element_a,
                                                            const MI MeshElement* element_b);

MI_NODISCARD MI_BASE_EXPORT STD vector<static_vector<MeshElement::Descriptor, 2>> get_edges(
    const MI MeshElement* element);

MI_NODISCARD MI_BASE_EXPORT STD vector<MI sortable<MeshElement::Descriptor, 2>> get_sorted_edges(
    const MI MeshElement* element);

MI_NODISCARD MI_BASE_EXPORT MI sortable<MeshElement::Descriptor, 2> get_sorted_edge(const MI MeshElement* element,
                                                                                    const MI MeshElement::Descriptor n);
MI_NODISCARD MI_BASE_EXPORT MI sortable<MeshElement::Descriptor, 2> get_sorted_edge(const MI Triangle& element,
                                                                                    const MI MeshElement::Descriptor n);

// -- [faces] ----------------------------------------------------------------------------------------------------------

MI_NODISCARD MI_BASE_EXPORT size_t n_common_faces(const MeshElement* el_a, const MeshElement* el_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_faces(const Hexahedron& hex_a, const Hexahedron& hex_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_faces(const Quad& quad_a, const Quad& quad_b);
MI_NODISCARD MI_BASE_EXPORT size_t n_common_faces(const Quad::m_indexes_type& quad_a,
                                                  const Quad::m_indexes_type& quad_b);

MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_face(const MeshElement* el_a, const MeshElement* el_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_face(const Hexahedron& hex_a, const Hexahedron& quad_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_face(const Quad& quad_a, const Quad& quad_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_face(const Quad::m_indexes_type& quad_a,
                                                          const Quad::m_indexes_type& quad_b);

MI_NODISCARD MI_BASE_EXPORT bool has_common_face(const MI MeshElement* element_a, const MI MeshElement* element_b);
MI_NODISCARD MI_BASE_EXPORT bool has_only_one_common_face(const MI MeshElement* element_a,
                                                          const MI MeshElement* element_b);

// Общие ребра.
// Никак не отсортированы. Если попадутся ребра {a, b} & {b, a}, то добавится первое.
MI_NODISCARD MI_BASE_EXPORT void get_common_faces(const MI MeshElement* element_a, const MI MeshElement* element_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT void get_one_common_face(const Quad::m_indexes_type& quad_a,
                                                     const Quad::m_indexes_type& quad_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT void get_one_common_face(const Quad& quad_a, const Quad& quad_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT void get_one_common_face(const Hexahedron& hex_a, const Hexahedron& hex_b);

// (!) Должно быть только 1 общее ребро.
MI_NODISCARD MI_BASE_EXPORT void get_one_common_face(const MI MeshElement* element_a, const MI MeshElement* element_b);

// Общее ребро с проверкой того, что оно только одно.
MI_NODISCARD MI_BASE_EXPORT void get_one_sorted_common_face(const MI MeshElement* element_a,
                                                            const MI MeshElement* element_b);

MI_NODISCARD MI_BASE_EXPORT void get_faces(const MI MeshElement* element);

MI_NODISCARD MI_BASE_EXPORT void get_sorted_faces(const MI MeshElement* element);

MI_NODISCARD MI_BASE_EXPORT void get_sorted_face(const MI MeshElement* element, const MI MeshElement::Descriptor n);

MI_NODISCARD MI_BASE_EXPORT bool elements_has_common_edges_and_common_vertex(const MI MeshElement* element_a,
                                                                             const MI MeshElement* element_b,
                                                                             const MI MeshElement* element_c);

// 4 индекса грани номер 'n_face', отсортированные по возрастанию.
MI_NODISCARD MI_BASE_EXPORT MI sortable<MI MeshElement::Descriptor, 4> sorted_indexes_quad_face(
    const MI MeshElement* element, const MI MeshElement::Descriptor n_face);

MI_NODISCARD MI_BASE_EXPORT bool two_faces_has_3_common_vertex(const MI Quad& element_a, const MI Quad& element_b);

MI_NODISCARD MI_BASE_EXPORT bool two_faces_has_2_common_vertex(const MI Quad& element_a, const MI Quad& element_b);
}  // namespace mi
