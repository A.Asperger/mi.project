#pragma once

#include "MI.Point.h"

namespace mi {
enum class vectors_dir {
  co_directional,     // Сонаправленные вектора.
  orthogonal,         // Ортогональные (перпендикулярные).
  multi_directional,  // Разнонаправленные.
};

// Функция, которая определяет, как относительно друг друга направлены два вектора.
// Всего может быть 3 варианта.
// 1. vectors_dir::co_directional     -> вектора сонаправленные,
// 2. vectors_dir::orthogonal         -> вектора ортогональны (перпендикулярны),
// 3. vectors_dir::multi_directional  -> вектора разнонаправленные.
//
// Функция будет работать намного быстрее, чем MI angle_between_normals, потому что она:
// * избегает вычисления арккосинуса,
// * избегает нормализации векторов.
inline vectors_dir vectors_directional(const MI point3d& vector_a, const MI point3d& vector_b) {
  const auto dot_result = vector_a.dot(vector_b);
  vectors_dir result;

  if (dot_result > 0.) {
    result = vectors_dir::co_directional;
  } else if (dot_result < 0.) {
    result = vectors_dir::multi_directional;
  } else {
    result = vectors_dir::orthogonal;
  }

  return result;
}
}  // namespace mi
