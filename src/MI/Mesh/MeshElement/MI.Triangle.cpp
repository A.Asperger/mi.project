// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include "MI/Mesh/MeshElement/MI.Triangle.h"

#include <MI/Common/MI.InBrackets.h>

namespace mi {

Triangle::Triangle() : Shell(MeshElementKind::Triangle) {}

Triangle::Triangle(const std::initializer_list<Descriptor> init_list)
    : Shell(MeshElementKind::Triangle), _m_global_indexes(init_list) {}

bool Triangle::class_of(const MeshElement* base) { return InheritsFromTriangle(base->get_kind()); }

MeshElement::size_type Triangle::n_vertices() const { return 3; }

MeshElement::size_type Triangle::n_edges() const { return 3; }

MeshElement::size_type Triangle::n_vertices_in_edge() const { return 2; }

MeshElement::size_type Triangle::n_faces() const { return 1; }

MeshElement::size_type Triangle::n_vertices_in_face() const { return 3; }

mi::span<MeshElement::Descriptor> Triangle::m_indexes() { return MI make_span(_m_global_indexes); }

mi::span<const MeshElement::Descriptor> Triangle::m_indexes() const { return MI make_span(_m_global_indexes); }

MeshElement::Descriptor Triangle::index(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor Triangle::index(const size_type n) { return _m_global_indexes[n]; }

const MeshElement::Descriptor& Triangle::index_ref(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Triangle::index_ref(const size_type n) { return _m_global_indexes[n]; }

mi::static_vector<MeshElement::Descriptor, 2> Triangle::indexes_of_edge(const size_type n_edge) const {
  MI_DCHECK(n_edge < n_edges());

  const auto global_index_0 = _m_global_indexes[local_edges[n_edge][0]];
  const auto global_index_1 = _m_global_indexes[local_edges[n_edge][1]];

  return static_vector{global_index_0, global_index_1};
}

mi::static_vector<MeshElement::Descriptor, 4> Triangle::indexes_of_face(const size_type n_face) const {
  MI_DCHECK(n_face < n_faces());

  const auto global_index_0 = _m_global_indexes[local_faces[n_face][0]];
  const auto global_index_1 = _m_global_indexes[local_faces[n_face][1]];
  const auto global_index_2 = _m_global_indexes[local_faces[n_face][2]];

  return MI static_vector<Descriptor, 4>{global_index_0, global_index_1, global_index_2};
}

STD unique_ptr<MeshElement> Triangle::get_edge(const size_type n) const {
  return STD make_unique<Edge>(indexes_of_edge(n));
}

STD unique_ptr<MeshElement> Triangle::get_face(const size_type n) const {
  return STD make_unique<Triangle>(indexes_of_face(n));
}

MeshElement::const_iterator Triangle::begin() const { return _m_global_indexes.begin(); }

MeshElement::iterator Triangle::begin() { return _m_global_indexes.begin(); }

MeshElement::const_iterator Triangle::end() const { return _m_global_indexes.end(); }

MeshElement::iterator Triangle::end() { return _m_global_indexes.end(); }

const MeshElement::Descriptor& Triangle::operator[](const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Triangle::operator[](const size_type n) { return _m_global_indexes[n]; }

std::string Triangle::str() const {
  STD stringstream ss;

  //"triangle = { 1, 2, 3 }"
  ss << ShellKindName(get_kind()) << " = "
     << in_brackets::curly(_m_global_indexes[0], _m_global_indexes[1], _m_global_indexes[2]);

  return ss.rdbuf()->str();
}

static_vector<MeshElement::Descriptor, 3> Triangle::_m_indexes() { return _m_global_indexes; }

static_vector<MeshElement::Descriptor, 3> Triangle::_m_indexes() const { return _m_global_indexes; }
}  // namespace mi
