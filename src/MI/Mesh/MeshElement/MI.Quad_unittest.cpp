// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Mesh/MI.Mesh.hpp"
#include "MI/Mesh/MeshElement/MI.Quad.h"

namespace mi::test {
TEST(Quad, Constructor) {
  const MI Quad q({1, 2, 3, 4});

  EXPECT_THAT(q.m_indexes(), testing::ElementsAre(1, 2, 3, 4));
}

TEST(Quad, NVertices) {
  const MI Quad q({1, 2, 3, 4});

  ASSERT_EQ(q.n_vertices(), 4);
}

TEST(Quad, NEdges) {
  const MI Quad q({1, 2, 3, 4});

  ASSERT_EQ(q.n_edges(), 4);
}

TEST(Quad, NFaces) {
  const MI Quad q({1, 2, 3, 4});

  ASSERT_EQ(q.n_faces(), 1);
}

TEST(Quad, NVerticesInEdge) {
  const MI Quad q({1, 2, 3, 4});

  ASSERT_EQ(q.n_vertices_in_edge(), 2);
}

TEST(Quad, NVerticesInFace) {
  const MI Quad q({1, 2, 3, 4});

  ASSERT_EQ(q.n_vertices_in_face(), 4);
}

TEST(Quad, MGlobalIndexes) {
  const MI Quad q({1, 2, 3, 4});
  MI Quad q1({2, 3, 4, 5});

  EXPECT_THAT(q.m_indexes(), testing::ElementsAre(1, 2, 3, 4));
  EXPECT_THAT(q1.m_indexes(), testing::ElementsAre(2, 3, 4, 5));
}

TEST(Quad, GlobalIndex) {
  const MI Quad q({1, 2, 3, 4});
  MI Quad q1({2, 3, 4, 5});

  EXPECT_EQ(q.index(0), 1);
  EXPECT_EQ(q.index(1), 2);
  EXPECT_EQ(q.index(2), 3);
  EXPECT_EQ(q.index(3), 4);

  EXPECT_EQ(q1.index(0), 2);
  EXPECT_EQ(q1.index(1), 3);
  EXPECT_EQ(q1.index(2), 4);
  EXPECT_EQ(q1.index(3), 5);

  q1.index_ref(0) = 10;
  q1.index_ref(1) = 20;
  q1.index_ref(2) = 30;
  q1.index_ref(3) = 40;

  EXPECT_EQ(q1.index(0), 10);
  EXPECT_EQ(q1.index(1), 20);
  EXPECT_EQ(q1.index(2), 30);
  EXPECT_EQ(q1.index(3), 40);
}

TEST(Quad, Edge) {
  const MI Quad q({1, 2, 3, 4});

  EXPECT_THAT(q.indexes_of_edge(0), testing::ElementsAre(1, 2));
  EXPECT_THAT(q.indexes_of_edge(1), testing::ElementsAre(2, 3));
  EXPECT_THAT(q.indexes_of_edge(2), testing::ElementsAre(3, 4));
  EXPECT_THAT(q.indexes_of_edge(3), testing::ElementsAre(4, 1));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.indexes_of_edge(4));
}

TEST(Quad, MEdges) {
  const MI Quad q({1, 2, 3, 4});

  // ASSERT_EQ(q.m_edges()[0], (mi::internal::traits<MI Quad>::edge_type{1, 2}));
  // ASSERT_EQ(q.m_edges()[1], (mi::internal::traits<MI Quad>::edge_type{2, 3}));
  // ASSERT_EQ(q.m_edges()[2], (mi::internal::traits<MI Quad>::edge_type{3, 4}));
  // ASSERT_EQ(q.m_edges()[3], (mi::internal::traits<MI Quad>::edge_type{4, 1}));
  //
  // MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.m_edges()[4]);
}

TEST(Quad, SortedEdge) {
  const MI Quad q({1, 2, 3, 4});

  // EXPECT_THAT(q.sorted_edge(0), testing::ElementsAre(1, 2));
  // EXPECT_THAT(q.sorted_edge(1), testing::ElementsAre(2, 3));
  // EXPECT_THAT(q.sorted_edge(2), testing::ElementsAre(3, 4));
  // EXPECT_THAT(q.sorted_edge(3), testing::ElementsAre(1, 4));
  //
  // MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.sorted_edge(5));
}

TEST(Quad, MSortedEdges) {
  const MI Quad q({1, 2, 3, 4});

  // EXPECT_THAT(q.m_sorted_edges()[0], testing::ElementsAre(1, 2));
  // EXPECT_THAT(q.m_sorted_edges()[1], testing::ElementsAre(2, 3));
  // EXPECT_THAT(q.m_sorted_edges()[2], testing::ElementsAre(3, 4));
  // EXPECT_THAT(q.m_sorted_edges()[3], testing::ElementsAre(1, 4));
  //
  // MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.m_sorted_edges()[4]);
}

TEST(Quad, Face) {
  // const MI Quad q({1, 2, 3, 4});
  //
  // ASSERT_EQ(q.face(0), q.m_faces()[0]);
  //
  // MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.face(1));
}

TEST(Quad, MFaces) {
  // const MI Quad q({1, 2, 3, 4});
  //
  // ASSERT_EQ(q.m_faces()[0], (MI internal::traits<MI Quad>::face_type{1, 2, 3, 4}));
  // MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.m_faces()[1]);
}

TEST(Quad, SortedFace) {
  // const MI Quad q({4, 3, 2, 1});
  //
  // EXPECT_EQ(q.sorted_face(0), (MI internal::traits<MI Quad>::sorted_face_type({1, 2, 3, 4})));
  //
  // MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.sorted_face(1));
}

TEST(Quad, MSortedFaces) {
  // const MI Quad q({1, 2, 3, 4});
  //
  // EXPECT_THAT((q.m_sorted_faces()[0]), testing::ElementsAre(1, 2, 3, 4));
  //
  // MI_EXPECT_DCHECK_DEATH((MI_DISABLE_4834)q.m_sorted_faces()[1]);
}
}  // namespace mi::test
