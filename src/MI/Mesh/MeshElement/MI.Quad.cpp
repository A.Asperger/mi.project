// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include "MI/Mesh/MeshElement/MI.Quad.h"

#include <MI/Common/MI.InBrackets.h>

namespace mi {

Quad::edge_type Quad::get_edge(const m_indexes_type& quad_vertices, const Descriptor n_edge) {
  MI_CHECK(quad_vertices.size() == kn_vertices);
  MI_CHECK(n_edge < kn_edges);

  const edge_type local_edge = local_edges[n_edge];

  const Descriptor local_v0 = local_edge[0];
  const Descriptor local_v1 = local_edge[1];

  return edge_type{quad_vertices[local_v0], quad_vertices[local_v1]};
}

Quad::face_type Quad::get_face(const m_indexes_type& quad_vertices, const Descriptor n_face) {
  MI_CHECK(quad_vertices.size() == kn_vertices);
  MI_CHECK(n_face < kn_faces);

  const face_type local_face = local_faces[n_face];

  const Descriptor local_v0 = local_face[0];
  const Descriptor local_v1 = local_face[1];
  const Descriptor local_v2 = local_face[2];
  const Descriptor local_v3 = local_face[3];

  return face_type{quad_vertices[local_v0], quad_vertices[local_v1], quad_vertices[local_v2], quad_vertices[local_v3]};
}

Quad::Quad() : Shell(MeshElementKind::Quad) {}

Quad::Quad(const std::initializer_list<Descriptor> init_list)
    : Shell(MeshElementKind::Quad), _m_global_indexes(init_list) {}

Quad::Quad(const Descriptor v0, const Descriptor v1, const Descriptor v2, const Descriptor v3)
    : Shell(MeshElementKind::Quad), _m_global_indexes({v0, v1, v2, v3}) {}

bool Quad::class_of(const MeshElement* base) { return InheritsFromQuad(base->get_kind()); }

MeshElement::size_type Quad::n_vertices() const { return 4; }

MeshElement::size_type Quad::n_edges() const { return 4; }

MeshElement::size_type Quad::n_vertices_in_edge() const { return 2; }

MeshElement::size_type Quad::n_faces() const { return 1; }

MeshElement::size_type Quad::n_vertices_in_face() const { return 4; }

span<MeshElement::Descriptor> Quad::m_indexes() { return MI make_span(_m_global_indexes); }

span<const MeshElement::Descriptor> Quad::m_indexes() const { return MI make_span(_m_global_indexes); }

MeshElement::Descriptor Quad::index(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor Quad::index(const size_type n) { return _m_global_indexes[n]; }

const MeshElement::Descriptor& Quad::index_ref(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Quad::index_ref(const size_type n) { return _m_global_indexes[n]; }

static_vector<MeshElement::Descriptor, 2> Quad::indexes_of_edge(const size_type n_edge) const {
  MI_DCHECK(n_edge < n_edges());

  const auto global_index_0 = _m_global_indexes[local_edges[n_edge][0]];
  const auto global_index_1 = _m_global_indexes[local_edges[n_edge][1]];

  return MI static_vector{global_index_0, global_index_1};
}

static_vector<MeshElement::Descriptor, 4> Quad::indexes_of_face(const size_type n_face) const {
  MI_DCHECK(n_face < n_faces());

  const auto global_index_0 = _m_global_indexes[local_faces[n_face][0]];
  const auto global_index_1 = _m_global_indexes[local_faces[n_face][1]];
  const auto global_index_2 = _m_global_indexes[local_faces[n_face][2]];
  const auto global_index_3 = _m_global_indexes[local_faces[n_face][3]];

  return MI static_vector{global_index_0, global_index_1, global_index_2, global_index_3};
}

STD unique_ptr<MeshElement> Quad::get_edge(const size_type n) const {
  return STD make_unique<Edge>(indexes_of_edge(n));
}

STD unique_ptr<MeshElement> Quad::get_face(const size_type n) const {
  return STD make_unique<Quad>(indexes_of_face(n));
}

MeshElement::const_iterator Quad::begin() const { return _m_global_indexes.begin(); }

MeshElement::iterator Quad::begin() { return _m_global_indexes.begin(); }

MeshElement::const_iterator Quad::end() const { return _m_global_indexes.end(); }

MeshElement::iterator Quad::end() { return _m_global_indexes.end(); }

const MeshElement::Descriptor& Quad::operator[](const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Quad::operator[](const size_type n) { return _m_global_indexes[n]; }

std::string Quad::str() const {
  STD stringstream ss;

  // "quad"
  ss << ShellKindName(get_kind()) << " = "
     << in_brackets::curly(_m_global_indexes[0], _m_global_indexes[1], _m_global_indexes[2], _m_global_indexes[3]);

  return ss.rdbuf()->str();
}
}  // namespace mi
