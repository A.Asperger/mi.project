#pragma once

#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Mesh/MeshElement/MI.MeshElement.h"

namespace mi {
class MI_BASE_EXPORT Shell : public MeshElement {
  public:
    ~Shell() override = default;

  public:
    Shell(const Shell&)            = default;
    Shell& operator=(const Shell&) = default;

  protected:
    Shell(MeshElementKind kind) : MeshElement(kind) {}

  public:
    static auto class_of(const MeshElement* node) -> bool { return InheritsFromShell(node->get_kind()); }

    auto get_kind() const -> ShellKind { return static_cast<ShellKind>(root_kind()); }
};
}  // namespace mi
