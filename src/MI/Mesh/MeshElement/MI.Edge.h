// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once
#include "MI.Shell.h"
#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Common/MI.TemplateUtil.hpp"
#include "MI/Container/MI.Span.hpp"

namespace mi {
class MI_BASE_EXPORT Edge : public Shell {
  public:
    Edge();

    Edge(const STD initializer_list<Descriptor> init_list);

    Edge(const Descriptor a, const Descriptor b);

    template <class Container, MI if_t<MI IsContainer<Container> && !IsInitList<Container>> = 0>
    Edge(const Container& cont) : Shell(MeshElementKind::Edge), _m_global_indexes(cont.begin(), cont.end()) {}

  public:
    static bool class_of(const MeshElement* base);

  public:
    MI_NODISCARD size_type n_vertices() const override;
    MI_NODISCARD size_type n_edges() const override;
    MI_NODISCARD size_type n_vertices_in_edge() const override;
    MI_NODISCARD size_type n_faces() const override;
    MI_NODISCARD size_type n_vertices_in_face() const override;

  public:
    MI_NODISCARD MI span<Descriptor> m_indexes() override;
    MI_NODISCARD MI span<const Descriptor> m_indexes() const override;

  public:
    MI_NODISCARD Descriptor index(const size_type n) const override;
    MI_NODISCARD Descriptor index(const size_type n) override;

  public:
    MI_NODISCARD const Descriptor& index_ref(const size_type n) const override;
    MI_NODISCARD Descriptor& index_ref(const size_type n) override;

  public:
    MI_NODISCARD MI static_vector<Descriptor, 2> indexes_of_edge(const size_type n_edge) const override;
    MI_NODISCARD MI static_vector<Descriptor, 4> indexes_of_face(const size_type /* n_face */) const override;

  public:
    MI_NODISCARD STD unique_ptr<MeshElement> get_edge(const size_type n) const override;
    MI_NODISCARD STD unique_ptr<MeshElement> get_face(const size_type /* n */) const override;

  public:
    MI_NODISCARD const_iterator begin() const override;
    MI_NODISCARD iterator begin() override;

  public:
    MI_NODISCARD const_iterator end() const override;
    MI_NODISCARD iterator end() override;

  public:
    MI_NODISCARD const Descriptor& operator[](const size_type n) const override;
    MI_NODISCARD Descriptor& operator[](const size_type n) override;

  public:
    MI_NODISCARD std::string str() const override;

  private:
    MI static_vector<Descriptor, 2> _m_global_indexes;
};
}  // namespace mi
