// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Mesh/MeshElement/MI.Hexahedron.h"

namespace mi::test {

TEST(Hexahedron, Constructor) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  EXPECT_THAT(q.m_indexes(), testing::ElementsAre(1, 2, 3, 4, 5, 6, 7, 8));
}

TEST(Hexahedron, NVertices) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  ASSERT_EQ(q.n_vertices(), 8);
}

TEST(Hexahedron, NEdges) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  ASSERT_EQ(q.n_edges(), 12);
}

TEST(Hexahedron, NFaces) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  ASSERT_EQ(q.n_faces(), 6);
}

TEST(Hexahedron, NVerticesInEdge) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  ASSERT_EQ(q.n_vertices_in_edge(), 2);
}

TEST(Hexahedron, NVerticesInFace) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  ASSERT_EQ(q.n_vertices_in_face(), 4);
}

TEST(Hexahedron, MIndexes) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});
  MI Hexahedron q1({2, 3, 4, 5, 6, 7, 8, 9});

  EXPECT_THAT(q.m_indexes(), testing::ElementsAre(1, 2, 3, 4, 5, 6, 7, 8));
  EXPECT_THAT(q1.m_indexes(), testing::ElementsAre(2, 3, 4, 5, 6, 7, 8, 9));
}

TEST(Hexahedron, Index) {
  MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  EXPECT_EQ(q.index(0), 1);
  EXPECT_EQ(q.index(1), 2);
  EXPECT_EQ(q.index(2), 3);
  EXPECT_EQ(q.index(3), 4);
  EXPECT_EQ(q.index(4), 5);
  EXPECT_EQ(q.index(5), 6);
  EXPECT_EQ(q.index(6), 7);
  EXPECT_EQ(q.index(7), 8);

  q.index_ref(0) = 10;
  q.index_ref(1) = 20;
  q.index_ref(2) = 30;
  q.index_ref(3) = 40;
  q.index_ref(4) = 50;
  q.index_ref(5) = 60;
  q.index_ref(6) = 70;
  q.index_ref(7) = 80;

  EXPECT_EQ(q.index(0), 10);
  EXPECT_EQ(q.index(1), 20);
  EXPECT_EQ(q.index(2), 30);
  EXPECT_EQ(q.index(3), 40);
  EXPECT_EQ(q.index(4), 50);
  EXPECT_EQ(q.index(5), 60);
  EXPECT_EQ(q.index(6), 70);
  EXPECT_EQ(q.index(7), 80);
}

TEST(Hexahedron, GetEdge) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  EXPECT_THAT(q.get_edge(0)->m_indexes(), testing::ElementsAre(1, 2));
  EXPECT_THAT(q.get_edge(1)->m_indexes(), testing::ElementsAre(2, 3));
  EXPECT_THAT(q.get_edge(2)->m_indexes(), testing::ElementsAre(3, 4));
  EXPECT_THAT(q.get_edge(3)->m_indexes(), testing::ElementsAre(4, 1));
  EXPECT_THAT(q.get_edge(4)->m_indexes(), testing::ElementsAre(1, 5));
  EXPECT_THAT(q.get_edge(5)->m_indexes(), testing::ElementsAre(2, 6));
  EXPECT_THAT(q.get_edge(6)->m_indexes(), testing::ElementsAre(3, 7));
  EXPECT_THAT(q.get_edge(7)->m_indexes(), testing::ElementsAre(4, 8));
  EXPECT_THAT(q.get_edge(8)->m_indexes(), testing::ElementsAre(5, 6));
  EXPECT_THAT(q.get_edge(9)->m_indexes(), testing::ElementsAre(6, 7));
  EXPECT_THAT(q.get_edge(10)->m_indexes(), testing::ElementsAre(7, 8));
  EXPECT_THAT(q.get_edge(11)->m_indexes(), testing::ElementsAre(8, 5));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.get_edge(12));
}

TEST(Hexahedron, GetFace) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  EXPECT_THAT(q.get_face(0)->m_indexes(), testing::ElementsAre(1, 4, 3, 2));
  EXPECT_THAT(q.get_face(1)->m_indexes(), testing::ElementsAre(1, 2, 6, 5));
  EXPECT_THAT(q.get_face(2)->m_indexes(), testing::ElementsAre(2, 3, 7, 6));
  EXPECT_THAT(q.get_face(3)->m_indexes(), testing::ElementsAre(3, 4, 8, 7));
  EXPECT_THAT(q.get_face(4)->m_indexes(), testing::ElementsAre(4, 1, 5, 8));
  EXPECT_THAT(q.get_face(5)->m_indexes(), testing::ElementsAre(5, 6, 7, 8));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.get_face(6));
}

TEST(Hexahedron, IndexesOfEdge) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  EXPECT_THAT(q.indexes_of_edge(0), testing::ElementsAre(1, 2));
  EXPECT_THAT(q.indexes_of_edge(1), testing::ElementsAre(2, 3));
  EXPECT_THAT(q.indexes_of_edge(2), testing::ElementsAre(3, 4));
  EXPECT_THAT(q.indexes_of_edge(3), testing::ElementsAre(4, 1));
  EXPECT_THAT(q.indexes_of_edge(4), testing::ElementsAre(1, 5));
  EXPECT_THAT(q.indexes_of_edge(5), testing::ElementsAre(2, 6));
  EXPECT_THAT(q.indexes_of_edge(6), testing::ElementsAre(3, 7));
  EXPECT_THAT(q.indexes_of_edge(7), testing::ElementsAre(4, 8));
  EXPECT_THAT(q.indexes_of_edge(8), testing::ElementsAre(5, 6));
  EXPECT_THAT(q.indexes_of_edge(9), testing::ElementsAre(6, 7));
  EXPECT_THAT(q.indexes_of_edge(10), testing::ElementsAre(7, 8));
  EXPECT_THAT(q.indexes_of_edge(11), testing::ElementsAre(8, 5));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.indexes_of_edge(12));
}

TEST(Hexahedron, IndexesOfFace) {
  const MI Hexahedron q({1, 2, 3, 4, 5, 6, 7, 8});

  EXPECT_THAT(q.indexes_of_face(0), testing::ElementsAre(1, 4, 3, 2));
  EXPECT_THAT(q.indexes_of_face(1), testing::ElementsAre(1, 2, 6, 5));
  EXPECT_THAT(q.indexes_of_face(2), testing::ElementsAre(2, 3, 7, 6));
  EXPECT_THAT(q.indexes_of_face(3), testing::ElementsAre(3, 4, 8, 7));
  EXPECT_THAT(q.indexes_of_face(4), testing::ElementsAre(4, 1, 5, 8));
  EXPECT_THAT(q.indexes_of_face(5), testing::ElementsAre(5, 6, 7, 8));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.indexes_of_face(6));
}

}  // namespace mi::test
