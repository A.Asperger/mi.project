// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#ifndef MI_MESHELEMENT_RTTI_TXT_
#define MI_MESHELEMENT_RTTI_TXT_

#include <MI/Base/MI.BaseExport.hpp>

#include <string_view>

namespace mi {

enum class MeshElementKind {
  Edge       = 0,
  Triangle   = 1,
  Quad       = 2,
  Hexahedron = 3,
};

MI_BASE_EXPORT std::string_view MeshElementKindName(MeshElementKind k);

enum class ShellKind {
  Edge     = 0,
  Triangle = 1,
  Quad     = 2,
};

MI_BASE_EXPORT std::string_view ShellKindName(ShellKind k);

inline bool InheritsFromShell(MeshElementKind kind) {
  return kind >= MeshElementKind::Edge && kind < MeshElementKind::Hexahedron;
}

inline bool InheritsFromEdge(MeshElementKind kind) { return kind == MeshElementKind::Edge; }

inline bool InheritsFromTriangle(MeshElementKind kind) { return kind == MeshElementKind::Triangle; }

inline bool InheritsFromQuad(MeshElementKind kind) { return kind == MeshElementKind::Quad; }

enum class SolidKind {
  Hexahedron = 3,
};

MI_BASE_EXPORT std::string_view SolidKindName(SolidKind k);

inline bool InheritsFromSolid(MeshElementKind kind) { return kind >= MeshElementKind::Hexahedron; }

inline bool InheritsFromHexahedron(MeshElementKind kind) { return kind == MeshElementKind::Hexahedron; }

}  // namespace mi

#endif  // MI_MESHELEMENT_RTTI_TXT_
