// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Mesh/MeshElement/MI.Edge.h"
#include "MI/Mesh/MeshElement/MI.Shell.h"

namespace mi {
class MI_BASE_EXPORT Triangle final : public Shell {
  public:
    using edge_type      = MI static_vector<Descriptor, 2>;
    using face_type      = MI static_vector<Descriptor, 3>;
    using m_indexes_type = MI static_vector<Descriptor, 3>;

  public:
    static constexpr STD integral_constant<Descriptor, 3> kn_vertices;
    static constexpr STD integral_constant<Descriptor, 3> kn_edges;
    static constexpr STD integral_constant<Descriptor, 1> kn_faces;
    static constexpr STD integral_constant<Descriptor, 2> kn_vertices_in_edge;
    static constexpr STD integral_constant<Descriptor, 3> kn_vertices_in_face;

  public:
    inline static MI static_vector<MI static_vector<Descriptor, 2>, 3> local_edges = {
        {0, 1},
        {1, 2},
        {2, 0},
    };

    inline static MI static_vector<MI static_vector<Descriptor, 4>, 4> local_faces = {
        {0, 1, 2},
    };

  public:
    Triangle();

    Triangle(const STD initializer_list<Descriptor> init_list);

    Triangle(Descriptor a, Descriptor b, Descriptor c)
        : Shell(MeshElementKind::Triangle), _m_global_indexes({a, b, c}) {}

    template <class Container, MI if_t<MI IsContainer<Container> && !IsInitList<Container>> = 0>
    Triangle(const Container& cont) : Shell(MeshElementKind::Triangle), _m_global_indexes(cont.begin(), cont.end()) {}

  public:
    static bool class_of(const MeshElement* base);

  public:
    MI_NODISCARD size_type n_vertices() const override;

    MI_NODISCARD size_type n_edges() const override;

    MI_NODISCARD size_type n_vertices_in_edge() const override;

    MI_NODISCARD size_type n_faces() const override;

    MI_NODISCARD size_type n_vertices_in_face() const override;

  public:
    MI_NODISCARD MI span<Descriptor> m_indexes() override;
    MI_NODISCARD MI span<const Descriptor> m_indexes() const override;

  public:
    MI_NODISCARD Descriptor index(const size_type n) const override;
    MI_NODISCARD Descriptor index(const size_type n) override;

  public:
    MI_NODISCARD const Descriptor& index_ref(const size_type n) const override;
    MI_NODISCARD Descriptor& index_ref(const size_type n) override;

    MI_NODISCARD MI static_vector<Descriptor, 2> indexes_of_edge(const size_type n_edge) const override;
    MI_NODISCARD MI static_vector<Descriptor, 4> indexes_of_face(const size_type n_face) const override;

  public:
    MI_NODISCARD STD unique_ptr<MeshElement> get_edge(const size_type n) const override;
    MI_NODISCARD STD unique_ptr<MeshElement> get_face(const size_type n) const override;

  public:
    MI_NODISCARD const_iterator begin() const override;
    MI_NODISCARD iterator begin() override;

  public:
    MI_NODISCARD const_iterator end() const override;
    MI_NODISCARD iterator end() override;

  public:
    MI_NODISCARD const Descriptor& operator[](const size_type n) const override;
    MI_NODISCARD Descriptor& operator[](const size_type n) override;

  public:
    MI_NODISCARD std::string str() const override;

  public:
    MI_NODISCARD MI static_vector<Descriptor, 3> _m_indexes();
    MI_NODISCARD MI static_vector<Descriptor, 3> _m_indexes() const;

  public:
    bool operator==(const Triangle& rhs) const { return _m_global_indexes == rhs._m_global_indexes; }
    bool operator!=(const Triangle& rhs) const { return !(*this == rhs); }

  private:
    MI static_vector<Descriptor, 3> _m_global_indexes;
};

}  // namespace mi
