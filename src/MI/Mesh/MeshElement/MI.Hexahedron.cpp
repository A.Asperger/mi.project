// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include "MI/Mesh/MeshElement/MI.Hexahedron.h"

#include "MI.Quad.h"
#include "MI/Common/MI.InBrackets.h"

namespace mi {

Hexahedron::edge_type Hexahedron::get_edge(const m_indexes_type& hex_vertices, const Descriptor n_edge) {
  MI_CHECK(hex_vertices.size() == kn_vertices);
  MI_CHECK(n_edge < kn_edges);

  const edge_type local_edge = local_edges[n_edge];

  const Descriptor local_v0 = local_edge[0];
  const Descriptor local_v1 = local_edge[1];

  return edge_type{hex_vertices[local_v0], hex_vertices[local_v1]};
}

Hexahedron::face_type Hexahedron::get_face(const m_indexes_type& hex_vertices, const Descriptor n_face) {
  MI_CHECK(hex_vertices.size() == kn_vertices);
  MI_CHECK(n_face < kn_faces);

  const face_type local_face = local_faces[n_face];

  const Descriptor local_v0 = local_face[0];
  const Descriptor local_v1 = local_face[1];
  const Descriptor local_v2 = local_face[2];
  const Descriptor local_v3 = local_face[3];

  return face_type{hex_vertices[local_v0], hex_vertices[local_v1], hex_vertices[local_v2], hex_vertices[local_v3]};
}

Hexahedron::Hexahedron() : Solid(MeshElementKind::Hexahedron) {}

Hexahedron::Hexahedron(const STD initializer_list<Descriptor> init_list)
    : Solid(MeshElementKind::Hexahedron), _m_global_indexes(init_list) {}

Hexahedron::Hexahedron(const Descriptor a, const Descriptor b, const Descriptor c, const Descriptor d,
                       const Descriptor e, const Descriptor f, const Descriptor g, const Descriptor h)
    : Solid(MeshElementKind::Hexahedron), _m_global_indexes({a, b, c, d, e, f, g, h}) {}

bool Hexahedron::class_of(const MeshElement* base) { return InheritsFromHexahedron(base->get_kind()); }

const Hexahedron::m_indexes_type& Hexahedron::_m_indexes() const { return _m_global_indexes; }

Hexahedron::m_indexes_type& Hexahedron::_m_indexes() { return _m_global_indexes; }

MeshElement::size_type Hexahedron::n_vertices() const { return 8; }

MeshElement::size_type Hexahedron::n_edges() const { return 12; }

MeshElement::size_type Hexahedron::n_vertices_in_edge() const { return 2; }

MeshElement::size_type Hexahedron::n_faces() const { return 6; }

MeshElement::size_type Hexahedron::n_vertices_in_face() const { return 4; }

span<MeshElement::Descriptor> Hexahedron::m_indexes() { return MI make_span(_m_global_indexes); }

span<const MeshElement::Descriptor> Hexahedron::m_indexes() const { return MI make_span(_m_global_indexes); }

MeshElement::Descriptor Hexahedron::index(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor Hexahedron::index(const size_type n) { return _m_global_indexes[n]; }

const MeshElement::Descriptor& Hexahedron::index_ref(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Hexahedron::index_ref(const size_type n) { return _m_global_indexes[n]; }

Hexahedron::edge_type Hexahedron::indexes_of_edge(const size_type n_edge) const {
  return get_edge(_m_global_indexes, n_edge);
}

Hexahedron::face_type Hexahedron::indexes_of_face(const size_type n_face) const {
  return get_face(_m_global_indexes, n_face);
}

STD unique_ptr<MeshElement> Hexahedron::get_edge(const size_type n) const {
  return STD make_unique<Edge>(indexes_of_edge(n));
}

STD unique_ptr<MeshElement> Hexahedron::get_face(const size_type n) const {
  return STD make_unique<Quad>(indexes_of_face(n));
}

MeshElement::const_iterator Hexahedron::begin() const { return _m_global_indexes.begin(); }
MeshElement::iterator Hexahedron::begin() { return _m_global_indexes.begin(); }

MeshElement::const_iterator Hexahedron::end() const { return _m_global_indexes.end(); }
MeshElement::iterator Hexahedron::end() { return _m_global_indexes.end(); }

const MeshElement::Descriptor& Hexahedron::operator[](const size_type n) const { return _m_global_indexes[n]; }
MeshElement::Descriptor& Hexahedron::operator[](const size_type n) { return _m_global_indexes[n]; }

STD string Hexahedron::str() const {
  STD stringstream ss;

  ss << SolidKindName(get_kind()) << " = "
     << in_brackets::curly(_m_global_indexes[0], _m_global_indexes[1], _m_global_indexes[2], _m_global_indexes[3],
                           _m_global_indexes[4], _m_global_indexes[5], _m_global_indexes[6], _m_global_indexes[7]);

  return ss.str();
}
}  // namespace mi
