Все типы узлов в `MeshElement` являются производными от `MeshElementNode` и используют RTTI в стиле LLVM для поддержки безопасного преобразования. Каждый абстрактный класс `Foo` в иерархии имеет метод `get_kind`, который возвращает перечисление FooKind, идентифицирующее конкретный тип объекта, и значение FooKind может быть безопасно преобразовано с помощью `static_cast` в `BarKind`, если это значение представляет тип, производный как от `Foo`, так и от `Bar`.

Мы полагаемся на генерацию кода, чтобы помочь обеспечить соблюдение этих инвариантов, поэтому каждый тип узла должен быть описан в `MI.MeshElement_rtti.txt`. Смотрите документацию в `gen_rtti.py` для получения подробной информации о формате файла и сгенерированном коде.

Иерархия классов MeshElement структурирована предсказуемым способом, с абстрактными классами, такими как `Shell` и `Solid`, и конкретными классами, представляющими отдельные синтаксические конструкции, такие как `Triangle`.

Иногда полезно работать с подмножеством типов узлов, которые "пересекают" иерархию первичных классов. Вместо того, чтобы разбираться с подводными камнями множественного наследования, мы обрабатываем эти случаи, используя форму удаления типов: мы указываем условный интерфейс, которому соответствуют эти типы, а затем определяем класс `..View`, который ведет себя как указатель на экземпляр этого интерфейса. Типы объявляют, что они моделируют интерфейс `Foo`, определяя общедоступный статический элемент с именем `ImplementsMIFoo`. Смотрите `MeshElementView` для примера этого шаблона.

### Например:

Имея слудующее дерево

```
root class MeshElement;
  abstract class Shell : MeshElement;
    class Edge : Shell;
    class Triangle : Shell;
    class Quad : Shell;
  abstract class Solid : MeshElement;
    class Hexahedron : Solid;
```

Появляется необходимость иметь базовый класс `FourEdgesElement` для `Quad` и `Hexahedron` по критерию "кол-во ребер в грани элемента". Чтобы сделать это красиво создается класс `FourEdgesElementView`, который имеет общий интерфейс, а в классах `Quad` и `Hexahedron` нужно добавить `ImplementsMIFourEdgesElement`.

```
template <typename NodeType, typename = void>
static constexpr bool ImplementsFourEdgesElement = false;

template <typename T>
static constexpr bool ImplementsFourEdgesElement<T,
	typename T::ImplementsMIMeshElementNode> = true;

class FourEdgesElementView {
  public:
    template <typename NodeType,
	      typename = std::enable_if_t<ImplementsFourEdgesElement<NodeType>>>
    FourEdgesElementView(Nonnull<const NodeType*> node)
        : base_(node),
          _m_indexes([](const MeshElement& base) -> MI span<const MeshElement::Descriptor> {
            return cast<NodeType>(base).m_indexes();
          })
    {}

    auto base() const -> const MeshElement& { return *base_; }
    auto m_indexes() const -> MI span<const MeshElement::Descriptor> { return _m_indexes(*base_); }

  private:
    Nonnull<const MeshElement*> base_;
    std::function<MI span<const MeshElement::Descriptor>(const MeshElement&)> _m_indexes;
};

class Quad {
  public:
    using ImplementsMIMeshElementNode = void;

  public:
    auto m_indexes() const -> MI span<const MeshElement::Descriptor> { return _m_indexes(*base_); }
  ...
};

class Hexahedron {
  public:
    using ImplementsMIMeshElementNode = void;

  public:
    auto m_indexes() const -> MI span<const MeshElement::Descriptor> { return _m_indexes(*base_); }
  ...
};
```
