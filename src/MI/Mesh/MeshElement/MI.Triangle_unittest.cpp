// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Mesh/MeshElement/MI.Triangle.h"

namespace mi::test {
/*
TEST(Triangle, Constructor) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT(q.m_global_indexes(), testing::ElementsAre(1, 2, 3));
}

TEST(Triangle, NVertices) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.n_vertices(), 3);
  static_assert(MI triangle::n_vertices() == 3, "");
}

TEST(Triangle, NEdges) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.n_edges(), 3);
  static_assert(MI triangle::n_edges() == 3, "");
}

TEST(Triangle, NFaces) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.n_faces(), 1);
  static_assert(MI triangle::n_faces() == 1, "");
}

TEST(Triangle, NVerticesInEdge) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.n_vertices_in_edge(), 2);
  static_assert(MI triangle::n_vertices_in_edge() == 2, "");
}

TEST(Triangle, NVerticesInFace) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.n_vertices_in_face(), 3);
  static_assert(MI triangle::n_vertices_in_face() == 3, "");
}

TEST(Triangle, MGlobalIndexes) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT(q.m_global_indexes(), testing::ElementsAre(1, 2, 3));
}

TEST(Triangle, GlobalIndex) {
  MI triangle q({1, 2, 3});

  EXPECT_EQ(q.global_index(0), 1);
  EXPECT_EQ(q.global_index(1), 2);
  EXPECT_EQ(q.global_index(2), 3);

  q.global_index(0) = 10;
  q.global_index(1) = 20;
  q.global_index(2) = 30;

  EXPECT_EQ(q.global_index(0), 10);
  EXPECT_EQ(q.global_index(1), 20);
  EXPECT_EQ(q.global_index(2), 30);
}

TEST(Triangle, Edge) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT(q.edge(0).m_global_indexes(), testing::ElementsAre(1, 2));
  EXPECT_THAT(q.edge(1).m_global_indexes(), testing::ElementsAre(2, 3));
  EXPECT_THAT(q.edge(2).m_global_indexes(), testing::ElementsAre(3, 1));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.edge(4));
}

TEST(Triangle, MEdges) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT(q.edge(0).m_global_indexes(), testing::ElementsAre(1, 2));
  EXPECT_THAT(q.edge(1).m_global_indexes(), testing::ElementsAre(2, 3));
  EXPECT_THAT(q.edge(2).m_global_indexes(), testing::ElementsAre(3, 1));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.m_edges()[3]);
}

TEST(Triangle, SortedEdge) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT(q.sorted_edge(0), testing::ElementsAre(1, 2));
  EXPECT_THAT(q.sorted_edge(1), testing::ElementsAre(2, 3));
  EXPECT_THAT(q.sorted_edge(2), testing::ElementsAre(1, 3));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.sorted_edge(3));
}

TEST(Triangle, MSortedEdges) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT(q.m_sorted_edges()[0], testing::ElementsAre(1, 2));
  EXPECT_THAT(q.m_sorted_edges()[1], testing::ElementsAre(2, 3));
  EXPECT_THAT(q.m_sorted_edges()[2], testing::ElementsAre(1, 3));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.m_sorted_edges()[3]);
}

TEST(Triangle, Face) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.face(0), (MI internal::traits<MI triangle>::face_type({1, 2, 3})));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.face(1));
}

TEST(Triangle, MFaces) {
  const MI triangle q({1, 2, 3});

  ASSERT_EQ(q.m_faces()[0], (MI internal::traits<MI triangle>::face_type({1, 2, 3})));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.m_faces()[1]);
}

TEST(Triangle, SortedFace) {
  const MI triangle q({3, 2, 1});

  EXPECT_EQ(q.sorted_face(0), (MI internal::traits<MI triangle>::sorted_face_type({1, 2, 3})));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.sorted_face(1));
}

TEST(Triangle, MSortedFaces) {
  const MI triangle q({1, 2, 3});

  EXPECT_THAT((q.m_sorted_faces()[0]), testing::ElementsAre(1, 2, 3));
  MI_EXPECT_DCHECK_DEATH((MI_DISABLE_4834)q.m_sorted_faces()[1]);
}

struct data {
    int m_data;
};

TEST(Triangle, Property) {
  MI triangle_with<data> q({1, 2, 3});

  q.property().m_data = 1;

  EXPECT_EQ(q.property().m_data, 1);
}

*/
}  // namespace mi::test
