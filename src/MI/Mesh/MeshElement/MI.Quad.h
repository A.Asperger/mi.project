// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include "MI.Edge.h"
#include "MI.Shell.h"
#include "MI/Base/MI.BaseExport.hpp"

namespace mi {
class MI_BASE_EXPORT Quad final : public Shell {
  public:
    using edge_type      = MI static_vector<Descriptor, 2>;
    using face_type      = MI static_vector<Descriptor, 4>;
    using m_indexes_type = MI static_vector<Descriptor, 4>;

  public:
    static constexpr STD integral_constant<Descriptor, 4> kn_vertices;
    static constexpr STD integral_constant<Descriptor, 4> kn_edges;
    static constexpr STD integral_constant<Descriptor, 1> kn_faces;
    static constexpr STD integral_constant<Descriptor, 2> kn_vertices_in_edge;
    static constexpr STD integral_constant<Descriptor, 4> kn_vertices_in_face;

  public:
    inline static MI static_vector<MI static_vector<Descriptor, 2>, 4> local_edges = {
        {0, 1}, // edge 0
        {1, 2}, // edge 1
        {2, 3}, // edge 2
        {3, 0}  // edge 3
    };

    inline static MI static_vector<MI static_vector<Descriptor, 4>, 4> local_faces = {
        {0, 1, 2, 3}  //  face 0
    };

  public:
    static edge_type get_edge(const m_indexes_type& quad_vertices, const Descriptor n_edge);

    static face_type get_face(const m_indexes_type& quad_vertices, const Descriptor n_face);

  public:
    Quad();

    Quad(STD initializer_list<Descriptor> init_list);

    Quad(Descriptor v0, Descriptor v1, Descriptor v2, Descriptor v3);

    template <class Container, if_t<IsContainer<Container> && !IsInitList<Container>> = 0>
    Quad(const Container& cont) : Shell(MeshElementKind::Quad), _m_global_indexes(cont.begin(), cont.end()) {}

  public:
    static bool class_of(const MeshElement* base);

  public:
    MI_NODISCARD size_type n_vertices() const override;

    MI_NODISCARD size_type n_edges() const override;
    MI_NODISCARD size_type n_vertices_in_edge() const override;

    MI_NODISCARD size_type n_faces() const override;
    MI_NODISCARD size_type n_vertices_in_face() const override;

  public:
    MI_NODISCARD MI span<Descriptor> m_indexes() override;
    MI_NODISCARD MI span<const Descriptor> m_indexes() const override;

  public:
    MI_NODISCARD Descriptor index(size_type n) const override;
    MI_NODISCARD Descriptor index(size_type n) override;

  public:
    MI_NODISCARD const Descriptor& index_ref(size_type n) const override;
    MI_NODISCARD Descriptor& index_ref(size_type n) override;

  public:
    MI_NODISCARD MI static_vector<Descriptor, 2> indexes_of_edge(size_type n_edge) const override;
    MI_NODISCARD MI static_vector<Descriptor, 4> indexes_of_face(size_type n_face) const override;

  public:
    MI_NODISCARD STD unique_ptr<MeshElement> get_edge(size_type n) const override;

    MI_NODISCARD STD unique_ptr<MeshElement> get_face(size_type n) const override;

  public:
    MI_NODISCARD const_iterator begin() const override;
    MI_NODISCARD iterator begin() override;

  public:
    MI_NODISCARD const_iterator end() const override;
    MI_NODISCARD iterator end() override;

  public:
    MI_NODISCARD const Descriptor& operator[](size_type n) const override;
    MI_NODISCARD Descriptor& operator[](size_type n) override;

  public:
    MI_NODISCARD std::string str() const override;

  public:
    bool operator==(const Quad& rhs) const { return _m_global_indexes == rhs._m_global_indexes; }
    bool operator!=(const Quad& rhs) const { return !(*this == rhs); }

  private:
    MI static_vector<Descriptor, 4> _m_global_indexes;
};

}  // namespace mi
