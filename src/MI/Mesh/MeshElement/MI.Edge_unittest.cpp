// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Mesh/MeshElement/MI.Edge.h"

namespace mi::test {

TEST(Edge2, Constructor) {
  const MI Edge q({1, 2});

  EXPECT_THAT(q.m_indexes(), testing::ElementsAre(1, 2));
}

TEST(Edge2, NVertices) {
  const MI Edge q({1, 2});

  ASSERT_EQ(q.n_vertices(), static_cast<MI MeshElement::size_type>(2));
}

TEST(Edge2, NEdges) {
  const MI Edge q({1, 2});

  ASSERT_EQ(q.n_edges(), static_cast<MI MeshElement::size_type>(1));
}

TEST(Edge2, NFaces) {
  const MI Edge q({1, 2});

  ASSERT_EQ(q.n_faces(), static_cast<MI MeshElement::size_type>(0));
}

TEST(Edge2, NVerticesInEdge) {
  const MI Edge q({1, 2});

  ASSERT_EQ(q.n_vertices_in_edge(), static_cast<MI MeshElement::size_type>(2));
}

TEST(Edge2, NVerticesInFace) {
  const MI Edge q({1, 2});

  ASSERT_EQ(q.n_vertices_in_face(), static_cast<MI MeshElement::size_type>(0));
}

TEST(Edge2, MIndexes) {
  const MI Edge q({1, 2});

  EXPECT_THAT(q.m_indexes(), testing::ElementsAre(1, 2));
}

TEST(Edge2, Index) {
  MI Edge q({1, 2});

  EXPECT_EQ(q.index(0), 1);
  EXPECT_EQ(q.index(1), 2);

  q.index_ref(0) = 10;
  q.index_ref(1) = 20;

  EXPECT_EQ(q.index(0), 10);
  EXPECT_EQ(q.index(1), 20);
}

TEST(Edge2, IndexesOfEdge) {
  const MI Edge q({1, 2});

  EXPECT_THAT(q.indexes_of_edge(0), testing::ElementsAre(1, 2));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.indexes_of_edge(1));
}

TEST(Edge2, IndexesOfFace) {
  const MI Edge q({1, 2});

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.indexes_of_face(0));
}

TEST(Edge2, GetEdge) {
  const MI Edge q({1, 2});

  EXPECT_THAT(q.get_edge(0)->m_indexes(), testing::ElementsAre(1, 2));

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.get_edge(1));
}

TEST(Edge, GetFace) {
  const MI Edge q({1, 2});

  MI_ASSERT_DCHECK_DEATH((MI_DISABLE_4834)q.get_face(0));
}

}  // namespace mi::test
