// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include "MI/Mesh/MeshElement/MI.Edge.h"

#include <MI/Common/MI.InBrackets.h>

#include "MI/Container/MI.Span.hpp"

namespace mi {

Edge::Edge() : Shell(MeshElementKind::Edge) {}

Edge::Edge(const STD initializer_list<Descriptor> init_list)
    : Shell(MeshElementKind::Edge), _m_global_indexes(init_list) {}

Edge::Edge(const Descriptor a, const Descriptor b) : Shell(MeshElementKind::Edge), _m_global_indexes({a, b}) {}

bool Edge::class_of(const MeshElement* base) { return InheritsFromEdge(base->get_kind()); }

MeshElement::size_type Edge::n_vertices() const { return 2; }

MeshElement::size_type Edge::n_edges() const { return 1; }

MeshElement::size_type Edge::n_vertices_in_edge() const { return 2; }

MeshElement::size_type Edge::n_faces() const { return 0; }

MeshElement::size_type Edge::n_vertices_in_face() const { return 0; }

span<MeshElement::Descriptor> Edge::m_indexes() { return MI make_span(_m_global_indexes); }

MI span<const MeshElement::Descriptor> Edge::m_indexes() const { return MI make_span(_m_global_indexes); }

MeshElement::Descriptor Edge::index(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor Edge::index(const size_type n) { return _m_global_indexes[n]; }

const MeshElement::Descriptor& Edge::index_ref(const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Edge::index_ref(const size_type n) { return _m_global_indexes[n]; }

MI static_vector<MeshElement::Descriptor, 2> Edge::indexes_of_edge(const size_type n_edge) const {
  MI_DCHECK(n_edge < n_edges());

  using LocalEdge  = MI static_vector<Descriptor, 2>;
  using GlobalEdge = MI static_vector<Descriptor, 2>;

  static MI static_vector<LocalEdge, 1> local_edges = {
      {0, 1},
  };

  const auto global_index_0 = _m_global_indexes[local_edges[n_edge][0]];
  const auto global_index_1 = _m_global_indexes[local_edges[n_edge][1]];

  return GlobalEdge{global_index_0, global_index_1};
}

MI static_vector<MeshElement::Descriptor, 4> Edge::indexes_of_face(const size_type) const {
  MI_NOT_REACHED();
  return {};
}

STD unique_ptr<MeshElement> Edge::get_edge(const size_type n) const {
  MI_DCHECK(n < n_edges());

  return STD make_unique<Edge>(*this);
}

STD unique_ptr<MeshElement> Edge::get_face(const size_type) const {
  MI_NOT_REACHED();
  return {};
}

MeshElement::const_iterator Edge::begin() const { return _m_global_indexes.begin(); }

MeshElement::iterator Edge::begin() { return _m_global_indexes.begin(); }

MeshElement::const_iterator Edge::end() const { return _m_global_indexes.end(); }

MeshElement::iterator Edge::end() { return _m_global_indexes.end(); }

const MeshElement::Descriptor& Edge::operator[](const size_type n) const { return _m_global_indexes[n]; }

MeshElement::Descriptor& Edge::operator[](const size_type n) { return _m_global_indexes[n]; }

std::string Edge::str() const {
  STD stringstream ss;

  // "edge = { 0, 1 }"
  ss << ShellKindName(get_kind()) << " = " << in_brackets::curly(_m_global_indexes[0], _m_global_indexes[1]);

  return ss.rdbuf()->str();
}
}  // namespace mi
