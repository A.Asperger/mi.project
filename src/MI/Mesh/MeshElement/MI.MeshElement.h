// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once
#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Base/MI.Property.hpp"
#include "MI/Container/MI.Span.hpp"
#include "MI/Container/MI.StaticVector.hpp"
#include "MI/Mesh/MeshElement/MI.MeshElement_rtti.h"

namespace mi {
class MI_BASE_EXPORT MeshElement {
  public:
    using Descriptor = uint32_t;

  public:
    using value_type = Descriptor;
    using size_type  = MI MeshElement::Descriptor;

    using iterator               = MI checked_contiguous_iterator<Descriptor>;
    using const_iterator         = MI checked_contiguous_const_iterator<Descriptor>;
    using reverse_iterator       = STD reverse_iterator<iterator>;
    using const_reverse_iterator = STD reverse_iterator<const_iterator>;

  public:
    virtual ~MeshElement() = default;

  public:
    explicit MeshElement(const MeshElementKind kind) : _kind(kind) {}

  public:
    MeshElement(const MeshElement&)     = default;
    MeshElement(MeshElement&&) noexcept = default;

  public:
    MeshElement& operator=(const MeshElement&)     = default;
    MeshElement& operator=(MeshElement&&) noexcept = default;

  public:
    MI_NODISCARD MeshElementKind get_kind() const { return _kind; }
    MI_NODISCARD MeshElementKind root_kind() const { return _kind; }

  public:
    MI_NODISCARD virtual size_type n_vertices() const = 0;

    MI_NODISCARD virtual size_type n_edges() const            = 0;
    MI_NODISCARD virtual size_type n_vertices_in_edge() const = 0;

    MI_NODISCARD virtual size_type n_faces() const            = 0;
    MI_NODISCARD virtual size_type n_vertices_in_face() const = 0;

  public:
    MI_NODISCARD virtual Descriptor index(size_type n) const = 0;
    MI_NODISCARD virtual Descriptor index(size_type n)       = 0;

  public:
    MI_NODISCARD virtual const Descriptor& index_ref(size_type n) const = 0;
    MI_NODISCARD virtual Descriptor& index_ref(size_type n)             = 0;

  public:
    MI_NODISCARD virtual MI span<Descriptor> m_indexes()             = 0;
    MI_NODISCARD virtual MI span<const Descriptor> m_indexes() const = 0;

  public:
    MI_NODISCARD virtual STD unique_ptr<MeshElement> get_edge(size_type n) const = 0;
    MI_NODISCARD virtual STD unique_ptr<MeshElement> get_face(size_type n) const = 0;

  public:
    MI_NODISCARD virtual MI static_vector<Descriptor, 2> indexes_of_edge(size_type n) const = 0;
    MI_NODISCARD virtual static_vector<Descriptor, 4> indexes_of_face(size_type n) const    = 0;

  public:
    MI_NODISCARD virtual const_iterator begin() const = 0;
    MI_NODISCARD virtual iterator begin()             = 0;

  public:
    MI_NODISCARD virtual const_iterator end() const = 0;
    MI_NODISCARD virtual iterator end()             = 0;

  public:
    MI_NODISCARD virtual const Descriptor& operator[](size_type n) const = 0;
    MI_NODISCARD virtual Descriptor& operator[](size_type n)             = 0;

  public:
    MI_NODISCARD virtual STD string str() const = 0;

  private:
    MeshElementKind _kind;
};
}  // namespace mi
