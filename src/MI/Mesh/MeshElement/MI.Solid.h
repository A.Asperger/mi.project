#pragma once

#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Mesh/MeshElement/MI.MeshElement.h"

namespace mi {

class MI_BASE_EXPORT Solid : public MeshElement {
  public:
    ~Solid() override = default;

  public:
    Solid(const Solid&)            = default;
    Solid& operator=(const Solid&) = default;

  protected:
    // Constructs a Declaration representing syntax at the given line number.
    // `kind` must be the enumerator corresponding to the most-derived type being
    // constructed.
    Solid(MeshElementKind kind) : MeshElement(kind) {}

  public:
    static auto class_of(const MeshElement* node) -> bool { return InheritsFromSolid(node->get_kind()); }

    // Returns the enumerator corresponding to the most-derived type of this
    // object.
    auto get_kind() const -> SolidKind { return static_cast<SolidKind>(root_kind()); }
};
}  // namespace mi
