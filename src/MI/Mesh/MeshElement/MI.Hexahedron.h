// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include <MI/Base/MI.BaseExport.hpp>
#include <MI/Common/MI.If.h>
#include <MI/Mesh/MeshElement/MI.Edge.h>
#include <MI/Mesh/MeshElement/MI.Solid.h>

#include <type_traits>

namespace mi {
class MI_BASE_EXPORT Hexahedron final : public Solid {
  public:
    using edge_type      = MI static_vector<Descriptor, 2>;
    using face_type      = MI static_vector<Descriptor, 4>;
    using m_indexes_type = MI static_vector<Descriptor, 8>;

  public:
    static constexpr STD integral_constant<Descriptor, 8> kn_vertices;
    static constexpr STD integral_constant<Descriptor, 12> kn_edges;
    static constexpr STD integral_constant<Descriptor, 6> kn_faces;
    static constexpr STD integral_constant<Descriptor, 2> kn_vertices_in_edge;
    static constexpr STD integral_constant<Descriptor, 4> kn_vertices_in_face;

  public:
    inline static MI static_vector<edge_type, 12> local_edges = {
        {0, 1}, // edge 0
        {1, 2}, // edge 1
        {2, 3}, // edge 2
        {3, 0}, // edge 3
        {0, 4}, // edge 4
        {1, 5}, // edge 5
        {2, 6}, // edge 6
        {3, 7}, // edge 7
        {4, 5}, // edge 8
        {5, 6}, // edge 9
        {6, 7}, // edge 10
        {7, 4}  // edge 11
    };

    inline static MI static_vector<face_type, 6> local_faces = {
        {0, 3, 2, 1}, // face 0
        {0, 1, 5, 4}, // face 1
        {1, 2, 6, 5}, // face 2
        {2, 3, 7, 6}, // face 3
        {3, 0, 4, 7}, // face 4
        {4, 5, 6, 7}  // face 5
    };

  public:
    static edge_type get_edge(const m_indexes_type& hex_vertices, const Descriptor n_edge);

    static face_type get_face(const m_indexes_type& hex_vertices, const Descriptor n_face);

  public:
    Hexahedron();

    Hexahedron(STD initializer_list<Descriptor> init_list);

    Hexahedron(Descriptor v0, Descriptor v1, Descriptor v2, Descriptor v3,  //
               Descriptor v4, Descriptor v5, Descriptor v6, Descriptor v7);

    template <class Container, if_t<IsContainer<Container> && !IsInitList<Container>> = 0>
    Hexahedron(const Container& cont)
        : Solid(MeshElementKind::Hexahedron), _m_global_indexes(cont.begin(), cont.end()) {}

  public:
    static bool class_of(const MeshElement* base);

  public:
    MI_NODISCARD const m_indexes_type& _m_indexes() /*override*/ const;
    MI_NODISCARD m_indexes_type& _m_indexes() /*override*/;

  public:
    MI_NODISCARD size_type n_vertices() const override;
    MI_NODISCARD size_type n_edges() const override;
    MI_NODISCARD size_type n_vertices_in_edge() const override;
    MI_NODISCARD size_type n_faces() const override;
    MI_NODISCARD size_type n_vertices_in_face() const override;

  public:
    MI_NODISCARD MI span<Descriptor> m_indexes() override;
    MI_NODISCARD MI span<const Descriptor> m_indexes() const override;

  public:
    MI_NODISCARD Descriptor index(size_type n) const override;
    MI_NODISCARD Descriptor index(size_type n) override;

  public:
    MI_NODISCARD const Descriptor& index_ref(size_type n) const override;
    MI_NODISCARD Descriptor& index_ref(size_type n) override;

    MI_NODISCARD edge_type indexes_of_edge(size_type n_edge) const override;
    MI_NODISCARD face_type indexes_of_face(size_type n_face) const override;

  public:
    MI_NODISCARD STD unique_ptr<MeshElement> get_edge(size_type n) const override;
    MI_NODISCARD STD unique_ptr<MeshElement> get_face(size_type n) const override;

  public:
    MI_NODISCARD const_iterator begin() const override;
    MI_NODISCARD iterator begin() override;

  public:
    MI_NODISCARD const_iterator end() const override;
    MI_NODISCARD iterator end() override;

  public:
    MI_NODISCARD const Descriptor& operator[](size_type n) const override;
    MI_NODISCARD Descriptor& operator[](size_type n) override;

  public:
    MI_NODISCARD STD string str() const override;

  private:
    m_indexes_type _m_global_indexes;
};

}  // namespace mi
