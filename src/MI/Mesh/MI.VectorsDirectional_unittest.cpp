// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI.VectorsDirectional.h"

namespace mi::test {
TEST(VectorsDirectional, CoDirectional) {
  const MI point3d a = {0., 0., 1.};
  const MI point3d b = {256., 0., 0.000001};
  const MI point3d c = {512., 0., -0.000001};
  const MI point3d d = {1024., 0., 0};

  ASSERT_TRUE(MI vectors_directional(a, b) == MI vectors_dir::co_directional);
  ASSERT_TRUE(MI vectors_directional(a, c) != MI vectors_dir::co_directional);
  ASSERT_TRUE(MI vectors_directional(a, d) != MI vectors_dir::co_directional);
}

TEST(VectorsDirectional, MultiDirectional) {
  const MI point3d a = {0., 0., 1.};
  const MI point3d b = {256., 0., 0.000001};
  const MI point3d c = {512., 0., -0.000001};
  const MI point3d d = {1024., 0., 0};

  ASSERT_TRUE(MI vectors_directional(a, b) != MI vectors_dir::multi_directional);
  ASSERT_TRUE(MI vectors_directional(a, c) == MI vectors_dir::multi_directional);
  ASSERT_TRUE(MI vectors_directional(a, d) != MI vectors_dir::multi_directional);
}

TEST(VectorsDirectional, Orthogonal) {
  const MI point3d a = {0., 0., 1.};
  const MI point3d b = {256., 0., 0.000001};
  const MI point3d c = {512., 0., -0.000001};
  const MI point3d d = {1024., 0., 0};

  ASSERT_TRUE(MI vectors_directional(a, b) != MI vectors_dir::orthogonal);
  ASSERT_TRUE(MI vectors_directional(a, c) != MI vectors_dir::orthogonal);
  ASSERT_TRUE(MI vectors_directional(a, d) == MI vectors_dir::orthogonal);
}
}  // namespace mi::test
