// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Mesh/MI.IsDegenerated.h"
#include "MI/Mesh/MI.Point.h"

namespace mi::test {
TEST(IsDegenerated, InternalDegeneratedIntoStraightLine) {
  MI Mesh mesh;

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0., 0.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q1 = MI Quad({0, 1, 2, 3});

  ASSERT_TRUE(MI internal::degenerated_into_straight_line(q1, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, -0.001, 0.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q2 = MI Quad({4, 5, 6, 7});

  ASSERT_FALSE(MI internal::degenerated_into_straight_line(q2, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0., 0.1);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q3 = MI Quad({8, 9, 10, 11});

  ASSERT_FALSE(MI internal::degenerated_into_straight_line(q3, mesh));
}

TEST(IsDegenerated, InternalIsCurved) {
  MI Mesh mesh;

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0.5, 0.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0.5, 1., 0.);

  const MI Quad q1 = MI Quad({0, 1, 2, 3});

  ASSERT_FALSE(MI internal::is_curved(q1, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, -0.001, 0.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q2 = MI Quad({4, 5, 6, 7});

  ASSERT_TRUE(MI internal::is_curved(q2, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0., 0.1);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q3 = MI Quad({8, 9, 10, 11});

  ASSERT_FALSE(MI internal::is_curved(q3, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0.5, 5.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0.5, 1., 0.);

  const MI Quad q4 = MI Quad({12, 13, 14, 15});

  ASSERT_FALSE(MI internal::is_curved(q4, mesh));
}

TEST(IsDegenerated, IsDegenerated) {
  MI Mesh mesh;

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0., 0.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q1 = MI Quad({0, 1, 2, 3});

  ASSERT_TRUE(MI is_degenerated(q1, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, -0.001, 0.);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q2 = MI Quad({4, 5, 6, 7});

  ASSERT_FALSE(MI is_degenerated(q2, mesh));

  mesh.add_vertex(0., 0., 0.);
  mesh.add_vertex(0.5, 0., 0.1);
  mesh.add_vertex(1., 0., 0.);
  mesh.add_vertex(0., 1., 0.);

  const MI Quad q3 = MI Quad({8, 9, 10, 11});

  ASSERT_TRUE(MI is_degenerated(q3, mesh));
}

}  // namespace mi::test
