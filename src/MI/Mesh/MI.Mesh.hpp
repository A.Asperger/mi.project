// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include <MI/Base/MI.Casting.hpp>
#include <MI/Container/MI.Matrix.hpp>
#include <filesystem>
#include <fstream>

#include "MI.Point.hpp"
#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.Arena.hpp"
#include "MI/Container/MI.STDContainers.h"
#include "MI/Container/MI.Sortable.hpp"
#include "MeshElement/MI.MeshElement.h"

namespace mi {

class MI_BASE_EXPORT Mesh {
  public:
    using size_type = MeshElement::size_type;  // NOLINT(readability-identifier-naming)

    using point_type   = MI point3d;
    using element_type = MeshElement*;
    using descriptor   = MeshElement::Descriptor;

  public:
    ~Mesh();

  public:
    Mesh() = default;

  public:
    Mesh(const Mesh& other);
    Mesh& operator=(const Mesh& other);

  public:
    Mesh(Mesh&& other) noexcept            = default;
    Mesh& operator=(Mesh&& other) noexcept = default;

  private:
    using m_vertices_type = STD vector<point_type>;
    using m_elements_type = STD vector<element_type>;

    // -----------------------------------------------------------------------------------------------------------------
    // Вершины.

  public:
    descriptor add_vertex(double x, double y, double z);
    descriptor add_vertex(const point3d& p);

  public:
    MI_NODISCARD const STD vector<point_type>& get_m_vertices() const;
    MI_NODISCARD STD vector<point_type>& get_m_vertices();

  public:
    MI_NODISCARD size_type get_n_vertices() const;

  public:
    MI_NODISCARD point_type& get_vertex(size_type n);
    MI_NODISCARD const point_type& get_vertex(size_type n) const;

  public:
    template <typename T, typename... Args, MI if_t<STD is_constructible_v<T, Args...>> = 0>
    descriptor add_element(Args&&... args) {
      _m_elements.emplace_back(_arena.New<T>(STD forward<Args>(args)...));
      return static_cast<descriptor>(_m_elements.size()) - 1;
    }

  public:
    MI_NODISCARD const m_elements_type& get_m_elements() const;
    MI_NODISCARD m_elements_type& get_m_elements();

  public:
    MI_NODISCARD size_type get_n_elements() const;

  public:
    MI_NODISCARD MeshElement* get_element(size_type n) const;
    MI_NODISCARD MeshElement* get_element(size_type n);

  public:
    // - Удаляет элемент с номером n из массива элементов.
    // - Удаляет дескрипторы элементов, ссылающиеся на этот элемент из всех тел.
    void remove_element(size_type n_element);

  public:
    MI_NODISCARD STD string str() const {
      STD stringstream ss;

      for (const MeshElement* element : get_m_elements()) {
        ss << element->str() << STD endl;
      }

      for (const point3d& vertex : get_m_vertices()) {
        ss << "vertex = { " << vertex.x() << ", " << vertex.y() << ", " << vertex.z() << " }" << STD endl;
      }

      return ss.rdbuf()->str();
    }

  private:
    m_vertices_type _m_vertices;
    m_elements_type _m_elements;
    Arena _arena;
};

inline void copy_vertices(const MI Mesh& from, MI Mesh& to) { to.get_m_vertices() = from.get_m_vertices(); }

inline void copy_elements(const MI Mesh& from, MI Mesh& to) { to.get_m_elements() = from.get_m_elements(); }

inline void debug_mesh_print(const MI Mesh& before, const MI Mesh& after) {
  STD filesystem::path cp = std::filesystem::current_path();

  while (cp.has_filename() && cp.filename() != "SuperProject") {
    cp = cp.parent_path();
  }

  STD filesystem::path s1 = cp / "tmp\\before_mesh.txt";
  STD filesystem::path s2 = cp / "tmp\\after_mesh.txt";
  STD filesystem::path s3 = cp / "_build\\bin\\Debug\\DebugMeshPrintd.exe";

  STD ofstream before_mesh(s1);
  before_mesh << before.str();
  before_mesh.close();

  STD ofstream after_mesh(s2);
  after_mesh << after.str();
  after_mesh.close();

  system((s3.string() + " "    //
          + s1.string() + " "  //
          + s2.string())
             .c_str());
}

template <class T>
MI_NODISCARD bool mesh_has(const Mesh& m) {
  return STD ranges::any_of(m.get_m_elements(), [](const MeshElement* el) { return isa<T>(el); });
}

template <class T>
MI_NODISCARD bool mesh_has_only(const Mesh& m) {
  return STD ranges::all_of(m.get_m_elements(), [](const MeshElement* el) { return isa<T>(el); });
}

using PairAdjacentFaceDescriptors = static_vector<MeshElement::Descriptor, 2>;
using SortedEdge                  = sortable<MeshElement::Descriptor, 2>;

using FacesAroundVertexMap = MI unordered_map<MeshElement::Descriptor, STD vector<MeshElement::Descriptor>>;

// Пара дескрипторов смежных граней, которые разделяет общее ребро.
//
// STD map вида:
// {SortedEdge: {FaceDescriptor1, FaceDescriptor2}}
using FacesAroundEdgeMap = MI unordered_map<SortedEdge, PairAdjacentFaceDescriptors>;

// Для каждого дескриптора грани находит 4 дескриптора граней таких, чтобы у каждой из последних было общее ребро с
// начальной.
//
// {a : {b, c, d, e}}
// {b : {a, e, f, g}}
using AdjacentFacesMap = MI unordered_map<MeshElement::Descriptor, STD vector<MeshElement::Descriptor>>;

MI_NODISCARD MI_BASE_EXPORT FacesAroundVertexMap get_faces_around_vertex_map(const Mesh& mesh);

MI_NODISCARD MI_BASE_EXPORT FacesAroundEdgeMap get_faces_around_edge_map(const Mesh& mesh);

MI_NODISCARD MI_BASE_EXPORT AdjacentFacesMap get_adjacent_faces_map(const Mesh& mesh);

}  // namespace mi
