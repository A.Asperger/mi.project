#pragma once

#include <algorithm>
#include <cassert>

#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.Check.hpp"
#include "MI/Common/MI.GetUnwrapped.h"
#include "MI/Common/MI.VerifyRange.h"

namespace mi {
// Копирует уникальные элементы из [first; last) в dest.
// Входная последовательность должна быть отсортирована.
template <class IteratorType, class OutIteratorType, class PredicateType>
void only_unique(IteratorType first, IteratorType last, OutIteratorType dest, PredicateType predicate) {
  //
  MI adl_verify_range(first, last);

  // Алгоритм работает только с отсортированным массивом данных
  MI_DCHECK(STD is_sorted(first, last));

  auto unwrapped_first      = MI get_unwrapped(first);
  auto unwrapped_second     = MI get_unwrapped(std::next(first));  // STD next ломает clang-format.
  const auto unwrapped_last = MI get_unwrapped(last);
  auto unwrapped_dest       = MI get_unwrapped(dest);

  // Перебираем все последовательно идущие пары
  while (unwrapped_second < unwrapped_last) {
    if (!predicate(*unwrapped_first, *unwrapped_second)) {
      *unwrapped_dest = *unwrapped_first;
      ++unwrapped_dest;
    } else {
      // Если два рядом стоящие элемента одинаковые, то перескакиваем все элемента с таким же значением.
      const auto unwrapped_bad_value = unwrapped_first;

      while (predicate(*unwrapped_bad_value, *unwrapped_first)) {
        ++unwrapped_first;
        ++unwrapped_second;
      }

      continue;
    }

    ++unwrapped_first;
    ++unwrapped_second;
  }

  const auto distance = STD distance(first, last);

  if (distance >= 2) {
    // Проверяем два последних элемента. Если они разные, то первый уже добавился в цикле, а второй нужно добавить
    if (!predicate(last[-2], last[-1])) {
      *unwrapped_dest = last[-1];
      ++unwrapped_dest;
    }
  } else if (distance == 1) {
    // Если последовательность состояла из одного элемента, то он уникальный и его нужно добавить.
    *unwrapped_dest = last[-1];
    ++unwrapped_dest;
  }
}

template <class IteratorType, class OutIteratorType>
void only_unique(IteratorType first, IteratorType last, OutIteratorType dest) {
  return (only_unique(first, last, dest, STD equal_to<>()));
}
}  // namespace mi
