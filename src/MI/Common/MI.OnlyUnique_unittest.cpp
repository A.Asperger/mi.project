// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <array>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Common/MI.OnlyUnique.h"

namespace mi::test {
TEST(OnlyUnique, SmallArray) {
  {
    std::array in = {1, 2, 3};
    std::vector<int> out;

    MI only_unique(in.begin(), in.end(), std::back_inserter(out));

    EXPECT_THAT(out, testing::ElementsAre(1, 2, 3));
  }

  {
    std::array in = {1, 2};
    std::vector<int> out;

    MI only_unique(in.begin(), in.end(), std::back_inserter(out));

    EXPECT_THAT(out, testing::ElementsAre(1, 2));
  }

  {
    std::array in = {1};
    std::vector<int> out;

    MI only_unique(in.begin(), in.end(), std::back_inserter(out));

    EXPECT_THAT(out, testing::ElementsAre(1));
  }
}

TEST(OnlyUnique, TransposedRange) {
  std::array in = {1, 2, 2, 2, 3, 1};
  std::vector<int> out;

  MI_EXPECT_DCHECK_DEATH(MI only_unique(in.end(), in.begin(), std::back_inserter(out)));
}

TEST(OnlyUnique, NotSorted) {
  std::array in = {1, 2, 2, 2, 3, 1};
  std::vector<int> out;

  MI_EXPECT_DCHECK_DEATH(MI only_unique(in.begin(), in.end(), std::back_inserter(out)));
}

TEST(OnlyUnique, OnlyUniqueBegin) {
  std::array in = {1, 2, 2, 2, 3, 3};
  std::vector<int> out;

  MI only_unique(in.begin(), in.end(), std::back_inserter(out));

  EXPECT_THAT(out, testing::ElementsAre(1));
}

TEST(OnlyUnique, OnlyUniqueMid) {
  std::array in = {1, 1, 1, 2, 3, 3};
  std::vector<int> out;

  MI only_unique(in.begin(), in.end(), std::back_inserter(out));

  EXPECT_THAT(out, testing::ElementsAre(2));
}

TEST(OnlyUnique, OnlyUniqueEnd) {
  std::array in = {1, 1, 2, 2, 2, 3};
  std::vector<int> out;

  MI only_unique(in.begin(), in.end(), std::back_inserter(out));

  EXPECT_THAT(out, testing::ElementsAre(3));
}
}  // namespace mi::test
