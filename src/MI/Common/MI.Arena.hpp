#pragma once
#include <vector>

#include "MI.If.h"
#include "MI/Base/MI.Pointer.hpp"
#include "MI/Base/MI.Property.hpp"

namespace mi {

class Arena {
  public:
    Arena() = default;

  public:
    Arena(const Arena& other)            = delete;
    Arena& operator=(const Arena& other) = delete;

  public:
    Arena(Arena&& other) noexcept            = default;
    Arena& operator=(Arena&& other) noexcept = default;

  public:
    // Allocates an object in the arena, returning a pointer to it.
    template <typename T, typename... Args, if_t<STD is_constructible_v<T, Args...>> = 0>
    auto New(Args&&... args) -> MI NonNull<T*> {
      STD unique_ptr<ArenaEntryTyped<T>> smart_ptr = STD make_unique<ArenaEntryTyped<T>>(STD forward<Args>(args)...);
      MI NonNull<T*> ptr                           = smart_ptr->Instance();

      arena_.push_back(STD move(smart_ptr));

      return ptr;
    }

    void clear() { arena_.clear(); }

  private:
    // Virtualizes arena entries so that a single vector can contain many types,
    // avoiding templated statics.
    class ArenaEntry {
      public:
        virtual ~ArenaEntry() = default;
    };

    // Templated destruction of a pointer.
    template <typename T>
    class ArenaEntryTyped : public ArenaEntry {
      public:
        template <typename... Args>
        explicit ArenaEntryTyped(Args&&... args) : instance_(STD forward<Args>(args)...) {}

        auto Instance() -> MI NonNull<T*> { return MI NonNull<T*>(&instance_); }

      private:
        T instance_;
    };

    // Manages allocations in an arena for destruction at shutdown.
    STD vector<STD unique_ptr<ArenaEntry>> arena_;
};

}  // namespace mi
