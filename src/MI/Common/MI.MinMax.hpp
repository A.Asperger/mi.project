#pragma once

#include <type_traits>

#include "MI/Base/MI.BaseExport.hpp"
#include "MI/Base/MI.CompilerSpecific.hpp"
#include "MI/Base/MI.Property.hpp"

namespace mi {
template <class T1, class T2>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE std::common_type_t<T1, T2>(max)(T1 v1, T2 v2) {
  return v1 > v2 ? v1 : v2;
}

template <class T1, class T2, class... Tys>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2, Tys...>(max)(T1 v1, T2 v2, Tys... values) {
  return (MI max)(v1, (MI max)(v2, values...));
}

template <class T1, class T2>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2>(min)(T1 v1, T2 v2) {
  return v1 < v2 ? v1 : v2;
}

template <class T1, class T2, class... Tys>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2, Tys...>(min)(T1 v1, T2 v2, Tys... values) {
  return (MI min)(v1, (MI min)(v2, values...));
}
}  // namespace mi

namespace mi {
template <class Comparator, class T1, class T2>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2> max_comp(Comparator comp, T1 v1, T2 v2) {
  return comp(v1, v2) ? v1 : v2;
}

template <class Comparator, class T1, class T2, class... Tys>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2, Tys...> max_comp(Comparator comp, T1 v1, T2 v2,
                                                                                        Tys... values) {
  return max_comp(comp, v1, (MI max)(comp, v2, values...));
}

template <class Comparator, class T1, class T2>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2> min_comp(Comparator comp, T1 v1, T2 v2) {
  return comp(v1, v2) ? v1 : v2;
}

template <class Comparator, class T1, class T2, class... Tys>
MI_NODISCARD MI_CONSTEXPR_17 MI_FORCE_INLINE STD common_type_t<T1, T2, Tys...> min_comp(Comparator comp, T1 v1, T2 v2,
                                                                                        Tys... values) {
  return min_comp(comp, v1, (MI min)(comp, v2, values...));
}
}  // namespace mi
