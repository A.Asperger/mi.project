#pragma once

#include <ostream>
#include <string>
#include <variant>

#include "MI/Common/MI.Check.hpp"

namespace mi {

// Success values should be represented as the presence of a value in ErrorOr,
// using `ErrorOr<Success>` and `return Success();` if no value needs to be
// returned.
struct Success {};

// Tracks an error message.
class Error {
  public:
    // Represents an error state.
    explicit Error(std::string prefix, std::string location, std::string message)
        : _prefix(prefix), _location(location), _message(message) {
      MI_CHECK(!_message.empty()) << "Errors must have a message.";
    }

    // Represents an error with no associated prefix or location.
    // TODO: Consider using two different types.
    explicit Error(std::string message) : Error("", "", message) {}

    Error(Error&& other) noexcept
        : _prefix(std::move(other._prefix)),
          _location(std::move(other._location)),
          _message(std::move(other._message)) {}

    // Prints the error string.
    void Print(std::stringstream& out) const {
      if (!prefix().empty()) {
        out << prefix() << ": ";
      }
      if (!location().empty()) {
        out << location() << ": ";
      }
      out << message();
    }

    // Returns the prefix to prepend to the error, such as "ERROR".
    const std::string& prefix() const { return _prefix; }

    // Returns a string describing the location of the error, such as
    // "file.cc:123".
    const std::string& location() const { return _location; }

    // Returns the error message.
    const std::string& message() const { return _message; }

  private:
    // A prefix, indicating the kind of error.
    std::string _prefix;
    // The location associated with the error.
    std::string _location;
    // The error message.
    std::string _message;
};

// Holds a value of type `T`, or an Error explaining why the value is
// unavailable.
template <typename T>
class ErrorOr {
  public:
    // Constructs with an error; the error must not be Error::Success().
    // Implicit for easy construction on returns.
    // NOLINTNEXTLINE(google-explicit-constructor)
    ErrorOr(Error err) : _val(std::move(err)) {}

    // Constructs with a value.
    // Implicit for easy construction on returns.
    // NOLINTNEXTLINE(google-explicit-constructor)
    ErrorOr(T val) : _val(std::move(val)) {}

    // Moves held state.
    ErrorOr(ErrorOr&& other) noexcept : _val(std::move(other._val)) {}

    // Returns true for success.
    MI_NODISCARD bool ok() const { return std::holds_alternative<T>(_val); }

    // Returns the contained error.
    // REQUIRES: `ok()` is false.
    MI_NODISCARD const Error& error() const& {
      MI_CHECK(!ok());
      return std::get<Error>(_val);
    }

    Error error() && {
      MI_CHECK(!ok());
      return std::get<Error>(std::move(_val));
    }

    // Returns the contained value.
    // REQUIRES: `ok()` is true.
    T& operator*() {
      MI_CHECK(ok());
      return std::get<T>(_val);
    }

    // Returns the contained value.
    // REQUIRES: `ok()` is true.
    const T& operator*() const {
      MI_CHECK(ok());
      return std::get<T>(_val);
    }

    // Returns the contained value.
    // REQUIRES: `ok()` is true.
    T* operator->() {
      MI_CHECK(ok());
      return &std::get<T>(_val);
    }

    // Returns the contained value.
    // REQUIRES: `ok()` is true.
    const T* operator->() const {
      MI_CHECK(ok());
      return &std::get<T>(_val);
    }

  private:
    // Either an error message or
    std::variant<Error, T> _val;
};

// A helper class for accumulating error message and converting to
// `Error` and `ErrorOr<T>`.
class ErrorBuilder {
  public:
    explicit ErrorBuilder(std::string prefix, std::string location)
        : _prefix(std::move(prefix)), _location(std::move(location)) {}

    explicit ErrorBuilder() : ErrorBuilder("", "") {}

    // Accumulates string message.
    template <typename T>
    ErrorBuilder& operator<<(const T& message) {
      _message << message;
      return *this;
    }

    // NOLINTNEXTLINE(google-explicit-constructor): Implicit cast for returns.
    operator Error() { return Error(_prefix, _location, _message.str()); }

    template <typename T>
    // NOLINTNEXTLINE(google-explicit-constructor): Implicit cast for returns.
    operator ErrorOr<T>() {
      return Error(_prefix, _location, _message.str());
    }

  private:
    std::string _prefix;
    std::string _location;
    std::stringstream _message;
};

}  // namespace mi

// Macro hackery to get a unique variable name.
#define MI_MAKE_UNIQUE_NAME_IMPL(a, b, c) a##_##b##_##c
#define MI_MAKE_UNIQUE_NAME(a, b, c)      MI_MAKE_UNIQUE_NAME_IMPL(a, b, c)

#define MI_RETURN_IF_ERROR_IMPL(unique_name, expr)                                                                     \
  if (auto unique_name = (expr); /* NOLINT(bugprone-macro-parentheses) */                                              \
      !(unique_name).ok())                                                                                             \
  return std::move(unique_name).error()

#define MI_RETURN_IF_ERROR(expr)                                                                                       \
  MI_RETURN_IF_ERROR_IMPL(MI_MAKE_UNIQUE_NAME(_mi_error_line, __LINE__, __COUNTER__), expr)

#define MI_ASSIGN_OR_RETURN_IMPL(unique_name, var, expr)                                                               \
  auto unique_name = (expr); /* NOLINT(bugprone-macro-parentheses) */                                                  \
  if (!(unique_name).ok()) {                                                                                           \
    return std::move(unique_name).error();                                                                             \
  }                                                                                                                    \
  var = std::move(*(unique_name)) /* NOLINT(bugprone-macro-parentheses) */

#define MI_ASSIGN_OR_RETURN(var, expr)                                                                                 \
  MI_ASSIGN_OR_RETURN_IMPL(MI_MAKE_UNIQUE_NAME(_mi_expected_line, __LINE__, __COUNTER__), var, expr)
