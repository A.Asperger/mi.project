#pragma once

#include "MI/Common/MI.Check.hpp"

namespace mi {
template <class Ty>
constexpr void verify_range(const Ty* const first, const Ty* const last) noexcept {
  MI_DCHECK(first <= last) << "transposed pointer range";
}

template <class It1, class It2>
static constexpr bool mi_range_verifiable_v =
    MI Requires<It1, It2>([](auto&& it1, auto&& it2) -> decltype(MI verify_range(it1, it2)) {});

template <class It1, class It2>
static constexpr bool std_range_verifiable_v =
    MI Requires<It1, It2>([](auto&& it1, auto&& it2) -> decltype(STD _Adl_verify_range(it1, it2)) {});

template <class It1, class It2>
constexpr void adl_verify_range(const It1& first, const It2& last) {
  if constexpr (mi_range_verifiable_v<It1, It2>) {
    MI verify_range(first, last);
  }

  if constexpr (std_range_verifiable_v<It1, It2>) {
    STD _Adl_verify_range(first, last);
  }
}
}  // namespace mi
