// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Common/MI.GetUnwrapped.h"
#include "MI/Common/MI.VerifyRange.h"
#include "MI/Container/MI.CheckedIterators.h"

namespace mi::test {

TEST(GetUnwrapped, Std) {
  STD vector<int>::iterator first;
  STD vector<int>::const_iterator last;

  auto first_unwrapped = MI get_unwrapped(first);
  auto last_unwrapped  = MI get_unwrapped(last);

  static_assert(STD is_same_v<decltype(first_unwrapped), int*>);
  static_assert(STD is_same_v<decltype(last_unwrapped), const int*>);

  static_assert(msvc_wrapped_seekable_v<STD vector<int>::iterator, int*>);
}

TEST(GetUnwrapped, Mi) {
  MI checked_contiguous_iterator<int> first;
  MI checked_contiguous_const_iterator<int> last;

  auto first_unwrapped = MI get_unwrapped(first);
  auto last_unwrapped  = MI get_unwrapped(last);

  static_assert(STD is_same_v<decltype(first_unwrapped), int*>);
  static_assert(STD is_same_v<decltype(last_unwrapped), const int*>);

  static_assert(mi_wrapped_seekable_v<MI checked_contiguous_iterator<int>, int*>);
}

TEST(VerifyRange, Std) {
  STD vector<int> v = {12, 23};
  MI adl_verify_range(v.begin(), v.end());
  MI_EXPECT_DCHECK_DEATH(MI adl_verify_range(v.end(), v.begin()));
}
}  // namespace mi::test
