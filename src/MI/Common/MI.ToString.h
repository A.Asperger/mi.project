#pragma once

#include <sstream>
#include <string>
#include <type_traits>

#include "MI.If.h"
#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.AlwaysFalse.h"
#include "MI/Common/MI.TemplateUtil.hpp"

namespace mi {
// По аналогии с hash.
// Если хотим, чтобы класс преобразовывался в std::string нужно реализовать частичную специализацию mi::to_string с
// operator(), который возвращает строку.

// Базовая реализация mi::to_string. Пустая, потому что для каждого типа требуется частичная специализация.
template <class Ty>
struct to_string;

template <class Ty>
struct to_string<Ty*> {
    STD string operator()(const Ty* value) const {
      STD stringstream string_stream;

      string_stream << static_cast<void*>(value);

      return string_stream.str();
    }
};

template <>
struct to_string<bool> {
    STD string operator()(const bool value) const { return value ? "true" : "false"; }
};

template <>
struct to_string<char> {
    STD string operator()(const char value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<signed char> {
    STD string operator()(const signed char value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<unsigned char> {
    STD string operator()(const unsigned char value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

#if defined(MI_HAS_CHAR8_T)
template <>
struct to_string<char8_t> {
    STD string operator()(const char8_t value) const {
      STD stringstream string_stream;

      // string_stream << value;

      return string_stream.str();
    }
};
#endif

#if defined(MI_HAS_UNICODE_CHAR_T)
template <>
struct to_string<char16_t> {
    STD string operator()(const char16_t value) const {
      STD stringstream string_stream;

      // string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<char32_t> {
    STD string operator()(const char32_t value) const {
      STD stringstream string_stream;

      // string_stream << value;

      return string_stream.str();
    }
};
#endif

template <>
struct to_string<wchar_t> {
    STD string operator()(const wchar_t value) const {
      STD stringstream string_stream;

      // string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<short> {
    STD string operator()(const short value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<unsigned short> {
    STD string operator()(const unsigned short value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<int> {
    STD string operator()(const int value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<unsigned int> {
    STD string operator()(const unsigned int value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<long> {
    STD string operator()(const long value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<unsigned long> {
    STD string operator()(const unsigned long value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<long long> {
    STD string operator()(const long long value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<unsigned long long> {
    STD string operator()(const unsigned long long value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

#if defined(MI_HAS_INT128)
template <>
struct to_string<int128_t> {
    STD string operator()(const int128_t value) const {
      STD stringstream string_stream;

      string_stream << value[0] << value[1];

      return string_stream.str();
    }
};

template <>
struct to_string<uint128_t> {
    STD string operator()(const uint128_t value) const {
      STD stringstream string_stream;

      string_stream << value[0] << value[1];

      return string_stream.str();
    }
};
#endif

template <>
struct to_string<float> {
    STD string operator()(const float value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<double> {
    STD string operator()(const double value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

template <>
struct to_string<long double> {
    STD string operator()(const long double value) const {
      STD stringstream string_stream;

      string_stream << value;

      return string_stream.str();
    }
};

#if MI_CPP_VERSION > 11
namespace internal {
template <class Ty, bool = STD is_enum_v<Ty>>
struct enum_to_string {
    STD string operator()(Ty value) const noexcept {
      using type = STD underlying_type_t<Ty>;
      return to_string<type>{}(static_cast<type>(value));
    }
};

template <class Ty>
struct enum_to_string<Ty, false> {
    ~enum_to_string()                                = default;
    enum_to_string()                                 = delete;
    enum_to_string(const enum_to_string&)            = delete;
    enum_to_string(enum_to_string&&)                 = delete;
    enum_to_string& operator=(const enum_to_string&) = delete;
    enum_to_string& operator=(enum_to_string&&)      = delete;
};
}  // namespace internal

template <class Ty>
struct to_string : public internal::enum_to_string<Ty> {};
#endif

#if MI_CPP_VERSION > 14
template <>
struct to_string<STD nullptr_t> {
    STD string operator()(const STD nullptr_t) const { return {"nullptr_t"}; }
};
#endif

namespace internal {
// 1. Сначала проверяется, поддерживает ли 'value' ostream operator (ostream << value).
template <class Ty, if_t<SupportsOstreamOperator<Ty>, int> = 0>
STD string convert_to_string(Ty value, PriorityTag<4> /* unused */) {
  STD stringstream ss;
  ss << value;

  return ss.str();
}

// 2. Если для типа существует частичная специализация MI to_string.
template <class Ty, if_t<SupportsToString<Ty>, int> = 0>
STD string convert_to_string(Ty value, PriorityTag<3> /* unused */) {
  return to_string<Ty>{}(value);
}

// 3. Если внутри типа есть функция to_string()
//    и эта функция возвращает тип, который можно конвертировать в std::string.
template <class Ty,
          if_t<SupportsToStringInside<Ty> && STD is_convertible_v<decltype(STD declval<Ty>().to_string()), std::string>,
               int> = 0>
STD string convert_to_string(Ty value, PriorityTag<2> /* unused */) {
  return value.to_string();
}

// 4. Если внутри типа есть функция to_string()
//    и эта функция возвращает тип, который (НЕЛЬЗЯ) конвертировать в std::string,
//    то возвращаемый тип проверяется заново сначала (пункты 1, 2, 3).
template <class Ty, if_t<SupportsToStringInside<Ty> &&
                             !STD is_convertible_v<decltype(STD declval<Ty>().to_string()), std::string>,
                         int> = 0>
STD string convert_to_string(Ty value, PriorityTag<1> /* unused */) {
  return internal::convert_to_string(value.to_string(), PriorityTag<4>{});
}

// 5. Если тип не может быть преобразован в std::string, то возвращается строка
//    "TYPE_NOT_HAVE_TO_STRING_PARTIAL_SPECIALIZATION".
template <class Ty>
STD string convert_to_string(Ty /* value */, PriorityTag<0> /* unused */) {
  static_assert(always_false_v<Ty>, "Type not have 'to_string' partial specialization. ");
  return "TYPE_NOT_HAVE_TO_STRING_PARTIAL_SPECIALIZATION";
}
}  // namespace internal

template <class Ty>
STD string convert_to_string(Ty value) {
  return internal::convert_to_string(value, internal::PriorityTag<4>{});
}

template <class T>
static constexpr bool SupportsConvertToString = Requires<T>([](auto&& t) -> decltype(convert_to_string(t)) {});

}  // namespace mi
