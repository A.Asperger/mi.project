#pragma once

// Превращает параметр макроса в строку,
// Пример: MI_STRINGIZE(is_same<int, double>) => "is_same<int, double>"
#define MI_STRINGIZE(X) #X

// MI_JOIN2(is_, one) <=> is_one
#define MI_JOIN2(X, Y) X##Y

// MI_JOIN3(is_, one, _impl) <=> is_one_impl
#define MI_JOIN3(X, Y, Z) X##Y##Z

// Превращает S в wstring.
// Пример: MI_STR("asd") => L"asd"
#define MI_STR(S) L##S
