#pragma once

#include <type_traits>

#include "MI/Base/MI.Property.hpp"

namespace mi {
template <class... Ty>
struct always_false : STD false_type {};

template <class... Ty>
constexpr bool always_false_v = always_false<Ty...>::value;
}  // namespace mi
