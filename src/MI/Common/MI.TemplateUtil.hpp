#pragma once

#include <forward_list>
#include <list>
#include <map>
#include <ostream>
#include <queue>
#include <set>
#include <stack>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "MI/Base/MI.CompilerSpecific.hpp"
#include "MI/Base/MI.Metaprogramming.h"
#include "MI/Base/MI.Property.hpp"

namespace mi {
template <class>
struct to_string;

template <class T>
struct IsNonConstReference : STD false_type {};

template <class T>
struct IsNonConstReference<T&> : STD true_type {};

template <class T>
struct IsNonConstReference<const T&> : STD false_type {};

namespace detail {

template <typename T>
struct IsStlContainer : STD false_type {};

template <typename T, STD size_t N>
struct IsStlContainer<STD array<T, N>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD vector<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD deque<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD list<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD forward_list<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD set<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD multiset<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD map<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD multimap<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD unordered_set<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD unordered_multiset<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD unordered_map<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD unordered_multimap<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD stack<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD queue<Args...>> : STD true_type {};

template <typename... Args>
struct IsStlContainer<STD priority_queue<Args...>> : STD true_type {};

template <typename T>
struct IsInitList : STD false_type {};

template <typename T>
struct IsInitList<STD initializer_list<T>> : STD true_type {};
}  // namespace detail

template <typename AlwaysVoid, typename... Ts>
struct HasCommonType_impl : STD false_type {};

template <typename... Ts>
struct HasCommonType_impl<STD void_t<STD common_type_t<Ts...>>, Ts...> : STD true_type {};

template <typename... Ts>
using HasCommonType = typename HasCommonType_impl<void, Ts...>::type;

template <class T>
static constexpr bool IsContainer =
    detail::IsStlContainer<T>::value || (  // HasValueType<T> &&                                         //
                                           // HasSizeType<T> &&                                          //
                                           // HasConstIterator<T> &&                                     //
                                           // HasConstPointer<T> &&                                      //
                                           // HasConstReference<T> );//&&                                    //
                                            Requires<T>([](auto&& t) -> decltype(t.begin()) {}) &&  //
                                            Requires<T>([](auto&& t) -> decltype(t.end()) {}) &&    //
                                            Requires<T>([](auto&& t) -> decltype(t.size()) {}) &&   //

                                            // Спан без `max_size`, но он контейнер
                                            // Requires<T>([](auto&& t) -> decltype(t.max_size()) {}) &&  //
                                            Requires<T>([](auto&& t) -> decltype(t.empty()) {}));

template <class T>
static constexpr bool IsInitList = detail::IsInitList<T>::value;

// Used to detech whether the given type is an iterator.  This is normally used
// with STD enable_if to provide disambiguation for functions that take
// templatzed iterators as input.
template <class T>
static constexpr bool IsIterator = HasValueType<T> &&         //
                                   HasDifferenceType<T> &&    //
                                   HasReference<T> &&         //
                                   HasPointer<T> &&           //
                                   HasIteratorCategory<T> &&  //
                                   Requires<T>([](auto&& t) {
                                     T j(t);
                                     ++j;  // require preincrement operator
                                     t++;  // require postincrement operator
                                   });

namespace internal {
// Helper to express preferences in an overload set. If more than one overload
// are available for a given set of parameters the overload with the higher
// priority will be chosen.
template <size_t I>
struct PriorityTag : PriorityTag<I - 1> {};

template <>
struct PriorityTag<0> {};

template <class T>
static constexpr bool SupportsOstreamOperator =
    Requires<T, STD ostream>([](auto&& t, auto&& out) -> decltype(out << t) {});

template <class T>
static constexpr bool SupportsToString =
    Requires<T, MI to_string<T>>([](auto&& t, auto&& to_str) -> decltype(to_str(t)) {});

template <class T>
static constexpr bool SupportsToStringInside = Requires<T>([](auto&& t) -> decltype(t.to_string()) {});

template <class... Ty>
struct IsAnyOf : STD disjunction<Ty...> {};

template <class... Ty>
constexpr bool IsAnyOfV = IsAnyOf<Ty...>::value;

template <class... Ty>
struct IsAllOf : STD conjunction<Ty...> {};

template <class... Ty>
constexpr bool IsAllOfV = IsAllOf<Ty...>::value;

//
template <class Ty, class... Types>
struct IsSameAny : STD disjunction<STD is_same<Ty, Types>...> {};

template <class Ty, class... Types>
struct IsSameAll : STD conjunction<STD is_same<Ty, Types>...> {};

template <class Ty, class... Types>
constexpr bool IsSameAnyV = IsSameAny<Ty, Types...>::value;

template <class Ty, class... Types>
constexpr bool IsSameAllV = IsSameAll<Ty, Types...>::value;

// Проверяет, что все типы в пакете параметров одинаковые.
// static_assert(STD conjunction_v<STD is_same<First, Rest>...>);
template <class First, class... Rest>
struct EnforceSame {
    static_assert(STD conjunction_v<STD is_same<First, Rest>...>,
                  "Requires: (is_same_v<T, U> && ...) is true. Otherwise the program is ill-formed.");
    using Type = First;
};

template <class First, class... Rest>
using EnforceSameT = typename EnforceSame<First, Rest...>::Type;

// The indirection with STD is_enum<T> is required, because instantiating
// STD underlying_type_t<T> when T is not an enum is UB prior to C++20.
template <typename T, bool = STD is_enum_v<T>>
struct IsScopedEnumImpl : STD false_type {};

template <typename T>
struct IsScopedEnumImpl<T, /*STD is_enum<T>::value=*/true>
    : STD negation<STD is_convertible<T, STD underlying_type_t<T>>> {};

}  // namespace internal

// ==========================================
// NOLINTBEGIN(readability-identifier-naming)
// ==========================================

// Implementation of C++23's STD is_scoped_enum
//
// Reference: https://en.cppreference.com/w/cpp/types/is_scoped_enum
template <typename T>
struct is_scoped_enum : internal::IsScopedEnumImpl<T> {};

// Implementation of C++20's STD remove_cvref.
//
// References:
// - https://en.cppreference.com/w/cpp/types/remove_cvref
// - https://wg21.link/meta.trans.other#lib:remove_cvref
template <typename T>
struct remove_cvref {
    using type = STD remove_cv_t<STD remove_reference_t<T>>;
};

// Implementation of C++20's STD remove_cvref_t.
//
// References:
// - https://en.cppreference.com/w/cpp/types/remove_cvref
// - https://wg21.link/meta.type.synop#lib:remove_cvref_t
template <typename T>
using remove_cvref_t = typename remove_cvref<T>::type;

// Implementation of C++20's STD is_constant_evaluated.
//
// References:
// - https://en.cppreference.com/w/cpp/types/is_constant_evaluated
// - https://wg21.link/meta.const.eval
constexpr bool is_constant_evaluated() noexcept {
#if MI_HAS_BUILTIN(__builtin_is_constant_evaluated)
  return __builtin_is_constant_evaluated();
#else
  return false;
#endif
}

// Simplified implementation of C++20's STD iter_value_t.
// As opposed to STD iter_value_t, this implementation does not restrict
// the type of `Iter` and does not consider specializations of
// `indirectly_readable_traits`.
//
// Reference: https://wg21.link/readable.traits#2
template <typename Iter>
using iter_value_t = typename STD iterator_traits<remove_cvref_t<Iter>>::value_type;

// Simplified implementation of C++20's STD iter_reference_t.
// As opposed to STD iter_reference_t, this implementation does not restrict
// the type of `Iter`.
//
// Reference: https://wg21.link/iterator.synopsis#:~:text=iter_reference_t
template <typename Iter>
using iter_reference_t = decltype(*STD declval<Iter&>());

// Simplified implementation of C++20's STD iter_difference_t.
// As opposed to STD iter_difference_t, this implementation does not restrict
// the type of `Iter`.
template <typename Iter>
using iter_difference_t = typename STD iterator_traits<remove_cvref_t<Iter>>::difference_type;

// Simplified implementation of C++20's STD indirect_result_t. As opposed to
// STD indirect_result_t, this implementation does not restrict the type of
// `Func` and `Iters`.
//
// Reference: https://wg21.link/iterator.synopsis#:~:text=indirect_result_t
template <typename Func, typename... Iters>
using indirect_result_t = STD invoke_result_t<Func, iter_reference_t<Iters>...>;

// Simplified implementation of C++20's STD projected. As opposed to
// STD projected, this implementation does not explicitly restrict the type of
// `Iter` and `Proj`, but rather does so implicitly by requiring
// `indirect_result_t<Proj, Iter>` is a valid type. This is required for SFINAE
// friendliness.
//
// Reference: https://wg21.link/projected
template <typename Iter, typename Proj, typename IndirectResultT = indirect_result_t<Proj, Iter>>
struct projected {
    using value_type = remove_cvref_t<IndirectResultT>;

    IndirectResultT operator*() const;  // not defined
};

template <class T>
using const_reference_t = typename T::const_reference;

template <class T>
using reference_t = typename T::reference;

template <class T>
using const_pointer_t = typename T::const_pointer;

template <class T>
using pointer_t = typename T::pointer;

}  // namespace mi
