#pragma once

#include <type_traits>

namespace mi {
template <bool Test, class Ty = int>
using if_t = std::enable_if_t<Test, Ty>;
}  // namespace mi
