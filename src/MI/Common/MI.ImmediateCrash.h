#pragma once

#include "MI/Base/MI.Property.hpp"

#if defined(MI_MSVC_COMPILER)
  // Вызывает немедленное прекращение работы программы.
  #define MI_IMMEDIATE_CRASH() __debugbreak()
#elif defined(MI_CLANG_COMPILER) || defined(MI_GNU_COMPILER)
  // Вызывает немедленное прекращение работы программы.
  #define MI_IMMEDIATE_CRASH() __builtin_trap()
#endif

// TODO(Anton M.): перенести в отдельный файл для дебага.
#define MI_DEBUG_BREAK_IF(cond) (cond) ? MI_IMMEDIATE_CRASH() : (void)0
