#pragma once

// TODO(Anton M.): Не забыть убрать.
#define DCHECK_ALWAYS_ON

#if defined(NDEBUG) && !defined(DCHECK_ALWAYS_ON)
  #define MI_DCHECK_IS_ON() false
#else
  #define MI_DCHECK_IS_ON() true
#endif
