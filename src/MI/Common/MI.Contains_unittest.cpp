// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <array>
#include <gmock/gmock.h>
#include <gtest/gtest-spi.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Common/MI.Contains.h"

namespace mi::test {

TEST(ContainsTest, GenericContains) {
  constexpr char allowed_chars[] = {'a', 'b', 'c', 'd'};

  EXPECT_TRUE(MI contains(allowed_chars, 'a'));
  EXPECT_TRUE(!MI contains(allowed_chars, 'z'));
  EXPECT_TRUE(!MI contains(allowed_chars, 0));

  constexpr char allowed_chars_including_nul[] = "abcd";
  EXPECT_TRUE(MI contains(allowed_chars_including_nul, 0));
}

TEST(ContainsTest, ContainsWithFindAndNpos) {
  std::string str = "abcd";

  EXPECT_TRUE(MI contains(str, 'a'));
  EXPECT_FALSE(MI contains(str, 'z'));
  // EXPECT_FALSE(MI contains(str, 0));
}

TEST(ContainsTest, ContainsWithFindAndEnd) {
  std::set<int> set = {1, 2, 3, 4};

  EXPECT_TRUE(MI contains(set, 1));
  EXPECT_FALSE(MI contains(set, 5));
  EXPECT_FALSE(MI contains(set, 0));
}

TEST(ContainsTest, Array) {
  STD array<int, 3> ar = {1, 2, 12};

  EXPECT_TRUE(MI contains(ar, 1));
  EXPECT_TRUE(MI contains(ar, 2));
  EXPECT_TRUE(MI contains(ar, 12));
  EXPECT_FALSE(MI contains(ar, 0));
}

TEST(ContainsTest, ContainerWithHashType) {}

}  // namespace mi::test
