#pragma once

#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.TemplateUtil.hpp"

namespace mi::internal {
// Проверяем, существует ли функция-член 'contains', и возвращаем результат
// 'container.contains(value)', если это допустимое выражение. Это
// вариант с наивысшим приоритетом.
template <typename Container, typename Value>
MI_CONSTEXPR_20 auto contains_impl(const Container& container, const Value& value, PriorityTag<2> /* unused */)
    -> decltype(container.contains(value)) {
  return container.contains(value);
}

// Проверяем, существует ли функция-член find и возвращает ли она значение, которое
// можно сравнить с 'container.end()'. Предназначен для карт и наборов в стиле STL.
// в которых отсутствует функция-член 'contains'.
template <typename Container, typename Value>
MI_CONSTEXPR_20 auto contains_impl(const Container& container, const Value& value, PriorityTag<1> /* unused */)
    -> decltype(container.find(value) != container.end()) {
  return container.find(value) != container.end();
}

// Проверяем, существует ли функция-член find и возвращает ли она значение, которое
// можно сравнить с 'Container::npos'. Предназначен для строк в стиле STL, в которых
// отсутствует функция-член 'contains'.
template <typename Container, typename Value>
MI_CONSTEXPR_20 auto contains_impl(const Container& container, const Value& value, PriorityTag<1> /* unused */)
    -> decltype(container.find(value) != Container::npos) {
  return container.find(value) != Container::npos;
}

// Общий запасной вариант, использующий линейный поиск по 'контейнеру', чтобы найти
// 'значение'. Имеет самый низкий приоритет. Это не будет компилироваться для ассоциативного
// контейнеры, так как это, вероятно, баг.
template <typename Container, typename Value>
MI_CONSTEXPR_20 bool contains_impl(const Container& container, const Value& value, PriorityTag<0> /* unused */) {
  static_assert(!HasKeyType<Container>,
                "Error: About to perform linear search on an associative container. "
                "Either use a more generic comparator (e.g. std::less<>) or, if a linear "
                "search is desired, provide an explicit projection parameter.");
  return STD find(STD begin(container), STD end(container), value) != STD end(container);
}
}  // namespace mi::internal

namespace mi {
// Функция общего назначения для проверки того, содержит ли 'container' значение 'value'.
//
// Порядок работы:
// 1. Если "container' содержит функцию contains(), проверяет наличие элемента с помощью нее.
// 2. Если "container' содержит функцию find(), проверяет наличие элемента с помощью нее.
//    (container.find(value) != container.end())
// 3. Если 'Container' это строка и у нее есть функция find(), проверяет наличие элемента с помощью нее.
//    (container.find(value) != Container::npos)
// 4. В последнюю очередь, если ни один из предыдущих пунктов не подошел, выполняет линейный поиск O(n).
//    (!) Важно, что при этом проверяется наличие алиаса key_type в Container, потому что, если он есть, то это явно
//        ошибка реализации.
//
// (!) НЕ ИСПОЛЬЗУЙ эту функцию внутри реализации функции contains() в каком-то объекте (foo.contains(...)),
//     иначе получится бесконечное зацикливание.
template <typename Container, typename Value>
MI_CONSTEXPR_20 bool contains(const Container& container, const Value& value) {
  return internal::contains_impl(container, value, internal::PriorityTag<2>());
}

#if (MI_CPP_VERSION >= 20)
// Перегрузка, позволяющая предоставить дополнительную вызываемую проекцию. Эта
// проекция будет применена к каждому элементу в контейнере перед сравнением с 'value'.
//
// (!) Всегда будет выполняться линейный поиск.
template <typename Container, typename Value, typename Proj>
MI_CONSTEXPR_20 bool contains(const Container& container, const Value& value, Proj proj) {
  return STD ranges::find(container, value, std::move(proj)) != STD ranges::end(container);
}
#endif
}  // namespace mi
