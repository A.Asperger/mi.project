#pragma once

#include <string>

#include "MI.HelperMacros.h"
#include "MI.TemplateUtil.hpp"
#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.GetUnwrapped.h"
#include "MI/Common/MI.If.h"
#include "MI/Common/MI.ToString.h"

namespace mi {
struct in_brackets {
  public:
    in_brackets()                                = delete;
    in_brackets(const in_brackets& brackets)     = delete;
    in_brackets(in_brackets&& brackets) noexcept = delete;

  private:
    template <class TyHead, class... Ty>
    static STD string body(TyHead&& head, Ty&&... values) {
      return MI convert_to_string(head) + ", " + body(values...);
    }

    template <class Ty>
    static STD string body(Ty&& val) {
      return MI convert_to_string(val);
    }

  public:
    template <class T, if_t<MI IsContainer<T>> = 0>
    static STD string round(T&& cont) {
      STD string res;

      for (size_t n = 0; n < cont.size(); n++) {
        res += MI convert_to_string(cont[n]);
        if (n + 1 < cont.size()) {
          res += ", ";
        }
      }
      return "(" + res + ")";
    }

    template <class T, if_t<!MI IsContainer<T>> = 0>
    static STD string round(T&& v) {
      return "(" + MI convert_to_string(v) + ")";
    }

    template <class T1, class T2, class... Ty>
    static STD string round(T1&& v1, T2&& v2, Ty&&... values) {
      return "(" + body(v1, v2, values...) + ")";
    }

  public:
    template <class T, if_t<MI IsContainer<T>> = 0>
    static STD string square(T&& cont) {
      STD string res;

      for (size_t n = 0; n < cont.size(); n++) {
        res += MI convert_to_string(cont[n]);
        if (n + 1 < cont.size()) {
          res += ", ";
        }
      }
      return "[" + res + "]";
    }

    template <class T, if_t<!MI IsContainer<T>> = 0>
    static STD string square(T&& v) {
      return "[" + MI convert_to_string(v) + "]";
    }

    template <class T1, class T2, class... Ty>
    static STD string square(T1&& v1, T2&& v2, Ty&&... values) {
      return "[" + body(v1, v2, values...) + "]";
    }

  public:
    template <class T, if_t<MI IsContainer<T>> = 0>
    static STD string curly(T&& cont) {
      STD string res;

      for (size_t n = 0; n < cont.size(); n++) {
        res += convert_to_string(cont[n]);
        if (n + 1 < cont.size()) {
          res += ", ";
        }
      }
      return "{" + res + "}";
    }

    template <class T, if_t<!MI IsContainer<T>> = 0>
    static STD string curly(T&& v) {
      return "{" + MI convert_to_string(v) + "}";
    }

    template <class T1, class T2, class... Ty>
    static STD string curly(T1&& v1, T2&& v2, Ty&&... values) {
      return "{" + body(v1, v2, values...) + "}";
    }
};
}  // namespace mi
