// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Base/Test/MI.GetMesh.h"
#include "MI/Common/MI.Error.h"

namespace mi::test {
namespace {
Error IndirectError() { return Error("test"); }

ErrorOr<int> IndirectErrorOrTest() { return Error("test"); }

ErrorOr<Success> IndirectErrorOrSuccessTest() { return Success(); }

struct Val {
    int val;
};
}  // namespace

TEST(ErrorTest, Error) {
  Error err("test");
  EXPECT_EQ(err.message(), "test");
}

TEST(ErrorTest, ErrorEmptyString) {
  MI_EXPECT_CHECK_DEATH({ Error err(""); });
}

TEST(ErrorTest, IndirectError) { EXPECT_EQ(IndirectError().message(), "test"); }

TEST(ErrorTest, ErrorOr) {
  ErrorOr<int> err(Error("test"));
  EXPECT_FALSE(err.ok());
  EXPECT_EQ(err.error().message(), "test");
}

TEST(ErrorTest, ErrorOrValue) { EXPECT_TRUE(ErrorOr<int>(0).ok()); }

TEST(ErrorTest, IndirectErrorOr) { EXPECT_FALSE(IndirectErrorOrTest().ok()); }

TEST(ErrorTest, ErrorOrArrowOp) {
  ErrorOr<Val> err({1});
  EXPECT_EQ(err->val, 1);
}

TEST(ErrorTest, IndirectErrorOrSuccess) { EXPECT_TRUE(IndirectErrorOrSuccessTest().ok()); }

TEST(ErrorTest, ReturnIfErrorNoError) {
  auto result = []() -> ErrorOr<Success> {
    MI_RETURN_IF_ERROR(ErrorOr<Success>(Success()));
    MI_RETURN_IF_ERROR(ErrorOr<Success>(Success()));
    return Success();
  }();
  EXPECT_TRUE(result.ok());
}

TEST(ErrorTest, ReturnIfErrorHasError) {
  auto result = []() -> ErrorOr<Success> {
    MI_RETURN_IF_ERROR(ErrorOr<Success>(Success()));
    MI_RETURN_IF_ERROR(ErrorOr<Success>(Error("error")));
    return Success();
  }();
  ASSERT_FALSE(result.ok());
  EXPECT_EQ(result.error().message(), "error");
}

TEST(ErrorTest, AssignOrReturnNoError) {
  auto result = []() -> ErrorOr<int> {
    MI_ASSIGN_OR_RETURN(int a, ErrorOr<int>(1));
    MI_ASSIGN_OR_RETURN(const int b, ErrorOr<int>(2));
    int c = 0;
    MI_ASSIGN_OR_RETURN(c, ErrorOr<int>(3));
    return a + b + c;
  }();
  ASSERT_TRUE(result.ok());
  EXPECT_EQ(6, *result);
}

TEST(ErrorTest, AssignOrReturnHasDirectError) {
  auto result = []() -> ErrorOr<int> {
    MI_RETURN_IF_ERROR(ErrorOr<int>(Error("error")));
    return 0;
  }();
  ASSERT_FALSE(result.ok());
}

TEST(ErrorTest, AssignOrReturnHasErrorInExpected) {
  auto result = []() -> ErrorOr<int> {
    MI_ASSIGN_OR_RETURN(int a, ErrorOr<int>(Error("error")));
    return a;
  }();
  ASSERT_FALSE(result.ok());
  EXPECT_EQ(result.error().message(), "error");
}

}  // namespace mi::test
