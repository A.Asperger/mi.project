// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once
#include <sstream>

#include "MI/Base/MI.BaseExport.hpp"

namespace mi::internal {

// Wraps a stream and exiting for fatal errors. Should only be used by check.h
// macros.
class MI_BASE_EXPORT ExitingStream {
  public:
    // A tag type that renders as ": " in an ExitingStream, but only if it is
    // followed by additional output. Otherwise, it renders as "". Primarily used
    // when building macros around these streams.
    struct AddSeparator {};

    // internal type used in macros to dispatch to the `operator|` overload.
    struct Helper {};

    // Prefix the buffer with the current bug report message.
    ExitingStream() = default;

    // Never called.
    [[noreturn]] ~ExitingStream();

    // If the bool cast occurs, it's because the condition is false. This supports
    // && short-circuiting the creation of ExitingStream.
    explicit operator bool() const { return true; }

    // Forward output to llvm::errs.
    template <typename T>
    auto operator<<(const T& message) -> ExitingStream& {
      if (separator_) {
        _ss << ": ";
        separator_ = false;
      }
      _ss << message;
      return *this;
    }

    auto operator<<(AddSeparator /*add_separator*/) -> ExitingStream& {
      separator_ = true;
      return *this;
    }

    // Low-precedence binary operator overload used in check.h macros to flush the
    // output and exit the program. We do this in a binary operator rather than
    // the destructor to ensure good debug info and backtraces for errors.
    [[noreturn]] friend auto operator|(Helper /*helper*/, ExitingStream& stream) -> void { stream.Done(); }  //-V1082

  private:
    [[noreturn]] auto Done() -> void;

    // Whether a separator should be printed if << is used again.
    bool separator_ = false;

    std::stringstream _ss;
};

}  // namespace mi::internal
