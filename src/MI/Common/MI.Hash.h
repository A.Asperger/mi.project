#pragma once

#include <climits>      // for CHAR_BIT
#include <cstring>      // for std::memcpy
#include <string>
#include <type_traits>  // for std::memcpy

#include "MI/Base/MI.CompilerSpecific.hpp"
#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.Check.hpp"

// Определяет, будут ли функции __forceinline

// Всегда просто inline

namespace mi::internal {
template <class SizeType>
MI_FORCE_INLINE SizeType loadword(const void* __restrict source) {
  SizeType r;
  STD memcpy(&r, source, sizeof(r));

  return r;
}

inline void hash_combine(size_t& seed, const size_t& value) {
  seed ^= value + 0x9e3779b9 + (seed << static_cast<size_t>(6)) + (seed >> static_cast<size_t>(2));
}

template <class SizeType, size_t = sizeof(SizeType) * CHAR_BIT>
struct boost_hash;

// 32-bit systems
template <class SizeType>
struct boost_hash<SizeType, 32> {
    MI_FORCE_INLINE SizeType operator()(const void* /* key */, SizeType /* len */) {
      MI_CHECK(false) << "Supported only x64-bit systems. ";

      return {};
    }
};

// 64-bit systems
template <class SizeType>
struct boost_hash<SizeType, 64> {
    MI_FORCE_INLINE SizeType operator()(const void* key, SizeType len) {
      const char* s = static_cast<const char*>(key);

      SizeType seed = 0;
      SizeType value;

      for (size_t n_bit = 0; n_bit < len; n_bit += 64) {
        value = loadword<SizeType>(s + n_bit);
        hash_combine(seed, value);
      }

      return seed;
    }
};

template <class Ty, size_t = sizeof(Ty) / sizeof(size_t)>
struct scalar_hash;

template <class Ty>
struct scalar_hash<Ty, 0> {
    MI_FORCE_INLINE size_t operator()(Ty value) const noexcept {
      union {
          Ty t;
          size_t a;  //-V117
      } u = {};

      u.a = 0;
      u.t = value;
      return u.a;
    }
};

template <class Ty>
struct scalar_hash<Ty, 1> {
    MI_FORCE_INLINE size_t operator()(Ty value) const noexcept {
      union {
          Ty t;
          size_t a;  //-V117
      } u = {};

      u.t = value;
      return u.a;
    }
};

template <class Ty>
struct scalar_hash<Ty, 2> {
    MI_FORCE_INLINE size_t operator()(Ty value) const noexcept {
      union {
          Ty t;

          struct {
              size_t a;
              size_t b;
          } s;
      } u;

      u.t = value;
      return boost_hash<size_t>{}(&u, sizeof(u));
    }
};

template <class Ty>
struct scalar_hash<Ty, 3> {
    MI_FORCE_INLINE size_t operator()(Ty value) const noexcept {
      union {
          Ty t;

          struct {
              size_t a;
              size_t b;
              size_t c;
          } s;
      } u;

      u.t = value;
      return boost_hash<size_t>{}(&u, sizeof(u));
    }
};

template <class Ty>
struct scalar_hash<Ty, 4> {
    MI_FORCE_INLINE size_t operator()(Ty value) const noexcept {
      union {
          Ty t;

          struct {
              size_t a;
              size_t b;
              size_t c;
              size_t d;
          } s;
      } u;

      u.t = value;
      return boost_hash<size_t>{}(&u, sizeof(u));
    }
};
}  // namespace mi::internal

namespace mi {
/// @brief Функция объединяет два хэша
MI_FORCE_INLINE size_t hash_combine(const size_t lhs, const size_t rhs) noexcept {
  size_t seed = 0;
  MI internal::hash_combine(seed, lhs);
  MI internal::hash_combine(seed, rhs);

  return seed;
}

template <class>
struct hash;

#define MI_DEFINE_HASH_TYPE(T)                                                                                         \
  template <>                                                                                                          \
  struct hash<T> : public MI internal::scalar_hash<T> {}

template <class Ty>
struct hash<Ty*> {
    MI_FORCE_INLINE size_t operator()(Ty* value) const noexcept {
      union {
          Ty* t;     //-V117
          size_t a;  //-V117
      } u;

      u.t = value;

      return MI internal::boost_hash<size_t>()(&u, sizeof(u));
    }
};

MI_DEFINE_HASH_TYPE(bool);
MI_DEFINE_HASH_TYPE(char);

MI_DEFINE_HASH_TYPE(int8_t);
MI_DEFINE_HASH_TYPE(uint8_t);

MI_DEFINE_HASH_TYPE(int16_t);
MI_DEFINE_HASH_TYPE(uint16_t);

MI_DEFINE_HASH_TYPE(int32_t);
MI_DEFINE_HASH_TYPE(uint32_t);

MI_DEFINE_HASH_TYPE(int64_t);
MI_DEFINE_HASH_TYPE(uint64_t);

#if defined(MI_HAS_CHAR8_T)
MI_DEFINE_HASH_TYPE(char8_t);
#endif  // !MI_HAS_CHAR8_T

#if defined(MI_HAS_UNICODE_CHAR_T)
MI_DEFINE_HASH_TYPE(char16_t);
MI_DEFINE_HASH_TYPE(char32_t);
#endif  // MI_HAS_UNICODE_CHAR_T

MI_DEFINE_HASH_TYPE(wchar_t);

#if defined(MI_HAS_INT128)
MI_DEFINE_HASH_TYPE(int128_t);
MI_DEFINE_HASH_TYPE(uint128_t);
#endif  // MI_HAS_INT128

template <>
struct hash<float_t> : public MI internal::scalar_hash<float> {
    MI_FORCE_INLINE size_t operator()(const float value) const noexcept {
      // -0.0 and 0.0 should return same hash
      if (value == 0.0F) {  //-V550
        return 0;
      }
      return scalar_hash<float>::operator()(value);
    }
};

template <>
struct hash<double_t> : public MI internal::scalar_hash<double> {
    MI_FORCE_INLINE size_t operator()(const double value) const noexcept {
      // -0.0 and 0.0 should return same hash
      if (value == 0.0) {  //-V550
        return 0;
      }
      return scalar_hash<double>::operator()(value);
    }
};

#if MI_CPP_VERSION > 11
namespace internal {
template <class Ty, bool = STD is_enum_v<Ty>>
struct enum_hash {
    MI_FORCE_INLINE size_t operator()(Ty value) const noexcept {
      using type = STD underlying_type_t<Ty>;
      return hash<type>{}(static_cast<type>(value));
    }
};

template <class Ty>
struct enum_hash<Ty, false> {
    enum_hash()                            = delete;
    enum_hash(const enum_hash&)            = delete;
    enum_hash& operator=(const enum_hash&) = delete;
};
}  // namespace internal

template <class Ty>
struct hash : public internal::enum_hash<Ty> {};
#endif  // MI_CPP_VERSION > 11

#if MI_CPP_VERSION > 14
template <>
struct hash<STD nullptr_t> {
    MI_FORCE_INLINE size_t operator()(STD nullptr_t) const noexcept { return 662607004ULL; }
};
#endif  // MI_CPP_VERSION > 14

template <>
struct hash<std::string> {
    MI_FORCE_INLINE size_t operator()(const std::string& value) const noexcept {
      // TODO(Anton Mitrokhin): Можно реализовать city_hash.
      return std::hash<std::string>{}(value);
    }
};

template <class KeyType, class HahType>
using check_hash_requirements =
    STD integral_constant<bool, STD is_copy_constructible_v<HahType> && STD is_move_constructible_v<HahType> &&
                                    STD is_invocable_r_v<size_t, HahType, const KeyType&>>;

template <class KeyType, class HahType = hash<KeyType>>
using has_enabled_hash = STD integral_constant<bool, check_hash_requirements<KeyType, HahType>::value &&
                                                         STD is_default_constructible_v<HahType>>;
}  // namespace mi
