#pragma once

#include "MI.If.h"
#include "MI/Base/MI.Metaprogramming.h"
#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.TemplateUtil.hpp"

namespace mi::internal {
template <class It>
static constexpr bool mi_unwrappable_v = IsIterator<It> &&  //
                                         MI Requires<It>([](auto&& it) -> decltype(it.seek_to(it.unwrapped())) {});

template <class It>
static constexpr bool msvc_unwrappable_v = MI IsIterator<It> &&  //
                                           MI Requires<It>([](auto&& it) -> decltype(it._Seek_to(it._Unwrapped())) {});

template <class T>
static constexpr bool mi_do_unwrap_when_unverified_v =
    MI Requires<T>([]() -> decltype(T::unwrap_when_unverified) {}) && static_cast<bool>(T::unwrap_when_unverified);

template <class ItTy>
static constexpr bool mi_unwrappable_for_unverified_v = mi_unwrappable_v<ItTy> && mi_do_unwrap_when_unverified_v<ItTy>;

template <class T>
static constexpr bool msvc_do_unwrap_when_unverified_v =
    MI Requires<T>([]() -> decltype(T::_Unwrap_when_unverified) {}) && static_cast<bool>(T::_Unwrap_when_unverified);

template <class ItTy>
static constexpr bool msvc_unwrappable_for_unverified_v =
    msvc_unwrappable_v<ItTy> && msvc_do_unwrap_when_unverified_v<ItTy>;
}  // namespace mi::internal

namespace mi {
// ---------------------------------------------------------------------------------------------------------------------
// get_unwrapped

// Получить развернутый итератор (т.е. обычный указатель).
//
// (!) Итератор должен быть предварительно проверен (MI verify_range).
template <class IteratorType, if_t<internal::mi_unwrappable_v<IteratorType>> = 0>
MI_NODISCARD constexpr auto get_unwrapped(const IteratorType& iterator) {
  return (iterator.unwrapped());
}

// Получить развернутый итератор (т.е. обычный указатель).
//
// (!) Итератор должен быть предварительно проверен (MI verify_range).
template <class IteratorType, if_t<internal::msvc_unwrappable_v<IteratorType>> = 0>
MI_NODISCARD constexpr auto get_unwrapped(const IteratorType& iterator) {
  return (iterator._Unwrapped());
}

// Ничего не делает.
template <class IteratorType,
          if_t<!internal::mi_unwrappable_v<IteratorType> && !internal::msvc_unwrappable_v<IteratorType>> = 0>
MI_NODISCARD constexpr const IteratorType& get_unwrapped(const IteratorType& iterator) {
  return (iterator);
}

// Ничего не делает.
template <class IteratorType,
          if_t<!internal::mi_unwrappable_v<IteratorType> && !internal::msvc_unwrappable_v<IteratorType>> = 0>
MI_NODISCARD constexpr const IteratorType&& get_unwrapped(const IteratorType&& iterator) {
  return (static_cast<const IteratorType&&>(iterator));
}

template <class Ty>
MI_NODISCARD constexpr const Ty* get_unwrapped(const Ty* pointer) {
  // Специальный случай для уже развернутых итераторов
  return (pointer);
}

template <class IteratorType>
using unwrapped_t =
    STD remove_cv_t<STD remove_reference_t<decltype(get_unwrapped(STD declval<const IteratorType&>()))>>;

// =====================================================================================================================
// get_unwrapped_unverified
// =====================================================================================================================

template <class ItTy, if_t<internal::mi_unwrappable_for_unverified_v<ItTy>> = 0>
MI_NODISCARD constexpr auto get_unwrapped_unverified(const ItTy& iterator) {
  // разворачиваем итератор, ранее не подвергавшийся проверке
  return (iterator.unwrapped());
}

template <class ItTy, if_t<internal::msvc_unwrappable_for_unverified_v<ItTy>> = 0>
MI_NODISCARD constexpr auto get_unwrapped_unverified(const ItTy& iterator) {
  // разворачиваем итератор, ранее не подвергавшийся проверке
  return (iterator._Unwrapped());
}

template <class IteratorType, if_t<!internal::mi_unwrappable_for_unverified_v<IteratorType> &&
                                   !internal::msvc_unwrappable_for_unverified_v<IteratorType>> = 0>
MI_NODISCARD constexpr const IteratorType&& get_unwrapped_unverified(const IteratorType&& iterator) {
  // (не) разворачивать итератор, ранее подвергнутый проверке диапазона Adl или иным образом проверенный
  return (static_cast<const IteratorType&&>(iterator));
}

template <class IteratorType, if_t<!internal::mi_unwrappable_for_unverified_v<IteratorType> &&
                                   !internal::msvc_unwrappable_for_unverified_v<IteratorType>> = 0>
MI_NODISCARD constexpr const IteratorType& get_unwrapped_unverified(const IteratorType& iterator) {
  // (не) разворачивать итератор, ранее подвергнутый проверке диапазона Adl или иным образом проверенный
  return (iterator);
}

template <class Ty>
MI_NODISCARD constexpr const Ty* get_unwrapped_unverified(const Ty* pointer) {
  // специальный случай уже развернутых итераторов
  return (pointer);
}

template <class IteratorType>
using unwrapped_unverified_t =
    STD remove_cv_t<STD remove_reference_t<decltype(get_unwrapped_unverified(STD declval<const IteratorType&>()))>>;

// FUNCTION TEMPLATE seek_wrapped

template <class It, class UnwrappedIt>
static constexpr bool msvc_wrapped_seekable_v =
    MI Requires<It, UnwrappedIt>([](auto&& it, auto&& unwrapped_it) -> decltype(it._Seek_to(unwrapped_it)) {});

template <class It, class UnwrappedIt>
static constexpr bool mi_wrapped_seekable_v =
    MI Requires<It, UnwrappedIt>([](auto&& it, auto&& unwrapped_it) -> decltype(it.seek_to(unwrapped_it)) {});

template <class Iterator, class UnwrappedIterator>
constexpr void seek_wrapped(Iterator& it, const UnwrappedIterator& u_it) {
  if constexpr (msvc_wrapped_seekable_v<Iterator, UnwrappedIterator>) {
    it._Seek_to(u_it);
  } else if constexpr (mi_wrapped_seekable_v<Iterator, UnwrappedIterator>) {
    it.seek_to(u_it);
  } else {
    it = u_it;
  }
}

}  // namespace mi
