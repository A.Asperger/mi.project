// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include "MI/Common/MI.CheckInternal.hpp"

#include <assert.h>
#include <iostream>

#include "MI/Base/MI.Property.hpp"
#include "MI/Common/MI.ImmediateCrash.h"

namespace mi::internal {

inline auto PrintAfterStackTrace(const STD string& message) -> void { std::cerr << message; }

ExitingStream::~ExitingStream() {
  assert(false &&
         "Exiting streams should only be constructed by check.h macros that "
         "ensure the special operator| exits the program prior to their "
         "destruction!");
}

auto ExitingStream::Done() -> void {
  _ss << "\n";

  // Register another signal handler to print the buffered message. This is
  // because we want it at the bottom of output, after LLVM's builtin stack
  // output, rather than the top.
  PrintAfterStackTrace(_ss.str());

  MI_IMMEDIATE_CRASH();

  // It's useful to exit the program with `std::abort()` for integration with
  // debuggers and other tools. We also assume LLVM's exit handling is
  // installed, which will stack trace on `std::abort()`.
  STD abort();
}
}  // namespace mi::internal
