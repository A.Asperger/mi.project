// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#pragma once

#include <MI/Common/MI.CheckInternal.hpp>
#include <MI/Common/MI.DCheckIsOn.h>

namespace mi {

// Raw exiting stream. This should be used when building forms of exiting
// macros. It evaluates to a temporary `ExitingStream` object that can be
// manipulated, streamed into, and then will exit the program.
#define MI_CHECK_INTERNAL_STREAM() mi::internal::ExitingStream::Helper() | mi::internal::ExitingStream()

// Checks the given condition, and if it's false, prints a stack, streams the
// error message, then exits. This should be used for unexpected errors, such as
// a bug in the application.
//
// For example:
//   MI_CHECK(is_valid) << "Data is not valid!";
#define MI_CHECK(...)                                                                                                  \
  (__VA_ARGS__) ? (void)0                                                                                              \
                : MI_CHECK_INTERNAL_STREAM() << "CHECK failure at " << __FILE__ << ":" << __LINE__                     \
                                             << ": " #__VA_ARGS__ << mi::internal::ExitingStream::AddSeparator()

// DCHECK calls CHECK in debug mode, and does nothing otherwise.
#if MI_DCHECK_IS_ON()
  #define MI_DCHECK(...) MI_CHECK(__VA_ARGS__)
#else
  #define MI_DCHECK(...) MI_CHECK(true || (__VA_ARGS__))
#endif

// This is similar to CHECK, but is unconditional. Writing MI_FATAL() is
// clearer than MI_CHECK(false) because it avoids confusion about control
// flow.
//
// For example:
//   MI_FATAL() << "Unreachable!";
#define MI_FATAL()                                                                                                     \
  MI_CHECK_INTERNAL_STREAM() << "FATAL failure at " << __FILE__ << ":" << __LINE__                                     \
                             << mi::internal::ExitingStream::AddSeparator()

#define MI_NOT_REACHED()          MI_FATAL() << "Not reached!"
#define MI_NOT_IMPLEMENTED()      MI_FATAL() << "Not implemented!"
#define MI_IMPLEMENTATION_ERROR() MI_FATAL() << "THIS LOOKS LIKE AN IMPLEMENTATION ERROR!"
}  // namespace mi
