// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Base/MI.CompilerSpecific.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MI/Base/Test/MI.GTestUtil.h"
#include "MI/Common/MI.Check.hpp"
#include "MI/Common/MI.DCheckIsOn.h"

// (!) Никакие тесты не должны писать что-то в файл, иначе там будет много мусора.

#define MI_EXPECT_LOG_ERROR(msg, expr, expected_line)                                                                  \
  do {                                                                                                                 \
    static bool got_log_message = false;                                                                               \
    ASSERT_EQ(logging::get_log_message_handler(), nullptr);                                                            \
    logging::set_log_message_handler([](logging::log_severity severity, const char* file, size_t line,                 \
                                        size_t message_start, const STD string& str) {                                 \
      EXPECT_FALSE(got_log_message);                                                                                   \
      got_log_message = true;                                                                                          \
      EXPECT_EQ(severity, logging::log_severity::fatal);                                                               \
      EXPECT_EQ(str.substr(message_start), (msg));                                                                     \
      EXPECT_STREQ(__FILE__, file);                                                                                    \
      EXPECT_EQ(expected_line, line);                                                                                  \
      return true;                                                                                                     \
    });                                                                                                                \
    expr;                                                                                                              \
    EXPECT_TRUE(got_log_message);                                                                                      \
    logging::set_log_message_handler(nullptr);                                                                         \
  } while (0)

// expr:
// - не вызывает падение программы
// - не генерирует логи
#define MI_EXPECT_NO_LOG(expr)                                                                                         \
  do {                                                                                                                 \
    ASSERT_EQ(logging::get_log_message_handler(), nullptr);                                                            \
    logging::set_log_message_handler([](logging::log_severity severity, const char* file, size_t line,                 \
                                        size_t message_start, const STD string& str) {                                 \
      EXPECT_TRUE(false) << "Unexpected log:" << STD endl << str;                                                      \
      return true;                                                                                                     \
    });                                                                                                                \
    expr;                                                                                                              \
    logging::set_log_message_handler(nullptr);                                                                         \
  } while (0)

// Проверка для MI_CHECK.
// - в 'Debug' проверяет сообщение и отслеживает прерывание,
// - в 'Release' отбрасывает сообщение, но все еще отслеживает прерывание.
#define MI_EXPECT_CHECK(msg, check_expr)                                                                               \
  do {                                                                                                                 \
    MI_EXPECT_CHECK_DEATH(check_expr);                                                                                 \
  } while (0)

// Проверка для MI_DCHECK.
#define MI_EXPECT_DCHECK(msg, check_expr)                                                                              \
  do {                                                                                                                 \
    if (MI_DCHECK_IS_ON()) {                                                                                           \
      MI_EXPECT_LOG_ERROR(msg, check_expr, __LINE__);                                                                  \
    } else {                                                                                                           \
      check_expr;                                                                                                      \
    }                                                                                                                  \
  } while (0)

namespace mi::test {
TEST(CheckTest, Basics) {
  MI_EXPECT_CHECK("Check failed: false. \n", MI_CHECK(false));
  MI_EXPECT_CHECK("Check failed: false. foo\n", MI_CHECK(false) << "foo");
}

TEST(CheckTest, CheckStreamsAreLazy) {
  int called_count     = 0;
  int not_called_count = 0;

  auto Called = [&]() {
    ++called_count;
    return 42;
  };
  auto NotCalled = [&]() {
    ++not_called_count;
    return 42;
  };

  MI_CHECK(Called()) << NotCalled();

  MI_DCHECK(Called()) << NotCalled();

  EXPECT_EQ(not_called_count, 0);
#if MI_DCHECK_IS_ON()
  EXPECT_EQ(called_count, 2);
#else
  EXPECT_EQ(called_count, 1);
#endif
}

TEST(CheckTest, DcheckReleaseBehavior) {
  int var1 = 1;
  int var2 = 2;
  int var3 = 3;
  int var4 = 4;

  // No warnings about unused variables even though no check fires and DCHECK
  // may or may not be enabled.
  MI_DCHECK(var1) << var2;
}

struct StructWithOstream {
    bool operator==(const StructWithOstream& o) const { return &o == this; }
};
#if !(defined(MI_RELEASE))
std::ostream& operator<<(std::ostream& out, const StructWithOstream&) { return out << "ostream"; }
#endif

struct StructWithToString {
    bool operator==(const StructWithToString& o) const { return &o == this; }

    std::string to_string() const { return "ToString"; }
};

struct StructWithToStringAndOstream {
    bool operator==(const StructWithToStringAndOstream& o) const { return &o == this; }

    std::string to_string() const { return "ToString"; }
};
#if !(defined(MI_RELEASE))
std::ostream& operator<<(std::ostream& out, const StructWithToStringAndOstream&) { return out << "ostream"; }
#endif

struct StructWithToStringNotStdString {
    struct PseudoString {};

    bool operator==(const StructWithToStringNotStdString& o) const { return &o == this; }

    PseudoString to_string() const { return PseudoString(); }
};
#if !(defined(MI_RELEASE))
std::ostream& operator<<(std::ostream& out, const StructWithToStringNotStdString::PseudoString&) {
  return out << "ToString+ostream";
}
#endif

TEST(CheckTest, NotReached) { MI_EXPECT_CHECK("Check failed: false. foo\n", MI_NOT_REACHED() << "foo"); }

TEST(CheckTest, NotImplemented) {
  static const std::string expected_msg = std::string("Not implemented reached in ") + MI_PRETTY_FUNCTION;

  // Expect MI_LOG(ERROR) with streamed params intact.
  MI_EXPECT_CHECK(expected_msg + ". foo\n", MI_NOT_IMPLEMENTED() << "foo");
}

}  // namespace mi::test

namespace mi::test {
TEST(CheckTest, CheckTrue) { MI_CHECK(true); }

TEST(CheckTest, CheckFalse) {
  ASSERT_DEATH({ MI_CHECK(false); }, "CHECK failure at .+MI\\\\Common\\\\MI.Check_unittest.cpp:.+: false");
}

TEST(CheckTest, CheckFalseHasStackDump) {
  // Для дампа нужно использовать LLVM.
  ASSERT_DEATH({ MI_CHECK(false); }, "");
}

TEST(CheckTest, CheckTrueCallbackNotUsed) {
  bool called   = false;
  auto callback = [&]() {
    called = true;
    return "called";
  };
  MI_CHECK(true) << callback();
  EXPECT_FALSE(called);
}

TEST(CheckTest, CheckFalseMessage) {
  ASSERT_DEATH({ MI_CHECK(false) << "msg"; },
               "CHECK failure at .+MI\\\\Common\\\\MI.Check_unittest.cpp:.+: false: msg\n");
}

TEST(CheckTest, CheckOutputForms) {
  const char msg[] = "msg";
  std::string str  = "str";
  int i            = 1;
  MI_CHECK(true) << msg << str << i << 0;
}

TEST(CheckTest, Fatal) {
  ASSERT_DEATH({ MI_FATAL() << "msg"; }, "FATAL failure at .+MI\\\\Common\\\\MI.Check_unittest.cpp:.+: msg\n");
}

TEST(CheckTest, FatalHasStackDump) {
  // Для дампа нужно использовать LLVM.
  ASSERT_DEATH({ MI_FATAL() << "msg"; }, "");
}

auto FatalNoReturnRequired() -> int { MI_FATAL() << "msg"; }

TEST(ErrorTest, FatalNoReturnRequired) {
  ASSERT_DEATH({ FatalNoReturnRequired(); }, "FATAL failure at .+MI\\\\Common\\\\MI.Check_unittest.cpp:.+: msg\n");
}

}  // namespace mi::test
