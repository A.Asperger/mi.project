// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
//
// ┏━┓┏━┓┏━━┓  ┏━━━┓┏━━━┓┏━━━┓  ┏┓┏━━━┓┏━━━┓┏━━━━┓
// ┃┃┗┛┃┃┗┫┣┛  ┃┏━┓┃┃┏━┓┃┃┏━┓┃  ┃┃┃┏━━┛┃┏━┓┃┃┏┓┏┓┃
// ┃┏┓┏┓┃ ┃┃   ┃┗━┛┃┃┗━┛┃┃┃ ┃┃  ┃┃┃┗━━┓┃┃ ┗┛┗┛┃┃┗┛
// ┃┃┃┃┃┃ ┃┃   ┃┏━━┛┃┏┓┏┛┃┃ ┃┃┏┓┃┃┃┏━━┛┃┃ ┏┓  ┃┃
// ┃┃┃┃┃┃┏┫┣┓┏┓┃┃   ┃┃┃┗┓┃┗━┛┃┃┗┛┃┃┗━━┓┃┗━┛┃ ┏┛┗┓
// ┗┛┗┛┗┛┗━━┛┗┛┗┛   ┗┛┗━┛┗━━━┛┗━━┛┗━━━┛┗━━━┛ ┗━━┛
// ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━

#include <MI/Common/MI.InBrackets.h>
#include <MI/Container/MI.StaticVector.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace mi::test {

TEST(InBrackets, Curly) {
  MI static_vector<int, 3> v = {42, -2, 11};
  STD vector<int> vv         = {42, -2, 11};

  EXPECT_STREQ(MI in_brackets::curly(v).c_str(), "{42, -2, 11}");
  EXPECT_STREQ(MI in_brackets::curly(vv).c_str(), "{42, -2, 11}");
  EXPECT_STREQ(MI in_brackets::curly(42, -2, 11).c_str(), "{42, -2, 11}");
}

TEST(InBrackets, Round) {
  MI static_vector<int, 3> v = {42, -2, 11};
  STD vector<int> vv         = {42, -2, 11};

  EXPECT_STREQ(MI in_brackets::round(v).c_str(), "(42, -2, 11)");
  EXPECT_STREQ(MI in_brackets::round(vv).c_str(), "(42, -2, 11)");
  EXPECT_STREQ(MI in_brackets::round(42, -2, 11).c_str(), "(42, -2, 11)");
}

TEST(InBrackets, Square) {
  MI static_vector<int, 3> v = {42, -2, 11};
  STD vector<int> vv         = {42, -2, 11};

  EXPECT_STREQ(MI in_brackets::square(v).c_str(), "[42, -2, 11]");
  EXPECT_STREQ(MI in_brackets::square(vv).c_str(), "[42, -2, 11]");
  EXPECT_STREQ(MI in_brackets::square(42, -2, 11).c_str(), "[42, -2, 11]");
}
}  // namespace mi::test
