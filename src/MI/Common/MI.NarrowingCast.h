#pragma once

#include "MI/Base/MI.Property.hpp"

namespace mi {
// Безопасное приведение типов с усечением
template <class ToType, class FromType>
MI_NODISCARD constexpr ToType narrow_cast(FromType value) {
  // Преобразовывает value из FromType в ToType и обратно в FromType,
  // получившееся значение сравнивается с исходным, должно получиться то же самое.
  MI_CHECK(static_cast<FromType>(static_cast<ToType>(value)) == value) << "MI narrow_cast<> was failed. ";

  return (static_cast<ToType>(STD forward<FromType>(value)));
}
}  // namespace mi
